#include "vl53l0x_platform.h"
#include "qos_debug.h"


#define    BYTES_PER_WORD        2
#define    BYTES_PER_DWORD       4


VL53L0X_Error VL53L0X_WriteMulti(VL53L0X_DEV Dev, uint8_t index, uint8_t *pdata, uint32_t count)
{
    if (count > VL53_I2C_BUFFER_SIZE) {
        qos_error_handler(Q_EXCEED_NUM_ITEMS);
    }

    Dev->buffer[0] = index;
    uint8_t *buf = &Dev->buffer[1];
    for (uint16_t i = 0; i < count; i++) {
        buf[i] = pdata[i];
    }
//    Dev->conf.reg = index;
    q_rawwrite(Dev->rd, Dev->buffer, count + 1);
    return VL53L0X_ERROR_NONE;
}


VL53L0X_Error VL53L0X_ReadMulti(VL53L0X_DEV Dev, uint8_t index, uint8_t *pdata, uint32_t count)
{
//    Dev->conf.reg = index;
    q_read(Dev->rd, index, pdata, count);
    return VL53L0X_ERROR_NONE;

}

VL53L0X_Error VL53L0X_WrByte(VL53L0X_DEV Dev, uint8_t index, uint8_t data)
{

    Dev->buffer[0] = index;
    Dev->buffer[1] = data;
//    Dev->conf.reg = index;
    q_rawwrite(Dev->rd, Dev->buffer, 2);
    return VL53L0X_ERROR_NONE;

}

VL53L0X_Error VL53L0X_WrWord(VL53L0X_DEV Dev, uint8_t index, uint16_t data)
{
    VL53L0X_Error status = VL53L0X_ERROR_NONE;
    uint8_t  buffer[BYTES_PER_WORD];

    // Split 16-bit word into MS and LS uint8_t
    buffer[0] = (uint8_t)(data >> 8);
    buffer[1] = (uint8_t)(data &  0x00FF);

    if(index % 2 == 1)
    {
        status = VL53L0X_WrByte(Dev, index, buffer[0]);
//        status = VL53L0X_WriteMulti(Dev, index, &buffer[0], 1);
        status = VL53L0X_WrByte(Dev, index, buffer[1]);
//        status = VL53L0X_WriteMulti(Dev, index + 1, &buffer[1], 1);
        // serial comms cannot handle word writes to non 2-byte aligned registers.
    }
    else
    {

        status = VL53L0X_WriteMulti(Dev, index, buffer, BYTES_PER_WORD);
    }

    return status;
}

VL53L0X_Error VL53L0X_WrDWord(VL53L0X_DEV Dev, uint8_t index, uint32_t data)
{
    VL53L0X_Error status = VL53L0X_ERROR_NONE;
    uint8_t  buffer[BYTES_PER_DWORD];

    // Split 32-bit word into MS ... LS bytes
    buffer[0] = (uint8_t) (data >> 24);
    buffer[1] = (uint8_t)((data &  0x00FF0000) >> 16);
    buffer[2] = (uint8_t)((data &  0x0000FF00) >> 8);
    buffer[3] = (uint8_t) (data &  0x000000FF);

    status = VL53L0X_WriteMulti(Dev, index, buffer, BYTES_PER_DWORD);

    return status;
}

VL53L0X_Error VL53L0X_UpdateByte(VL53L0X_DEV Dev, uint8_t index, uint8_t AndData, uint8_t OrData)
{
    VL53L0X_Error status = VL53L0X_ERROR_NONE;
    uint8_t data;

    status = VL53L0X_RdByte(Dev, index, &data);
    if (status == VL53L0X_ERROR_NONE) {
        data = (data & AndData) | OrData;
        status = VL53L0X_WrByte(Dev, index, data);
    }
    return status;
}

VL53L0X_Error VL53L0X_RdByte(VL53L0X_DEV Dev, uint8_t index, uint8_t *data)
{
//    Dev->conf.reg = index;
    q_read(Dev->rd, index, data, 1);
    return VL53L0X_ERROR_NONE;
}

VL53L0X_Error VL53L0X_RdWord(VL53L0X_DEV Dev, uint8_t index, uint16_t *data)
{
    VL53L0X_Error status = VL53L0X_ERROR_NONE;
    uint8_t  buffer[BYTES_PER_WORD];
    status = VL53L0X_ReadMulti(Dev, index, buffer, BYTES_PER_WORD);
    *data = ((uint16_t)buffer[0]<<8) + (uint16_t)buffer[1];

    return status;
}

VL53L0X_Error VL53L0X_RdDWord(VL53L0X_DEV Dev, uint8_t index, uint32_t *data)
{
     VL53L0X_Error status = VL53L0X_ERROR_NONE;
    uint8_t  buffer[BYTES_PER_DWORD];
    status = VL53L0X_ReadMulti(Dev, index, buffer, BYTES_PER_DWORD);
    *data = ((uint32_t)buffer[0]<<24) + ((uint32_t)buffer[1]<<16) + ((uint32_t)buffer[2]<<8) + (uint32_t)buffer[3];

    return status;
}

VL53L0X_Error VL53L0X_PollingDelay(VL53L0X_DEV Dev)
{
    q_delay(5);
    return VL53L0X_ERROR_NONE;
}
