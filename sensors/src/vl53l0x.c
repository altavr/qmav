#include "vl53l0x.h"
#include "qos_sys_functions.h"
#include "vl53l0x_api.h"
#include "qos_debug.h"
//#include "qos_drivers/general.h"

#define  NO_OF_MEASURMENTS 3200

static VL53L0X_Dev_t MyDevice;
static VL53L0X_Dev_t *pMyDevice = &MyDevice;
static VL53L0X_DeviceInfo_t   DeviceInfo;

uint16_t pResults[NO_OF_MEASURMENTS];

void print_pal_error(VL53L0X_Error status){
    char buf[VL53L0X_MAX_STRING_LENGTH];
    VL53L0X_GetPalErrorString(status, buf);
    q_printf("API status: %d : %s\r\n", status, buf);
}

void print_range_status(VL53L0X_RangingMeasurementData_t* pRangingMeasurementData)
{
    char buf[VL53L0X_MAX_STRING_LENGTH];
    uint8_t Rangestatus;

    /*
     * New Range status: data is valid when pRangingMeasurementData->Rangestatus = 0
     */

    Rangestatus = pRangingMeasurementData->RangeStatus;

    VL53L0X_GetRangeStatusString(Rangestatus, buf);
    q_printf("Range status: %d : %s\r\n", Rangestatus, buf);

}


VL53L0X_Error rangingTest(void)
{
    VL53L0X_Error status = VL53L0X_ERROR_NONE;
    VL53L0X_RangingMeasurementData_t    RangingMeasurementData;
    int i;
    FixPoint1616_t LimitCheckCurrent;
    uint32_t refSpadCount;
    uint8_t isApertureSpads;
    uint8_t VhvSettings;
    uint8_t PhaseCal;

    if(status == VL53L0X_ERROR_NONE)
    {
        q_printf ("Call of VL53L0X_StaticInit\r\n");
        status = VL53L0X_StaticInit(pMyDevice); // Device Initialization
        print_pal_error(status);
    }

    if(status == VL53L0X_ERROR_NONE)
    {
        q_printf ("Call of VL53L0X_PerformRefCalibration\r\n");
        status = VL53L0X_PerformRefCalibration(pMyDevice,
                &VhvSettings, &PhaseCal); // Device Initialization
        print_pal_error(status);
    }

    if(status == VL53L0X_ERROR_NONE)
    {
        q_printf ("Call of VL53L0X_PerformRefSpadManagement\r\n");
        status = VL53L0X_PerformRefSpadManagement(pMyDevice,
                &refSpadCount, &isApertureSpads); // Device Initialization
        q_printf ("refSpadCount = %d, isApertureSpads = %d\r\n", refSpadCount, isApertureSpads);
        print_pal_error(status);
    }

    if(status == VL53L0X_ERROR_NONE)
    {

        // no need to do this when we use VL53L0X_PerformSingleRangingMeasurement
        q_printf ("Call of VL53L0X_SetDeviceMode\r\n");
        status = VL53L0X_SetDeviceMode(pMyDevice, VL53L0X_DEVICEMODE_SINGLE_RANGING); // Setup in single ranging mode
        print_pal_error(status);
    }

    // Enable/Disable Sigma and Signal check
    if (status == VL53L0X_ERROR_NONE) {
        status = VL53L0X_SetLimitCheckEnable(pMyDevice,
                VL53L0X_CHECKENABLE_SIGMA_FINAL_RANGE, 1);
    }
    if (status == VL53L0X_ERROR_NONE) {
        status = VL53L0X_SetLimitCheckEnable(pMyDevice,
                VL53L0X_CHECKENABLE_SIGNAL_RATE_FINAL_RANGE, 1);
    }

    if (status == VL53L0X_ERROR_NONE) {
        status = VL53L0X_SetLimitCheckEnable(pMyDevice,
                VL53L0X_CHECKENABLE_RANGE_IGNORE_THRESHOLD, 1);
    }

    if (status == VL53L0X_ERROR_NONE) {
        status = VL53L0X_SetLimitCheckValue(pMyDevice,
                VL53L0X_CHECKENABLE_RANGE_IGNORE_THRESHOLD,
                (FixPoint1616_t)(1.5*0.023*65536));
    }


    /*
     *  Step  4 : Test ranging mode
     */

    if(status == VL53L0X_ERROR_NONE)
    {
        for(i=0;i<10;i++){
            q_printf ("Call of VL53L0X_PerformSingleRangingMeasurement\r\n");
            status = VL53L0X_PerformSingleRangingMeasurement(pMyDevice,
                    &RangingMeasurementData);

            print_pal_error(status);
            print_range_status(&RangingMeasurementData);

            VL53L0X_GetLimitCheckCurrent(pMyDevice,
                    VL53L0X_CHECKENABLE_RANGE_IGNORE_THRESHOLD, &LimitCheckCurrent);

            q_printf("RANGE IGNORE THRESHOLD: %f\r\n\r\n", (double)(LimitCheckCurrent/65536.0));


            if (status != VL53L0X_ERROR_NONE) break;

            q_printf("Measured distance: %d\r\n\r\n", RangingMeasurementData.RangeMilliMeter);


        }
    }
    return status;
}
/*
void lidar_init(void)
{
    VL53L0X_Error status = VL53L0X_ERROR_NONE;

    pMyDevice->I2cDevAddr = 0x52 >> 1;
    MyDevice.conf.dev = 0x52 >> 1;


    MyDevice.rd = q_open(Q_I2C2);
    if (MyDevice.rd < 0) {
        qos_error_handler(MyDevice.rd);
    }
    q_ioctl(MyDevice.rd, &MyDevice.conf);


    status = VL53L0X_DataInit(&MyDevice);
    q_printf("Call of VL53L0X_DataInit status - %d\r\n", status);


    status = VL53L0X_GetDeviceInfo(&MyDevice, &DeviceInfo);
    if(status == VL53L0X_ERROR_NONE)
    {
        q_printf("VL53L0X_GetDeviceInfo:\r\n");
        q_printf("Device Name : %s\r\n", DeviceInfo.Name);
        q_printf("Device Type : %s\r\n", DeviceInfo.Type);
        q_printf("Device ID : %s\r\n", DeviceInfo.ProductId);
        q_printf("ProductRevisionMajor : %d\r\n", DeviceInfo.ProductRevisionMajor);
        q_printf("ProductRevisionMinor : %d\r\n", DeviceInfo.ProductRevisionMinor);
    }


}
*/
VL53L0X_Error WaitMeasurementDataReady(void) {
    VL53L0X_Error Status = VL53L0X_ERROR_NONE;
    uint8_t NewDatReady=0;
    uint32_t LoopNb;

    // Wait until it finished
    // use timeout to avoid deadlock
    if (Status == VL53L0X_ERROR_NONE) {
        LoopNb = 0;
        do {
            Status = VL53L0X_GetMeasurementDataReady(pMyDevice, &NewDatReady);
            if ((NewDatReady == 0x01) || Status != VL53L0X_ERROR_NONE) {
                break;
            }
            LoopNb = LoopNb + 1;
            VL53L0X_PollingDelay(pMyDevice);
        } while (LoopNb < VL53L0X_DEFAULT_MAX_LOOP);

        if (LoopNb >= VL53L0X_DEFAULT_MAX_LOOP) {
            Status = VL53L0X_ERROR_TIME_OUT;
        }
    }

    return Status;
}

VL53L0X_Error WaitStopCompleted(void) {
    VL53L0X_Error Status = VL53L0X_ERROR_NONE;
    uint32_t StopCompleted=0;
    uint32_t LoopNb;

    // Wait until it finished
    // use timeout to avoid deadlock
    if (Status == VL53L0X_ERROR_NONE) {
        LoopNb = 0;
        do {
            Status = VL53L0X_GetStopCompletedStatus(pMyDevice, &StopCompleted);
            if ((StopCompleted == 0x00) || Status != VL53L0X_ERROR_NONE) {
                break;
            }
            LoopNb = LoopNb + 1;
            VL53L0X_PollingDelay(pMyDevice);
        } while (LoopNb < VL53L0X_DEFAULT_MAX_LOOP);

        if (LoopNb >= VL53L0X_DEFAULT_MAX_LOOP) {
            Status = VL53L0X_ERROR_TIME_OUT;
        }

    }

    return Status;
}


VL53L0X_Error rangingTestContinues(void)
{
    VL53L0X_RangingMeasurementData_t    RangingMeasurementData;
    VL53L0X_RangingMeasurementData_t   *pRangingMeasurementData    = &RangingMeasurementData;
    VL53L0X_Error Status = VL53L0X_ERROR_NONE;
    uint32_t refSpadCount;
    uint8_t isApertureSpads;
    uint8_t VhvSettings;
    uint8_t PhaseCal;

    if(Status == VL53L0X_ERROR_NONE)
    {
        q_printf ("Call of VL53L0X_StaticInit\r\n");
        Status = VL53L0X_StaticInit(pMyDevice); // Device Initialization
        // StaticInit will set interrupt by default
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE)
    {
        q_printf ("Call of VL53L0X_PerformRefCalibration\r\n");
        Status = VL53L0X_PerformRefCalibration(pMyDevice,
                &VhvSettings, &PhaseCal); // Device Initialization
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE)
    {
        q_printf ("Call of VL53L0X_PerformRefSpadManagement\r\n");
        Status = VL53L0X_PerformRefSpadManagement(pMyDevice,
                &refSpadCount, &isApertureSpads); // Device Initialization
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE)
    {

        q_printf ("Call of VL53L0X_SetDeviceMode\r\n");
        Status = VL53L0X_SetDeviceMode(pMyDevice, VL53L0X_DEVICEMODE_CONTINUOUS_RANGING); // Setup in single ranging mode
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE)
    {
        q_printf ("Call of VL53L0X_StartMeasurement\r\n");
        Status = VL53L0X_StartMeasurement(pMyDevice);
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE)
    {
        uint32_t measurement;
//        uint32_t NO_OF_MEASURMENTS = 3200;

//        uint16_t* pResults = (uint16_t*)malloc(sizeof(uint16_t) * NO_OF_MEASURMENTS);

        for(measurement=0; measurement<NO_OF_MEASURMENTS; measurement++)
        {

            Status = WaitMeasurementDataReady();

            if(Status == VL53L0X_ERROR_NONE)
            {
                Status = VL53L0X_GetRangingMeasurementData(pMyDevice, pRangingMeasurementData);

                *(pResults + measurement) = pRangingMeasurementData->RangeMilliMeter;
                q_printf("In loop measurement %d: %d\r\n", measurement, pRangingMeasurementData->RangeMilliMeter);

                // Clear the interrupt
                VL53L0X_ClearInterruptMask(pMyDevice, VL53L0X_REG_SYSTEM_INTERRUPT_GPIO_NEW_SAMPLE_READY);
                VL53L0X_PollingDelay(pMyDevice);
            } else {
                break;
            }
        }

        if(Status == VL53L0X_ERROR_NONE)
        {
            for(measurement=0; measurement<NO_OF_MEASURMENTS; measurement++)
            {
                q_printf("measurement %d: %d\r\n", measurement, *(pResults + measurement));
            }
        }

//        free(pResults);
    }


    if(Status == VL53L0X_ERROR_NONE)
    {
        q_printf ("Call of VL53L0X_StopMeasurement\r\n");
        Status = VL53L0X_StopMeasurement(pMyDevice);
    }

    if(Status == VL53L0X_ERROR_NONE)
    {
        q_printf ("Wait Stop to be competed\r\n");
        Status = WaitStopCompleted();
    }

    if(Status == VL53L0X_ERROR_NONE)
    Status = VL53L0X_ClearInterruptMask(pMyDevice,
        VL53L0X_REG_SYSTEM_INTERRUPT_GPIO_NEW_SAMPLE_READY);

    return Status;
}

VL53L0X_Error rangingTestLong(void)
{
    VL53L0X_Error Status = VL53L0X_ERROR_NONE;
    VL53L0X_RangingMeasurementData_t    RangingMeasurementData;
    int i;
    uint32_t refSpadCount;
    uint8_t isApertureSpads;
    uint8_t VhvSettings;
    uint8_t PhaseCal;

    if(Status == VL53L0X_ERROR_NONE)
    {
        q_printf ("Call of VL53L0X_StaticInit\r\n");
        Status = VL53L0X_StaticInit(pMyDevice); // Device Initialization
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE)
    {
        q_printf ("Call of VL53L0X_PerformRefCalibration\r\n");
        Status = VL53L0X_PerformRefCalibration(pMyDevice,
                &VhvSettings, &PhaseCal); // Device Initialization
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE)
    {
        q_printf ("Call of VL53L0X_PerformRefSpadManagement\r\n");
        Status = VL53L0X_PerformRefSpadManagement(pMyDevice,
                &refSpadCount, &isApertureSpads); // Device Initialization
        q_printf ("refSpadCount = %d, isApertureSpads = %d\r\n", refSpadCount, isApertureSpads);
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE)
    {

        // no need to do this when we use VL53L0X_PerformSingleRangingMeasurement
        q_printf ("Call of VL53L0X_SetDeviceMode\r\n");
        Status = VL53L0X_SetDeviceMode(pMyDevice, VL53L0X_DEVICEMODE_SINGLE_RANGING); // Setup in single ranging mode
        print_pal_error(Status);
    }

    // Enable/Disable Sigma and Signal check

 /*   if (Status == VL53L0X_ERROR_NONE) {
        Status = VL53L0X_SetSequenceStepEnable(pMyDevice,VL53L0X_SEQUENCESTEP_DSS, 1);
    }*/

    if (Status == VL53L0X_ERROR_NONE) {
        Status = VL53L0X_SetLimitCheckEnable(pMyDevice,
                VL53L0X_CHECKENABLE_SIGMA_FINAL_RANGE, 1);
    }
    if (Status == VL53L0X_ERROR_NONE) {
        Status = VL53L0X_SetLimitCheckEnable(pMyDevice,
                VL53L0X_CHECKENABLE_SIGNAL_RATE_FINAL_RANGE, 1);
    }

    if (Status == VL53L0X_ERROR_NONE) {
        Status = VL53L0X_SetLimitCheckValue(pMyDevice,
                VL53L0X_CHECKENABLE_SIGNAL_RATE_FINAL_RANGE,
                (FixPoint1616_t)(0.1*65536));
    }
    if (Status == VL53L0X_ERROR_NONE) {
        Status = VL53L0X_SetLimitCheckValue(pMyDevice,
                VL53L0X_CHECKENABLE_SIGMA_FINAL_RANGE,
                (FixPoint1616_t)(60*65536));
    }
    if (Status == VL53L0X_ERROR_NONE) {
        Status = VL53L0X_SetMeasurementTimingBudgetMicroSeconds(pMyDevice,
                33000);
    }

    if (Status == VL53L0X_ERROR_NONE) {
        Status = VL53L0X_SetVcselPulsePeriod(pMyDevice,
                VL53L0X_VCSEL_PERIOD_PRE_RANGE, 18);
    }
    if (Status == VL53L0X_ERROR_NONE) {
        Status = VL53L0X_SetVcselPulsePeriod(pMyDevice,
                VL53L0X_VCSEL_PERIOD_FINAL_RANGE, 14);
    }


    /*
     *  Step  4 : Test ranging mode
     */

    if(Status == VL53L0X_ERROR_NONE)
    {
        for(i=0;i<5000;i++){
            q_printf ("Call of VL53L0X_PerformSingleRangingMeasurement\r\n");
            Status = VL53L0X_PerformSingleRangingMeasurement(pMyDevice,
                    &RangingMeasurementData);

            print_pal_error(Status);
            print_range_status(&RangingMeasurementData);


            if (Status != VL53L0X_ERROR_NONE) break;

            q_printf("Measured distance: %d\r\n\r\n", RangingMeasurementData.RangeMilliMeter);


        }
    }
    return Status;
}
