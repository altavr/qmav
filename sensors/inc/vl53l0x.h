#ifndef VL53L0X_H
#define VL53L0X_H

#include "vl53l0x_platform.h"

VL53L0X_Error rangingTest(void);
void lidar_init(void);
VL53L0X_Error rangingTestContinues(void);
VL53L0X_Error rangingTestLong(void);

#endif // VL53L0X_H
