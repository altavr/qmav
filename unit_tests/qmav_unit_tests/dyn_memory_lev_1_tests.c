#include "stdio.h"
#include <math.h>
#include "unity.h"
#include "fff.h"
#include "qos_memory.h"
#include "qos_ln_list.h"
#include "ut_utils.h"
#include "dyn_mem_init_tests.h"


DEFINE_FFF_GLOBALS



FAKE_VALUE_FUNC(uint, _q_mem_start_mb_idx)
FAKE_VALUE_FUNC(uint, _q_mem_end_mb_idx)
FAKE_VALUE_FUNC(uint, _q_mem_res_addr2map_idx, size_t)



#define FAKE_FUNCTION_LIST(ACTION) \
    ACTION(_q_mem_start_mb_idx) \
    ACTION(_q_mem_end_mb_idx) \
    ACTION(_q_mem_res_addr2map_idx)


#define FF_STAT(func) printf(#func " stat:\n");\
    printf("call count: %d\n", func##_fake.call_count)



void tearDown(void) {
    // clean stuff up here
}


void setUp(void)
{
    mcb_reset();
    FAKE_FUNCTION_LIST(RESET_FAKE);


}



void _q_mem_mb_idx2addr_start_idx_test(void)
{

//    uint st_addr = 0x20000;
//    _q_mem_start_mb_idx_fake.return_val = st_addr;
    uint idx = 0;
    size_t ref_mem_addr = mcb.mb_mpu_start + mcb.mb_raw_blocksize * idx;

    size_t memaddr = _q_mem_mb_idx2addr(idx);
    TEST_ASSERT_EQUAL_UINT64(ref_mem_addr, memaddr);
}

void _q_mem_mb_idx2addr_end_idx_test(void)
{

//    uint st_addr = 0x40000;
//    _q_mem_start_mb_idx_fake.return_val = st_addr;
    uint idx = _Q_MPU_DYN_BLOCK_NUM;
    uintptr_t ref_mem_addr = mcb.mb_mpu_start + mcb.mb_raw_blocksize * idx;
    size_t memaddr = _q_mem_mb_idx2addr(idx);
    TEST_ASSERT_EQUAL_UINT64(ref_mem_addr, memaddr);
}

void _q_mem_mb_idx2addr_middle_idx_test(void)
{
    uint idx = 4;
    uintptr_t ref_mem_addr = mcb.mb_mpu_start + mcb.mb_raw_blocksize * idx;

    size_t memaddr = _q_mem_mb_idx2addr(idx);
    TEST_ASSERT_EQUAL_UINT64(ref_mem_addr, memaddr);
}


void _q_mem_mb_addr2idx__start_addr_test(void)
{
    uintptr_t addr = mcb.mb_mpu_start;
    uint idx = _q_mem_mb_addr2idx(addr );
    TEST_ASSERT_EQUAL_UINT(0, idx);

}

void _q_mem_mb_addr2idx__end_addr_test(void)
{
    uintptr_t addr = mcb.mb_mpu_start + _Q_MPU_DYN_BLOCK_NUM * mcb.mb_raw_blocksize;
    uint idx = _q_mem_mb_addr2idx(addr);
    TEST_ASSERT_EQUAL_UINT(_Q_MPU_DYN_BLOCK_NUM, idx);

}

void _q_mem_mb_addr2idx__middle_addr_test(void)
{
    uintptr_t addr = mcb.mb_mpu_start + 4 * mcb.mb_raw_blocksize;
    uint idx = _q_mem_mb_addr2idx(addr);
    TEST_ASSERT_EQUAL_UINT(4, idx);
}

void _q_mem_mb_find_free_blocks_start_free_0_max_need_1_test(void)
{
    _q_mem_start_mb_idx_fake.return_val = 0;
    _q_mem_end_mb_idx_fake.return_val = _Q_MPU_DYN_BLOCK_NUM;
    int idx = _q_mem_mb_find_free_blocks(1);
    TEST_ASSERT_EQUAL_INT(0, idx);

}

void _q_mem_mb_find_free_blocks_start_free_0_max_need_6_test(void)
{
    _q_mem_start_mb_idx_fake.return_val = 0;
    _q_mem_end_mb_idx_fake.return_val = _Q_MPU_DYN_BLOCK_NUM;
    int idx = _q_mem_mb_find_free_blocks(6);
    TEST_ASSERT_EQUAL_INT(0, idx);

}

void _q_mem_mb_find_free_blocks_start_free_9_max_need_2_test(void)
{
    _q_mem_start_mb_idx_fake.return_val = 0;
    _q_mem_end_mb_idx_fake.return_val = _Q_MPU_DYN_BLOCK_NUM;
    uint free_bl_idx = 9;
    for (uint i = 0; i < free_bl_idx; i++) {
        mcb.mem2mb[i] = (mbcb_t*)0xFF;
    }
    int idx = _q_mem_mb_find_free_blocks(2);
    TEST_ASSERT_EQUAL_INT(free_bl_idx, idx);

}

void _q_mem_mb_find_free_blocks_start_free_spacer1_11_max_need_3_test(void)
{
    _q_mem_start_mb_idx_fake.return_val = 0;
    _q_mem_end_mb_idx_fake.return_val = _Q_MPU_DYN_BLOCK_NUM;
    uint free_bl_idx = 11;
    for (uint i = 0; i < free_bl_idx; i++) {
        mcb.mem2mb[i] = (mbcb_t*)0xFF;
    }
    mcb.mem2mb[6] = (mbcb_t*)0;
    mcb.mem2mb[7] = (mbcb_t*)0;

    int idx = _q_mem_mb_find_free_blocks(3);
    TEST_ASSERT_EQUAL_INT(free_bl_idx, idx);
}

void _q_mem_mb_find_free_blocks_start_free_spacer3_max2max_need_3_test(void)
{
    _q_mem_start_mb_idx_fake.return_val = 0;
    _q_mem_end_mb_idx_fake.return_val = _Q_MPU_DYN_BLOCK_NUM;
    uint free_bl_idx = _Q_MPU_DYN_BLOCK_NUM;
    for (uint i = 0; i < free_bl_idx; i++) {
        mcb.mem2mb[i] = (mbcb_t*)0xFF;
    }
    mcb.mem2mb[6] = (mbcb_t*)0;
    mcb.mem2mb[7] = (mbcb_t*)0;
    mcb.mem2mb[8] = (mbcb_t*)0;

    int idx = _q_mem_mb_find_free_blocks(3);
    TEST_ASSERT_EQUAL_INT(6, idx);
}

void _q_mem_mb_find_free_blocks_start_nofree_test(void)
{
    _q_mem_start_mb_idx_fake.return_val = 0;
    _q_mem_end_mb_idx_fake.return_val = _Q_MPU_DYN_BLOCK_NUM;
//    uint free_bl_idx = _Q_MPU_DYN_BLOCK_NUM;
    for (uint i = 0; i < _Q_MPU_DYN_BLOCK_NUM; i++) {
        mcb.mem2mb[i] = (mbcb_t*)0xFF;
    }
    mcb.mem2mb[6] = (mbcb_t*)0;
    mcb.mem2mb[7] = (mbcb_t*)0;
    mcb.mem2mb[8] = (mbcb_t*)0;

    int idx = _q_mem_mb_find_free_blocks(4);
    TEST_ASSERT_EQUAL_INT(-1, idx);
}




void _mem_res_find_seq_map_in_region__8_from_6_free_need_4_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
    mcb.res_mem_startblock = 64;
    for (uint i = 0; i < mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL; i++) {
        mcb.memmap[i] = 0xFF;
    }

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6] = 0xFF;
    int ref_idx = (int)((mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6) * _Q_MMAP_CH_IN_BL);
    int idx = _mem_res_find_seq_map_in_region(4, mcb.res_mem_startblock, _Q_MMAP_BLOCKNUM);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);

}


void _q_mem_res_find_seq_map_8_from_6_free_need_4_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
    mcb.res_mem_startblock = 64;
    for (uint i = 0; i < mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL; i++) {
        mcb.memmap[i] = 0xFF;
    }

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6] = 0xFF;
    int ref_idx = (int)((mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6) * _Q_MMAP_CH_IN_BL);
    int idx = _q_mem_res_find_seq_map(4);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);

}

void _q_mem_res_find_seq_map_8_from_6_free_need_8_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
    mcb.res_mem_startblock = 64;

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6] = 0xFF;
    int ref_idx = (int)((mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6) * _Q_MMAP_CH_IN_BL);
    int idx = _q_mem_res_find_seq_map(8);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);

}

void _q_mem_res_find_seq_map_0_from_0_free_need_4_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
     mcb.res_mem_startblock = 64;
    mcb.memmap[1] = 0xFF;
    int ref_idx = -1;
    int idx = _q_mem_res_find_seq_map(4);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);
}

void _q_mem_res_find_seq_map_12_from_6_4_free_need_8_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
    mcb.res_mem_startblock = 64;

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6] = 0xF0;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 7] = 0xFF;
    int ref_idx = (int)((mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6) * _Q_MMAP_CH_IN_BL + 4);
    int idx = _q_mem_res_find_seq_map(8);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);

}

void _q_mem_res_find_seq_map_16_from_6_4_free_need_16_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
    mcb.res_mem_startblock = 64;

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6] = 0xF0;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 7] = 0xFF;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 8] = 0x0F;
    int ref_idx = (int)((mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6) * _Q_MMAP_CH_IN_BL + 4);
    int idx = _q_mem_res_find_seq_map(16);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);

}

void _q_mem_res_find_seq_map_20_from_6_4_free_need_16_gap_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
    mcb.res_mem_startblock = 64;

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 4] = 0xFF;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 5] = 0x0F;

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6] = 0xF0;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 7] = 0xFF;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 8] = 0xFF;
    int ref_idx = (int)((mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6) * _Q_MMAP_CH_IN_BL + 4);
    int idx = _q_mem_res_find_seq_map(16);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);

}

void _q_mem_res_find_seq_map_15_from_6_4_free_need_16_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
    mcb.res_mem_startblock = 64;

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6] = 0xF0;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 7] = 0xFF;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 8] = 0b00000111;
    int ref_idx = -1;
    int idx = _q_mem_res_find_seq_map(16);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);
}


void _q_mem_res_num_free_chunk_from_idx_8_align_test(void)
{
    mcb.memmap[0] = 0;
    mcb.memmap[1] = 0xFF;
    mcb.memmap[2] = 0;
 _q_mem_res_addr2map_idx_fake.return_val = 32;
    size_t  num  = _q_mem_res_num_free_chunk_from_idx(8);
    TEST_ASSERT_EQUAL_INT(8, num);

}

void _q_mem_res_num_free_chunk_from_idx_7_align_test(void)
{
    mcb.memmap[0] = 0;
    mcb.memmap[1] = 0x7F;
    mcb.memmap[2] = 0;

    _q_mem_res_addr2map_idx_fake.return_val = 32;

    size_t  num  = _q_mem_res_num_free_chunk_from_idx(8);
    TEST_ASSERT_EQUAL_INT(7, num);

}

void _q_mem_res_num_free_chunk_from_idx_7_not_align_test(void)
{
    mcb.memmap[0] = 0;
    mcb.memmap[1] = 0xFE;
    mcb.memmap[2] = 0;

    _q_mem_res_addr2map_idx_fake.return_val = 32;
    size_t  num  = _q_mem_res_num_free_chunk_from_idx(9);
    TEST_ASSERT_EQUAL_INT(7, num);

}

void _q_mem_res_num_free_chunk_from_idx_0_not_align_test(void)
{
    mcb.memmap[0] = 0;
    mcb.memmap[1] = 0;
    mcb.memmap[2] = 0;

    _q_mem_res_addr2map_idx_fake.return_val = 32;
    size_t  num  = _q_mem_res_num_free_chunk_from_idx(9);
    TEST_ASSERT_EQUAL_INT(0, num);

}

void _q_mem_res_num_free_chunk_from_idx_10_not_align_test(void)
{
    mcb.memmap[0] = 0;
    mcb.memmap[1] = 0xFE;
    mcb.memmap[2] = 0b111;

    _q_mem_res_addr2map_idx_fake.return_val = 32;
    size_t  num  = _q_mem_res_num_free_chunk_from_idx(9);
    TEST_ASSERT_EQUAL_INT(10, num);

}


void _q_mem_res_num_free_chunk_from_idx_19_not_align_test(void)
{
    mcb.memmap[0] = 0;
    mcb.memmap[1] = 0xFE;
    mcb.memmap[2] = 0xFF;
    mcb.memmap[3] = 0b1111;

    _q_mem_res_addr2map_idx_fake.return_val = 32;
    size_t  num  = _q_mem_res_num_free_chunk_from_idx(9);
    TEST_ASSERT_EQUAL_INT(19, num);
}

void _q_mem_res_num_free_chunk_from_idx_1_not_align_test(void)
{
    mcb.memmap[0] = 0;
    mcb.memmap[1] = 0x2;
    mcb.memmap[2] = 0x0;
    mcb.memmap[3] = 0x0;

    _q_mem_res_addr2map_idx_fake.return_val = 32;
    size_t  num  = _q_mem_res_num_free_chunk_from_idx(9);
    TEST_ASSERT_EQUAL_INT(1, num);
}


void _q_mem_res_find_seq_map_8_from_6_free_need_4_align_1_mpu_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
    mcb.res_mem_startblock = 64;
    for (uint i = 0; i < mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL; i++) {
        mcb.memmap[i] = 0xFF;
    }

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6] = 0xFF;
    int ref_idx = (int)((mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6) * _Q_MMAP_CH_IN_BL);
    int idx = _q_mem_res_find_seq_map_mpu(4);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);

}

void _q_mem_res_find_seq_map_8_from_6_free_need_8_align_1_mpu_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
    mcb.res_mem_startblock = 64;

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6] = 0xFF;
    int ref_idx = (int)((mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6) * _Q_MMAP_CH_IN_BL);
    int idx = _q_mem_res_find_seq_map_mpu(8U);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);

}

void _q_mem_res_find_seq_map_0_from_0_free_need_4_align_1_mpu_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
     mcb.res_mem_startblock = 64;
    mcb.memmap[1] = 0xFF;
    int ref_idx = -1;
    int idx = _q_mem_res_find_seq_map_mpu(4);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);
}

void _q_mem_res_find_seq_map_12_from_6_4_free_need_8_align_1_mpu_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
    mcb.res_mem_startblock = 64;

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6] = 0xF0;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 7] = 0xFF;
    int ref_idx = (int)((mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 7) * _Q_MMAP_CH_IN_BL);
    int idx = _q_mem_res_find_seq_map_mpu(8);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);

}

void _q_mem_res_find_seq_map_16_from_6_4_free_need_16_align_1_mpu_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
    mcb.res_mem_startblock = 64;

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6] = 0xF0;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 7] = 0xFF;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 8] = 0x0F;
//    int ref_idx = (int)((mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6) * _Q_MMAP_CH_IN_BL + 4);
    int ref_idx = -1;
    int idx = _q_mem_res_find_seq_map_mpu(16U);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);

}

void _q_mem_res_find_seq_map_20_from_6_4_free_need_16_gap_align_1_mpu_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
    mcb.res_mem_startblock = 64;

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 4] = 0xFF;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 5] = 0x0F;

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6] = 0xF0;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 7] = 0xFF;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 8] = 0xFF;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 9] = 0xFF;
    int ref_idx = (int)((mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 8) * _Q_MMAP_CH_IN_BL);
    int idx = _q_mem_res_find_seq_map_mpu(16);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);

}

void _q_mem_res_find_seq_map_15_from_6_4_free_need_16_align_1_mpu_test(void)
{
    _q_mem_res_addr2map_idx_fake.return_val = _Q_MMAP_BLOCKNUM;
    mcb.res_mem_startblock = 64;

    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 6] = 0xF0;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 7] = 0xFF;
    mcb.memmap[mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL + 8] = 0b00000111;
    int ref_idx = -1;
    int idx = _q_mem_res_find_seq_map_mpu(16);
    TEST_ASSERT_EQUAL_INT(ref_idx, idx);
}


int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(_q_mem_mb_idx2addr_start_idx_test);
    RUN_TEST(_q_mem_mb_idx2addr_end_idx_test);
    RUN_TEST(_q_mem_mb_idx2addr_middle_idx_test);

    RUN_TEST(_q_mem_mb_addr2idx__start_addr_test);
    RUN_TEST(_q_mem_mb_addr2idx__end_addr_test);
    RUN_TEST(_q_mem_mb_addr2idx__middle_addr_test);

    RUN_TEST(_q_mem_mb_find_free_blocks_start_free_0_max_need_1_test);
    RUN_TEST(_q_mem_mb_find_free_blocks_start_free_0_max_need_6_test);
    RUN_TEST(_q_mem_mb_find_free_blocks_start_free_9_max_need_2_test);
    RUN_TEST(_q_mem_mb_find_free_blocks_start_free_spacer1_11_max_need_3_test);
    RUN_TEST(_q_mem_mb_find_free_blocks_start_free_spacer3_max2max_need_3_test);
    RUN_TEST(_q_mem_mb_find_free_blocks_start_nofree_test);


    RUN_TEST(_mem_res_find_seq_map_in_region__8_from_6_free_need_4_test);

    RUN_TEST(_q_mem_res_find_seq_map_8_from_6_free_need_4_test);
    RUN_TEST(_q_mem_res_find_seq_map_8_from_6_free_need_8_test);
    RUN_TEST(_q_mem_res_find_seq_map_0_from_0_free_need_4_test);
    RUN_TEST(_q_mem_res_find_seq_map_12_from_6_4_free_need_8_test);
    RUN_TEST(_q_mem_res_find_seq_map_16_from_6_4_free_need_16_test);
    RUN_TEST(_q_mem_res_find_seq_map_20_from_6_4_free_need_16_gap_test);
    RUN_TEST(_q_mem_res_find_seq_map_15_from_6_4_free_need_16_test);

    RUN_TEST(_q_mem_res_num_free_chunk_from_idx_8_align_test);
    RUN_TEST(_q_mem_res_num_free_chunk_from_idx_7_align_test);
    RUN_TEST(_q_mem_res_num_free_chunk_from_idx_7_not_align_test);
    RUN_TEST(_q_mem_res_num_free_chunk_from_idx_0_not_align_test);
    RUN_TEST(_q_mem_res_num_free_chunk_from_idx_10_not_align_test);
    RUN_TEST(_q_mem_res_num_free_chunk_from_idx_19_not_align_test);
    RUN_TEST(_q_mem_res_num_free_chunk_from_idx_1_not_align_test);

    RUN_TEST(_q_mem_res_find_seq_map_8_from_6_free_need_4_align_1_mpu_test);
    RUN_TEST(_q_mem_res_find_seq_map_8_from_6_free_need_8_align_1_mpu_test);
    RUN_TEST(_q_mem_res_find_seq_map_0_from_0_free_need_4_align_1_mpu_test);
    RUN_TEST(_q_mem_res_find_seq_map_12_from_6_4_free_need_8_align_1_mpu_test);
    RUN_TEST(_q_mem_res_find_seq_map_16_from_6_4_free_need_16_align_1_mpu_test);
    RUN_TEST(_q_mem_res_find_seq_map_20_from_6_4_free_need_16_gap_align_1_mpu_test);
    RUN_TEST(_q_mem_res_find_seq_map_15_from_6_4_free_need_16_align_1_mpu_test);


    return UNITY_END();

}
