#include "stdio.h"
#include <math.h>
#include "unity.h"
#include "fff.h"
#include "qos_memory.h"
#include "qos_ln_list.h"
#include "ut_utils.h"
#include "dyn_mem_init_tests.h"


DEFINE_FFF_GLOBALS


FAKE_VALUE_FUNC(void*, qos_memblock_malloc, size_t, mbcb_t*)
FAKE_VALUE_FUNC(uint, _q_mem_mb_len2blocknum, size_t)
FAKE_VALUE_FUNC(int, _q_mem_mb_find_free_blocks, uint)
FAKE_VALUE_FUNC(size_t, _q_mem_mb_idx2addr, uint)

FAKE_VOID_FUNC(qos_memblock_init, memaddr_t, size_t, mbcb_t*)
FAKE_VOID_FUNC(_q_mem_mb_set_used, uint, uint, mbcb_t*)
FAKE_VOID_FUNC(_q_mpu_add_heap_region_to_tread, uint, uint, mpu_rp_t*)


FAKE_VALUE_FUNC(uint, _q_mem_res_len2blocknum, size_t)
FAKE_VALUE_FUNC(int, _q_mem_res_find_seq_map, uint )
FAKE_VALUE_FUNC(size_t, _qos_mb_array_extend_down, size_t)
FAKE_VALUE_FUNC(uint , _q_mem_res_addr2map_idx, size_t)
FAKE_VALUE_FUNC(size_t,  _q_mem_res_map_idx2addr, uint)

FAKE_VOID_FUNC(_q_mem_res_set_map, uint, uint)
FAKE_VOID_FUNC(_q_mem_res_unset_map, uint, uint)
FAKE_VOID_FUNC(_q_mpu_add_stack_region_to_tread, uint, sbcb_t *, mpu_rp_t*)
FAKE_VOID_FUNC(_q_mpu_remove_stack_region_to_tread, uint, sbcb_t *, mpu_rp_t*)

FAKE_VALUE_FUNC(uint, _q_mem_mb_get_free_status, mbcb_t *)
FAKE_VALUE_FUNC(uint, _q_mem_mb_addr2idx, size_t)

FAKE_VOID_FUNC(qos_memblock_free, void*, mbcb_t*)
FAKE_VOID_FUNC(_q_mpu_remove_heap_region_to_tread, uint, uint, mpu_rp_t*)
FAKE_VOID_FUNC(_q_mem_mb_set_free, uint, uint)

FAKE_VOID_FUNC(_qos_ins_to_ln_after_item, q_lnim_t *, q_lnim_t *)
FAKE_VOID_FUNC(_qos_rem_item_from_ln_list, q_lnim_t *)

FAKE_VALUE_FUNC(size_t, _q_mem_res_num_free_chunk_from_idx, size_t)



#define FAKE_FUNCTION_LIST(ACTION) \
    ACTION(qos_memblock_malloc) \
    ACTION(_q_mem_mb_len2blocknum) \
    ACTION(_q_mem_mb_find_free_blocks) \
    ACTION(_q_mem_mb_idx2addr) \
    ACTION(qos_memblock_init) \
    ACTION(_q_mem_mb_set_used) \
    ACTION(_q_mpu_add_heap_region_to_tread) \
    ACTION(_q_mem_res_len2blocknum) \
    ACTION(_q_mem_res_find_seq_map) \
    ACTION(_qos_mb_array_extend_down) \
    ACTION(_q_mem_res_addr2map_idx) \
    ACTION(_q_mem_res_map_idx2addr) \
    ACTION(_q_mem_res_set_map) \
    ACTION(_q_mem_res_unset_map) \
    ACTION(_q_mpu_add_stack_region_to_tread) \
    ACTION(_q_mem_mb_get_free_status) \
    ACTION(_q_mem_mb_addr2idx) \
    ACTION(qos_memblock_free) \
    ACTION(_q_mpu_remove_heap_region_to_tread) \
    ACTION(_q_mem_mb_set_free) \
    ACTION(_qos_ins_to_ln_after_item) \
    ACTION(_qos_rem_item_from_ln_list) \
    ACTION(_q_mem_res_num_free_chunk_from_idx)

void tearDown(void) {
    // clean stuff up here
}


void setUp(void)
{
    mcb_reset();
    FAKE_FUNCTION_LIST(RESET_FAKE);
}

void qalloc_handler_new_mb_test(void)
{
    q_lnim_t thread_mbs;
    thread_mbs.next = &thread_mbs;
    thread_mbs.prev = &thread_mbs;

    uint len = 200;
    mpu_rp_t mpu_conf = {0x45, 0x56};

    uint first_free_block_idx = 1;
    uint num_blocks = 1;

    void* free_space_ptr =(void*) 0x700;

    _q_mem_mb_len2blocknum_fake.return_val = num_blocks;
    _q_mem_mb_find_free_blocks_fake.return_val =first_free_block_idx;

    _q_mem_mb_idx2addr_fake.return_val = 0x600;
    qos_memblock_malloc_fake.return_val = free_space_ptr;

    void *ptr = qalloc_dynmem(len, &thread_mbs, &mpu_conf);

    TEST_ASSERT_EQUAL(1, qos_memblock_malloc_fake.call_count);

    TEST_ASSERT_EQUAL(len, _q_mem_mb_len2blocknum_fake.arg0_val);
    TEST_ASSERT_EQUAL(num_blocks, _q_mem_mb_find_free_blocks_fake.arg0_val);
    TEST_ASSERT_EQUAL(first_free_block_idx, _q_mem_mb_idx2addr_fake.arg0_val);

    TEST_ASSERT_EQUAL(0x600, qos_memblock_init_fake.arg0_val);
    TEST_ASSERT_EQUAL(num_blocks * mcb.mb_raw_blocksize, qos_memblock_init_fake.arg1_val);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[first_free_block_idx], qos_memblock_init_fake.arg2_val);
    TEST_ASSERT_EQUAL(first_free_block_idx, _q_mem_mb_set_used_fake.arg0_val);
    TEST_ASSERT_EQUAL(num_blocks, _q_mem_mb_set_used_fake.arg1_val);

    TEST_ASSERT_EQUAL(len, qos_memblock_malloc_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[first_free_block_idx], qos_memblock_malloc_fake.arg1_val);
    TEST_ASSERT_EQUAL(first_free_block_idx, _q_mpu_add_heap_region_to_tread_fake.arg0_val );
    TEST_ASSERT_EQUAL(num_blocks, _q_mpu_add_heap_region_to_tread_fake.arg1_val );
    TEST_ASSERT_EQUAL_PTR(&mpu_conf, _q_mpu_add_heap_region_to_tread_fake.arg2_val );

    TEST_ASSERT_EQUAL_PTR(free_space_ptr, ptr);
}

void qalloc_handler_used_present_free_space_mb_test(void)
{
    uint first_free_block_idx = 1;
    void* free_space_ptr = &mcb.mb_mpu[first_free_block_idx]  + 0x700;

    q_lnim_t thread_mbs;
    thread_mbs.next = &mcb.mb_mpu[first_free_block_idx].ll_item_thread;
    thread_mbs.prev = &mcb.mb_mpu[first_free_block_idx].ll_item_thread;
    mcb.mb_mpu[first_free_block_idx].ll_item_thread.next =  &thread_mbs;
    mcb.mb_mpu[first_free_block_idx].ll_item_thread.prev =  &thread_mbs;

    uint len = 200;
    mpu_rp_t mpu_conf = {0x45, 0x56};

    qos_memblock_malloc_fake.return_val = free_space_ptr;

    void *ptr = qalloc_dynmem(len, &thread_mbs, &mpu_conf);

    TEST_ASSERT_EQUAL(1, qos_memblock_malloc_fake.call_count);
    TEST_ASSERT_EQUAL(0, _q_mem_mb_len2blocknum_fake.call_count);

    TEST_ASSERT_EQUAL(len, qos_memblock_malloc_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[first_free_block_idx] , qos_memblock_malloc_fake.arg1_val);
    TEST_ASSERT_EQUAL_PTR(free_space_ptr, ptr);
}


void qalloc_handler_used_present_nonfree_space_mb_free_new_mb_test(void)
{
    q_lnim_t thread_mbs;
    thread_mbs.next = &mcb.mb_mpu[0].ll_item_thread;
    thread_mbs.prev = &mcb.mb_mpu[0].ll_item_thread;
    mcb.mb_mpu[0].ll_item_thread.next =  &thread_mbs;
    mcb.mb_mpu[0].ll_item_thread.prev =  &thread_mbs;

    uint len = 200;
    mpu_rp_t mpu_conf = {0x45, 0x56};

    uint first_free_block_idx = 1;
    uint num_blocks = 1;

    void* free_space_ptr = (void*)0x700;

    _q_mem_mb_len2blocknum_fake.return_val = num_blocks;
    _q_mem_mb_find_free_blocks_fake.return_val =first_free_block_idx;

    _q_mem_mb_idx2addr_fake.return_val = 0x600;

    void * mb_malloc_returns[] = {0, free_space_ptr};
    SET_RETURN_SEQ(qos_memblock_malloc, mb_malloc_returns, 2);

    void *ptr = qalloc_dynmem(len, &thread_mbs, &mpu_conf);

    TEST_ASSERT_EQUAL(2, qos_memblock_malloc_fake.call_count);

    TEST_ASSERT_EQUAL(len, _q_mem_mb_len2blocknum_fake.arg0_val);
    TEST_ASSERT_EQUAL(num_blocks, _q_mem_mb_find_free_blocks_fake.arg0_val);
    TEST_ASSERT_EQUAL(first_free_block_idx, _q_mem_mb_idx2addr_fake.arg0_val);

    TEST_ASSERT_EQUAL(0x600, qos_memblock_init_fake.arg0_val);
    TEST_ASSERT_EQUAL(num_blocks * mcb.mb_raw_blocksize, qos_memblock_init_fake.arg1_val);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[first_free_block_idx], qos_memblock_init_fake.arg2_val);
    TEST_ASSERT_EQUAL(first_free_block_idx, _q_mem_mb_set_used_fake.arg0_val);
    TEST_ASSERT_EQUAL(num_blocks, _q_mem_mb_set_used_fake.arg1_val);

    TEST_ASSERT_EQUAL(len, qos_memblock_malloc_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[first_free_block_idx], qos_memblock_malloc_fake.arg1_val);
    TEST_ASSERT_EQUAL(first_free_block_idx, _q_mpu_add_heap_region_to_tread_fake.arg0_val );
    TEST_ASSERT_EQUAL(num_blocks, _q_mpu_add_heap_region_to_tread_fake.arg1_val );
    TEST_ASSERT_EQUAL_PTR(&mpu_conf, _q_mpu_add_heap_region_to_tread_fake.arg2_val );

    TEST_ASSERT_EQUAL_PTR(free_space_ptr, ptr);
}

void qalloc_handler_used__nonfree_space_test(void)
{
    q_lnim_t thread_mbs;
    thread_mbs.next = &mcb.mb_mpu[0].ll_item_thread;
    thread_mbs.prev = &mcb.mb_mpu[0].ll_item_thread;
    mcb.mb_mpu[0].ll_item_thread.next =  &thread_mbs;
    mcb.mb_mpu[0].ll_item_thread.prev =  &thread_mbs;

    uint len = 200;
    mpu_rp_t mpu_conf = {0x45, 0x56};

    uint first_free_block_idx = 1;
    uint num_blocks = 1;

    void* free_space_ptr = (void*)0;

    _q_mem_mb_len2blocknum_fake.return_val = num_blocks;
    _q_mem_mb_find_free_blocks_fake.return_val =first_free_block_idx;

    _q_mem_mb_idx2addr_fake.return_val = 0x600;

    void * mb_malloc_returns[] = {0, 0};
    SET_RETURN_SEQ(qos_memblock_malloc, mb_malloc_returns, 2);


    void *ptr = qalloc_dynmem(len, &thread_mbs, &mpu_conf);

    TEST_ASSERT_EQUAL(2, qos_memblock_malloc_fake.call_count);
    TEST_ASSERT_EQUAL(0, _q_mpu_add_heap_region_to_tread_fake.call_count);

    TEST_ASSERT_EQUAL(len, _q_mem_mb_len2blocknum_fake.arg0_val);
    TEST_ASSERT_EQUAL(num_blocks, _q_mem_mb_find_free_blocks_fake.arg0_val);
    TEST_ASSERT_EQUAL(first_free_block_idx, _q_mem_mb_idx2addr_fake.arg0_val);

    TEST_ASSERT_EQUAL(0x600, qos_memblock_init_fake.arg0_val);
    TEST_ASSERT_EQUAL(num_blocks * mcb.mb_raw_blocksize, qos_memblock_init_fake.arg1_val);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[first_free_block_idx], qos_memblock_init_fake.arg2_val);
    TEST_ASSERT_EQUAL(first_free_block_idx, _q_mem_mb_set_used_fake.arg0_val);
    TEST_ASSERT_EQUAL(num_blocks, _q_mem_mb_set_used_fake.arg1_val);

    TEST_ASSERT_EQUAL(len, qos_memblock_malloc_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[first_free_block_idx], qos_memblock_malloc_fake.arg1_val);

    TEST_ASSERT_EQUAL_PTR(free_space_ptr, ptr);
}


void qalloc_uniq_handler_free_space_available_test(void)
{
    size_t len = 200;
    sbcb_t stack_cb = {0, 0};
    mpu_rp_t mpu_conf = {0x45, 0x56};

    uint blocknum = 2;
    uint free_block_idx = 1;

    void *ref_ptr =(void*) 0x456;



    _q_mem_res_len2blocknum_fake.return_val = blocknum;
    _q_mem_res_find_seq_map_fake.return_val = free_block_idx;
    _q_mem_res_map_idx2addr_fake.return_val = (size_t)ref_ptr;


    void *ptr = qalloc_uniqmem(len, &stack_cb, &mpu_conf);

    TEST_ASSERT_EQUAL(0, _qos_mb_array_extend_down_fake.call_count);

    TEST_ASSERT_EQUAL(len, _q_mem_res_len2blocknum_fake.arg0_val);
    TEST_ASSERT_EQUAL(blocknum, _q_mem_res_find_seq_map_fake.arg0_val);
    TEST_ASSERT_EQUAL(free_block_idx, _q_mem_res_unset_map_fake.arg0_val);
    TEST_ASSERT_EQUAL(blocknum, _q_mem_res_unset_map_fake.arg1_val);
    TEST_ASSERT_EQUAL(free_block_idx, _q_mem_res_map_idx2addr_fake.arg0_val);
    TEST_ASSERT_EQUAL(0, _q_mpu_add_stack_region_to_tread_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&stack_cb, _q_mpu_add_stack_region_to_tread_fake.arg1_val);
    TEST_ASSERT_EQUAL_PTR(&mpu_conf, _q_mpu_add_stack_region_to_tread_fake.arg2_val);
    TEST_ASSERT_EQUAL_PTR(ref_ptr, ptr);

}


void qalloc_uniq_handler_free_space_extend_test(void)
{
    const uint need_blocknum = 4;
    const uint possess_blocknum = 2;
    const uint get_blocknum = 8;
    uint free_block_idx[] = {20, 20 - get_blocknum};

    size_t len = _Q_MMAP_CHUNKLEN * need_blocknum;
    sbcb_t stack_cb = {0, 0};
    mpu_rp_t mpu_conf = {0x45, 0x56};
    size_t mpu_end_0 = 0x20000;
    size_t mpu_end_1 = mpu_end_0 - _Q_MMAP_CHUNKLEN * get_blocknum;
    mcb.mb_mpu_end = mpu_end_1;

//    uint blocknum = 2;


    void *ref_ptr =(void*) (mpu_end_0 - (_Q_MMAP_CHUNKLEN * (need_blocknum - possess_blocknum)));


    uint blocknums[] = {need_blocknum, get_blocknum};
    SET_RETURN_SEQ(_q_mem_res_len2blocknum, blocknums, sizeof(blocknums) / sizeof(blocknums[0]));

    _q_mem_res_find_seq_map_fake.return_val = -1;

    _q_mem_res_num_free_chunk_from_idx_fake.return_val = possess_blocknum;

    _qos_mb_array_extend_down_fake.return_val = (need_blocknum - possess_blocknum) * _Q_MMAP_CHUNKLEN;

    SET_RETURN_SEQ(_q_mem_res_addr2map_idx, free_block_idx, 2);

    _q_mem_res_map_idx2addr_fake.return_val = (size_t)ref_ptr;


    void *ptr = qalloc_uniqmem(len, &stack_cb, &mpu_conf);

    TEST_ASSERT_EQUAL(1, _qos_mb_array_extend_down_fake.call_count);
    TEST_ASSERT_EQUAL(2, _q_mem_res_len2blocknum_fake.call_count);

    TEST_ASSERT_EQUAL(len, _q_mem_res_len2blocknum_fake.arg0_history[0]);

    TEST_ASSERT_EQUAL(need_blocknum, _q_mem_res_find_seq_map_fake.arg0_val);

    TEST_ASSERT_EQUAL(free_block_idx[0], _q_mem_res_num_free_chunk_from_idx_fake.arg0_val);
    TEST_ASSERT_EQUAL(_Q_MMAP_CHUNKLEN * (need_blocknum - possess_blocknum), _qos_mb_array_extend_down_fake.arg0_val);

    TEST_ASSERT_EQUAL((need_blocknum - possess_blocknum) * _Q_MMAP_CHUNKLEN, _q_mem_res_len2blocknum_fake.arg0_history[1]);


    TEST_ASSERT_EQUAL(mcb.mb_mpu_end, _q_mem_res_addr2map_idx_fake.arg0_val);
     TEST_ASSERT_EQUAL(free_block_idx[1], _q_mem_res_set_map_fake.arg0_val);
     TEST_ASSERT_EQUAL(get_blocknum, _q_mem_res_set_map_fake.arg1_val);


    TEST_ASSERT_EQUAL(free_block_idx[0] - (need_blocknum - possess_blocknum), _q_mem_res_unset_map_fake.arg0_val);
    TEST_ASSERT_EQUAL(need_blocknum, _q_mem_res_unset_map_fake.arg1_val);
    TEST_ASSERT_EQUAL(free_block_idx[0] - (need_blocknum - possess_blocknum), _q_mem_res_map_idx2addr_fake.arg0_val);
    TEST_ASSERT_EQUAL(0, _q_mpu_add_stack_region_to_tread_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&stack_cb, _q_mpu_add_stack_region_to_tread_fake.arg1_val);
    TEST_ASSERT_EQUAL_PTR(&mpu_conf, _q_mpu_add_stack_region_to_tread_fake.arg2_val);
    TEST_ASSERT_EQUAL_PTR(ref_ptr, ptr);

    TEST_ASSERT_EQUAL(len, stack_cb.len);
    TEST_ASSERT_EQUAL((size_t)ref_ptr, stack_cb.base);

}


void qalloc_uniq_handler_nofree_space_test(void)
{
    size_t len = 200;
    sbcb_t stack_cb = {0, 0};
    mpu_rp_t mpu_conf = {0x45, 0x56};
    mcb.mb_mpu_end = 0x20000;

    uint blocknum = 2;

    void *ref_ptr =(void*) NULL;

    _q_mem_res_len2blocknum_fake.return_val = 2;
    _q_mem_res_find_seq_map_fake.return_val = -1;

    _qos_mb_array_extend_down_fake.return_val = 0;

    _q_mem_res_map_idx2addr_fake.return_val = (size_t)ref_ptr;


    void *ptr = qalloc_uniqmem(len, &stack_cb, &mpu_conf);

    TEST_ASSERT_EQUAL(1, _qos_mb_array_extend_down_fake.call_count);
    TEST_ASSERT_EQUAL(1, _q_mem_res_len2blocknum_fake.call_count);
    TEST_ASSERT_EQUAL(1, _q_mem_res_addr2map_idx_fake.call_count);
    TEST_ASSERT_EQUAL(0, _q_mem_res_unset_map_fake.call_count);
    TEST_ASSERT_EQUAL(len, _q_mem_res_len2blocknum_fake.arg0_val);
    TEST_ASSERT_EQUAL(blocknum, _q_mem_res_find_seq_map_fake.arg0_val);
    TEST_ASSERT_EQUAL(len, _qos_mb_array_extend_down_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(ref_ptr, ptr);
}

void qalloc_uniq_handler_no_stack_slots_test(void)
{
    size_t len = 200;
    sbcb_t stack_cb = {12, 0};
    mpu_rp_t mpu_conf = {0x45, 0x56};

    void *ref_ptr =(void*) NULL;
    _q_mem_res_map_idx2addr_fake.return_val = (size_t)ref_ptr;

    void *ptr = qalloc_uniqmem(len, &stack_cb, &mpu_conf);

    TEST_ASSERT_EQUAL(0, _qos_mb_array_extend_down_fake.call_count);
    TEST_ASSERT_EQUAL(0, _q_mem_res_len2blocknum_fake.call_count);
    TEST_ASSERT_EQUAL(0, _q_mem_res_addr2map_idx_fake.call_count);
    TEST_ASSERT_EQUAL(0, _q_mem_res_unset_map_fake.call_count);
    TEST_ASSERT_EQUAL_PTR(ref_ptr, ptr);
}


void _q_free_main_mem_second_mb_not_free_test(void)
{
    void *ptr =(void*) 0x2020;
    q_lnim_t thread_memblocks;
    mpu_rp_t mpu_conf = {0x45, 0x56};

    thread_memblocks.prev = &mcb.mb_mpu[1].ll_item_thread;
    thread_memblocks.next = &mcb.mb_mpu[0].ll_item_thread;

    mcb.mb_mpu[0].ll_item_thread.prev = &thread_memblocks;
    mcb.mb_mpu[0].ll_item_thread.next = &mcb.mb_mpu[1].ll_item_thread;

    mcb.mb_mpu[1].ll_item_thread.prev = &mcb.mb_mpu[0].ll_item_thread;
    mcb.mb_mpu[1].ll_item_thread.next = &thread_memblocks;

    mcb.mb_mpu[0].head = (mch_head_t*)0x1000;
    mcb.mb_mpu[0].size = 0x800;
    mcb.mb_mpu[1].head = (mch_head_t*)0x1800;
    mcb.mb_mpu[1].size = 0x3000;

    mcb.mb_mpu_start = 0x500;
    mcb.mb_mpu_end= 0x5500;

    _q_mem_mb_get_free_status_fake.return_val = 0;

    _q_free_main_mem(ptr, &thread_memblocks, &mpu_conf);

    TEST_ASSERT_EQUAL(1, qos_memblock_free_fake.call_count);
    TEST_ASSERT_EQUAL_PTR(ptr, qos_memblock_free_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[1], qos_memblock_free_fake.arg1_val);
}

void _q_free_main_mem_second_mb_free_test(void)
{
    void *ptr =(void*) 0x1880;
    q_lnim_t thread_memblocks;
    mpu_rp_t mpu_conf = {0x45, 0x56};

    thread_memblocks.prev = &mcb.mb_mpu[1].ll_item_thread;
    thread_memblocks.next = &mcb.mb_mpu[0].ll_item_thread;

    mcb.mb_mpu[0].ll_item_thread.prev = &thread_memblocks;
    mcb.mb_mpu[0].ll_item_thread.next = &mcb.mb_mpu[1].ll_item_thread;

    mcb.mb_mpu[1].ll_item_thread.prev = &mcb.mb_mpu[0].ll_item_thread;
    mcb.mb_mpu[1].ll_item_thread.next = &thread_memblocks;

    mcb.mb_mpu[0].head = (mch_head_t*)0x1000;
    mcb.mb_mpu[0].size = 0x800;
    mcb.mb_mpu[1].head = (mch_head_t*)0x1800;
    mcb.mb_mpu[1].size = 2048;

    mcb.mb_mpu_start = 0x500;
    mcb.mb_mpu_end= 0x5500;

    _q_mem_mb_get_free_status_fake.return_val = 1;
    _q_mem_mb_addr2idx_fake.return_val = 1;

    _q_free_main_mem(ptr, &thread_memblocks, &mpu_conf);

    TEST_ASSERT_EQUAL(1, qos_memblock_free_fake.call_count);
    TEST_ASSERT_EQUAL_PTR(ptr, qos_memblock_free_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[1], qos_memblock_free_fake.arg1_val);
    TEST_ASSERT_EQUAL_UINT64((size_t)mcb.mb_mpu[1].head, _q_mem_mb_addr2idx_fake.arg0_val);
    TEST_ASSERT_EQUAL(1, _q_mem_mb_set_free_fake.arg0_val);
    TEST_ASSERT_EQUAL(1, _q_mem_mb_set_free_fake.arg1_val);
}


void _q_free_residual_mem_test(void)
{
    uint idx = 0;
    sbcb_t stack_cb = {0x160, 0x20};
    mpu_rp_t mpu_conf = {0x45, 0x56};

    mcb.low_border = 0x100;
    mcb.mb_mpu_start = 0x200;
    mcb.mb_mpu_end = 0x300;
    mcb.high_border = 0x400;

    _q_mem_res_addr2map_idx_fake.return_val = 10;
    _q_mem_res_len2blocknum_fake.return_val = 6;


    _q_free_residual_mem(idx, &stack_cb, &mpu_conf);

    TEST_ASSERT_EQUAL(10, _q_mem_res_set_map_fake.arg0_val);
    TEST_ASSERT_EQUAL(6, _q_mem_res_set_map_fake.arg1_val);
    TEST_ASSERT_EQUAL(0, stack_cb.base);

}

void _q_free_residual_mem_invalide_ptr_test(void)
{
    uint idx = 0;
    sbcb_t stack_cb = {0x1600, 0x20};
    mpu_rp_t mpu_conf = {0x45, 0x56};

    mcb.low_border = 0x100;
    mcb.mb_mpu_start = 0x200;
    mcb.mb_mpu_end = 0x300;
    mcb.high_border = 0x400;


    _q_free_residual_mem(idx, &stack_cb, &mpu_conf);

    TEST_ASSERT_EQUAL(0, _q_mem_res_set_map_fake.call_count );
    TEST_ASSERT_NOT_EQUAL(0, stack_cb.base);

}


int main(void)
{
    UNITY_BEGIN();

    RUN_TEST( qalloc_handler_new_mb_test);
    RUN_TEST(qalloc_handler_used_present_free_space_mb_test);
    RUN_TEST(qalloc_handler_used_present_nonfree_space_mb_free_new_mb_test);
    RUN_TEST(qalloc_handler_used__nonfree_space_test);

    RUN_TEST(qalloc_uniq_handler_free_space_available_test);
    RUN_TEST(qalloc_uniq_handler_free_space_extend_test);
    RUN_TEST(qalloc_uniq_handler_nofree_space_test);
    RUN_TEST(qalloc_uniq_handler_no_stack_slots_test);

    RUN_TEST(_q_free_main_mem_second_mb_not_free_test);
    RUN_TEST(_q_free_main_mem_second_mb_free_test);

    RUN_TEST(_q_free_residual_mem_test);
    RUN_TEST(_q_free_residual_mem_invalide_ptr_test);


    return UNITY_END();

}
