#include "stdio.h"
#include <math.h>
#include "unity.h"
#include "fff.h"
#include "qos_memory.h"
#include "qos_ln_list.h"
#include "ut_utils.h"
#include "dyn_mem_init_tests.h"


DEFINE_FFF_GLOBALS

//#define RAM_SIZE 1024UL * 128





#define FAKE_FUNCTION_LIST(ACTION)

u8 memmap[_Q_MMAP_GR_BLOCKNUM * 8];


void tearDown(void) {
    // clean stuff up here
}

void setUp(void)
{

    CLEAR_ARRAY(memmap);
    mcb_reset();
    FAKE_FUNCTION_LIST(RESET_FAKE);
}

void _q_mem_start_mb_idx_non_zero_test(void)
{
    uint mbnum = 6;
    mcb.mb_mpu_start = ram_origin + _RAM_SIZE / 2 + mbnum * mcb.mb_raw_blocksize;
    uint mbnum_res = _q_mem_start_mb_idx();

    TEST_ASSERT_EQUAL_UINT32(mbnum, mbnum_res);
}


void _q_mem_start_mb_idx_zero_test(void)
{
    uint mbnum = 0;
    mcb.mb_mpu_start = ram_origin + _RAM_SIZE / 2;
    mcb.mb_mpu_end = mcb.mb_mpu_end - mcb.mb_raw_blocksize;
    uint mbnum_res = _q_mem_start_mb_idx();

    TEST_ASSERT_EQUAL_UINT32(mbnum, mbnum_res);
}

void _q_mem_end_mb_idx_zero_start_test(void)
{
    uint mbnum = _Q_MPU_DYN_BLOCK_NUM - 1;
    mcb.mb_mpu_end = mcb.mb_mpu_end - mcb.mb_raw_blocksize;
    uint mbnum_res = _q_mem_end_mb_idx();
    TEST_ASSERT_EQUAL_UINT32(mbnum, mbnum_res);
}

void _q_mem_end_mb_idx_non_zero_start_test(void)
{
    mcb.mb_mpu_start = ram_origin + _RAM_SIZE / 2 + 6 * mcb.mb_raw_blocksize;
    mcb.mb_mpu_end = mcb.mb_mpu_end - mcb.mb_raw_blocksize;
    uint mbnum = _Q_MPU_DYN_BLOCK_NUM - 1;
    uint mbnum_res = _q_mem_end_mb_idx();
    TEST_ASSERT_EQUAL_UINT32(mbnum, mbnum_res);
}




void _q_mem_mb_len2blocknum_smaller_mb_test(void)
{
    uint mbnum = 1;
    uint mbnum_res = _q_mem_mb_len2blocknum(200);
    TEST_ASSERT_EQUAL_UINT32(mbnum, mbnum_res);
}

void _q_mem_mb_len2blocknum_zero_mb_test(void)
{
    uint mbnum = 0;
    uint mbnum_res = _q_mem_mb_len2blocknum(0);
    TEST_ASSERT_EQUAL_UINT32(mbnum, mbnum_res);
}

void _q_mem_mb_len2blocknum_len_equal_mb_test(void)
{
    uint mbnum = 1;
    uint mbnum_res = _q_mem_mb_len2blocknum(mcb.mb_raw_blocksize -  sizeof(mch_head_t));
    TEST_ASSERT_EQUAL_UINT32(mbnum, mbnum_res);
}

void _q_mem_mb_len2blocknum_equal_mb_test(void)
{
    uint mbnum = 2;
    uint mbnum_res = _q_mem_mb_len2blocknum(mcb.mb_raw_blocksize);
    TEST_ASSERT_EQUAL_UINT32(mbnum, mbnum_res);
}

void _q_mem_mb_len2blocknum_larger_mb_test(void)
{
    uint mbnum = 2;
    uint mbnum_res = _q_mem_mb_len2blocknum(mcb.mb_raw_blocksize + 50);
    TEST_ASSERT_EQUAL_UINT32(mbnum, mbnum_res);
}

void _q_mem_mb_len2blocknum_need_3_mb_test(void)
{
    uint mbnum = 3;
    uint mbnum_res = _q_mem_mb_len2blocknum(mcb.mb_raw_blocksize * 2 + 20);
    TEST_ASSERT_EQUAL_UINT32(mbnum, mbnum_res);
}


void _q_mem_mb_set_free_first_1_test(void)
{
    mbcb_t* ref_mem2mb[_Q_MPU_DYN_BLOCK_NUM];

    for (uint i = 0; i < _Q_MPU_DYN_BLOCK_NUM; i++) {
//        mcb.mem2mb[i] = (mbcb_t*)0xFF;
        ref_mem2mb[i] = (mbcb_t*)0;
    }
    mcb.mem2mb[0] = (mbcb_t*)0xFF;
    _q_mem_mb_set_free(0, 1);
    TEST_ASSERT_EQUAL_PTR_ARRAY(ref_mem2mb, mcb.mem2mb, _Q_MPU_DYN_BLOCK_NUM);
}

void _q_mem_mb_set_free_second_2__filled_test(void)
{
    mbcb_t* ref_mem2mb[_Q_MPU_DYN_BLOCK_NUM];

    for (uint i = 0; i < _Q_MPU_DYN_BLOCK_NUM; i++) {
        mcb.mem2mb[i] = (mbcb_t*)0xFF;
        ref_mem2mb[i] = (mbcb_t*)0xFF;
    }
    ref_mem2mb[1] = (mbcb_t*)0;
    ref_mem2mb[2] = (mbcb_t*)0;

    _q_mem_mb_set_free(1, 2);
    TEST_ASSERT_EQUAL_PTR_ARRAY(ref_mem2mb, mcb.mem2mb, _Q_MPU_DYN_BLOCK_NUM);
}

void _q_mem_mb_set_used_first_1_test(void)
{
    mbcb_t* ref_mem2mb[_Q_MPU_DYN_BLOCK_NUM];

    for (uint i = 0; i < _Q_MPU_DYN_BLOCK_NUM; i++) {
//        mcb.mem2mb[i] = (mbcb_t*)0xFF;
        ref_mem2mb[i] = (mbcb_t*)0;
    }
    ref_mem2mb[0] = (mbcb_t*)0xFF;

    mcb.mem2mb[0] = (mbcb_t*)0xFF;
    _q_mem_mb_set_used(0, 1, (mbcb_t*)0xFF);
    TEST_ASSERT_EQUAL_PTR_ARRAY(ref_mem2mb, mcb.mem2mb, _Q_MPU_DYN_BLOCK_NUM);
}


void _q_mem_mb_set_used_init10_2_set_test(void)
{
    mbcb_t* ref_mem2mb[_Q_MPU_DYN_BLOCK_NUM];

    for (uint i = 0; i < _Q_MPU_DYN_BLOCK_NUM; i++) {
        mcb.mem2mb[i] = (mbcb_t*)0xFF;
        ref_mem2mb[i] = (mbcb_t*)0xFF;
    }
    ref_mem2mb[10] = (mbcb_t*)0xFF;
    mcb.mem2mb[11] = (mbcb_t*)0xFF;

    _q_mem_mb_set_used(10, 2, (mbcb_t*)0xFF);
    TEST_ASSERT_EQUAL_PTR_ARRAY(ref_mem2mb, mcb.mem2mb, _Q_MPU_DYN_BLOCK_NUM);
}


//*******************************************************************
// case p2 < p1

void _q_set_8bit_map__in_middle_2_elem_2_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2] = 0b11000U;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_set_8bit_map__in_middle_2_elem_2_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    memmap[2] = (u8)(~0b11000);
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}


// case p2 == p1



void _q_set_8bit_map__in_start_1_elem_2_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[1] = 0b11;
    _q_set_8bit_map(8, 2, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);

}
void _q_set_8bit_map__in_start_1_elem_2_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    memmap[1] = (u8)(~0b11);
    _q_set_8bit_map(8, 2, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);

}


void _q_set_8bit_map__in_middle_2_elem_5_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2] = 0b11000000;
    ref_memmap[3] = 0b111;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_set_8bit_map__in_middle_2_elem_5_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11000000;
    memmap[2] = (u8)(~reg);
    memmap[3] = (u8)(~0b111);
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_set_8bit_map__in_middle_2_elem_5_end_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2] = 0b11111000;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_set_8bit_map__in_middle_2_elem_5_end_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11111000;
    memmap[2] = (u8)(~reg);
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

// case p2 > p1

void _q_set_8bit_map__in_start_2_elem_8_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2] = 0xFF;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2, 8, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_set_8bit_map__in_start_2_elem_8_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    memmap[2] = 0;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2, 8, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}




void _q_set_8bit_map__in_middle_2_elem_14_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    u8 reg = 0xFF;
    ref_memmap[2] = (u8)(reg << 6);
    ref_memmap[3] = 0xFF;
    ref_memmap[4] = 0b1111;

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_set_8bit_map__in_middle_2_elem_14_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0xFF;
    memmap[2] = (u8)(~(reg << 6));
    memmap[3] = 0;
    memmap[4] = (u8)(~(0b1111));

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_set_8bit_map__in_start_2_elem_12_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2] = 0xFF;
    ref_memmap[3] = 0b1111;

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2, 12, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_set_8bit_map__in_start_2_elem_12_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    memmap[2] = 0;
    memmap[3] = (u8)(~0b1111);

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2, 12, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_set_8bit_map__in_start_0_elem_12_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[0] = 0xFF;
    ref_memmap[1] = 0b1111;

    _q_set_8bit_map(0, 12, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_set_8bit_map__in_start_0_elem_12_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    memmap[0] = 0;
    memmap[1] = (u8)(~0b1111);

    _q_set_8bit_map(0, 12, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_set_8bit_map__in_middle_2_elem_10_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    u8 reg = 0xFF;
    ref_memmap[2] = (u8)(reg << 6);
    ref_memmap[3] = 0xFF;

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_set_8bit_map__in_middle_2_elem_10_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0xFF;
    memmap[2] = (u8)(~(reg << 6));
    memmap[3] = 0;

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_set_8bit_map__num_0_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
           CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 0, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

/***********************************************************************/

//*****************************************************************

//***********************************************************************


// case p2 < p1

void _q_set_8bit_map__in_middle_2_elem_2_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);


    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2 << spacer] = 0b11000;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_set_8bit_map__in_middle_2_elem_2_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);


    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    memmap[2 << spacer] = (u8)(~0b11000);

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}


// case p2 == p1



void _q_set_8bit_map__in_start_1_elem_2_unset_1_spacer_test(void)
{
    const u8 spacer = 1;

    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << 1; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[1 << spacer] = 0b11;
    _q_set_8bit_map(8, 2, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);

}
void _q_set_8bit_map__in_start_1_elem_2_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << 1; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    memmap[1 << spacer] = (u8)(~0b11);
    _q_set_8bit_map(8, 2, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);

}


void _q_set_8bit_map__in_middle_2_elem_5_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2 << spacer] = 0b11000000;
    ref_memmap[3 << spacer] = 0b111;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}


void _q_set_8bit_map__in_middle_2_elem_5_set_1_spacer_test(void)
{
    const u8 spacer = 1;

    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11000000;
    memmap[2 << spacer] = (u8)(~reg);
    memmap[3 << spacer] = (u8)(~0b111);
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_set_8bit_map__in_middle_2_elem_5_end_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2 << spacer] = 0b11111000;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_set_8bit_map__in_middle_2_elem_5_end_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11111000;
    memmap[2 << spacer] = (u8)(~reg);
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

// case p2 > p1

void _q_set_8bit_map__in_start_2_elem_8_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2 << spacer] = 0xFF;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2, 8, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_set_8bit_map__in_start_2_elem_8_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    memmap[2 << spacer] = 0;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2, 8, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}




void _q_set_8bit_map__in_middle_2_elem_14_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    u8 reg = 0xFF;
    ref_memmap[2 << spacer] = (u8)(reg << 6);
    ref_memmap[3 << spacer] = 0xFF;
    ref_memmap[4 << spacer] = 0b1111;

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_set_8bit_map__in_middle_2_elem_14_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];

    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i ] = 0xFF;
    }
    u8 reg = 0xFF;
    memmap[2 << spacer] = (u8)(~(reg << 6));
    memmap[3 << spacer] = 0;
    memmap[4 << spacer] = (u8)(~(0b1111));

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}

void _q_set_8bit_map__in_start_2_elem_12_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i ] = 0;
        ref_memmap[i ] = 0;
    }
    ref_memmap[2 << spacer] = 0xFF;
    ref_memmap[3 << spacer] = 0b1111;

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2, 12, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}


void _q_set_8bit_map__in_start_2_elem_12_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i ] = 0xFF;
        ref_memmap[i ] = 0xFF;
    }
    memmap[2 << spacer] = 0;
    memmap[3 << spacer] = (u8)(~0b1111);

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2, 12, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}

void _q_set_8bit_map__in_start_0_elem_12_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i ] = 0;
        ref_memmap[i ] = 0;
    }
    ref_memmap[0 << spacer] = 0xFF;
    ref_memmap[1 << spacer] = 0b1111;

    _q_set_8bit_map(0, 12, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}


void _q_set_8bit_map__in_start_0_elem_12_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i ] = 0xFF;
        ref_memmap[i ] = 0xFF;
    }
    memmap[0 << spacer] = 0;
    memmap[1 << spacer] = (u8)(~0b1111);

    _q_set_8bit_map(0, 12, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}


void _q_set_8bit_map__in_middle_2_elem_10_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i ] = 0;
    }
    u8 reg = 0xFF;
    ref_memmap[2 << spacer] = (u8)(reg << 6);
    ref_memmap[3 << spacer] = 0xFF;

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10, memmap,  spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}

void _q_set_8bit_map__in_middle_2_elem_10_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i ] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0xFF;
    memmap[2 << spacer] = (u8)(~(reg << 6));
    memmap[3 << spacer] = 0;

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}

void _q_set_8bit_map__num_0_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
           CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 0, memmap,  spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}



/***********************************************************************/

//*****************************************************************

//***********************************************************************


// case p2 < p1

void _q_set_8bit_map__in_middle_2_elem_2_unset_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);


    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2 << spacer] = 0b11000;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_set_8bit_map__in_middle_2_elem_2_set_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);


    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    memmap[2 << spacer] = (u8)(~0b11000);

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}


// case p2 == p1



void _q_set_8bit_map__in_start_1_elem_2_unset_2_spacer_test(void)
{
    const u8 spacer = 2;

    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << 1; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[1 << spacer] = 0b11;
    _q_set_8bit_map(8, 2, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);

}
void _q_set_8bit_map__in_start_1_elem_2_set_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << 1; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    memmap[1 << spacer] = (u8)(~0b11);
    _q_set_8bit_map(8, 2, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);

}


void _q_set_8bit_map__in_middle_2_elem_5_unset_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2 << spacer] = 0b11000000;
    ref_memmap[3 << spacer] = 0b111;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}


void _q_set_8bit_map__in_middle_2_elem_5_set_2_spacer_test(void)
{
    const u8 spacer = 2;

    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11000000;
    memmap[2 << spacer] = (u8)(~reg);
    memmap[3 << spacer] = (u8)(~0b111);
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_set_8bit_map__in_middle_2_elem_5_end_unset_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2 << spacer] = 0b11111000;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_set_8bit_map__in_middle_2_elem_5_end_set_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11111000;
    memmap[2 << spacer] = (u8)(~reg);
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

// case p2 > p1

void _q_set_8bit_map__in_start_2_elem_8_unset_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2 << spacer] = 0xFF;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2, 8, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_set_8bit_map__in_start_2_elem_8_set_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    memmap[2 << spacer] = 0;
    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2, 8, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}




void _q_set_8bit_map__in_middle_2_elem_14_unset_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    u8 reg = 0xFF;
    ref_memmap[2 << spacer] = (u8)(reg << 6);
    ref_memmap[3 << spacer] = 0xFF;
    ref_memmap[4 << spacer] = 0b1111;

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_set_8bit_map__in_middle_2_elem_14_set_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];

    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i ] = 0xFF;
    }
    u8 reg = 0xFF;
    memmap[2 << spacer] = (u8)(~(reg << 6));
    memmap[3 << spacer] = 0;
    memmap[4 << spacer] = (u8)(~(0b1111));

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}

void _q_set_8bit_map__in_start_2_elem_12_unset_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i ] = 0;
        ref_memmap[i ] = 0;
    }
    ref_memmap[2 << spacer] = 0xFF;
    ref_memmap[3 << spacer] = 0b1111;

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2, 12, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}


void _q_set_8bit_map__in_start_2_elem_12_set_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i ] = 0xFF;
        ref_memmap[i ] = 0xFF;
    }
    memmap[2 << spacer] = 0;
    memmap[3 << spacer] = (u8)(~0b1111U);

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2, 12, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}

void _q_set_8bit_map__in_start_0_elem_12_unset_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i ] = 0;
        ref_memmap[i ] = 0;
    }
    ref_memmap[0 << spacer] = 0xFF;
    ref_memmap[1 << spacer] = 0b1111;

    _q_set_8bit_map(0, 12, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}


void _q_set_8bit_map__in_start_0_elem_12_set_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i ] = 0xFF;
        ref_memmap[i ] = 0xFF;
    }
    memmap[0 << spacer] = 0;
    memmap[1 << spacer] = (u8)(~0b1111U);

    _q_set_8bit_map(0, 12, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}


void _q_set_8bit_map__in_middle_2_elem_10_unset_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i ] = 0;
    }
    u8 reg = 0xFF;
    ref_memmap[2 << spacer] = (u8)(reg << 6U);
    ref_memmap[3 << spacer] = 0xFF;

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10, memmap,  spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}

void _q_set_8bit_map__in_middle_2_elem_10_set_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
        CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i ] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0xFF;
    memmap[2 << spacer] = (u8)(~(reg << 6U));
    memmap[3 << spacer] = 0;

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}

void _q_set_8bit_map__num_0_set_2_spacer_test(void)
{
    const u8 spacer = 2;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM  << spacer];
           CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }

    _q_set_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 0, memmap,  spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM  << spacer);
}


/***********************************************************************/

//*****************************************************************

//***********************************************************************


// case p2 < p1

void _q_unset_8bit_map__in_middle_2_elem_2_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[2] = 0b11000;
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_unset_8bit_map__in_middle_2_elem_2_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[2] = (u8)(~0b11000U);
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}


// case p2 == p1


void _q_unset_8bit_map__in_start_1_elem_2_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[1] = 0b11;
    _q_unset_8bit_map(8, 2, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);

}
void _q_unset_8bit_map__in_start_1_elem_2_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[1] = (u8)(~0b11U);
    _q_unset_8bit_map(8, 2, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);

}


void _q_unset_8bit_map__in_middle_2_elem_5_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[2] = 0b11000000;
    memmap[3] = 0b111;
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_unset_8bit_map__in_middle_2_elem_5_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11000000;
    ref_memmap[2] = (u8)(~reg);
    ref_memmap[3] = (u8)(~0b111U);
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_unset_8bit_map__in_middle_2_elem_5_end_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[2] = 0b11111000;
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_unset_8bit_map__in_middle_2_elem_5_end_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11111000;
    ref_memmap[2] = (u8)(~reg);
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

// case p2 > p1

void _q_unset_8bit_map__in_start_2_elem_8_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[2] = 0xFF;
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2, 8, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_unset_8bit_map__in_start_2_elem_8_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[2] = 0;
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2, 8, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}




void _q_unset_8bit_map__in_middle_2_elem_14_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    u8 reg = 0xFF;
    memmap[2] = (u8)(reg << 6U);
    memmap[3] = 0xFF;
    memmap[4] = 0b1111;

    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_unset_8bit_map__in_middle_2_elem_14_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0xFF;
    ref_memmap[2] = (u8)(~(reg << 6U));
    ref_memmap[3] = 0;
    ref_memmap[4] = (u8)(~(0b1111U));

    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_unset_8bit_map__in_start_2_elem_12_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[2] = 0xFF;
    memmap[3] = 0b1111;

    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2, 12, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_unset_8bit_map__in_start_2_elem_12_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[2] = 0;
    ref_memmap[3] = (u8)(~0b1111U);

    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2, 12, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_unset_8bit_map__in_start_0_elem_12_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[0] = 0xFF;
    memmap[1] = 0b1111;

    _q_unset_8bit_map(0, 12, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_unset_8bit_map__in_start_0_elem_12_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[0] = 0;
    ref_memmap[1] = (u8)(~0b1111U);

    _q_unset_8bit_map(0, 12, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_unset_8bit_map__in_middle_2_elem_10_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    u8 reg = 0xFF;
    memmap[2] = (u8)(reg << 6U);
    memmap[3] = 0xFF;

    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_unset_8bit_map__in_middle_2_elem_10_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0xFF;
    ref_memmap[2] = (u8)(~(reg << 6U));
    ref_memmap[3] = 0;

    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10, memmap, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM);
}








/***********************************************************************/

//*****************************************************************

//***********************************************************************


// case p2 < p1

void _q_unset_8bit_map__in_middle_2_elem_2_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[2 << spacer] = 0b11000;
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_unset_8bit_map__in_middle_2_elem_2_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[2 << spacer] = (u8)(~0b11000U);
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}


// case p2 == p1


void _q_unset_8bit_map__in_start_1_elem_2_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[1 << spacer] = 0b11;
    _q_unset_8bit_map(8, 2, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);

}
void _q_unset_8bit_map__in_start_1_elem_2_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[1 << spacer] = (u8)(~0b11U);
    _q_unset_8bit_map(8, 2, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);

}


void _q_unset_8bit_map__in_middle_2_elem_5_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[2 << spacer] = 0b11000000;
    memmap[3 << spacer] = 0b111;
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}


void _q_unset_8bit_map__in_middle_2_elem_5_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11000000;
    ref_memmap[2 << spacer] = (u8)(~reg);
    ref_memmap[3 << spacer] = (u8)(~0b111U);
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_unset_8bit_map__in_middle_2_elem_5_end_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[2 << spacer] = (u8)(0b11111000);
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_unset_8bit_map__in_middle_2_elem_5_end_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11111000;
    ref_memmap[2 << spacer] = (u8)(~reg);
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

// case p2 > p1

void _q_unset_8bit_map__in_start_2_elem_8_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[2 << spacer] = 0xFF;
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2, 8, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_unset_8bit_map__in_start_2_elem_8_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[2 << spacer] = 0;
    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2, 8, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}




void _q_unset_8bit_map__in_middle_2_elem_14_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    u8 reg = 0xFF;
    memmap[2 << spacer] = (u8)(reg << 6U);
    memmap[3 << spacer] = 0xFF;
    memmap[4 << spacer] = 0b1111;

    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_unset_8bit_map__in_middle_2_elem_14_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0xFF;
    ref_memmap[2 << spacer] = (u8)(~(reg << 6U));
    ref_memmap[3 << spacer] = 0;
    ref_memmap[4 << spacer] = (u8)(~(0b1111U));

    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_unset_8bit_map__in_start_2_elem_12_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[2 << spacer] = 0xFF;
    memmap[3 << spacer] = 0b1111;

    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2, 12, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}


void _q_unset_8bit_map__in_start_2_elem_12_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[2 << spacer] = 0;
    ref_memmap[3 << spacer] = (u8)(~0b1111U);

    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2, 12, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_unset_8bit_map__in_start_0_elem_12_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    memmap[0 << spacer] = 0xFF;
    memmap[1 << spacer] = 0b1111;

    _q_unset_8bit_map(0, 12, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}


void _q_unset_8bit_map__in_start_0_elem_12_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[0 << spacer] = 0;
    ref_memmap[1 << spacer] = (u8)(~0b1111U);

    _q_unset_8bit_map(0, 12, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}


void _q_unset_8bit_map__in_middle_2_elem_10_unset_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    u8 reg = 0xFF;
    memmap[2 << spacer] = (u8)(reg << 6U);
    memmap[3 << spacer] = 0xFF;

    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}

void _q_unset_8bit_map__in_middle_2_elem_10_set_1_spacer_test(void)
{
    const u8 spacer = 1;
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM << spacer];
    CLEAR_ARRAY(ref_memmap);
    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM << spacer; i++) {
        memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0xFF;
    ref_memmap[2 << spacer] = (u8)(~(reg << 6U));
    ref_memmap[3 << spacer] = 0;

    _q_unset_8bit_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10, memmap, spacer);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, memmap, _Q_MMAP_GR_BLOCKNUM << spacer);
}








/***********************************************************************/

//*****************************************************************

//***********************************************************************

/*

// case p2 < p1

void _q_mem_res_set_map__in_middle_2_elem_2_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];


    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2] = 0b11000;
    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_set_map__in_middle_2_elem_2_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];


    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    mcb.memmap[2] = ~0b11000;
    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}


// case p2 == p1







void _q_mem_res_set_map__in_start_1_elem_2_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];


    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[1] = 0b11;
    _q_mem_res_set_map(8, 2);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);

}
void _q_mem_res_set_map__in_start_1_elem_2_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];


    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    mcb.memmap[1] = ~0b11;
    _q_mem_res_set_map(8, 2);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);

}


void _q_mem_res_set_map__in_middle_2_elem_5_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2] = 0b11000000;
    ref_memmap[3] = 0b111;
    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_mem_res_set_map__in_middle_2_elem_5_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11000000;
    mcb.memmap[2] = ~reg;
    mcb.memmap[3] = ~0b111;
    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_set_map__in_middle_2_elem_5_end_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2] = 0b11111000;
    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_set_map__in_middle_2_elem_5_end_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11111000;
    mcb.memmap[2] = ~reg;
    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

// case p2 > p1

void _q_mem_res_set_map__in_start_2_elem_8_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2] = 0xFF;
    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2, 8);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_set_map__in_start_2_elem_8_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    mcb.memmap[2] = 0;
    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2, 8);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}




void _q_mem_res_set_map__in_middle_2_elem_14_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    u8 reg = 0xFF;
    ref_memmap[2] = reg << 6;
    ref_memmap[3] = 0xFF;
    ref_memmap[4] = 0b1111;

    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_set_map__in_middle_2_elem_14_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0xFF;
    mcb.memmap[2] = ~(reg << 6);
    mcb.memmap[3] = 0;
    mcb.memmap[4] = ~(0b1111);

    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_set_map__in_start_2_elem_12_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[2] = 0xFF;
    ref_memmap[3] = 0b1111;

    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2, 12);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_mem_res_set_map__in_start_2_elem_12_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    mcb.memmap[2] = 0;
    mcb.memmap[3] = ~0b1111;

    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2, 12);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_set_map__in_start_0_elem_12_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    ref_memmap[0] = 0xFF;
    ref_memmap[1] = 0b1111;

    _q_mem_res_set_map(0, 12);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_mem_res_set_map__in_start_0_elem_12_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    mcb.memmap[0] = 0;
    mcb.memmap[1] = ~0b1111;

    _q_mem_res_set_map(0, 12);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_mem_res_set_map__in_middle_2_elem_10_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    u8 reg = 0xFF;
    ref_memmap[2] = reg << 6;
    ref_memmap[3] = 0xFF;

    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_set_map__in_middle_2_elem_10_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0xFF;
    mcb.memmap[2] = ~(reg << 6);
    mcb.memmap[3] = 0;

    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_set_map__num_0_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }

    _q_mem_res_set_map(_Q_MMAP_CH_IN_BL * 2 + 6, 0);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

/***********************************************************************/

/*
// case p2 < p1

void _q_mem_res_unset_map__in_middle_2_elem_2_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];


    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    mcb.memmap[2] = 0b11000;
    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_unset_map__in_middle_2_elem_2_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];


    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[2] = ~0b11000;
    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2 + 3, 2);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}


// case p2 == p1


void _q_mem_res_unset_map__in_start_1_elem_2_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];


    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    mcb.memmap[1] = 0b11;
    _q_mem_res_unset_map(8, 2);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);

}
void _q_mem_res_unset_map__in_start_1_elem_2_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];


    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[1] = ~0b11;
    _q_mem_res_unset_map(8, 2);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);

}


void _q_mem_res_unset_map__in_middle_2_elem_5_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    mcb.memmap[2] = 0b11000000;
    mcb.memmap[3] = 0b111;
    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_mem_res_unset_map__in_middle_2_elem_5_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11000000;
    ref_memmap[2] = ~reg;
    ref_memmap[3] = ~0b111;
    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2 + 6, 5);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_unset_map__in_middle_2_elem_5_end_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    mcb.memmap[2] = 0b11111000;
    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_unset_map__in_middle_2_elem_5_end_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0b11111000;
    ref_memmap[2] = ~reg;
    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2 + 3, 5);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

// case p2 > p1

void _q_mem_res_unset_map__in_start_2_elem_8_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    mcb.memmap[2] = 0xFF;
    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2, 8);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_unset_map__in_start_2_elem_8_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[2] = 0;
    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2, 8);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}




void _q_mem_res_unset_map__in_middle_2_elem_14_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    u8 reg = 0xFF;
    mcb.memmap[2] = reg << 6;
    mcb.memmap[3] = 0xFF;
    mcb.memmap[4] = 0b1111;

    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_unset_map__in_middle_2_elem_14_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0xFF;
    ref_memmap[2] = ~(reg << 6);
    ref_memmap[3] = 0;
    ref_memmap[4] = ~(0b1111);

    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2 + 6, 14);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_unset_map__in_start_2_elem_12_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    mcb.memmap[2] = 0xFF;
    mcb.memmap[3] = 0b1111;

    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2, 12);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_mem_res_unset_map__in_start_2_elem_12_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[2] = 0;
    ref_memmap[3] = ~0b1111;

    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2, 12);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_unset_map__in_start_0_elem_12_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    mcb.memmap[0] = 0xFF;
    mcb.memmap[1] = 0b1111;

    _q_mem_res_unset_map(0, 12);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_mem_res_unset_map__in_start_0_elem_12_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    ref_memmap[0] = 0;
    ref_memmap[1] = ~0b1111;

    _q_mem_res_unset_map(0, 12);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}


void _q_mem_res_unset_map__in_middle_2_elem_10_unset_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0;
        ref_memmap[i] = 0;
    }
    u8 reg = 0xFF;
    mcb.memmap[2] = reg << 6;
    mcb.memmap[3] = 0xFF;

    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}

void _q_mem_res_unset_map__in_middle_2_elem_10_set_test(void)
{
    u8 ref_memmap[_Q_MMAP_GR_BLOCKNUM];

    for (uint i = 0; i < _Q_MMAP_GR_BLOCKNUM; i++) {
        mcb.memmap[i] = 0xFF;
        ref_memmap[i] = 0xFF;
    }
    u8 reg = 0xFF;
    ref_memmap[2] = ~(reg << 6);
    ref_memmap[3] = 0;

    _q_mem_res_unset_map(_Q_MMAP_CH_IN_BL * 2 + 6, 10);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
}
*/

void _q_mem_res_addr2map_idx__test(void)
{
    uint ref_idx = 11;
    uintptr_t addr = ram_origin + _Q_MMAP_CHUNKLEN * ref_idx;


    uint idx = _q_mem_res_addr2map_idx(addr);

    TEST_ASSERT_EQUAL_UINT(ref_idx, idx);
}


void _q_mem_res_len2blocknum__test(void)
{

    size_t len = 12341;
    uint ref_num = (uint)(len / _Q_MMAP_CHUNKLEN);
    if (len % _Q_MMAP_CHUNKLEN) {
        ref_num++;
    }
    uint num = _q_mem_res_len2blocknum(len);

    TEST_ASSERT_EQUAL_UINT(ref_num, num);
}


void _q_mem_res_len2blocknum_even_test(void)
{

    size_t len = _Q_MMAP_CHUNKLEN * 3;
    uint ref_num = (uint)(len / _Q_MMAP_CHUNKLEN);
    if (len % _Q_MMAP_CHUNKLEN) {
        ref_num++;
    }
    uint num = _q_mem_res_len2blocknum(len);

    TEST_ASSERT_EQUAL_UINT(ref_num, num);
}

void _q_mem_res_map_idx2addr__test(void)
{

    uint idx = 11;
    size_t ref_addr = ram_origin + idx * _Q_MMAP_CHUNKLEN;

    size_t addr = _q_mem_res_map_idx2addr(idx);
    TEST_ASSERT_EQUAL_UINT(ref_addr, addr);
}


void _q_mem_mb_get_free_status__positive_test(void)
{
    uint mb_size = 4564;
    mbcb_t mb;
    mb.size = mb_size;
    mch_head_t head;
    head.free = 1;
    head.len = mb_size - (uint)sizeof(head);
    mb.head = &head;
    uint ref_status = 1;

    uint status = _q_mem_mb_get_free_status(&mb);
    TEST_ASSERT_EQUAL_UINT(ref_status, status);
}

void _q_mem_mb_get_free_status__negative_test(void)
{
    mbcb_t mb;
    mch_head_t head;
    head.free = 0;

    mb.head = &head;
    int ref_status = 0;

    uint status = _q_mem_mb_get_free_status(&mb);
    TEST_ASSERT_EQUAL_INT(ref_status, status);
}


void __MPU_DEF_SIZE_128_test(void)
{
    u32 ref_size_enc = 0b0110 << MPU_RASR_SIZE_Pos;
    u32 size_enc = _MPU_DEF_SIZE(128);
    TEST_ASSERT_EQUAL_UINT32(ref_size_enc, size_enc);
}

void __MPU_DEF_SIZE_32_test(void)
{
    u32 ref_size_enc = 0b0100 << MPU_RASR_SIZE_Pos;
    u32 size_enc = _MPU_DEF_SIZE(32);
    TEST_ASSERT_EQUAL_UINT32(ref_size_enc, size_enc);
}

void __MPU_DEF_SIZE_64k_test(void)
{
    u32 ref_size_enc = 0b1111 << MPU_RASR_SIZE_Pos;
    u32 size_enc = _MPU_DEF_SIZE(64* 1024);
    TEST_ASSERT_EQUAL_UINT32(ref_size_enc, size_enc);
}


void q_mpu_encode_pref_test(void)
{
    mpu_rp_t ref_pref, pref;
    CLEAR_OBJ(&ref_pref);
    CLEAR_OBJ(&pref);

    ref_pref.base_addr = 0b10101010101010101010101010110010;
    ref_pref.attr_size = 0b10101000001001010111000011101;

    q_mpu_encode_pref(&pref,
                      0b0010,
                      0b10101010101010101010101010100000,
                       32*1024,
                      _MPU_EXEC_NEVER,
                      _MPU_AP_P_RO_UP_NA,
                      _MPU_IN_SRAM_T,
                      0b10101110 );

    TEST_ASSERT_EQUAL_MEMORY(&ref_pref, &pref, sizeof(pref));

}


int main(void)
{
    UNITY_BEGIN();

    RUN_TEST(_q_mem_start_mb_idx_non_zero_test);
    RUN_TEST(_q_mem_start_mb_idx_zero_test);

    RUN_TEST(_q_mem_end_mb_idx_zero_start_test);
    RUN_TEST(_q_mem_end_mb_idx_non_zero_start_test);

    RUN_TEST(_q_mem_mb_len2blocknum_smaller_mb_test);
    RUN_TEST(_q_mem_mb_len2blocknum_zero_mb_test);
    RUN_TEST(_q_mem_mb_len2blocknum_len_equal_mb_test);
    RUN_TEST(_q_mem_mb_len2blocknum_equal_mb_test);
    RUN_TEST(_q_mem_mb_len2blocknum_larger_mb_test);
    RUN_TEST(_q_mem_mb_len2blocknum_need_3_mb_test);

    RUN_TEST(_q_mem_mb_set_free_first_1_test);
    RUN_TEST( _q_mem_mb_set_free_second_2__filled_test);

    RUN_TEST(_q_mem_mb_set_used_first_1_test);
    RUN_TEST(_q_mem_mb_set_used_init10_2_set_test);



    //****************************************************

    RUN_TEST( _q_set_8bit_map__in_middle_2_elem_2_unset_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_2_set_test);


    RUN_TEST(_q_set_8bit_map__in_start_1_elem_2_unset_test);
    RUN_TEST(_q_set_8bit_map__in_start_1_elem_2_set_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_5_unset_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_5_set_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_5_end_unset_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_5_end_set_test);


    RUN_TEST(_q_set_8bit_map__in_start_2_elem_8_unset_test);
    RUN_TEST( _q_set_8bit_map__in_start_2_elem_8_set_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_14_unset_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_14_set_test);
    RUN_TEST(_q_set_8bit_map__in_start_2_elem_12_unset_test);
    RUN_TEST(_q_set_8bit_map__in_start_2_elem_12_set_test);
    RUN_TEST(_q_set_8bit_map__in_start_0_elem_12_unset_test);
    RUN_TEST(_q_set_8bit_map__in_start_0_elem_12_set_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_10_unset_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_10_set_test);
    RUN_TEST(_q_set_8bit_map__num_0_set_test);


    //********************************************************
    //****************************************************

    RUN_TEST( _q_set_8bit_map__in_middle_2_elem_2_unset_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_2_set_1_spacer_test);


    RUN_TEST(_q_set_8bit_map__in_start_1_elem_2_unset_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_start_1_elem_2_set_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_5_unset_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_5_set_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_5_end_unset_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_5_end_set_1_spacer_test);


    RUN_TEST(_q_set_8bit_map__in_start_2_elem_8_unset_1_spacer_test);
    RUN_TEST( _q_set_8bit_map__in_start_2_elem_8_set_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_14_unset_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_14_set_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_start_2_elem_12_unset_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_start_2_elem_12_set_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_start_0_elem_12_unset_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_start_0_elem_12_set_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_10_unset_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_10_set_1_spacer_test);
    RUN_TEST(_q_set_8bit_map__num_0_set_1_spacer_test);

    //****************************************************

    RUN_TEST( _q_set_8bit_map__in_middle_2_elem_2_unset_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_2_set_2_spacer_test);


    RUN_TEST(_q_set_8bit_map__in_start_1_elem_2_unset_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_start_1_elem_2_set_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_5_unset_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_5_set_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_5_end_unset_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_5_end_set_2_spacer_test);


    RUN_TEST(_q_set_8bit_map__in_start_2_elem_8_unset_2_spacer_test);
    RUN_TEST( _q_set_8bit_map__in_start_2_elem_8_set_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_14_unset_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_14_set_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_start_2_elem_12_unset_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_start_2_elem_12_set_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_start_0_elem_12_unset_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_start_0_elem_12_set_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_10_unset_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__in_middle_2_elem_10_set_2_spacer_test);
    RUN_TEST(_q_set_8bit_map__num_0_set_2_spacer_test);
    //********************************************************

    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_2_unset_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_2_set_test);


    RUN_TEST(_q_unset_8bit_map__in_start_1_elem_2_unset_test);
    RUN_TEST(_q_unset_8bit_map__in_start_1_elem_2_set_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_5_unset_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_5_set_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_5_end_unset_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_5_end_set_test);


    RUN_TEST(_q_unset_8bit_map__in_start_2_elem_8_unset_test);
    RUN_TEST(_q_unset_8bit_map__in_start_2_elem_8_set_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_14_unset_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_14_set_test);
    RUN_TEST(_q_unset_8bit_map__in_start_2_elem_12_unset_test);
    RUN_TEST(_q_unset_8bit_map__in_start_2_elem_12_set_test);
    RUN_TEST(_q_unset_8bit_map__in_start_0_elem_12_unset_test);
    RUN_TEST(_q_unset_8bit_map__in_start_0_elem_12_set_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_10_unset_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_10_set_test);



    //****************************************************
    //********************************************************

    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_2_unset_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_2_set_1_spacer_test);


    RUN_TEST(_q_unset_8bit_map__in_start_1_elem_2_unset_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_start_1_elem_2_set_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_5_unset_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_5_set_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_5_end_unset_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_5_end_set_1_spacer_test);


    RUN_TEST(_q_unset_8bit_map__in_start_2_elem_8_unset_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_start_2_elem_8_set_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_14_unset_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_14_set_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_start_2_elem_12_unset_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_start_2_elem_12_set_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_start_0_elem_12_unset_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_start_0_elem_12_set_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_10_unset_1_spacer_test);
    RUN_TEST(_q_unset_8bit_map__in_middle_2_elem_10_set_1_spacer_test);



    //****************************************************
/*

    RUN_TEST( _q_mem_res_set_map__in_middle_2_elem_2_unset_test);
    RUN_TEST(_q_mem_res_set_map__in_middle_2_elem_2_set_test);


    RUN_TEST(_q_mem_res_set_map__in_start_1_elem_2_unset_test);
    RUN_TEST(_q_mem_res_set_map__in_start_1_elem_2_set_test);
    RUN_TEST(_q_mem_res_set_map__in_middle_2_elem_5_unset_test);
    RUN_TEST(_q_mem_res_set_map__in_middle_2_elem_5_set_test);
    RUN_TEST(_q_mem_res_set_map__in_middle_2_elem_5_end_unset_test);
    RUN_TEST(_q_mem_res_set_map__in_middle_2_elem_5_end_set_test);


    RUN_TEST(_q_mem_res_set_map__in_start_2_elem_8_unset_test);
    RUN_TEST( _q_mem_res_set_map__in_start_2_elem_8_set_test);
    RUN_TEST(_q_mem_res_set_map__in_middle_2_elem_14_unset_test);
    RUN_TEST(_q_mem_res_set_map__in_middle_2_elem_14_set_test);
    RUN_TEST(_q_mem_res_set_map__in_start_2_elem_12_unset_test);
    RUN_TEST(_q_mem_res_set_map__in_start_2_elem_12_set_test);
    RUN_TEST(_q_mem_res_set_map__in_start_0_elem_12_unset_test);
    RUN_TEST(_q_mem_res_set_map__in_start_0_elem_12_set_test);
    RUN_TEST(_q_mem_res_set_map__in_middle_2_elem_10_unset_test);
    RUN_TEST(_q_mem_res_set_map__in_middle_2_elem_10_set_test);
    RUN_TEST(_q_mem_res_set_map__num_0_set_test);
*/
/*****************************************************/
/*
    RUN_TEST(_q_mem_res_unset_map__in_middle_2_elem_2_unset_test);
    RUN_TEST(_q_mem_res_unset_map__in_middle_2_elem_2_set_test);


    RUN_TEST(_q_mem_res_unset_map__in_start_1_elem_2_unset_test);
    RUN_TEST(_q_mem_res_unset_map__in_start_1_elem_2_set_test);
    RUN_TEST(_q_mem_res_unset_map__in_middle_2_elem_5_unset_test);
    RUN_TEST(_q_mem_res_unset_map__in_middle_2_elem_5_set_test);
    RUN_TEST(_q_mem_res_unset_map__in_middle_2_elem_5_end_unset_test);
    RUN_TEST(_q_mem_res_unset_map__in_middle_2_elem_5_end_set_test);


    RUN_TEST(_q_mem_res_unset_map__in_start_2_elem_8_unset_test);
    RUN_TEST(_q_mem_res_unset_map__in_start_2_elem_8_set_test);
    RUN_TEST(_q_mem_res_unset_map__in_middle_2_elem_14_unset_test);
    RUN_TEST(_q_mem_res_unset_map__in_middle_2_elem_14_set_test);
    RUN_TEST(_q_mem_res_unset_map__in_start_2_elem_12_unset_test);
    RUN_TEST(_q_mem_res_unset_map__in_start_2_elem_12_set_test);
    RUN_TEST(_q_mem_res_unset_map__in_start_0_elem_12_unset_test);
    RUN_TEST(_q_mem_res_unset_map__in_start_0_elem_12_set_test);
    RUN_TEST(_q_mem_res_unset_map__in_middle_2_elem_10_unset_test);
    RUN_TEST(_q_mem_res_unset_map__in_middle_2_elem_10_set_test);
*/
    /*****************************************************/

    RUN_TEST(_q_mem_res_addr2map_idx__test);
    RUN_TEST(_q_mem_res_len2blocknum__test);
    RUN_TEST(_q_mem_res_len2blocknum_even_test);
    RUN_TEST(_q_mem_res_map_idx2addr__test);
    RUN_TEST(_q_mem_mb_get_free_status__positive_test);
    RUN_TEST(_q_mem_mb_get_free_status__negative_test);

    RUN_TEST(__MPU_DEF_SIZE_32_test);
    RUN_TEST(__MPU_DEF_SIZE_128_test);
    RUN_TEST(__MPU_DEF_SIZE_64k_test);
    RUN_TEST(q_mpu_encode_pref_test);

    return UNITY_END();

}
