#ifndef DYN_MEM_INIT_TESTS_H
#include <stddef.h>
#include "qos_memory.h"

extern char* ram;
extern char rawram[_RAM_SIZE * 2];
extern size_t _estack;

extern size_t ram_origin;
extern size_t ram_size;
extern MemCB_t mcb;


extern size_t _p_sdata;
extern  size_t _p_sdata_delta;
extern  size_t _Priv_Data_Size;
extern  size_t _Priv_Bss_Size;

extern size_t _sdata;
extern  size_t _sdata_delta;
extern  size_t _Data_Size;
extern  size_t _Bss_Size;

extern  size_t _Min_Stack_Size;
extern  size_t _Min_Heap_Size;

 void mcb_reset(void);

 extern mpu_rp_t thread_mpu_conf[];
 extern  q_lnim_t thread_mb_list;
 extern sbcb_t thread_stack[];


#define DYN_MEM_INIT_TESTS_H

#endif // DYN_MEM_INIT_TESTS_H
