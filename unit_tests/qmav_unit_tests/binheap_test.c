#include "stdio.h"
#include <math.h>
#include "unity.h"
#include "misc/qos_bin_heap.h"

//#include "binheap_test_libs.h"

#define NODE_NUM 20

static bheap_t bin_heap;
static bh_node_t nodes[NODE_NUM];
//int keys[NODE_NUM];

static int _get_keys_len;


bh_node_t* get_parent_node(uint level, uint row, bh_node_t* node) {

    uint num = (uint)powl(2, level - 1) + row / 2U;
    return node - num;
}

void fill_node_array(u32 keys[], uint num, bh_node_t nodes[]) {

    uint level = 1, row = 1;

    nodes[0].key = keys[0];
    for (uint i = 1; i < num; i++) {
        bh_node_t* node = &nodes[i];
        bh_node_t* parent = get_parent_node(level, row, node);
        if (row % 2) {
            parent->lchild = node;
        }
        else {
            parent->rchild = node;
        }
        node->parent = parent;
        node->key = keys[i];
        if (row == powl(2, level)) {
            level++;
            row = 1;
        }
        else {
            row++;
        }
    }
}

void make_heap(uint keys[], uint num) {

    if (num) {
        fill_node_array(keys, num, nodes);
        bin_heap.root = &nodes[0];
        bin_heap.last = &nodes[num - 1];
    }
    else {
        bin_heap.last = NULL;
        bin_heap.root = NULL;
    }
}

void node_observe(bh_node_t* node, uint level, uint row, uint keys[]) {

    uint idx = (uint)powl(2U, level) - 1U + row;
    keys[idx] = node->key;
    _get_keys_len++;
    if (node->lchild) {
        node_observe(node->lchild, level + 1, row * 2, keys);
    }
    if (node->rchild) {
        node_observe(node->rchild, level + 1 , row * 2 + 1, keys);
    }
}

int get_keys(uint keys[]) {

    _get_keys_len = 0;
    if (bin_heap.root) {
        node_observe(bin_heap.root, 0, 0, keys);
    }
    return _get_keys_len;
}


void setUp(void) {
    // set stuff up here

    char *p = (char*)nodes;
    for (size_t i = 0; i < sizeof(*nodes) * NODE_NUM; i++) {
        *(p++) = 0;
    }
    p = (char*)&bin_heap;
    for (size_t i = 0; i < sizeof(bin_heap); i++) {
        *(p++) = 0;
    }
//    p = (char*)&keys;
//    for (size_t i = 0; i < sizeof(*keys) * NODE_NUM; i++) {
//        *(p++) = 0;
//    }
}


void tearDown(void) {
    // clean stuff up here
}


void test_get_keys_11(void)
{
    const uint len = 11;
    uint ref_keys[] = {1, 3, 5, 8, 9, 9 ,9, 12, 14, 15, 16};
    uint keys[len];

    make_heap(ref_keys, len);
    int len_heap = get_keys(keys);

    TEST_ASSERT_EQUAL_INT(len, len_heap);
    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, keys, len);

}

void test_get_keys_1(void)
{
    const uint len = 1;
    uint ref_keys[] = {12};
    uint keys[len];

    make_heap(ref_keys, len);
    int len_heap = get_keys(keys);

    TEST_ASSERT_EQUAL_INT(len, len_heap);
    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, keys, len);

}

void test_get_keys_2(void)
{
    const uint len = 2;
    uint ref_keys[] = {12, 56};
    uint keys[len];

    make_heap(ref_keys, len);
    int len_heap = get_keys(keys);

    TEST_ASSERT_EQUAL_INT(len, len_heap);
    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, keys, len);

}

void test_get_keys_3(void)
{
    const uint len = 3;
    uint ref_keys[] = {12, 45, 12};
    uint keys[len];

    make_heap(ref_keys, len);
    int len_heap = get_keys(keys);

    TEST_ASSERT_EQUAL_INT(len, len_heap);
    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, keys, len);

}

void test_get_keys_4(void)
{
    const uint len = 4;
    uint ref_keys[] = {12, 23, 56, 57};
    uint keys[len];

    make_heap(ref_keys, len);
    int len_heap = get_keys(keys);

    TEST_ASSERT_EQUAL_INT(len, len_heap);
    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, keys, len);

}

void test_get_keys_0(void)
{
    const uint len = 0;
    uint ref_keys[] = {12, 23, 56, 57};
    uint keys[len];

    make_heap(ref_keys, len);
    int len_heap = get_keys(keys);

    TEST_ASSERT_EQUAL_INT(len, len_heap);

}


void test_append_first_node(void)
{

    const uint len = 1;
    uint start_keys[] = {};
    uint end_keys[len];
    uint ref_keys[] = {1};
    nodes[len - 1].key = 1;

    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len - 1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_2nd_min_node(void)
{
    const uint len = 2;
    uint start_keys[] = {2};
    uint ref_keys[] = {1,2};
    nodes[len-1].key = 1;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_2nd_max_node(void)
{
    const uint len = 2;
    uint start_keys[] = {2};
    uint ref_keys[] = {2, 4};
    nodes[len-1].key = 4;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_3rd_min_node(void)
{
    const uint len = 3;
    uint start_keys[] = {4, 6};
    uint ref_keys[] = {2, 6, 4};
    nodes[len-1].key = 2;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_3rd_max_node(void)
{
    const uint len = 3;
    uint start_keys[] = {4, 6};
    uint ref_keys[] = {4, 6, 8};
    nodes[len-1].key = 8;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_4th_min_node(void)
{
    const uint len = 4;
    uint start_keys[] = {4, 6, 15};
    uint ref_keys[] = {2, 4, 15, 6};
    nodes[len-1].key = 2;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_4th_max_node(void)
{
    const uint len = 4;
    uint start_keys[] = {4, 6, 8};
    uint ref_keys[] = {4, 6, 8, 15};
    nodes[len-1].key = 15;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_4th_mid_node(void)
{
    const uint len = 4;
    uint start_keys[] = {4, 6, 8};
    uint ref_keys[] = {4, 5, 8, 6};
    nodes[len-1].key = 5;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_5th_min_node(void)
{
    const uint len = 5;
    uint start_keys[] = {4, 6, 8, 15};
    uint ref_keys[] = {2, 4, 8, 15, 6};
    nodes[len-1].key = 2;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_5th_max_node(void)
{
    const uint len = 5;
    uint start_keys[] = {4, 6, 8, 15};
    uint ref_keys[] = {4, 6, 8, 15, 19};
    nodes[len-1].key = 19;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_5th_mid_node(void)
{
    const uint len = 5;
    uint start_keys[] = {4, 6, 8, 15};
    uint ref_keys[] = {4, 5,  8, 15, 6};
    nodes[len-1].key = 5;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}


void test_append_6th_min_node(void)
{
    const uint len = 6;
    uint start_keys[] = {4, 6, 8, 15, 19};
    uint ref_keys[] = {2, 6, 4, 15, 19, 8};
    nodes[len-1].key = 2;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_6th_max_node(void)
{
    const uint len = 6;
    uint start_keys[] = {4, 6, 8, 15, 19};
    uint ref_keys[] = {4, 6, 8, 15, 19, 31};
    nodes[len-1].key = 31;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_6th_mid_node(void)
{
    const uint len = 6;
    uint start_keys[] = {4, 6, 8, 15, 19};
    uint ref_keys[] = {4, 6,  7, 15, 19, 8};
    nodes[len-1].key = 7;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}


void test_append_8th_min_node(void)
{
    const uint len = 8;
    uint start_keys[] = {4, 6, 8, 15, 19, 31, 42};
    uint ref_keys[] = {2, 4, 8, 6, 19, 31, 42, 15};
    nodes[len-1].key = 2;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_8th_max_node(void)
{
    const uint len = 8;
    uint start_keys[] = {4, 6, 8, 15, 19, 31, 42};
    uint ref_keys[] = {4, 6, 8, 15, 19, 31, 42, 44};
    nodes[len-1].key = 44;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_append_8th_mid_node(void)
{
    const uint len = 8;
    uint start_keys[] = {4, 6, 8, 15, 19, 31, 42};
    uint ref_keys[] = {4, 5, 8, 6, 19, 31, 42, 15};
    nodes[len-1].key = 5;

    uint end_keys[len];
    make_heap(start_keys, len - 1);
    _q_bh_min_add(&nodes[len-1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}


void test_heap_len_1_remove_1st_node(void)
{
    const uint len = 0;
    uint start_keys[] = {4};
//    uint ref_keys[] = {};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[0], &bin_heap);
    int len_heap = get_keys(end_keys);

//    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
    TEST_ASSERT_EQUAL_PTR(NULL, bin_heap.root);
    TEST_ASSERT_EQUAL_PTR(NULL, bin_heap.last);
}

void test_heap_len_2_remove_1st_node(void)
{
    const uint len = 1;
    uint start_keys[] = {4, 6};
    uint ref_keys[] = {6};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[0], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);

}

void test_heap_len_2_remove_2nd_node(void)
{
    const uint len = 1;
    uint start_keys[] = {4, 6};
    uint ref_keys[] = {4};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_heap_len_3_remove_1st_node(void)
{
    const uint len = 2;
    uint start_keys[] = {4, 6, 8};
    uint ref_keys[] = {6, 8};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[0], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_heap_len_3_remove_2nd_node(void)
{
    const uint len = 2;
    uint start_keys[] = {4, 6, 8};
    uint ref_keys[] = {4, 8};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_heap_len_3_remove_3rd_node(void)
{
    const uint len = 2;
    uint start_keys[] = {4, 6, 8};
    uint ref_keys[] = {4, 6};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[2], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}
void test_heap_len_4_remove_1st_node(void)
{
    const uint len = 3;
    uint start_keys[] = {4, 6, 8, 15};
    uint ref_keys[] = {6, 15, 8};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[0], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_heap_len_4_remove_2nd_node(void)
{
    const uint len = 3;
    uint start_keys[] = {4, 6, 8, 15};
    uint ref_keys[] = {4, 15, 8};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_heap_len_4_remove_3rd_node(void)
{
    const uint len = 3;
    uint start_keys[] = {4, 6, 8, 15};
    uint ref_keys[] = {4, 6, 15};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[2], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_heap_len_4_remove_4th_node(void)
{
    const uint len = 3;
    uint start_keys[] = {4, 6, 8, 15};
    uint ref_keys[] = {4, 6, 8};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[3], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_heap_len_10_remove_1st_node(void)
{
    const uint len = 9;
    uint start_keys[] = {4, 8, 6, 19, 15, 31, 42, 81, 84, 90};
    uint ref_keys[] = {6, 8, 31, 19, 15, 90, 42, 81, 84, 90};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[0], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_heap_len_10_remove_1st_node_eq(void)
{
    const uint len = 9;
    uint start_keys[] = {4, 8, 6, 19, 15, 42, 42, 81, 84, 90};
    uint ref_keys[] = {6, 8, 42, 19, 15, 90, 42, 81, 84, 90};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[0], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_heap_len_10_remove_prelast_node_eq(void)
{
    const uint len = 9;
    uint start_keys[] = {4, 8, 6, 19, 15, 42, 42, 81, 84, 90};
    uint ref_keys[] = {4, 8, 6, 19, 15, 42, 42, 81, 90};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[8], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_heap_len_10_remove_last_node_eq(void)
{
    const uint len = 9;
    uint start_keys[] = {4, 8, 6, 19, 15, 42, 42, 81, 84, 90};
    uint ref_keys[] = {4, 8, 6, 19, 15, 42, 42, 81, 84};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[9], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

void test_heap_len_10_remove_2rd_node_eq(void)
{
    const uint len = 9;
    uint start_keys[] = {4, 8, 6, 19, 15, 42, 42, 81, 84, 90};
    uint ref_keys[] = {4, 15, 6, 19, 90, 42, 42, 81, 84};

    uint end_keys[len];
    make_heap(start_keys, len + 1);
    _q_bh_remove(&nodes[1], &bin_heap);
    int len_heap = get_keys(end_keys);

    TEST_ASSERT_EQUAL_INT_ARRAY(ref_keys, end_keys, len);
    TEST_ASSERT_EQUAL_INT(len, len_heap);
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_get_keys_1);
    RUN_TEST(test_get_keys_2);
    RUN_TEST(test_get_keys_3);
    RUN_TEST(test_get_keys_4);
    RUN_TEST(test_get_keys_11);
    RUN_TEST(test_get_keys_0);
    RUN_TEST(test_append_first_node);
    RUN_TEST(test_append_2nd_min_node);
    RUN_TEST(test_append_2nd_max_node);
    RUN_TEST(test_append_3rd_min_node);
    RUN_TEST(test_append_3rd_max_node);
    RUN_TEST(test_append_4th_min_node);
    RUN_TEST(test_append_4th_max_node);
    RUN_TEST(test_append_4th_mid_node);
    RUN_TEST(test_append_5th_min_node);
    RUN_TEST(test_append_5th_max_node);
    RUN_TEST(test_append_5th_mid_node);
    RUN_TEST(test_append_6th_min_node);
    RUN_TEST(test_append_6th_max_node);
    RUN_TEST(test_append_6th_mid_node);
    RUN_TEST(test_append_8th_min_node);
    RUN_TEST(test_append_8th_max_node);
    RUN_TEST(test_append_8th_mid_node);
    RUN_TEST(test_heap_len_1_remove_1st_node);
    RUN_TEST(test_heap_len_2_remove_1st_node);
    RUN_TEST(test_heap_len_2_remove_2nd_node);
    RUN_TEST(test_heap_len_3_remove_1st_node);
    RUN_TEST(test_heap_len_3_remove_2nd_node);
    RUN_TEST(test_heap_len_3_remove_3rd_node);
    RUN_TEST(test_heap_len_4_remove_1st_node);
    RUN_TEST(test_heap_len_4_remove_2nd_node);
    RUN_TEST(test_heap_len_4_remove_3rd_node);
    RUN_TEST(test_heap_len_4_remove_4th_node);
    RUN_TEST(test_heap_len_10_remove_1st_node);
    RUN_TEST(test_heap_len_10_remove_1st_node_eq);
    RUN_TEST(test_heap_len_10_remove_prelast_node_eq);
    RUN_TEST(test_heap_len_10_remove_last_node_eq);
    RUN_TEST(test_heap_len_10_remove_2rd_node_eq);

    return UNITY_END();
}
