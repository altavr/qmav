#include "stdio.h"
#include <math.h>
#include "unity.h"
#include "fff.h"
#include "qos_memory.h"
#include "qos_ln_list.h"
#include "ut_utils.h"
#include "dyn_mem_init_tests.h"


DEFINE_FFF_GLOBALS

FAKE_VOID_FUNC(_q_bh_min_init, bheap_t*)
FAKE_VOID_FUNC(_q_bh_node_init, bh_node_t*, u32)
FAKE_VOID_FUNC(_q_bh_min_add, bh_node_t*, bheap_t*)
FAKE_VOID_FUNC(_qos_close_item_lns, q_lnim_t*)
FAKE_VOID_FUNC(_qos_ins_to_ln_after_item, q_lnim_t*, q_lnim_t*)


FAKE_VOID_FUNC(_q_mem_mb_set_free, uint, uint)
FAKE_VOID_FUNC(_q_mem_res_set_map, uint, uint)
FAKE_VALUE_FUNC(uint, _q_mem_res_addr2map_idx, size_t)

FAKE_VALUE_FUNC(uint, _q_mem_mb_len2blocknum, size_t)
FAKE_VALUE_FUNC(uint, _q_mem_end_mb_idx)
FAKE_VALUE_FUNC(uint, _q_mem_start_mb_idx)



FAKE_VALUE_FUNC(bh_node_t*, _bh_next_node, bh_node_t*)
//FAKE_VALUE_FUNC()
//FAKE_VALUE_FUNC()
//FAKE_VALUE_FUNC()
//FAKE_VALUE_FUNC()



FAKE_VOID_FUNC(_q_bh_remove, bh_node_t*, bheap_t* )
FAKE_VOID_FUNC(_qos_rem_item_from_ln_list, q_lnim_t *)
//FAKE_VOID_FUNC()
//FAKE_VOID_FUNC()



#define FFF_FAKE_LIST(FAKE) \
    FAKE(_q_mem_mb_set_free) \
    FAKE(_q_mem_res_set_map) \
    FAKE(_q_mem_res_addr2map_idx) \
    FAKE(_q_bh_min_init) \
    FAKE(_q_bh_node_init) \
    FAKE(_q_bh_min_add) \
    FAKE(_qos_close_item_lns) \
    FAKE(_qos_ins_to_ln_after_item) \
    FAKE(_q_mem_mb_len2blocknum) \
    FAKE(_q_mem_end_mb_idx) \
    FAKE(_q_mem_start_mb_idx) \
    FAKE(_bh_next_node) \
    FAKE(_q_bh_remove) \
    FAKE(_qos_rem_item_from_ln_list)

#define FF_STAT(func) printf(#func " stat:\n");\
    printf("call count: %d\n", func##_fake.call_count)

void setUp(void)
{
    mcb_reset();
    FFF_FAKE_LIST(RESET_FAKE);

}

void tearDown(void) {
    // clean stuff up here
}


void _qos_close_item_lns_custom_fake(q_lnim_t* item)
{
    item->next = item;
    item->prev = item;
}

void _qos_ins_to_ln_after_item_custom_fake(q_lnim_t* item, q_lnim_t* base_item)
{
    base_item->next->prev = item;
    item->next = base_item->next;
    base_item->next = item;
    item->prev = base_item;
}

void _q_bh_node_init_custom_fake(bh_node_t* node, u32 key)
{
    node->key = key;
    node->lchild = NULL;
    node->parent = NULL;
    node->rchild = NULL;

}

void _q_bh_min_add_custom_fake(bh_node_t* node, bheap_t* heap)
{
    heap->last = node;
    heap->root = node;
}


void _q_bh_min_init_init_custom_fake(bheap_t* heap)
{
    heap->last = NULL;
    heap->root = NULL;
}

void _q_mem_mb_set_free_custom(uint idx, uint num)
{

}

void _q_mem_res_set_map_custom(uint idx, uint num)
{
    if (mcb.low_border < mcb.mb_mpu_start) {
        if (idx == (mcb.low_border - ram_origin) / _Q_MMAP_CHUNKLEN ) {
            for (uint i = idx; i < idx + num; i++) {
                mcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
            }
        }
    }
    if (idx == (mcb.mb_mpu_end - ram_origin) / _Q_MMAP_CHUNKLEN) {
        for (uint i = (idx);
             i < idx + num; i++) {
            mcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }
}

uint _q_mem_res_addr2map_idx_custom(size_t addr)
{
    return (addr - ram_origin) / _Q_MMAP_CHUNKLEN;

}


void qos_dyn_memory_init__test(void)
{
     MemCB_t ref_mcb;
     CLEAR_OBJ(&ref_mcb);
     CLEAR_OBJ(&mcb);

     _q_mem_mb_set_free_fake.custom_fake = _q_mem_mb_set_free_custom;
     _q_mem_res_set_map_fake.custom_fake = _q_mem_res_set_map_custom;
     _q_mem_res_addr2map_idx_fake.custom_fake = _q_mem_res_addr2map_idx_custom;

     ref_mcb.low_border = _sdata + _Data_Size + _Bss_Size;
     ref_mcb.high_border = ram_origin + _RAM_SIZE - _Q_MMAP_CHUNKLEN*3;
     ref_mcb.res_mem_startblock = (ref_mcb.low_border - ram_origin) % _Q_MMAP_CHUNKLEN ?
                 (ref_mcb.low_border - ram_origin) / _Q_MMAP_CHUNKLEN + 1 :
                 (ref_mcb.low_border - ram_origin) / _Q_MMAP_CHUNKLEN ;
     ref_mcb.mpu_area_start = ram_origin + _RAM_SIZE / 4;
     ref_mcb.mb_mpu_start = ref_mcb.mpu_area_start;
     ref_mcb.mb_mpu_end = ram_origin + (_RAM_SIZE * 3) / 4;
     ref_mcb.mb_raw_blocksize = _RAM_SIZE / (2 *  _Q_MPU_DYN_BLOCK_NUM);


     for (uint i = ref_mcb.res_mem_startblock;
          i < (ref_mcb.mb_mpu_start - ram_origin) / _Q_MMAP_CHUNKLEN; i++) {
         ref_mcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
     }
     for (uint i = (ref_mcb.mb_mpu_end - ram_origin) / _Q_MMAP_CHUNKLEN;
          i < (ref_mcb.high_border - ram_origin) / _Q_MMAP_CHUNKLEN; i++) {
         ref_mcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
     }
    printf("low_border %ld\n", ref_mcb.low_border - ram_origin);
    qos_dyn_memory_init();

     TEST_ASSERT_EQUAL_UINT(ref_mcb.low_border, mcb.low_border);
     TEST_ASSERT_EQUAL_UINT(ref_mcb.high_border, mcb.high_border);
     TEST_ASSERT_EQUAL_UINT(ref_mcb.res_mem_startblock, mcb.res_mem_startblock);
     TEST_ASSERT_EQUAL_UINT64(ref_mcb.mpu_area_start, mcb.mpu_area_start);
     TEST_ASSERT_EQUAL_UINT64(ref_mcb.mb_mpu_start, mcb.mb_mpu_start);
     TEST_ASSERT_EQUAL_UINT64(ref_mcb.mb_mpu_end, mcb.mb_mpu_end);

     TEST_ASSERT_EQUAL_MEMORY_ARRAY(ref_mcb.mb_mpu, mcb.mb_mpu, sizeof(mbcb_t),
                                    _Q_MPU_DYN_BLOCK_NUM);
     TEST_ASSERT_EQUAL_PTR_ARRAY(ref_mcb.mem2mb, mcb.mem2mb, _Q_MPU_DYN_BLOCK_NUM);
     TEST_ASSERT_EQUAL_INT8_ARRAY(ref_mcb.memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);

     TEST_ASSERT_EQUAL_UINT64(ref_mcb.mb_raw_blocksize, mcb.mb_raw_blocksize);

}

void qos_dyn_memory_init_static_data_greater_quater__test(void)
{
     MemCB_t ref_mcb;
     CLEAR_OBJ(&ref_mcb);
     CLEAR_OBJ(&mcb);
     _Data_Size  = 0x4000;

     _q_mem_mb_set_free_fake.custom_fake = _q_mem_mb_set_free_custom;
     _q_mem_res_set_map_fake.custom_fake = _q_mem_res_set_map_custom;
     _q_mem_res_addr2map_idx_fake.custom_fake = _q_mem_res_addr2map_idx_custom;

     ref_mcb.low_border = _sdata + _Data_Size + _Bss_Size;
     ref_mcb.high_border = ram_origin + _RAM_SIZE - _Q_MMAP_CHUNKLEN*3;

     ref_mcb.mpu_area_start = ram_origin + _RAM_SIZE / 4;
     ref_mcb.mb_raw_blocksize = _RAM_SIZE / (2 *  _Q_MPU_DYN_BLOCK_NUM);
     ref_mcb.mb_mpu_start = (ref_mcb.low_border % ref_mcb.mb_raw_blocksize) ?
                  ref_mcb.low_border + (ref_mcb.mb_raw_blocksize - ref_mcb.low_border % ref_mcb.mb_raw_blocksize) :
                 ref_mcb.low_border;
     ref_mcb.mb_mpu_end = ram_origin + (_RAM_SIZE * 3) / 4;
    ref_mcb.res_mem_startblock = (ref_mcb.mb_mpu_end - ram_origin) / _Q_MMAP_CHUNKLEN;

     printf("low_border %ld\n", ref_mcb.low_border);

//     for (uint i = ref_mcb.res_mem_startblock;
//          i < (ref_mcb.mb_mpu_start - ram_origin) / _Q_MMAP_CHUNKLEN; i++) {
//         ref_mcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
//     }
     for (uint i = (ref_mcb.mb_mpu_end - ram_origin) / _Q_MMAP_CHUNKLEN;
          i < (ref_mcb.high_border - ram_origin) / _Q_MMAP_CHUNKLEN; i++) {
         ref_mcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
     }

    qos_dyn_memory_init();

     TEST_ASSERT_EQUAL_UINT(ref_mcb.low_border, mcb.low_border);
     TEST_ASSERT_EQUAL_UINT(ref_mcb.high_border, mcb.high_border);
     TEST_ASSERT_EQUAL_UINT(ref_mcb.res_mem_startblock, mcb.res_mem_startblock);
     TEST_ASSERT_EQUAL_UINT64(ref_mcb.mpu_area_start, mcb.mpu_area_start);
     TEST_ASSERT_EQUAL_UINT64(ref_mcb.mb_mpu_start, mcb.mb_mpu_start);
     TEST_ASSERT_EQUAL_UINT64(ref_mcb.mb_mpu_end, mcb.mb_mpu_end);

     TEST_ASSERT_EQUAL_MEMORY_ARRAY(ref_mcb.mb_mpu, mcb.mb_mpu, sizeof(mbcb_t),
                                    _Q_MPU_DYN_BLOCK_NUM);
     TEST_ASSERT_EQUAL_PTR_ARRAY(ref_mcb.mem2mb, mcb.mem2mb, _Q_MPU_DYN_BLOCK_NUM);
     TEST_ASSERT_EQUAL_INT8_ARRAY(ref_mcb.memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);

     TEST_ASSERT_EQUAL_UINT64(ref_mcb.mb_raw_blocksize, mcb.mb_raw_blocksize);

}

void qos_memblock_init_size_200_test(void)
{
    MemCB_t ref_mcb;
    CLEAR_OBJ(&ref_mcb);
    CLEAR_OBJ(&mcb);
    mcb.mb_mpu_start = ram_origin;
    mch_head_t* head =  (mch_head_t*)mcb.mb_mpu_start;

    size_t len = 200;
    mbcb_t ref_memblock;
    mch_head_t ref_head;
    CLEAR_OBJ(&ref_memblock);
    CLEAR_OBJ(&ref_head);


    _q_bh_min_init_fake.custom_fake = _q_bh_min_init_init_custom_fake;
    _qos_close_item_lns_fake.custom_fake = _qos_close_item_lns_custom_fake;
    _q_bh_node_init_fake.custom_fake = _q_bh_node_init_custom_fake;
    _q_bh_min_add_fake.custom_fake = _q_bh_min_add_custom_fake;
    _qos_ins_to_ln_after_item_fake.custom_fake = _qos_ins_to_ln_after_item_custom_fake;

    ref_head.len = len - sizeof(ref_head);
    ref_head.free = 1;
    ref_head.lle.next = &ref_head.lle;
    ref_head.lle.prev = &ref_head.lle;
    ref_head.node.lchild = NULL;
    ref_head.node.rchild = NULL;
    ref_head.node.parent = NULL;

    ref_memblock.bheap.root = &ref_head.node;
    ref_memblock.bheap.last = &ref_head.node;
//    ref_memblock.ll_chunks.next = &ref_head.lle;
//    ref_memblock.ll_chunks.prev = &ref_head.lle;
    ref_memblock.ll_item_thread.next = &ref_memblock.ll_item_thread;
    ref_memblock.ll_item_thread.prev = &ref_memblock.ll_item_thread;
    ref_memblock.size = len;
    ref_memblock.head = &ref_head;


    mbcb_t* memblock = &mcb.mb_mpu[0];
    qos_memblock_init(mcb.mb_mpu_start, len, memblock);



    TEST_ASSERT_EQUAL_INT32 (ref_head.len, head->len);
    TEST_ASSERT_EQUAL_UINT8(ref_head.free, head->free);
    TEST_ASSERT_EQUAL_PTR(&head->lle, head->lle.next);
    TEST_ASSERT_EQUAL_PTR(&head->lle, head->lle.prev);
    TEST_ASSERT_EQUAL_PTR(ref_head.node.lchild, head->node.lchild);
    TEST_ASSERT_EQUAL_PTR(ref_head.node.rchild, head->node.rchild);
    TEST_ASSERT_EQUAL_PTR(ref_head.node.parent, head->node.parent);

    TEST_ASSERT_EQUAL_PTR(&head->node, memblock->bheap.root);
    TEST_ASSERT_EQUAL_PTR(&head->node, memblock->bheap.last);
    TEST_ASSERT_EQUAL_PTR(&memblock->ll_item_thread, memblock->ll_item_thread.next);
    TEST_ASSERT_EQUAL_PTR(&memblock->ll_item_thread, memblock->ll_item_thread.prev);
    TEST_ASSERT_EQUAL_PTR(ref_memblock.size, memblock->size);
    TEST_ASSERT_EQUAL_PTR(head, memblock->head);
}

void _qos_mb_array_extend_down_200_test(void)
{
    size_t len = 200;
    mcb.mpu_area_start = 0x2000;

    mcb.mb_mpu_end = mcb.mb_raw_blocksize * 8 + mcb.mpu_area_start;
    size_t ref_mb_mpu_end = mcb.mb_mpu_end - mcb.mb_raw_blocksize;
    size_t ref_space = mcb.mb_raw_blocksize;

    for (uint i = 0; i < 8; i++) {
        mcb.mem2mb[i] = 0;
    }

//    _q_mem_mb_len2blocknum_fake.return_val = 1;
    _q_mem_end_mb_idx_fake.return_val = 8;
    _q_mem_start_mb_idx_fake.return_val = 0;

    size_t space = _qos_mb_array_extend_down(len);


    TEST_ASSERT_EQUAL_INT64 (ref_space, space);
    TEST_ASSERT_EQUAL_INT64 (ref_mb_mpu_end, mcb.mb_mpu_end);
}

void _qos_mb_array_extend_down_3_mb_zero_return_test(void)
{
    size_t len = mcb.mb_raw_blocksize * 2 + 200;
    mcb.mpu_area_start = 0x20000;

    mcb.mb_mpu_end = mcb.mb_raw_blocksize * 8 + mcb.mpu_area_start;
//    size_t ref_space = mcb.mb_raw_blocksize;
    size_t ref_mb_mpu_end = mcb.mb_mpu_end - mcb.mb_raw_blocksize*0;

    for (uint i = 0; i < 5; i++) {
        mcb.mem2mb[i] = (mbcb_t*)0x55;
    }
    mcb.mem2mb[7] = (mbcb_t*)0x55;


//    _q_mem_mb_len2blocknum_fake.return_val = 2;
    _q_mem_end_mb_idx_fake.return_val = 8;
    _q_mem_start_mb_idx_fake.return_val = 0;

    size_t space = _qos_mb_array_extend_down(len);


    TEST_ASSERT_EQUAL_INT64 (0, space);
    TEST_ASSERT_EQUAL_INT64 (ref_mb_mpu_end, mcb.mb_mpu_end);
}


void _qos_mb_array_extend_down_3_mb_test(void)
{
    size_t len = mcb.mb_raw_blocksize * 2 + 200;
    mcb.mpu_area_start = 0x20000;

    mcb.mb_mpu_end = mcb.mb_raw_blocksize * 8 + mcb.mpu_area_start;
    size_t ref_space = mcb.mb_raw_blocksize * 3;
    size_t ref_mb_mpu_end = mcb.mb_mpu_end - mcb.mb_raw_blocksize*3;

    for (uint i = 0; i < 5; i++) {
        mcb.mem2mb[i] = (mbcb_t*)0x55;
    }
    mcb.mem2mb[8] = (mbcb_t*)0x55;
//    mcb.mem2mb[7] = (mbcb_t*)0x55;

    _q_mem_mb_len2blocknum_fake.return_val = 2;
    _q_mem_end_mb_idx_fake.return_val = 8;
    _q_mem_start_mb_idx_fake.return_val = 0;

    size_t space = _qos_mb_array_extend_down(len);


    TEST_ASSERT_EQUAL_INT64 (ref_space, space);
    TEST_ASSERT_EQUAL_INT64 (ref_mb_mpu_end, mcb.mb_mpu_end);
}

void _qos_mb_array_extend_no_free_mb_test(void)
{
    size_t len = mcb.mb_raw_blocksize * 200;
    mcb.mpu_area_start = 0x2000;

    mcb.mb_mpu_end = mcb.mb_raw_blocksize * 8 + mcb.mpu_area_start;
    size_t ref_space = 0;
    size_t ref_mb_mpu_end = mcb.mb_mpu_end;

    for (uint i = 0; i < 6; i++) {
        mcb.mem2mb[i] = (mbcb_t*)0x55;
    }

    _q_mem_mb_len2blocknum_fake.return_val = 2;
    _q_mem_end_mb_idx_fake.return_val = 8;
    _q_mem_start_mb_idx_fake.return_val = 0;

    size_t space = _qos_mb_array_extend_down(len);


    TEST_ASSERT_EQUAL_INT64 (ref_space, space);
    TEST_ASSERT_EQUAL_INT64 (ref_mb_mpu_end, mcb.mb_mpu_end);
}


void _qos_mb_array_extend_no_free_zero_mb_test(void)
{
    size_t len = mcb.mb_raw_blocksize * 200;
    mcb.mpu_area_start = 0x2000;

    mcb.mb_mpu_end = mcb.mb_raw_blocksize * 8 + mcb.mpu_area_start;
    size_t ref_space = 0;
    size_t ref_mb_mpu_end = mcb.mb_mpu_end;


    _q_mem_mb_len2blocknum_fake.return_val = 2;
    _q_mem_end_mb_idx_fake.return_val = 8;
    _q_mem_start_mb_idx_fake.return_val = 0;

    size_t space = _qos_mb_array_extend_down(len);


    TEST_ASSERT_EQUAL_INT64 (ref_space, space);
    TEST_ASSERT_EQUAL_INT64 (ref_mb_mpu_end, mcb.mb_mpu_end);
}


void qos_memblock_malloc__test(void)
{

    size_t len = 180;
    mbcb_t* mb = &mcb.mb_mpu[0];


    mch_head_t heads[3];
    bh_node_t* nodes[3];
    for (uint i=0; i < 3; i++) {
        nodes[i] = &heads[i].node;
        heads[i].free = 1;
    }
    nodes[0]->key = 100;
    nodes[1]->key = 250;
    mb->bheap.root = nodes[0];
    uint new_len = heads[1].len - sizeof(mch_head_t) - len;

    SET_RETURN_SEQ(_bh_next_node, nodes, 2);

    void *ptr =  qos_memblock_malloc(len, mb);

    TEST_ASSERT_EQUAL(2, _bh_next_node_fake.call_count);
    mch_head_t *new_free_chunk = (mch_head_t *)((size_t)(&heads[1]) + sizeof(mch_head_t) + len);


    TEST_ASSERT_EQUAL_PTR(&new_free_chunk->node, _q_bh_node_init_fake.arg0_val);
    TEST_ASSERT_EQUAL(new_len, _q_bh_node_init_fake.arg1_val);
    TEST_ASSERT_EQUAL_PTR(&new_free_chunk->lle, _qos_ins_to_ln_after_item_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&heads[1].lle, _qos_ins_to_ln_after_item_fake.arg1_val);
    TEST_ASSERT_EQUAL(1, new_free_chunk->free);
    TEST_ASSERT_EQUAL(0, heads[1].free);
    TEST_ASSERT_EQUAL_PTR(&new_free_chunk->node, _q_bh_min_add_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mb->bheap, _q_bh_min_add_fake.arg1_val);

    TEST_ASSERT_EQUAL_PTR(&heads[1].node, _q_bh_remove_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mb->bheap, _q_bh_remove_fake.arg1_val);
    TEST_ASSERT_EQUAL_PTR((void*)((size_t)&heads[1] + sizeof(mch_head_t)), ptr);

}


void qos_memblock_malloc_no_free_space_for_new_chunk_test(void)
{

    size_t len = 250;
    mbcb_t* mb = &mcb.mb_mpu[0];


    mch_head_t heads[3];
    bh_node_t* nodes[3];
    for (uint i=0; i < 3; i++) {
        nodes[i] = &heads[i].node;
        heads[i].free = 1;
    }
    nodes[0]->key = 100;
    nodes[1]->key = 250;
    mb->bheap.root = nodes[0];

    SET_RETURN_SEQ(_bh_next_node, nodes, 2);

    void *ptr =  qos_memblock_malloc(len, mb);

    TEST_ASSERT_EQUAL(2, _bh_next_node_fake.call_count);


    TEST_ASSERT_EQUAL(0, heads[1].free);

    TEST_ASSERT_EQUAL_PTR(&heads[1].node, _q_bh_remove_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mb->bheap, _q_bh_remove_fake.arg1_val);
    TEST_ASSERT_EQUAL_PTR((void*)((size_t)&heads[1] + sizeof(mch_head_t)), ptr);

}

void qos_memblock_malloc_no_free_space_test(void)
{

    size_t len = 300;
    mbcb_t* mb = &mcb.mb_mpu[0];


    mch_head_t heads[3];
    bh_node_t* nodes[3];
    for (uint i=0; i < 3; i++) {
        nodes[i] = &heads[i].node;
        heads[i].free = 1;
    }
    nodes[0]->key = 100;
    nodes[1]->key = 250;
    nodes[2] = (bh_node_t*)NULL;
    mb->bheap.root = nodes[0];

    SET_RETURN_SEQ(_bh_next_node, nodes, 3);

    void *ptr =  qos_memblock_malloc(len, mb);

    TEST_ASSERT_EQUAL(3, _bh_next_node_fake.call_count);
    TEST_ASSERT_EQUAL(0, _q_bh_node_init_fake.call_count);

    TEST_ASSERT_EQUAL_PTR((void*)0, ptr);

}

void qos_memblock_free_midle_wo_union_test(void)
{

    mbcb_t* mb = &mcb.mb_mpu[0];


    mch_head_t* heads[3];

    size_t sum_len = 0;
    size_t len[3];
    size_t mb_origin = ram_origin + 0x100;
    for (uint i=0; i < 3; i++) {
        len[i] = 0x60 + 0x40 * i;
        sum_len += len[i];
        heads[i] = (mch_head_t*)(mb_origin);
        heads[i]->free = 0;
        heads[i]->len = len[i] - sizeof(mch_head_t);
        mb_origin += len[i];
    }
    mb->head = heads[0];
    mb->size = sum_len;

    heads[0]->lle.next = &heads[1]->lle;
    heads[1]->lle.next = &heads[2]->lle;
    heads[2]->lle.next = &heads[0]->lle;

    heads[0]->lle.prev = &heads[2]->lle;
    heads[1]->lle.prev = &heads[0]->lle;
    heads[2]->lle.prev = &heads[1]->lle;

    void *ptr= (void*)(ram_origin + 0x100 + len[0] + sizeof(mch_head_t));


    qos_memblock_free(ptr, mb);

    TEST_ASSERT_EQUAL(0, _qos_rem_item_from_ln_list_fake.call_count);
    TEST_ASSERT_EQUAL(1, heads[1]->free);

//    TEST_ASSERT_EQUAL_PTR(&heads[1]->lle, _qos_rem_item_from_ln_list_fake.arg0_val);

    TEST_ASSERT_EQUAL_PTR(&heads[1]->node, _q_bh_node_init_fake.arg0_val);
    TEST_ASSERT_EQUAL(len[1] - sizeof(mch_head_t), _q_bh_node_init_fake.arg1_val);

    TEST_ASSERT_EQUAL_PTR(&heads[1]->node, _q_bh_min_add_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mb->bheap, _q_bh_min_add_fake.arg1_val);

}

void qos_memblock_free_midle_up_union_test(void)
{

    mbcb_t* mb = &mcb.mb_mpu[0];


    mch_head_t* heads[3];

    size_t sum_len = 0;
    size_t len[3];
    size_t mb_origin = ram_origin + 0x100;
    for (uint i=0; i < 3; i++) {
        len[i] = 0x60 + 0x40 * i;
        sum_len += len[i];
        heads[i] = (mch_head_t*)(mb_origin);
        heads[i]->free = 0;
        heads[i]->len = len[i] - sizeof(mch_head_t);
        mb_origin += len[i];
    }

    heads[2]->free = 1;
    mb->head = heads[0];
    mb->size = sum_len;

    heads[0]->lle.next = &heads[1]->lle;
    heads[1]->lle.next = &heads[2]->lle;
    heads[2]->lle.next = &heads[0]->lle;

    heads[0]->lle.prev = &heads[2]->lle;
    heads[1]->lle.prev = &heads[0]->lle;
    heads[2]->lle.prev = &heads[1]->lle;

    void *ptr= (void*)(ram_origin + 0x100 + len[0] + sizeof(mch_head_t));


    qos_memblock_free(ptr, mb);

    TEST_ASSERT_EQUAL(1, _qos_rem_item_from_ln_list_fake.call_count);
    TEST_ASSERT_EQUAL(1, heads[1]->free);

    TEST_ASSERT_EQUAL_PTR(&heads[2]->lle, _qos_rem_item_from_ln_list_fake.arg0_val);

    TEST_ASSERT_EQUAL_PTR(&heads[2]->node, _q_bh_remove_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mb->bheap, _q_bh_remove_fake.arg1_val);

    TEST_ASSERT_EQUAL_PTR(&heads[1]->node, _q_bh_node_init_fake.arg0_val);
    TEST_ASSERT_EQUAL(len[1] + len[2] -   sizeof(mch_head_t), _q_bh_node_init_fake.arg1_val);

    TEST_ASSERT_EQUAL_PTR(&heads[1]->node, _q_bh_min_add_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mb->bheap, _q_bh_min_add_fake.arg1_val);


}


void qos_memblock_free_midle_down_union_test(void)
{

    mbcb_t* mb = &mcb.mb_mpu[0];


    mch_head_t* heads[3];

    size_t sum_len = 0;
    size_t len[3];
    size_t mb_origin = ram_origin + 0x100;
    for (uint i=0; i < 3; i++) {
        len[i] = 0x60 + 0x40 * i;
        sum_len += len[i];
        heads[i] = (mch_head_t*)(mb_origin);
        heads[i]->free = 0;
        heads[i]->len = len[i] - sizeof(mch_head_t);
        mb_origin += len[i];
    }

    heads[0]->free = 1;
    mb->head = heads[0];
    mb->size = sum_len;

    heads[0]->lle.next = &heads[1]->lle;
    heads[1]->lle.next = &heads[2]->lle;
    heads[2]->lle.next = &heads[0]->lle;

    heads[0]->lle.prev = &heads[2]->lle;
    heads[1]->lle.prev = &heads[0]->lle;
    heads[2]->lle.prev = &heads[1]->lle;

    void *ptr= (void*)(ram_origin + 0x100 + len[0] + sizeof(mch_head_t));


    qos_memblock_free(ptr, mb);

    TEST_ASSERT_EQUAL(1, _qos_rem_item_from_ln_list_fake.call_count);
    TEST_ASSERT_EQUAL(1, heads[0]->free);

    TEST_ASSERT_EQUAL_PTR(&heads[1]->lle, _qos_rem_item_from_ln_list_fake.arg0_val);

    TEST_ASSERT_EQUAL_PTR(&heads[0]->node, _q_bh_remove_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mb->bheap, _q_bh_remove_fake.arg1_val);

    TEST_ASSERT_EQUAL_PTR(&heads[0]->node, _q_bh_node_init_fake.arg0_val);
    TEST_ASSERT_EQUAL(len[0] + len[1] -   sizeof(mch_head_t), _q_bh_node_init_fake.arg1_val);

    TEST_ASSERT_EQUAL_PTR(&heads[0]->node, _q_bh_min_add_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mb->bheap, _q_bh_min_add_fake.arg1_val);

}

void qos_memblock_free_last_wo_union_test(void)
{

    mbcb_t* mb = &mcb.mb_mpu[0];


    mch_head_t* heads[3];

    size_t sum_len = 0;
    size_t len[3];
    size_t mb_origin = ram_origin + 0x100;
    for (uint i=0; i < 3; i++) {
        len[i] = 0x60 + 0x40 * i;
        sum_len += len[i];
        heads[i] = (mch_head_t*)(mb_origin);
        heads[i]->free = 0;
        heads[i]->len = len[i] - sizeof(mch_head_t);
        mb_origin += len[i];
    }

    mb->head = heads[0];
    mb->size = sum_len + 10;

    heads[0]->lle.next = &heads[1]->lle;
    heads[1]->lle.next = &heads[2]->lle;
    heads[2]->lle.next = &heads[0]->lle;

    heads[0]->lle.prev = &heads[2]->lle;
    heads[1]->lle.prev = &heads[0]->lle;
    heads[2]->lle.prev = &heads[1]->lle;

    void *ptr= (void*)(ram_origin + 0x100 + len[0] + len[1] + sizeof(mch_head_t));


    qos_memblock_free(ptr, mb);

    TEST_ASSERT_EQUAL(0, _qos_rem_item_from_ln_list_fake.call_count);
    TEST_ASSERT_EQUAL(1, heads[2]->free);

    TEST_ASSERT_EQUAL_PTR(&heads[2]->node, _q_bh_node_init_fake.arg0_val);
    TEST_ASSERT_EQUAL(len[2] -   sizeof(mch_head_t) + 10, _q_bh_node_init_fake.arg1_val);

    TEST_ASSERT_EQUAL_PTR(&heads[2]->node, _q_bh_min_add_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mb->bheap, _q_bh_min_add_fake.arg1_val);

}

void qos_memblock_free_first_wo_union_test(void)
{

    mbcb_t* mb = &mcb.mb_mpu[0];


    mch_head_t* heads[3];

    size_t sum_len = 0;
    size_t len[3];
    size_t mb_origin = ram_origin + 0x100;
    for (uint i=0; i < 3; i++) {
        len[i] = 0x60 + 0x40 * i;
        sum_len += len[i];
        heads[i] = (mch_head_t*)(mb_origin);
        heads[i]->free = 0;
        heads[i]->len = len[i] - sizeof(mch_head_t);
        mb_origin += len[i];
    }

    mb->head = heads[0];
    mb->size = sum_len + 10;

    heads[0]->lle.next = &heads[1]->lle;
    heads[1]->lle.next = &heads[2]->lle;
    heads[2]->lle.next = &heads[0]->lle;

    heads[0]->lle.prev = &heads[2]->lle;
    heads[1]->lle.prev = &heads[0]->lle;
    heads[2]->lle.prev = &heads[1]->lle;

    void *ptr= (void*)(ram_origin + 0x100 + sizeof(mch_head_t));


    qos_memblock_free(ptr, mb);

    TEST_ASSERT_EQUAL(0, _qos_rem_item_from_ln_list_fake.call_count);
    TEST_ASSERT_EQUAL(1, heads[0]->free);

    TEST_ASSERT_EQUAL_PTR(&heads[0]->node, _q_bh_node_init_fake.arg0_val);
    TEST_ASSERT_EQUAL(len[0] -   sizeof(mch_head_t), _q_bh_node_init_fake.arg1_val);

    TEST_ASSERT_EQUAL_PTR(&heads[0]->node, _q_bh_min_add_fake.arg0_val);
    TEST_ASSERT_EQUAL_PTR(&mb->bheap, _q_bh_min_add_fake.arg1_val);

}


int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(qos_dyn_memory_init__test);
    RUN_TEST(qos_dyn_memory_init_static_data_greater_quater__test);
    RUN_TEST(qos_memblock_init_size_200_test);

    RUN_TEST(_qos_mb_array_extend_down_200_test);
    RUN_TEST(_qos_mb_array_extend_down_3_mb_zero_return_test);
    RUN_TEST(_qos_mb_array_extend_down_3_mb_test);
    RUN_TEST(_qos_mb_array_extend_no_free_mb_test);
    RUN_TEST(_qos_mb_array_extend_no_free_zero_mb_test);

    RUN_TEST(qos_memblock_malloc__test);
    RUN_TEST(qos_memblock_malloc_no_free_space_for_new_chunk_test);
    RUN_TEST(qos_memblock_malloc_no_free_space_test);

    RUN_TEST(qos_memblock_free_midle_wo_union_test);
    RUN_TEST(qos_memblock_free_midle_up_union_test);
    RUN_TEST(qos_memblock_free_midle_down_union_test);
    RUN_TEST(qos_memblock_free_last_wo_union_test);
    RUN_TEST(qos_memblock_free_first_wo_union_test);

    return UNITY_END();

}
