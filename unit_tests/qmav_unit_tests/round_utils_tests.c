#include "unity.h"
#include "qos_def.h"


void setUp(void) {
    // set stuff up here

}


void tearDown(void) {
    // clean stuff up here
}

void test_num_of_leading_zeros(void)
{
    uint r;
    r = __builtin_clz(1UL);
    TEST_ASSERT_EQUAL_UINT(31, r);
}


void test_ceil_round_to_x_zero(void)
{
    uint r;
    r = ceil_round_to_x(0, 8);
    TEST_ASSERT_EQUAL_UINT(0, r);
}

void test_ceil_round_to_x_noequal(void)
{
    uint r;
    r = ceil_round_to_x(22, 8);
    TEST_ASSERT_EQUAL_UINT(24, r);
}

void test_ceil_round_to_x_noequal6500(void)
{
    uint r;
    r = ceil_round_to_x(6500, 64);
    TEST_ASSERT_EQUAL_UINT(6528, r);
}

void test_ceil_round_to_x_equal(void)
{
    uint r;
    r = ceil_round_to_x(8, 8);
    TEST_ASSERT_EQUAL_UINT(8, r);
}

void test_ceil_round_to_x_equal1024(void)
{
    uint r;
    r = ceil_round_to_x(1024, 1024);
    TEST_ASSERT_EQUAL_UINT(1024, r);
}

//void test_ceil_round_to_x_equal3_align_1(void)
//{
//    uint r;
//    r = ceil_round_to_x(3, 1);
//    TEST_ASSERT_EQUAL_UINT(3, r);
//}

void test_ceil_round_to_x_equal3_align_2(void)
{
    uint r;
    r = ceil_round_to_x(3, 2);
    TEST_ASSERT_EQUAL_UINT(4, r);
}

void test_floor_round_to_x_noequal(void)
{
    uint r;
    r = floor_round_to_x(22, 8);
    TEST_ASSERT_EQUAL_UINT(16, r);
}

void test_floor_round_to_x_noequal1023(void)
{
    uint r;
    r = floor_round_to_x(1023, 128);
    TEST_ASSERT_EQUAL_UINT(896, r);
}

void test_floor_round_to_x_equal(void)
{
    uint r;
    r = floor_round_to_x(8, 8);
    TEST_ASSERT_EQUAL_UINT(8, r);
}

void test_ceil_round_to_2_degree_noequal2000(void)
{
    uint r;
    r = ceil_round_to_2_degree(2000);
    TEST_ASSERT_EQUAL_UINT(2048, r);
}

void test_ceil_round_to_2_degree_equal128(void)
{
    uint r;
    r = ceil_round_to_2_degree(128);
    TEST_ASSERT_EQUAL_UINT(128, r);
}

void test_ceil_round_to_2_degree_equal2(void)
{
    uint r;
    r = ceil_round_to_2_degree(2);
    TEST_ASSERT_EQUAL_UINT(2, r);
}

void test_ceil_round_to_2_degree_equal1(void)
{
    uint r;
    r = ceil_round_to_2_degree(1);
    TEST_ASSERT_EQUAL_UINT(1, r);
}

void test_ceil_round_to_2_degree_equal4(void)
{
    uint r;
    r = ceil_round_to_2_degree(3);
    TEST_ASSERT_EQUAL_UINT(4, r);
}

void test_floor_round_to_2_degree_noequal5000(void)
{
    uint r;
    r = floor_round_to_2_degree(5000);
    TEST_ASSERT_EQUAL_UINT(4096, r);
}

void test_floor_round_to_2_degree_noequal256(void)
{
    uint r;
    r = floor_round_to_2_degree(256);
    TEST_ASSERT_EQUAL_UINT(256, r);
}


int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_num_of_leading_zeros);

    RUN_TEST(test_ceil_round_to_x_noequal);
    RUN_TEST(test_ceil_round_to_x_noequal6500);
    RUN_TEST(test_ceil_round_to_x_equal);
    RUN_TEST(test_ceil_round_to_x_equal1024);
//    RUN_TEST(test_ceil_round_to_x_equal3_align_1);
    RUN_TEST(test_ceil_round_to_x_equal3_align_2);

    RUN_TEST(test_ceil_round_to_x_zero);
    RUN_TEST(test_floor_round_to_x_noequal);
    RUN_TEST(test_floor_round_to_x_noequal1023);
    RUN_TEST(test_floor_round_to_x_equal);

    RUN_TEST(test_ceil_round_to_2_degree_noequal2000);
    RUN_TEST(test_ceil_round_to_2_degree_equal128);
    RUN_TEST(test_ceil_round_to_2_degree_equal2);
    RUN_TEST(test_ceil_round_to_2_degree_equal1);
    RUN_TEST(test_ceil_round_to_2_degree_equal4);

    RUN_TEST(test_floor_round_to_2_degree_noequal5000);
    RUN_TEST(test_floor_round_to_2_degree_noequal256);


    return UNITY_END();

}
