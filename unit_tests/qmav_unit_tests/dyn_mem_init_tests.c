#include <stddef.h>
#include "qos_memory.h"
#include "ut_utils.h"
#include "core_cm4.h"


char* ram;
char rawram[_RAM_SIZE * 2];
size_t _estack;

size_t ram_origin;
size_t ram_size;
MemCB_t mcb;


size_t _p_sdata;
size_t _p_sdata_delta = 1024UL * 2;
size_t _Priv_Data_Size  = 1024UL * 2;
size_t _Priv_Bss_Size  = 1024UL * 6;

size_t _sdata;
size_t _sdata_delta  = 1024UL * 16;
size_t _Data_Size  = 1024UL * 4;
size_t _Bss_Size  = 1024UL * 8;

size_t _Min_Stack_Size = 300UL;
size_t _Min_Heap_Size = 300UL;


MPU_Type mpu_struct;
mpu_rp_t thread_mpu_conf[_Q_MPU_STACK_REGIONS + _Q_MPU_DYNAMIC_REGIONS];
q_lnim_t thread_mb_list;
sbcb_t thread_stack[_Q_MPU_STACK_REGIONS];

void mcb_reset(void)
{

     _p_sdata_delta = 1024UL * 2;
     _Priv_Data_Size  = 1024UL * 2;
     _Priv_Bss_Size  = 1024UL * 6;


     _sdata_delta  = 1024UL * 16;
     _Data_Size  = 1024UL * 4;
     _Bss_Size  = 1024UL * 8;

     _Min_Stack_Size = 300UL;
     _Min_Heap_Size = 300UL;

     ram_origin = (size_t)rawram + _RAM_SIZE - (size_t)rawram % _RAM_SIZE;
     ram_size = _RAM_SIZE;
     CLEAR_AREA((void *)ram_origin, _RAM_SIZE);
     CLEAR_OBJ(&mcb);
     CLEAR_OBJ(&mpu_struct);
     CLEAR_OBJ(&thread_mpu_conf);
     CLEAR_OBJ(&thread_stack);

     _estack = ram_origin + _RAM_SIZE - 1;
     _p_sdata = ram_origin + _p_sdata_delta;
     _sdata = ram_origin + _sdata_delta;

     thread_mb_list.next = &thread_mb_list;
     thread_mb_list.prev = &thread_mb_list;

#if !defined(__QOS_MEM_ITGR_TEST)



     mcb.low_border = _sdata + _Data_Size + _Bss_Size;
     mcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;

     mcb.mpu_area_start = ram_origin + _RAM_SIZE / 2;
     mcb.mb_mpu_start = mcb.mpu_area_start;
     mcb.mb_raw_blocksize = _RAM_SIZE / (2 *  _Q_MPU_DYN_BLOCK_NUM);
     mcb.mb_mpu_end = ram_origin + _RAM_SIZE;

     mcb.res_mem_startblock = (mcb.low_border -  ram_origin) / _Q_MMAP_CHUNKLEN + 14;

#endif

 }
