import qbs
import qbs.TextFile
import qbs.File

Project {

    property var warningCompilerFlags: [

        "-Wextra",
        "-Wno-comment",
        "-Wconversion",
        "-Wpointer-arith",
        '-Wimplicit-int',
        "-Wfloat-conversion",
        "-Wsign-conversion",
        "-Wcast-qual",
        "-Wredundant-decls",
        "-Wdouble-promotion",
        "-Wnull-dereference",
        "-Wno-unused-parameter",
//            "-Wno-unused-variable"
]


    Product {

        name: "test_frameworks"
        Depends{name:"cpp"}
        cpp.includePaths: ["../../rtos/inc",
            "../../../../documents/c/Unity-master/src/",
        "../../../../documents/c/ut_utils/"]
        consoleApplication: true


        type: ["staticlibrary"]
        files: [
        ]

        cpp.defines: ["DESKTOP_TEST"]
        cpp.warningLevel: 'all'
//        cpp.staticLibraries: ["m"]


        Group {
            prefix: "../../../../documents/c/Unity-master/src/"
            files: [
                "unity_internals.h",
                "unity.h",
                "unity.c",
            ]
            name: "Unity"
        }

        Group {
            prefix: "../../../../documents/c/fff-master/"
            files: [
                "fff.h",
            ]
            name: "Fake Function Framework"
        }

        Group {
            prefix: "../../../../documents/c/ut_utils/"
            files: [
                "ut_utils.c",
                "ut_utils.h",
            ]
            name: "Fake Function Framework"
        }

    }


    Product {

        name: "binary_heap_tests"
        Depends {name:"test_frameworks"}
        Depends{name:"cpp"}

        cpp.warningLevel: 'all'
        cpp.commonCompilerFlags: warningCompilerFlags
        cpp.defines: ["DESKTOP_TEST"]
        cpp.includePaths: ["../../rtos/inc",
            "../../../../documents/c/Unity-master/src/",
            "../../../../documents/c/ut_utils/",
        "../../tests/binheap/"]
        consoleApplication: true
        type: ["application"]
        files: [
            "binheap_test.c"
        ]

        cpp.staticLibraries: ["m"]

        Group {
            name: "Binheap"
            files: [
                "../../rtos/inc/misc/qos_bin_heap.h",
                "../../rtos/src/misc/qos_bin_heap.c",
                "../../rtos/inc/qos_debug.h",
                "../../rtos/inc/qos_def.h"
            ]

        }
//        Group {
//            name: "Binheap libs"
//            prefix: "../../tests/binheap/"
//            files: [
//                "binheap_test_libs.c",
//                "binheap_test_libs.h"
//            ]

//        }

    }


    Product {

        name: "dyn_memory_lev_0_tests"
        Depends {name:"test_frameworks"}
        Depends{name:"cpp"}

        cpp.defines: ["DESKTOP_TEST", "STM32F40_41xxx", "__MPU_PRESENT = 1", "MPU_ENABLE",
        "___Q_MEM_START_MB_IDX__UNIT_TESTS",
        "___Q_MEM_END_MB_IDX__UNIT_TESTS",
        "___Q_MEM_MB_LEN2BLOCKNUM__UNIT_TESTS",
        "___Q_MEM_MB_SET_FREE__UNIT_TESTS",
        "___Q_MEM_MB_SET_USED__UNIT_TESTS",
        "___Q_MEM_RES_SET_MAP__UNIT_TESTS",
        "___Q_MEM_RES_UNSET_MAP__UNIT_TESTS",
        "___Q_MEM_RES_ADDR2MAP_IDX__UNIT_TESTS",
        "___Q_MEM_RES_LEN2BLOCKNUM__UNIT_TESTS",
        "___Q_MEM_RES_MAP_IDX2ADDR__UNIT_TESTS",
        "___Q_MEM_MB_GET_FREE_STATUS__UNIT_TESTS",
            
        "__Q_MPU_ADD_SUBREG_PREF__UNIT_TESTS",
        "__Q_MPU_REM_SUBREG_PREF__UNIT_TESTS",
        "__Q_MPU_SET_BASE_REGION__UNIT_TESTS",
        "__Q_MPU_APPLY_PREF__UNIT_TESTS",
        "__Q_MPU_APPLY_GROUP_PREF__UNIT_TESTS",
        "__Q_MPU_REGION_UNSET__UNIT_TESTS",
        "__Q_MPU_ENCODE_PREF_SPEC__UNIT_TESTS",
        "__Q_MPU_ENCODE_PREF__UNIT_TESTS",
        "___Q_SET_8BIT_MAP__UNIT_TESTS",
        "___Q_UNSET_8BIT_MAP__UNIT_TESTS"]

        cpp.commonCompilerFlags: warningCompilerFlags
        cpp.includePaths: ["../../rtos/inc",
            "../../../../documents/c/Unity-master/src/",
        "../../../../documents/c/fff-master",
            "../../../../documents/c/ut_utils/",
            "/home/alex/documents/c/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Include",
            "/home/alex/documents/c/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Device/ST/STM32F4xx/Include"]
        consoleApplication: true
        type: ["application"]
        files: [
            "dyn_memory_lev_0_tests.c",
        ]

//        cpp.staticLibraries: ["m"]

        Group {
            name: "QOS memory"
            prefix: "../../rtos/"
            files: [
                "inc/qos_memory.h",
                "src/qos_memory.c"
            ]
}
            Group {
                name: "dyn memory test misc"

                files: [
                    "dyn_mem_init_tests.c",
                    "dyn_mem_init_tests.h",
                ]
        }
    }

    Product {

        name: "dyn_memory_lev_1_tests"
        Depends {name:"test_frameworks"}
        Depends{name:"cpp"}

        cpp.defines: ["DESKTOP_TEST", "STM32F40_41xxx", "__MPU_PRESENT = 1", "MPU_ENABLE",
        "___Q_MEM_MB_IDX2ADDR__UNIT_TESTS",
        "___Q_MEM_MB_ADDR2IDX__UNIT_TESTS",
        "___Q_MEM_MB_FIND_FREE_BLOCKS__UNIT_TESTS",
        "___Q_MEM_RES_FIND_SEQ_MAP__UNIT_TESTS",
        "___Q_MEM_RES_NUM_FREE_CHUNK_FROM_IDX",
        "___Q_MEM_RES_FIND_SEQ_MAP_MPU__UNIT_TESTS"]

        cpp.commonCompilerFlags: warningCompilerFlags
        cpp.includePaths: ["../../rtos/inc",
            "../../../../documents/c/Unity-master/src/",
        "../../../../documents/c/fff-master",
            "../../../../documents/c/ut_utils/",
        "/home/alex/documents/c/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Include",
        "/home/alex/documents/c/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Device/ST/STM32F4xx/Include"]
        consoleApplication: true
        type: ["application"]
        files: [
            "dyn_memory_lev_1_tests.c",
        ]

//        cpp.staticLibraries: ["m"]

        Group {
            name: "QOS memory"
            prefix: "../../rtos/"
            files: [
                "inc/qos_memory.h",
                "src/qos_memory.c"
            ]
        }
        Group {
            name: "dyn memory test misc"

            files: [
                "dyn_mem_init_tests.c",
                "dyn_mem_init_tests.h",
            ]
    }
    }



    Product {

        name: "dyn_memory_lev_2_tests"
        Depends {name:"test_frameworks"}
        Depends{name:"cpp"}

        cpp.defines: ["DESKTOP_TEST", "DYN_MEM_ALLOC_GEN_TESTS", "STM32F40_41xxx", "__MPU_PRESENT = 1", "MPU_ENABLE",
        "__QOS_DYN_MEMORY_INIT__UNIT_TESTS",
        "__QOS_MEMBLOCK_INIT__UNIT_TESTS",
        "___QOS_MB_ARRAY_EXTEND_DOWN__UNIT_TESTS",
        "__QOS_MEMBLOCK_MALLOC__UNIT_TESTS",
        "__QOS_MEMBLOCK_FREE__UNIT_TESTS"]

        cpp.commonCompilerFlags: warningCompilerFlags
        cpp.includePaths: ["../../rtos/inc",
            "../../../../documents/c/Unity-master/src/",
        "../../../../documents/c/fff-master",
            "../../../../documents/c/ut_utils/",
            "/home/alex/documents/c/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Include",
            "/home/alex/documents/c/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Device/ST/STM32F4xx/Include"]
        consoleApplication: true
        type: ["application"]
        files: [
            "dyn_memory_lev_2_tests.c",
        ]


        Group {
            name: "QOS memory"
            prefix: "../../rtos/"
            files: [
                "inc/qos_memory.h",
                "src/qos_memory.c"
            ]
        }
        Group {
            name: "dyn memory test misc"

            files: [
                "dyn_mem_init_tests.c",
                "dyn_mem_init_tests.h",
            ]
        }
    }


    Product {

        name: "dyn_memory_lev_3_tests"
        Depends {name:"test_frameworks"}
        Depends{name:"cpp"}

        cpp.defines: ["DESKTOP_TEST", "DYN_MEM_ALLOC_GEN_TESTS", "STM32F40_41xxx", "__MPU_PRESENT = 1", "MPU_ENABLE",
        "__QALLOC_HANDLER__UNIT_TESTS",
            "__QALLOC_UNIQ_HANDLER__UNIT_TESTS",
            "___Q_FREE_MAIN_MEM__UNIT_TESTS",
            "___Q_FREE_RESIDUAL_MEM__UNIT_TESTS"
        ]

        cpp.commonCompilerFlags: warningCompilerFlags
        cpp.includePaths: ["../../rtos/inc",
            "../../../../documents/c/Unity-master/src/",
        "../../../../documents/c/fff-master",
            "../../../../documents/c/ut_utils/",
            "/home/alex/documents/c/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Include",
            "/home/alex/documents/c/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Device/ST/STM32F4xx/Include"]
        consoleApplication: true
        type: ["application"]
        files: [
            "dyn_memory_lev_3_tests.c",
        ]


        Group {
            name: "QOS memory"
            prefix: "../../rtos/"
            files: [
                "inc/qos_memory.h",
                "src/qos_memory.c"
            ]
        }
        Group {
            name: "dyn memory test misc"

            files: [

                "dyn_mem_init_tests.c",
                "dyn_mem_init_tests.h",
            ]
        }
    }


    Product {

        name: "round_utils_tests"
        Depends {name:"test_frameworks"}
        Depends{name:"cpp"}

        cpp.commonCompilerFlags: warningCompilerFlags
        cpp.defines: ["DESKTOP_TEST",
        "__ROUND_FUNCTIONS_TESTS"]
        cpp.includePaths: ["../../rtos/inc",
            "../../../../documents/c/Unity-master/src/",
        "../../../../documents/c/fff-master"]
        consoleApplication: true
        type: ["application"]
        files: [
            "round_utils_tests.c",
        ]
    }

    Product {
        Depends {name:"round_utils_tests"}
        Depends {name:"binary_heap_tests"}
        Depends {name:"dyn_memory_lev_0_tests"}
        Depends {name:"dyn_memory_lev_1_tests"}
        Depends {name:"dyn_memory_lev_2_tests"}
        Depends {name:"dyn_memory_lev_3_tests"}
        Depends {name:"dyn_memory_itg_test"}
        name: "tests"
        type: ["application"]
//        targetName: "jopa"

        Rule {
            //                            inputs: ["application"]
            inputsFromDependencies: ["application"]
            Artifact {
                filePath: product.targetName + ".sh"
                fileTags: ["application"]
            }
            multiplex: true

            prepare: {
                var commands = []
                var cmd = new JavaScriptCommand();
                cmd.description = product.name
                cmd.silent = false;
                //                cmd.highlight = "compiler";
                cmd.sourceCode = function() {
                    var file_ = new TextFile(output.filePath, TextFile.WriteOnly)
                    file_.writeLine("#!/bin/bash")
                    for (i=0; i < inputs.application.length; i++) {
                        file_.writeLine(inputs.application[i].filePath);
                    }
                    file_.close()
                };
                commands.push(cmd)

                var make_x = new Command("chmod", ["+x", output.filePath])
                make_x.silent = true
                commands.push(make_x)

                return commands;
            }
        }
    }

    Product {
        files: [
            "dyn_memory_itg_test.c",
        ]
        name: "dyn_memory_itg_test" //dynamic memory ITG test

        Depends {name:"test_frameworks"}
        Depends{name:"cpp"}
        cpp.defines: ["DESKTOP_TEST", "__QOS_MEM_ITGR_TEST", "STM32F40_41xxx", "__MPU_PRESENT = 1", "MPU_ENABLE",]

        cpp.includePaths: ["../../rtos/inc",
            "../../../../documents/c/Unity-master/src/",
            "../../../../documents/c/fff-master",
            "../../../../documents/c/ut_utils/",
            "/home/alex/documents/c/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Include",
            "/home/alex/documents/c/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Device/ST/STM32F4xx/Include"]
        consoleApplication: true
        type: ["application"]

        Group {
            name: "QOS memory and bheap"
            prefix: "../../rtos/"
            files: [
                "inc/qos_memory.h",
                "src/qos_memory.c",
                "src/misc/qos_bin_heap.c"
            ]
        }
        Group {
            name: "dyn memory test misc"

            files: [
                "dyn_mem_init_tests.c",
                "dyn_mem_init_tests.h",
            ]
        }
    }
}
