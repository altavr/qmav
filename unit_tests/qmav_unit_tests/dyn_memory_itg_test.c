#include "stdio.h"
#include "unity.h"
#include "fff.h"
#include "qos_memory.h"
#include "qos_ln_list.h"
#include "ut_utils.h"
#include "dyn_mem_init_tests.h"

extern


void tearDown(void) {
    // clean stuff up here
}

void setUp(void)
{
    mcb_reset();
}


void init_and_alloc_dyn_memory_test(void)
{
    void *ptr;
    void *ref_ptr;
    MemCB_t ref_memcb;

    size_t len = 128;

    CLEAR_OBJ(&ref_memcb);

    mpu_rp_t ref_mpu_thread_conf[_Q_MPU_STACK_REGIONS + _Q_MPU_DYNAMIC_REGIONS];
    CLEAR_ARRAY(ref_mpu_thread_conf);
    for (uint i =  _Q_MPU_HEAP_PREF_IDX; i < _Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX; i++) {
        ref_mpu_thread_conf[i].attr_size = 0xFFFFFFFF;
        thread_mpu_conf[i].attr_size = 0xFFFFFFFF;
    }

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }
    ref_mpu_thread_conf[_Q_MPU_HEAP_PREF_IDX + 0].attr_size = 0xFFFFFEFF;
    qos_dyn_memory_init();

    ref_ptr = (void*)(mcb.mb_mpu_start + sizeof( mch_head_t));


    mch_head_t *chunk = (mch_head_t *)((size_t)ref_ptr - sizeof( mch_head_t));
    mch_head_t *free_chunk = (mch_head_t *)((size_t)chunk + sizeof( mch_head_t) + len);


    ptr = qalloc_dynmem(len, &thread_mb_list, thread_mpu_conf);

    TEST_ASSERT_EQUAL_PTR(ref_ptr, ptr);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.low_border, mcb.low_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.high_border, mcb.high_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_start, mcb.mb_mpu_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mpu_area_start, mcb.mpu_area_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_end, mcb.mb_mpu_end);
    TEST_ASSERT_EQUAL_PTR(chunk, mcb.mb_mpu[0].head);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[0], mcb.mem2mb[0]);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memcb.memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
    TEST_ASSERT_EQUAL_UINT64(len, mcb.mb_mpu[0].head->len);
    TEST_ASSERT_EQUAL_PTR(thread_mb_list.next, &mcb.mb_mpu[0].ll_item_thread);

    TEST_ASSERT_EQUAL_PTR(chunk->lle.next, &free_chunk->lle);
    TEST_ASSERT_EQUAL_UINT(mcb.mb_raw_blocksize - chunk->len - 2 * sizeof(*chunk),
                           free_chunk->len);

    TEST_ASSERT_EQUAL_UINT32_ARRAY(ref_mpu_thread_conf, thread_mpu_conf,
                                   (_Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX) * 2);
}

void init_and_alloc_free_dyn_memory_test(void)
{
    void *ptr;
    void *ref_ptr;
    MemCB_t ref_memcb;
    size_t len = 128;

    CLEAR_OBJ(&ref_memcb);

    mpu_rp_t ref_mpu_thread_conf[_Q_MPU_STACK_REGIONS + _Q_MPU_DYNAMIC_REGIONS];
    CLEAR_ARRAY(ref_mpu_thread_conf);
    for (uint i =  _Q_MPU_HEAP_PREF_IDX; i < _Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX; i++) {
        ref_mpu_thread_conf[i].attr_size = 0x0000FF00;
        thread_mpu_conf[i].attr_size = 0x0000FF00;
    }
    ref_mpu_thread_conf[_Q_MPU_HEAP_PREF_IDX + 0].attr_size = 0x0000FE00;

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();

    ref_ptr = (void*)(mcb.mb_mpu_start + sizeof( mch_head_t));
    ptr = qalloc_dynmem(len, &thread_mb_list, thread_mpu_conf);

    TEST_ASSERT_EQUAL_UINT32_ARRAY(ref_mpu_thread_conf, thread_mpu_conf,
                                   (_Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX) * 2);
    ref_mpu_thread_conf[_Q_MPU_HEAP_PREF_IDX + 0].attr_size = 0x0000FF00;
    _q_free_main_mem(ptr, &thread_mb_list, thread_mpu_conf);

    TEST_ASSERT_EQUAL_PTR(0, mcb.mem2mb[0]);
    TEST_ASSERT_EQUAL_PTR(&thread_mb_list, thread_mb_list.next);
    TEST_ASSERT_EQUAL_UINT32_ARRAY(ref_mpu_thread_conf, thread_mpu_conf,
                                   (_Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX) * 2);

}

void init_and_alloc_2_dyn_memory_test(void)
{
    const uint n = 2;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {128, 200};
    mch_head_t *chunk[n];

    CLEAR_OBJ(&ref_memcb);

    mpu_rp_t ref_mpu_thread_conf[_Q_MPU_STACK_REGIONS + _Q_MPU_DYNAMIC_REGIONS];
    CLEAR_ARRAY(ref_mpu_thread_conf);
    for (uint i =  _Q_MPU_HEAP_PREF_IDX; i < _Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX; i++) {
        ref_mpu_thread_conf[i].attr_size = 0x0000FF00;
        thread_mpu_conf[i].attr_size = 0x0000FF00;
    }
    ref_mpu_thread_conf[_Q_MPU_HEAP_PREF_IDX + 0].attr_size = 0x0000FE00;

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();

    ref_ptr[0] = (void*)(mcb.mb_mpu_start + sizeof( mch_head_t));
    ref_ptr[1] = (void*)((size_t)ref_ptr[0] + sizeof( mch_head_t) + len[0]);
    for (uint i = 0; i < n; i++) {
        chunk[i] = (mch_head_t *)(ref_ptr[i] - sizeof( mch_head_t));
    }
    mch_head_t *free_chunk = (mch_head_t *)((size_t)ref_ptr[1]  + len[1]);

    ptr[0] = qalloc_dynmem(len[0], &thread_mb_list, thread_mpu_conf);
    TEST_ASSERT_EQUAL_UINT(mcb.mb_raw_blocksize - len[0]  - 2 * sizeof(mch_head_t),
                           chunk[1]->len);
    ptr[1] = qalloc_dynmem(len[1], &thread_mb_list, thread_mpu_conf);


    TEST_ASSERT_EQUAL_UINT64(ref_memcb.low_border, mcb.low_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.high_border, mcb.high_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_start, mcb.mb_mpu_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mpu_area_start, mcb.mpu_area_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_end, mcb.mb_mpu_end);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memcb.memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
    TEST_ASSERT_EQUAL_PTR(thread_mb_list.next, &mcb.mb_mpu[0].ll_item_thread);

    TEST_ASSERT_EQUAL_PTR(ref_ptr[0], ptr[0]);
    TEST_ASSERT_EQUAL_PTR(ref_ptr[1], ptr[1]);

    TEST_ASSERT_EQUAL_PTR(chunk[0], mcb.mb_mpu[0].head);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[0], mcb.mem2mb[0]);

    TEST_ASSERT_EQUAL_UINT64(len[0], chunk[0]->len);
    TEST_ASSERT_EQUAL_UINT64(len[1], chunk[1]->len);

    TEST_ASSERT_EQUAL_UINT64((size_t)chunk[0]+ sizeof(mch_head_t)+chunk[0]->len, (size_t)chunk[1]);
    TEST_ASSERT_EQUAL_UINT64((size_t)chunk[1]+ sizeof(mch_head_t)+chunk[1]->len, (size_t)free_chunk);

    TEST_ASSERT_EQUAL_UINT64(free_chunk->len, mcb.mb_mpu[0].bheap.root->key);
    TEST_ASSERT_EQUAL_PTR(chunk[1]->lle.next, &free_chunk->lle);


    TEST_ASSERT_EQUAL_UINT(mcb.mb_raw_blocksize - len[0] - len[1]  - (n + 1) * sizeof(mch_head_t),
                           free_chunk->len);
    TEST_ASSERT_EQUAL_UINT32_ARRAY(ref_mpu_thread_conf, thread_mpu_conf,
                                   (_Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX) * 2);

}


void init_and_alloc_3_dyn_memory_test(void)
{
    const uint n = 3;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {128, 200, 400};
    mch_head_t *chunk[n];

    CLEAR_OBJ(&ref_memcb);

    mpu_rp_t ref_mpu_thread_conf[_Q_MPU_STACK_REGIONS + _Q_MPU_DYNAMIC_REGIONS];
    CLEAR_ARRAY(ref_mpu_thread_conf);
    for (uint i =  _Q_MPU_HEAP_PREF_IDX; i < _Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX; i++) {
        ref_mpu_thread_conf[i].attr_size = 0x0000FF00;
        thread_mpu_conf[i].attr_size = 0x0000FF00;
    }
    ref_mpu_thread_conf[_Q_MPU_HEAP_PREF_IDX + 0].attr_size = 0x0000FE00;

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();

    ref_ptr[0] = (void*)(mcb.mb_mpu_start + sizeof( mch_head_t));
    ref_ptr[1] = (void*)((size_t)ref_ptr[0] + sizeof( mch_head_t) + len[0]);
    ref_ptr[2] = (void*)((size_t)ref_ptr[1] + sizeof( mch_head_t) + len[1]);
    for (uint i = 0; i < n; i++) {
        chunk[i] = (mch_head_t *)(ref_ptr[i] - sizeof( mch_head_t));
    }
    mch_head_t *free_chunk = (mch_head_t *)((size_t)chunk[2] + sizeof( mch_head_t) + len[2]);

    ptr[0] = qalloc_dynmem(len[0], &thread_mb_list, thread_mpu_conf);
    ptr[1] = qalloc_dynmem(len[1], &thread_mb_list, thread_mpu_conf);
    ptr[2] = qalloc_dynmem(len[2], &thread_mb_list, thread_mpu_conf);




    TEST_ASSERT_EQUAL_UINT64(ref_memcb.low_border, mcb.low_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.high_border, mcb.high_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_start, mcb.mb_mpu_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mpu_area_start, mcb.mpu_area_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_end, mcb.mb_mpu_end);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memcb.memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);

    TEST_ASSERT_EQUAL_PTR(ref_ptr[0], ptr[0]);
    TEST_ASSERT_EQUAL_PTR(ref_ptr[1], ptr[1]);
    TEST_ASSERT_EQUAL_PTR(ref_ptr[2], ptr[2]);

    TEST_ASSERT_EQUAL_PTR(chunk[0], mcb.mb_mpu[0].head);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[0], mcb.mem2mb[0]);
    TEST_ASSERT_EQUAL_UINT64(len[0], mcb.mb_mpu[0].head->len);

    TEST_ASSERT_EQUAL_UINT64(len[1], chunk[1]->len);
    TEST_ASSERT_EQUAL_UINT64(len[2], chunk[2]->len);

    TEST_ASSERT_EQUAL_UINT64(free_chunk->len, mcb.mb_mpu[0].bheap.root->key);
    TEST_ASSERT_EQUAL_PTR(chunk[2]->lle.next, &free_chunk->lle);

    TEST_ASSERT_EQUAL_PTR(thread_mb_list.next, &mcb.mb_mpu[0].ll_item_thread);
    TEST_ASSERT_EQUAL_UINT(mcb.mb_raw_blocksize - len[0] - len[1] - len[2]  - (n + 1) * sizeof(mch_head_t),
                           free_chunk->len);
//    TEST_ASSERT_EQUAL_PTR(free_chunk->lle.next, &thread_mb_list);
    TEST_ASSERT_EQUAL_UINT32_ARRAY(ref_mpu_thread_conf, thread_mpu_conf,
                                   (_Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX) * 2);

}


void init_and_alloc_3_free_map_0_dyn_memory_test(void)
{
    const uint n = 3;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {128, 200, 400};
    mch_head_t *chunk[n];

    CLEAR_OBJ(&ref_memcb);

    mpu_rp_t ref_mpu_thread_conf[_Q_MPU_STACK_REGIONS + _Q_MPU_DYNAMIC_REGIONS];
    CLEAR_ARRAY(ref_mpu_thread_conf);
    for (uint i =  _Q_MPU_HEAP_PREF_IDX; i < _Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX; i++) {
        ref_mpu_thread_conf[i].attr_size = 0x0000FF00;
        thread_mpu_conf[i].attr_size = 0x0000FF00;
    }
    ref_mpu_thread_conf[_Q_MPU_HEAP_PREF_IDX + 0].attr_size = 0x0000FE00;

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();

    ref_ptr[0] = (void*)(mcb.mb_mpu_start + sizeof( mch_head_t));
    ref_ptr[1] = (void*)((size_t)ref_ptr[0] + sizeof( mch_head_t) + len[0]);
    ref_ptr[2] = (void*)((size_t)ref_ptr[1] + sizeof( mch_head_t) + len[1]);
    for (uint i = 0; i < n; i++) {
        chunk[i] = (mch_head_t *)(ref_ptr[i] - sizeof( mch_head_t));
    }
    mch_head_t *free_chunk = (mch_head_t *)((size_t)chunk[2] + sizeof( mch_head_t) + len[2]);

    ptr[0] = qalloc_dynmem(len[0], &thread_mb_list, thread_mpu_conf);
    ptr[1] = qalloc_dynmem(len[1], &thread_mb_list, thread_mpu_conf);
    ptr[2] = qalloc_dynmem(len[2], &thread_mb_list, thread_mpu_conf);

    _q_free_main_mem(ptr[0], &thread_mb_list, thread_mpu_conf);

    TEST_ASSERT_EQUAL_PTR(ref_ptr[1], ptr[1]);
    TEST_ASSERT_EQUAL_PTR(ref_ptr[2], ptr[2]);

    TEST_ASSERT_EQUAL_PTR(chunk[0], mcb.mb_mpu[0].head);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[0], mcb.mem2mb[0]);

    TEST_ASSERT_EQUAL_UINT64(len[1], chunk[1]->len);
    TEST_ASSERT_EQUAL_UINT64(len[2], chunk[2]->len);
    TEST_ASSERT_EQUAL_UINT(0, chunk[1]->free);
    TEST_ASSERT_EQUAL_UINT(0,  chunk[2]->free);

    TEST_ASSERT_EQUAL_PTR(chunk[2]->lle.next, &free_chunk->lle);

    TEST_ASSERT_EQUAL_PTR(thread_mb_list.next, &mcb.mb_mpu[0].ll_item_thread);
    TEST_ASSERT_EQUAL_UINT(mcb.mb_raw_blocksize - len[0] - len[1] - len[2]  - (n + 1) * sizeof(mch_head_t),
                           free_chunk->len);

    TEST_ASSERT_EQUAL_UINT(1, chunk[0]->free);
    TEST_ASSERT_EQUAL_UINT(1,  free_chunk->free);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root, &chunk[0]->node);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root->lchild, &free_chunk->node);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root->rchild, 0);
    TEST_ASSERT_EQUAL_UINT32_ARRAY(ref_mpu_thread_conf, thread_mpu_conf,
                                   (_Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX) * 2);
}

void init_and_alloc_3_free_map_1_dyn_memory_test(void)
{
    const uint n = 3;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {128, 200, 400};
    mch_head_t *chunk[n];

    CLEAR_OBJ(&ref_memcb);

    mpu_rp_t ref_mpu_thread_conf[_Q_MPU_STACK_REGIONS + _Q_MPU_DYNAMIC_REGIONS];
    CLEAR_ARRAY(ref_mpu_thread_conf);
    for (uint i =  _Q_MPU_HEAP_PREF_IDX; i < _Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX; i++) {
        ref_mpu_thread_conf[i].attr_size = 0x0000FF00;
        thread_mpu_conf[i].attr_size = 0x0000FF00;
    }
    ref_mpu_thread_conf[_Q_MPU_HEAP_PREF_IDX + 0].attr_size = 0x0000FE00;

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();

    ref_ptr[0] = (void*)(mcb.mb_mpu_start + sizeof( mch_head_t));
    ref_ptr[1] = (void*)((size_t)ref_ptr[0] + sizeof( mch_head_t) + len[0]);
    ref_ptr[2] = (void*)((size_t)ref_ptr[1] + sizeof( mch_head_t) + len[1]);
    for (uint i = 0; i < n; i++) {
        chunk[i] = (mch_head_t *)(ref_ptr[i] - sizeof( mch_head_t));
    }
    mch_head_t *free_chunk = (mch_head_t *)((size_t)chunk[2] + sizeof( mch_head_t) + len[2]);

    ptr[0] = qalloc_dynmem(len[0], &thread_mb_list, thread_mpu_conf);
    ptr[1] = qalloc_dynmem(len[1], &thread_mb_list, thread_mpu_conf);
    ptr[2] = qalloc_dynmem(len[2], &thread_mb_list, thread_mpu_conf);

    _q_free_main_mem(ptr[1], &thread_mb_list, thread_mpu_conf);

    TEST_ASSERT_EQUAL_PTR(ref_ptr[0], ptr[0]);
    TEST_ASSERT_EQUAL_PTR(ref_ptr[2], ptr[2]);

    TEST_ASSERT_EQUAL_PTR(chunk[0], mcb.mb_mpu[0].head);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[0], mcb.mem2mb[0]);

    TEST_ASSERT_EQUAL_UINT64(len[0], chunk[0]->len);
    TEST_ASSERT_EQUAL_UINT64(len[1], chunk[1]->len);
    TEST_ASSERT_EQUAL_UINT64(len[2], chunk[2]->len);
    TEST_ASSERT_EQUAL_UINT(0, chunk[0]->free);
    TEST_ASSERT_EQUAL_UINT(0,  chunk[2]->free);

    TEST_ASSERT_EQUAL_PTR(chunk[2]->lle.next, &free_chunk->lle);

    TEST_ASSERT_EQUAL_PTR(thread_mb_list.next, &mcb.mb_mpu[0].ll_item_thread);
    TEST_ASSERT_EQUAL_UINT(mcb.mb_raw_blocksize - len[0] - len[1] - len[2]  - (n + 1) * sizeof(mch_head_t),
                           free_chunk->len);

    TEST_ASSERT_EQUAL_UINT(1, chunk[1]->free);
    TEST_ASSERT_EQUAL_UINT(1,  free_chunk->free);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root, &chunk[1]->node);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root->lchild, &free_chunk->node);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root->rchild, 0);
    TEST_ASSERT_EQUAL_UINT32_ARRAY(ref_mpu_thread_conf, thread_mpu_conf,
                                   (_Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX) * 2);
}

void init_and_alloc_3_free_map_2_dyn_memory_test(void)
{
    const uint n = 3;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {128, 200, 400};
    mch_head_t *chunk[n];

    CLEAR_OBJ(&ref_memcb);

    mpu_rp_t ref_mpu_thread_conf[_Q_MPU_STACK_REGIONS + _Q_MPU_DYNAMIC_REGIONS];
    CLEAR_ARRAY(ref_mpu_thread_conf);
    for (uint i =  _Q_MPU_HEAP_PREF_IDX; i < _Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX; i++) {
        ref_mpu_thread_conf[i].attr_size = 0x0000FF00;
        thread_mpu_conf[i].attr_size = 0x0000FF00;
    }
    ref_mpu_thread_conf[_Q_MPU_HEAP_PREF_IDX + 0].attr_size = 0x0000FE00;

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();

    ref_ptr[0] = (void*)(mcb.mb_mpu_start + sizeof( mch_head_t));
    ref_ptr[1] = (void*)((size_t)ref_ptr[0] + sizeof( mch_head_t) + len[0]);
    ref_ptr[2] = (void*)((size_t)ref_ptr[1] + sizeof( mch_head_t) + len[1]);
    for (uint i = 0; i < n; i++) {
        chunk[i] = (mch_head_t *)(ref_ptr[i] - sizeof( mch_head_t));
    }
    mch_head_t *free_chunk = (mch_head_t *)((size_t)chunk[2] + sizeof( mch_head_t) + len[2]);

    ptr[0] = qalloc_dynmem(len[0], &thread_mb_list, thread_mpu_conf);
    ptr[1] = qalloc_dynmem(len[1], &thread_mb_list, thread_mpu_conf);
    ptr[2] = qalloc_dynmem(len[2], &thread_mb_list, thread_mpu_conf);

    _q_free_main_mem(ptr[2], &thread_mb_list, thread_mpu_conf);

    TEST_ASSERT_EQUAL_PTR(ref_ptr[0], ptr[0]);
    TEST_ASSERT_EQUAL_PTR(ref_ptr[1], ptr[1]);

    TEST_ASSERT_EQUAL_PTR(chunk[0], mcb.mb_mpu[0].head);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[0], mcb.mem2mb[0]);

    TEST_ASSERT_EQUAL_UINT64(len[0], chunk[0]->len);
    TEST_ASSERT_EQUAL_UINT64(len[1], chunk[1]->len);
//    TEST_ASSERT_EQUAL_UINT64(len[2], chunk[2]->len);
    TEST_ASSERT_EQUAL_UINT(0, chunk[0]->free);
    TEST_ASSERT_EQUAL_UINT(0,  chunk[1]->free);

    TEST_ASSERT_EQUAL_PTR(chunk[2]->lle.next, &chunk[0]->lle);

    TEST_ASSERT_EQUAL_PTR(thread_mb_list.next, &mcb.mb_mpu[0].ll_item_thread);
    TEST_ASSERT_EQUAL_UINT(mcb.mb_raw_blocksize - len[0] - len[1]  - 3 * sizeof(mch_head_t),
                           chunk[2]->len);

    TEST_ASSERT_EQUAL_UINT(1, chunk[2]->free);
//    TEST_ASSERT_EQUAL_UINT(1,  free_chunk->free);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root, &chunk[2]->node);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root->lchild, 0);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root->rchild, 0);

    TEST_ASSERT_EQUAL_UINT32_ARRAY(ref_mpu_thread_conf, thread_mpu_conf,
                                   (_Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX) * 2);

}

void init_and_alloc_3_free_map_1_2_dyn_memory_test(void)
{
    const uint n = 3;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {128, 200, 400};
    mch_head_t *chunk[n];

    CLEAR_OBJ(&ref_memcb);

    mpu_rp_t ref_mpu_thread_conf[_Q_MPU_STACK_REGIONS + _Q_MPU_DYNAMIC_REGIONS];
    CLEAR_ARRAY(ref_mpu_thread_conf);
    for (uint i =  _Q_MPU_HEAP_PREF_IDX; i < _Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX; i++) {
        ref_mpu_thread_conf[i].attr_size = 0x0000FF00;
        thread_mpu_conf[i].attr_size = 0x0000FF00;
    }
    ref_mpu_thread_conf[_Q_MPU_HEAP_PREF_IDX + 0].attr_size = 0x0000FE00;

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();

    ref_ptr[0] = (void*)(mcb.mb_mpu_start + sizeof( mch_head_t));
    ref_ptr[1] = (void*)((size_t)ref_ptr[0] + sizeof( mch_head_t) + len[0]);
    ref_ptr[2] = (void*)((size_t)ref_ptr[1] + sizeof( mch_head_t) + len[1]);
    for (uint i = 0; i < n; i++) {
        chunk[i] = (mch_head_t *)(ref_ptr[i] - sizeof( mch_head_t));
    }
    mch_head_t *free_chunk = (mch_head_t *)((size_t)chunk[2] + sizeof( mch_head_t) + len[2]);

    ptr[0] = qalloc_dynmem(len[0], &thread_mb_list, thread_mpu_conf);
    ptr[1] = qalloc_dynmem(len[1], &thread_mb_list, thread_mpu_conf);
    ptr[2] = qalloc_dynmem(len[2], &thread_mb_list, thread_mpu_conf);

    _q_free_main_mem(ptr[1], &thread_mb_list, thread_mpu_conf);
    _q_free_main_mem(ptr[2], &thread_mb_list, thread_mpu_conf);

    TEST_ASSERT_EQUAL_PTR(ref_ptr[0], ptr[0]);
//    TEST_ASSERT_EQUAL_PTR(ref_ptr[2], ptr[2]);

    TEST_ASSERT_EQUAL_PTR(chunk[0], mcb.mb_mpu[0].head);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[0], mcb.mem2mb[0]);

    TEST_ASSERT_EQUAL_UINT64(len[0], chunk[0]->len);
//    TEST_ASSERT_EQUAL_UINT64(len[1], chunk[1]->len);
//    TEST_ASSERT_EQUAL_UINT64(len[2], chunk[2]->len);
    TEST_ASSERT_EQUAL_UINT(0, chunk[0]->free);
//    TEST_ASSERT_EQUAL_UINT(0,  chunk[2]->free);

    TEST_ASSERT_EQUAL_PTR(chunk[0]->lle.next, &chunk[1]->lle);
    TEST_ASSERT_EQUAL_PTR(chunk[1]->lle.next, &chunk[0]->lle);

    TEST_ASSERT_EQUAL_PTR(thread_mb_list.next, &mcb.mb_mpu[0].ll_item_thread);
    TEST_ASSERT_EQUAL_UINT(mcb.mb_raw_blocksize - len[0]  - 2 * sizeof(mch_head_t),
                           chunk[1]->len);

    TEST_ASSERT_EQUAL_UINT(1, chunk[1]->free);
    TEST_ASSERT_EQUAL_UINT(1,  free_chunk->free);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root, &chunk[1]->node);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root->lchild, 0);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root->rchild, 0);

    TEST_ASSERT_EQUAL_UINT32_ARRAY(ref_mpu_thread_conf, thread_mpu_conf,
                                   (_Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX) * 2);
}

void init_and_alloc_3_free_map_0_alloc_60b_dyn_memory_test(void)
{
    const uint n = 4;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {128, 200, 400, 60};
    mch_head_t *chunk[n];

    CLEAR_OBJ(&ref_memcb);

    mpu_rp_t ref_mpu_thread_conf[_Q_MPU_STACK_REGIONS + _Q_MPU_DYNAMIC_REGIONS];
    CLEAR_ARRAY(ref_mpu_thread_conf);
    for (uint i =  _Q_MPU_HEAP_PREF_IDX; i < _Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX; i++) {
        ref_mpu_thread_conf[i].attr_size = 0x0000FF00;
        thread_mpu_conf[i].attr_size = 0x0000FF00;
    }
    ref_mpu_thread_conf[_Q_MPU_HEAP_PREF_IDX + 0].attr_size = 0x0000FE00;

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();

    ref_ptr[0] = (void*)(mcb.mb_mpu_start + sizeof( mch_head_t));
    ref_ptr[1] = (void*)((size_t)ref_ptr[0] + sizeof( mch_head_t) + len[0]);
    ref_ptr[2] = (void*)((size_t)ref_ptr[1] + sizeof( mch_head_t) + len[1]);
    for (uint i = 0; i < 3; i++) {
        chunk[i] = (mch_head_t *)(ref_ptr[i] - sizeof( mch_head_t));
    }
    chunk[3] = (mch_head_t *)((size_t)chunk[0] + sizeof( mch_head_t) + len[3]);
    mch_head_t *free_chunk = (mch_head_t *)((size_t)chunk[2] + sizeof( mch_head_t) + len[2]);

    ptr[0] = qalloc_dynmem(len[0], &thread_mb_list, thread_mpu_conf);
    ptr[1] = qalloc_dynmem(len[1], &thread_mb_list, thread_mpu_conf);
    ptr[2] = qalloc_dynmem(len[2], &thread_mb_list, thread_mpu_conf);

    _q_free_main_mem(ptr[0], &thread_mb_list, thread_mpu_conf);

    ptr[3] = qalloc_dynmem(len[3], &thread_mb_list, thread_mpu_conf);

    TEST_ASSERT_EQUAL_PTR(ref_ptr[1], ptr[1]);
    TEST_ASSERT_EQUAL_PTR(ref_ptr[2], ptr[2]);
    TEST_ASSERT_EQUAL_PTR(ref_ptr[0], ptr[3]);

    TEST_ASSERT_EQUAL_PTR(chunk[0], mcb.mb_mpu[0].head);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[0], mcb.mem2mb[0]);

    TEST_ASSERT_EQUAL_UINT64(len[3], chunk[0]->len);
    TEST_ASSERT_EQUAL_UINT64(len[1], chunk[1]->len);
    TEST_ASSERT_EQUAL_UINT64(len[2], chunk[2]->len);

    TEST_ASSERT_EQUAL_UINT(0, chunk[0]->free);
    TEST_ASSERT_EQUAL_UINT(0, chunk[1]->free);
    TEST_ASSERT_EQUAL_UINT(0,  chunk[2]->free);

    TEST_ASSERT_EQUAL_PTR(chunk[0]->lle.next, &chunk[3]->lle);
    TEST_ASSERT_EQUAL_PTR(chunk[3]->lle.next, &chunk[1]->lle);

    TEST_ASSERT_EQUAL_PTR(thread_mb_list.next, &mcb.mb_mpu[0].ll_item_thread);
    TEST_ASSERT_EQUAL_UINT(mcb.mb_raw_blocksize - len[0] - len[1] - len[2]  - 4 * sizeof(mch_head_t),
                           free_chunk->len);
    TEST_ASSERT_EQUAL_UINT(len[0] - len[3] -  sizeof(mch_head_t),
                           chunk[3]->len);

    TEST_ASSERT_EQUAL_UINT(1, chunk[3]->free);
    TEST_ASSERT_EQUAL_UINT(1,  free_chunk->free);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root, &chunk[3]->node);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root->lchild, &free_chunk->node);
    TEST_ASSERT_EQUAL_PTR(mcb.mem2mb[0]->bheap.root->rchild, 0);

    TEST_ASSERT_EQUAL_UINT32_ARRAY(ref_mpu_thread_conf, thread_mpu_conf,
                                   (_Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX) * 2);
}

void init_and_alloc_3000b_dyn_memory_test(void)
{
    const uint n = 1;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {3000};
    mch_head_t *chunk[n];

    CLEAR_OBJ(&ref_memcb);

    mpu_rp_t ref_mpu_thread_conf[_Q_MPU_STACK_REGIONS + _Q_MPU_DYNAMIC_REGIONS];
    CLEAR_ARRAY(ref_mpu_thread_conf);
    for (uint i =  _Q_MPU_HEAP_PREF_IDX; i < _Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX; i++) {
        ref_mpu_thread_conf[i].attr_size = 0x0000FF00;
        thread_mpu_conf[i].attr_size = 0x0000FF00;
    }
    ref_mpu_thread_conf[_Q_MPU_HEAP_PREF_IDX + 0].attr_size = 0x0000FC00;

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();

    ref_ptr[0] = (void*)(mcb.mb_mpu_start + sizeof( mch_head_t));
//    ref_ptr[1] = (void*)((size_t)ref_ptr[0] + sizeof( mch_head_t) + len[0]);
    for (uint i = 0; i < n; i++) {
        chunk[i] = (mch_head_t *)(ref_ptr[i] - sizeof( mch_head_t));
    }
    mch_head_t *free_chunk = (mch_head_t *)((size_t)ref_ptr[n-1]  + len[n-1]);

    ptr[0] = qalloc_dynmem(len[0], &thread_mb_list, thread_mpu_conf);
//    ptr[1] = qalloc_handler(len[1], &thread_mb_list, thread_mpu_conf);


    TEST_ASSERT_EQUAL_UINT64(ref_memcb.low_border, mcb.low_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.high_border, mcb.high_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_start, mcb.mb_mpu_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mpu_area_start, mcb.mpu_area_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_end, mcb.mb_mpu_end);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memcb.memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
    TEST_ASSERT_EQUAL_PTR(thread_mb_list.next, &mcb.mb_mpu[0].ll_item_thread);

    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[0], mcb.mem2mb[0]);
    TEST_ASSERT_EQUAL_PTR(&mcb.mb_mpu[0], mcb.mem2mb[1]);
    TEST_ASSERT_EQUAL_PTR(0, mcb.mem2mb[2]);

    TEST_ASSERT_EQUAL_PTR(ref_ptr[0], ptr[0]);
//    TEST_ASSERT_EQUAL_PTR(ref_ptr[1], ptr[1]);

    TEST_ASSERT_EQUAL_PTR(chunk[0], mcb.mb_mpu[0].head);


    TEST_ASSERT_EQUAL_UINT64(len[0], chunk[0]->len);
//    TEST_ASSERT_EQUAL_UINT64(len[1], chunk[1]->len);

//    TEST_ASSERT_EQUAL_UINT64((size_t)chunk[0]+ sizeof(mch_head_t)+chunk[0]->len, (size_t)chunk[1]);
    TEST_ASSERT_EQUAL_UINT64((size_t)chunk[0] + sizeof(mch_head_t)+chunk[0]->len, (size_t)free_chunk);

    TEST_ASSERT_EQUAL_UINT64(free_chunk->len, mcb.mb_mpu[0].bheap.root->key);
    TEST_ASSERT_EQUAL_UINT64(0, mcb.mb_mpu[0].bheap.root->lchild);
    TEST_ASSERT_EQUAL_UINT64(0, mcb.mb_mpu[0].bheap.root->rchild);

    TEST_ASSERT_EQUAL_PTR(chunk[0]->lle.next, &free_chunk->lle);


    TEST_ASSERT_EQUAL_UINT(mcb.mb_raw_blocksize * 2 - len[0]  - (n + 1) * sizeof(mch_head_t),
                           free_chunk->len);

    TEST_ASSERT_EQUAL_UINT32_ARRAY(ref_mpu_thread_conf, thread_mpu_conf,
                                   (_Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX) * 2);

}

void init_and_alloc_3000b_200b_free_map_0_1_dyn_memory_test(void)
{
    const uint n = 2;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {3000, 200};

    CLEAR_OBJ(&ref_memcb);

    mpu_rp_t ref_mpu_thread_conf[_Q_MPU_STACK_REGIONS + _Q_MPU_DYNAMIC_REGIONS];
    CLEAR_ARRAY(ref_mpu_thread_conf);
    for (uint i =  _Q_MPU_HEAP_PREF_IDX; i < _Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX; i++) {
        ref_mpu_thread_conf[i].attr_size = 0x0000FF00;
        thread_mpu_conf[i].attr_size = 0x0000FF00;
    }
    ref_mpu_thread_conf[_Q_MPU_HEAP_PREF_IDX + 0].attr_size = 0x0000FF00;

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();

    ref_ptr[0] = (void*)(mcb.mb_mpu_start + sizeof( mch_head_t));
    ref_ptr[1] = (void*)((size_t)ref_ptr[0] + sizeof( mch_head_t) + len[0]);

    ptr[0] = qalloc_dynmem(len[0], &thread_mb_list, thread_mpu_conf);
    ptr[1] = qalloc_dynmem(len[1], &thread_mb_list, thread_mpu_conf);

    _q_free_main_mem(ptr[0], &thread_mb_list, thread_mpu_conf);
    _q_free_main_mem(ptr[1], &thread_mb_list, thread_mpu_conf);


    TEST_ASSERT_EQUAL_UINT64(ref_memcb.low_border, mcb.low_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.high_border, mcb.high_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_start, mcb.mb_mpu_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mpu_area_start, mcb.mpu_area_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_end, mcb.mb_mpu_end);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memcb.memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);
    TEST_ASSERT_EQUAL_PTR(&thread_mb_list, thread_mb_list.next);

    TEST_ASSERT_EQUAL_PTR(0, mcb.mem2mb[0]);
    TEST_ASSERT_EQUAL_PTR(0, mcb.mem2mb[1]);
    TEST_ASSERT_EQUAL_PTR(0, mcb.mem2mb[2]);

    TEST_ASSERT_EQUAL_UINT32_ARRAY(ref_mpu_thread_conf, thread_mpu_conf,
                                   (_Q_MPU_DYNAMIC_REGIONS+ _Q_MPU_HEAP_PREF_IDX) * 2);

}

void init_qalloc_uniq_1_test(void)
{
    const uint n = 1;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {256};
    size_t idx[n];

    CLEAR_OBJ(&ref_memcb);

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();


    idx[0]  = (ref_memcb.low_border - ram_origin) / _Q_MMAP_CHUNKLEN;
    ref_memcb.memmap[idx[0] / _Q_MMAP_CH_IN_BL] &=  ~(1 << (idx[0] % _Q_MMAP_CH_IN_BL));
    ref_ptr[0] = (void*)(ram_origin + _Q_MMAP_CHUNKLEN* idx[0]);

    ptr[0] = qalloc_uniqmem(len[0], thread_stack, thread_mpu_conf);

    TEST_ASSERT_EQUAL_UINT64(ref_memcb.low_border, mcb.low_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.high_border, mcb.high_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_start, mcb.mb_mpu_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mpu_area_start, mcb.mpu_area_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_end, mcb.mb_mpu_end);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memcb.memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);

    TEST_ASSERT_EQUAL_PTR(ref_ptr[0], ptr[0]);
    TEST_ASSERT_EQUAL_UINT64((size_t)ptr[0], thread_stack->base);
    TEST_ASSERT_EQUAL_UINT64(len[0], thread_stack->len);
}

void init_qalloc_uniq_1__free_test(void)
{
    const uint n = 1;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {256};
    size_t idx[n];
    mpu_rp_t mpu_conf = {0x45, 0x56};

    CLEAR_OBJ(&ref_memcb);

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();


    idx[0]  = (ref_memcb.low_border - ram_origin) / _Q_MMAP_CHUNKLEN;
//    ref_memcb.memmap[idx[0] / _Q_MMAP_CH_IN_BL] &=  ~(1 << (idx[0] % _Q_MMAP_CH_IN_BL));
    ref_ptr[0] = (void*)(ram_origin + _Q_MMAP_CHUNKLEN* idx[0]);

    ptr[0] = qalloc_uniqmem(len[0], thread_stack, thread_mpu_conf);
    _q_free_residual_mem(0, thread_stack, &mpu_conf);

    TEST_ASSERT_EQUAL_UINT64(ref_memcb.low_border, mcb.low_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.high_border, mcb.high_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_start, mcb.mb_mpu_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mpu_area_start, mcb.mpu_area_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_end, mcb.mb_mpu_end);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memcb.memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);

//    TEST_ASSERT_EQUAL_PTR(ref_ptr[0], ptr[0]);
    TEST_ASSERT_EQUAL_UINT64(0, thread_stack->base);
}


void init_qalloc_uniq__free_test(void)
{
    const uint n = 1;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {_Q_MMAP_CHUNKLEN*17};
    size_t idx[n];

    CLEAR_OBJ(&ref_memcb);

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();

    idx[0]  = (ref_memcb.mb_mpu_end - ram_origin) / _Q_MMAP_CHUNKLEN;
    for (uint i = idx[0]; i < idx[0] + len[0] / _Q_MMAP_CHUNKLEN; i++) {
        ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] &=  ~(1 << (i % _Q_MMAP_CH_IN_BL));
    }
    ref_ptr[0] = (void*)(ram_origin + _Q_MMAP_CHUNKLEN * idx[0]);

    ptr[0] = qalloc_uniqmem(len[0], thread_stack, thread_mpu_conf);
//    _q_free_residual_mem(0, &thread_stack);

    TEST_ASSERT_EQUAL_UINT64(ref_memcb.low_border, mcb.low_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.high_border, mcb.high_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_start, mcb.mb_mpu_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mpu_area_start, mcb.mpu_area_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_end, mcb.mb_mpu_end);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memcb.memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);

    TEST_ASSERT_EQUAL_PTR(ref_ptr[0], ptr[0]);
    TEST_ASSERT_EQUAL_UINT64((size_t)ptr[0], thread_stack->base);
    TEST_ASSERT_EQUAL_UINT64(len[0], thread_stack->len);
}

void init_qalloc_uniq_128k_test(void)
{
    const uint n = 1;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {1024*128};
    size_t idx[n];

    CLEAR_OBJ(&ref_memcb);

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= ref_memcb.mb_mpu_end) && (addr < ref_memcb.high_border))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }

    qos_dyn_memory_init();


    idx[0]  = (ref_memcb.low_border - ram_origin) / _Q_MMAP_CHUNKLEN;
//    ref_memcb.memmap[idx[0] / _Q_MMAP_CH_IN_BL] &=  ~(1 << (idx[0] % _Q_MMAP_CH_IN_BL));
    ref_ptr[0] = (void*)0;

    ptr[0] = qalloc_uniqmem(len[0], thread_stack, thread_mpu_conf);

    TEST_ASSERT_EQUAL_UINT64(ref_memcb.low_border, mcb.low_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.high_border, mcb.high_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_start, mcb.mb_mpu_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mpu_area_start, mcb.mpu_area_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_end, mcb.mb_mpu_end);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memcb.memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);

    TEST_ASSERT_EQUAL_PTR(ref_ptr[0], ptr[0]);
    TEST_ASSERT_EQUAL_UINT64((size_t)ptr[0], thread_stack->base);
    TEST_ASSERT_EQUAL_UINT64(0, thread_stack->len);
}

void init_qalloc_uniq_64k_test(void)
{
    const uint n = 1;
    const uint need_blocks = 1024*64 / _Q_MMAP_CHUNKLEN;
    void *ptr[n];
    void *ref_ptr[n];
    MemCB_t ref_memcb;
    size_t len[] = {1024*64};
    size_t idx[n];

    CLEAR_OBJ(&ref_memcb);

    ref_memcb.low_border = _sdata + _Data_Size + _Bss_Size;
    ref_memcb.high_border = ram_origin + _RAM_SIZE - 256 * 3;
    ref_memcb.mb_mpu_start = ram_origin + ram_size / 4;
    ref_memcb.mpu_area_start = ram_origin + ram_size / 4;
    ref_memcb.mb_mpu_end = ref_memcb.mpu_area_start + ram_size / 2;
    ref_memcb.mem2mb[0] = &ref_memcb.mb_mpu[0];
    ref_memcb.mb_raw_blocksize = ram_size / (2 *  _Q_MPU_DYN_BLOCK_NUM);

    uint possess_blocks = (ref_memcb.high_border - ref_memcb.mb_mpu_end) / _Q_MMAP_CHUNKLEN;
    uint add_blocks = need_blocks - possess_blocks;
    uint add_blocks_round = ceil_round_to_x(add_blocks, ref_memcb.mb_raw_blocksize / _Q_MMAP_CHUNKLEN);

    for (uint i = 0; i < _Q_MMAP_BLOCKNUM; i++) {
        size_t addr = ram_origin + i * _Q_MMAP_CHUNKLEN;
        if (((addr >= ref_memcb.low_border) && (addr < ref_memcb.mb_mpu_start)) ||
             ((addr >= (ref_memcb.mb_mpu_end - add_blocks_round * _Q_MMAP_CHUNKLEN)) && (addr < (ref_memcb.mb_mpu_end - add_blocks * _Q_MMAP_CHUNKLEN)))) {
            ref_memcb.memmap[i / _Q_MMAP_CH_IN_BL] |= 1 << (i % _Q_MMAP_CH_IN_BL);
        }
    }
    ref_memcb.mb_mpu_end -= add_blocks_round * _Q_MMAP_CHUNKLEN;

    qos_dyn_memory_init();


//    idx[0]  = (ref_memcb.low_border - ram_origin) / _Q_MMAP_CHUNKLEN;
//    ref_memcb.mb_mpu_end &=  ~(1 << (idx[0] % _Q_MMAP_CH_IN_BL));
    ref_ptr[0] = (void*)(ref_memcb.mb_mpu_end + _Q_MMAP_CHUNKLEN * (add_blocks_round -add_blocks ));

    ptr[0] = qalloc_uniqmem(len[0], thread_stack, thread_mpu_conf);

    TEST_ASSERT_EQUAL_UINT64(ref_memcb.low_border, mcb.low_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.high_border, mcb.high_border);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_start, mcb.mb_mpu_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mpu_area_start, mcb.mpu_area_start);
    TEST_ASSERT_EQUAL_UINT64(ref_memcb.mb_mpu_end, mcb.mb_mpu_end);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(ref_memcb.memmap, mcb.memmap, _Q_MMAP_GR_BLOCKNUM);

    TEST_ASSERT_EQUAL_PTR(ref_ptr[0], ptr[0]);
    TEST_ASSERT_EQUAL_UINT64((size_t)ptr[0], thread_stack->base);
    TEST_ASSERT_EQUAL_UINT64(len[0], thread_stack->len);
}



int main(void)
{
    UNITY_BEGIN();

    RUN_TEST(init_and_alloc_dyn_memory_test);
    RUN_TEST(init_and_alloc_free_dyn_memory_test);

    RUN_TEST(init_and_alloc_2_dyn_memory_test);
    RUN_TEST(init_and_alloc_3_dyn_memory_test);
    RUN_TEST(init_and_alloc_3_free_map_0_dyn_memory_test);
    RUN_TEST(init_and_alloc_3_free_map_1_dyn_memory_test);
    RUN_TEST(init_and_alloc_3_free_map_2_dyn_memory_test);
    RUN_TEST(init_and_alloc_3_free_map_1_2_dyn_memory_test);
    RUN_TEST(init_and_alloc_3_free_map_0_alloc_60b_dyn_memory_test);

    RUN_TEST(init_and_alloc_3000b_dyn_memory_test);
    RUN_TEST(init_and_alloc_3000b_200b_free_map_0_1_dyn_memory_test);

    RUN_TEST(init_qalloc_uniq_1_test);
    RUN_TEST(init_qalloc_uniq_1__free_test);
    RUN_TEST(init_qalloc_uniq__free_test);
    RUN_TEST(init_qalloc_uniq_128k_test);
    RUN_TEST(init_qalloc_uniq_64k_test);

    return UNITY_END();

}
