import qbs
import qbs.Environment
import "mcu_spec.js" as PREF
//import "unit_tests/qmav_unit_tests/qmav_unit_tests.qbs" as UTESTS


Project{


//    AutotestRunner {name: "binary_heap_tests"}
    Product {

        property var mcu_type: "f407"
        files: [
            "inc/ekf.h",
            "inc/qmav_math.h",
            "inc/stm32f4xx_conf.h",
            "rtos/inc/misc/qos_bin_heap.h",
            "rtos/inc/misc/qos_mem_misc.h",
            "rtos/inc/misc/qos_string.h",
            "rtos/inc/qos_core.h",
            "rtos/inc/qos_debug.h",
            "rtos/inc/qos_def.h",
            "rtos/inc/qos_drivers/dma_handlers.h",
            "rtos/inc/qos_drivers/general.h",
            "rtos/inc/qos_drivers/i2c.h",
            "rtos/inc/qos_drivers/levelman.h",
            "rtos/inc/qos_drivers/mcu_f303_gpio.h",
            "rtos/inc/qos_drivers/mcu_f407_gpio.h",
            "rtos/inc/qos_drivers/spi.h",
            "rtos/inc/qos_drivers/stm_types.h",
            "rtos/inc/qos_drivers/uart.h",
            "rtos/inc/qos_itc.h",
            "rtos/inc/qos_ln_list.h",
            "rtos/inc/qos_memory.h",
            "rtos/inc/qos_scheduler.h",
            "rtos/inc/qos_sys_functions.h",
            "rtos/inc/qos_threads.h",
            "rtos/inc/qos_timers.h",
            "rtos/inc/qos_utils.h",
            "rtos/qos_test.c",
            "rtos/qos_test.h",
            "rtos/qos_test_gpio_macro.c",
            "rtos/qos_test_gpio_macro.h",
            "rtos/src/asm_routines.S",
            "rtos/src/misc/qos_bin_heap.c",
            "rtos/src/misc/qos_mem_misc.c",
            "rtos/src/misc/qos_string.c",
            "rtos/src/qos_core.c",
            "rtos/src/qos_debug.c",
            "rtos/src/qos_drivers/dma_handlers.c",
            "rtos/src/qos_drivers/general.c",
            "rtos/src/qos_drivers/i2c.c",
            "rtos/src/qos_drivers/levelman.c",
            "rtos/src/qos_drivers/spi.c",
            "rtos/src/qos_drivers/uart.c",
            "rtos/src/qos_itc.c",
            "rtos/src/qos_memory.c",
            "rtos/src/qos_scheduler.c",
            "rtos/src/qos_threads.c",
            "rtos/src/qos_timers.c",
            "rtos/src/qos_utils.c",
            "sensors/inc/vl53l0x.h",
            "sensors/inc/vl53l0x_platform.h",
            "sensors/inc/vl53l0x_platform_log.h",
            "sensors/inc/vl53l0x_types.h",
            "sensors/src/vl53l0x.c",
            "sensors/src/vl53l0x_platform.c",
            "src/ekf.c",
            "src/main.c",
            "inc/main.h",
            "inc/stm32f30x_conf.h",
            "src/qmav_math.c",
        ]
        name: "Qmav"

        type: ["application", "hex"]
        Depends{name: "qos_struct_offsets"}
        Depends{name:"cpp"}


//        cpp.assemblerFlags: ["-x assembler-with-cpp"]
        cpp.includePaths: PREF.mcu_def(mcu_type).inc
        cpp.libraryPaths: PREF.mcu_def(mcu_type).lib
        cpp.staticLibraries: ["arm_cortexM4lf_math", "m"]

        cpp.positionIndependentCode: false

        cpp.optimization: "none"
        cpp.useRPaths: false

        cpp.defines: PREF.mcu_def(mcu_type).def
        cpp.commonCompilerFlags: baseCompilerFlags.concat(warningCompilerFlags )
        cpp.linkerFlags: linkerFlags

        cpp.driverFlags: [
            "-mcpu=cortex-m4",
            "-mthumb",
            "-mfloat-abi=hard",
            "-mfpu=fpv4-sp-d16",
//            "-pipe",
            "-march=armv7e-m",
             "--specs=nosys.specs"
        ]


        property var baseCompilerFlags: [

            "-c",
            "-g3",
//            "-pipe",
            "-O0",
            "-munaligned-access",
//            "-std=gnu99",
//            "-Wstrict-prototypes",
            "-Wundef",
            "-Wshadow",
            "-fno-builtin",
//            "-g",
//            "-fno-common",
            "-fno-strict-aliasing",
            "-ffunction-sections",
            "-fdata-sections",
            "-fstack-usage",
//            "--param max-inline-insns-single=500",
            "-nostdlib",
//            "-MMD",
//            "-mlittle-endian",

            "-mcpu=cortex-m4",
            "-mthumb",
//            "-arch=armv7e-m",
            "-mfloat-abi=hard",
            "-fsingle-precision-constant",
            "-mfpu=fpv4-sp-d16"


        ]

        property var warningCompilerFlags: [

            "-Wextra",
            "-Wno-comment",
            "-Wconversion",
            "-Wpointer-arith",
            '-Wimplicit-int',
            "-Wfloat-conversion",
            "-Wsign-conversion",
            "-Wcast-qual",
            "-Wredundant-decls",
            "-Wdouble-promotion",
            "-Wnull-dereference",
            "-Wno-unused-parameter",
//            "-Wno-unused-variable"
    ]


        property var linkerFlags: [

            //            "-Wl,--entry=Reset_Handler",
//                        "--static",
            //            "-lc",
            //            "-lnosys",
                        "--cref",
                        "-Map=" + buildDirectory + "/tut.map",
//                        "-mcpu=cortex-m4",
//                        "-mthumb",
//                        "-mfloat-abi=hard",
//                        "-mfpu=fpv4-sp-d16",
            //            "-pipe",
                        "--gc-sections",
                        "--start-group",
//                        "-march=armv7e-m",

            //            "-print-multi-lib"
                        "-nostdlib"
        ]
//        FileTagger {
//            patterns: "*.S"
//            fileTags: ["asm"]
//        }

            Group {
                name: "linker script"
                files: PREF.mcu_def(mcu_type).ls
                fileTags: ["linkerscript"]
            }


            Group {
                name: "CMSIS Startup"
                prefix: PREF.mcu_def(mcu_type).root
                files: PREF.mcu_def(mcu_type).startup
//                fileTags: ["staticlibrary"]
            }

            Group {
                name: "SPL"
                prefix: PREF.mcu_def(mcu_type).perifRoot
                files: PREF.mcu_def(mcu_type).spl
                cpp.warningLevel: "none"

        }

            Group {
                name: "system"
                prefix: PREF.mcu_def(mcu_type).sysRoot
                files: PREF.mcu_def(mcu_type).sys
                cpp.warningLevel: "none"

            }


            Group {
                name: "VL headers"
                prefix: "/home/alex/documents/c/control/optical/laser/VL53L0X_1.0.2/Api/core/inc/"
                files: [
                    "*.h"
                ]
                cpp.warningLevel: "none"
            }
            Group {
                name: "VL source"
                prefix: "/home/alex/documents/c/control/optical/laser/VL53L0X_1.0.2/Api/core/src/"
                files: [
                    "*.c"
                ]
                cpp.warningLevel: "none"
            }



        targetName:{
                    var tn = name
                    tn = tn + ".elf"
                    return tn
                }




        Rule {
            id: bindary
            inputs: ["application"]
            Artifact {
                            fileTags: ['hex']
                            filePath: product.name + '.hex'
                        }
            prepare: {
                var sizePath = "arm-none-eabi-size";
                var argsSize = [input.filePath];
                var cmdSize = new Command(sizePath, argsSize);
                cmdSize.description = "Size of sections:";
                cmdSize.highlight = "linker";

                var args = ["-O",
                            "ihex",
                            input.filePath,
                            output.filePath]
                var objcopy = "arm-none-eabi-objcopy"
                var cmdBin = new Command(objcopy, args)
                cmdBin.description = "Make binary"
                cmdBin.highlight = "linker"
                return [cmdSize, cmdBin]


            }
        }
    }
    Product {
        name: "qos_struct_offsets"
        type: ["h"]
        files: [
            "rtos/src/qos_src_struct_offsets.c",
        ]

        FileTagger {
            patterns: "*.c"
            fileTags: ["c"]
        }

        Rule {
            inputs: ["c"]
            Artifact {
                fileTags: ["s"]
                filePath: product.targetName + ".s"
            }
            prepare: {
                var compiler = "arm-none-eabi-gcc"
                var args = PREF.gen_includes(PREF.mcu_def("f407").inc)
                var add_args = ["-S",
                            input.filePath,
                                "-D",
                                "STM32F40_41xxx",
                            "-o",
                            output.filePath]
                args =  args.concat(add_args)
                var cmd = new Command(compiler, args)
                cmd.description = product.name + " compiling "
                cmd.silent = false;
                cmd.workingDirectory = project.sourceDirectory
                return cmd
            }
        }
        Rule {
            inputs: ["s"]
            Artifact {
                fileTags: "h"
                filePath: project.sourceDirectory + "/rtos/inc/" + product.name + ".h"
            }
            prepare: {
                var script = "bash"
                var args = [project.sourceDirectory + "/rtos/aux/as_define_replace.sh",
                            input.filePath,
                            output.filePath]
                var cmd = new Command(script, args)
                cmd.description = product.name + " parsing"
                cmd.silent = false;
                return cmd
            }
        }

    }

}
