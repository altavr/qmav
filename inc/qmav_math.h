#ifndef QMAV_MATH_H
#define QMAV_MATH_H

#include <arm_math.h>
#include "misc/qos_string.h"

#define CHECK_ARM_MATH(f) if ((arm_math_status = f) != ARM_MATH_SUCCESS) return arm_math_status
#define PRINT_ARM_M_STATUS(f) if (0 != f) q_printf("arm math error\r\n")

arm_status q_rotate_vector(arm_matrix_instance_f32 *vector, arm_matrix_instance_f32 *new_vector, arm_matrix_instance_f32 *q);
arm_status mult_quternions(arm_matrix_instance_f32 *qLeft, arm_matrix_instance_f32 *qRight, arm_matrix_instance_f32 *q);
void q2R(arm_matrix_instance_f32 *srcq, arm_matrix_instance_f32 *dstR);
arm_status integrate_mid_point(arm_matrix_instance_f32 *w_vect, arm_matrix_instance_f32 *w_dt_vect, arm_matrix_instance_f32 *q_delta);
arm_status normalize(arm_matrix_instance_f32 *vSrc, arm_matrix_instance_f32 *vNorm);
arm_status normalize_in_place(arm_matrix_instance_f32 *v);
void q_from_vectors(arm_matrix_instance_f32 *u, arm_matrix_instance_f32 *v, arm_matrix_instance_f32 *q);
void euler_to_quaternion(arm_matrix_instance_f32 *eu_angles, arm_matrix_instance_f32 *q);
void quternion_to_euler(arm_matrix_instance_f32 *q, arm_matrix_instance_f32 *eu_angles);
void vector_to_skew_symm_mat(arm_matrix_instance_f32 *v, arm_matrix_instance_f32 *mat);
void rot_mat_to_q(arm_matrix_instance_f32 *R, arm_matrix_instance_f32 *q);
void matrix_insert(arm_matrix_instance_f32 *srcMat, uint8_t row, uint8_t col, arm_matrix_instance_f32 *dstMat);
void jacobian_qxvxq(arm_matrix_instance_f32 *q, arm_matrix_instance_f32 *v, arm_matrix_instance_f32 *J);
void fill_diagonal_matrix(arm_matrix_instance_f32 *mat, float val);



void ekf_math_func_test(void);
void matrix_print(arm_matrix_instance_f32 *m);
float invSqrt(float x);


#endif // QMAV_MATH_H
