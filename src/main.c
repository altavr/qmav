#include "main.h"
#include "qos_threads.h"
#include "qos_core.h"
#include "qos_sys_functions.h"
#include "qos_debug.h"
#include "qos_drivers/general.h"
#include "misc/qos_string.h"
#include "qos_test.h"
#include "qos_drivers/levelman.h"
#include "vl53l0x.h"
#include "assert.h"

//#include "stm32f30x_rcc.h"

uint8_t data_spi[3];

//SCB_Type* scb;
RCC_TypeDef * ff ;
 RCC_TypeDef *rcc_;
 GPIO_TypeDef *port_b, *port_a;
 I2C_TypeDef* i2c2;

uint32_t dt1, dt2, dt3;
extern q_th_t *next_run_thread;
//uint32_t exc_acc_res[2];

q_th_t *th1, *th2, *th3;
q_th_t *th0;
extern uint32_t sysTickNum;

q_mutex_t mutex;
volatile uint32_t mpu_counter;


/*
void gyro_i2c_test_thread(void)
{
    int rd = q_open(Q_USART1);
    char s[60];
    int len;

    int rd_gyro = q_open(Q_I2C2);

    uint8_t r_buf[10];
    uint8_t data_w_r[10];
    uint8_t *data = &data_w_r[1];

    q_conf_i2c_t conf;

    q_snprintf(s, 60, "Systick - %d\r\n", sysTickNum);
    len = q_strlen(s);
    q_write(rd, (uint8_t*)s, len);

    conf.dev = 0x68;
    q_ioctl(rd_gyro, (void*)&conf);

    uint8_t MPU6050_RA_CONFIG = 0x1A;
    conf.reg = MPU6050_RA_CONFIG;
    data_w_r[0] = MPU6050_RA_CONFIG;

    q_read(rd_gyro, r_buf, 1);
    uint8_t backup = r_buf[0];
    q_snprintf(s, 60, "Before write MPU6050_RA_CONFIG - %d\r\n", r_buf[0]);
    len = q_strlen(s);
    q_write(rd, (uint8_t*)s, len);

    data[0] = 0x03;
    q_write(rd_gyro, data_w_r, 2);
    data[0] = 99;
    q_read(rd_gyro, r_buf, 1);
    q_snprintf(s, 60, "After write MPU6050_RA_CONFIG - %d\r\n", r_buf[0]);
    len = q_strlen(s);
    q_write(rd, (uint8_t*)s, len);

    data[0] = backup;
    q_write(rd_gyro, data_w_r, 2);

    q_close(rd_gyro);
    q_close(rd);


}

void thread_1(void)
{
    int len;
    int rd = q_open(Q_USART1);
    char s[100];

    int rd_gyro = q_open(Q_I2C2);
    uint8_t data[10];
    q_conf_i2c_t conf;

    conf.dev = 0x68;
    conf.reg = 107;
    q_ioctl(rd_gyro, (void*)&conf);

    data[0] = 107;
    data[1] = 0;
    q_write(rd_gyro, data, 2);
    data[0] = 108;
    data[1] = 0;
    q_write(rd_gyro, data, 2);

    conf.reg = 0x75;
    q_read(rd_gyro, data, 1);
    q_snprintf(s, 100, "TREAD_1::ICM-20689 reply - %d, top stack - %d\r\n", data[0], qos_threads_pool[1].stack_top);
    len = q_strlen(s);
//    q_mutex_lock(&mutex);
    q_write(rd, (uint8_t*)s, len);


    for (int i= 0; i < 10; i++) {
        conf.reg = 65;
        q_read(rd_gyro, data, 2);
        q_snprintf(s, 100, "ICM-20689 temp high - %d, temp low - %d ____\r\n", data[0], data[1]);
        len = q_strlen(s);
        q_write(rd, (uint8_t*)s, len);
        q_delay(100);
    }

//    q_mutex_unlock(&mutex);
    q_close(rd_gyro);
    q_close(rd);
}
*/
/*
void thread_2(void)
{
    int rd = q_open(Q_USART3);
    char s[100];

    int rd_gyro = q_open(Q_I2C2);
    uint8_t data[10];
    q_conf_i2c_t conf;

    conf.dev = 0x68;
    conf.reg = 0x75;
    q_ioctl(rd_gyro, (void*)&conf);

    q_read(rd_gyro, data, 1);
    q_snprintf(s, 100, "______TREAD_2::MPU9250 reply - %d, top stack - %d____\r\n", data[0], qos_threads_pool[2].stack_top);
    int len = q_strlen(s);

    q_mutex_lock(&mutex);
    q_write(rd, s, len);
    q_mutex_unlock(&mutex);

    q_close(rd_gyro);
    q_close(rd);
}


void thread_6(void)
{
    int rd = q_open(Q_USART3);
    char s[100];

    int rd_gyro = q_open(Q_I2C2);
    uint8_t data[10];
    q_conf_i2c_t conf;

    conf.dev = 0x68;
    conf.reg = 0x75;
    q_ioctl(rd_gyro, (void*)&conf);

    q_read(rd_gyro, data, 1);
    q_snprintf(s, 100, "______TREAD_6::MPU9250 reply - %d, top stack - %d____\r\n", data[0], qos_threads_pool[2].stack_top);
    int len = q_strlen(s);

    q_mutex_lock(&mutex);
    q_write(rd, s, len);
    q_mutex_unlock(&mutex);

    q_close(rd_gyro);
    q_close(rd);
}

void thread_7(void)
{
    int rd = q_open(Q_USART3);
    char s[100];

    int rd_gyro = q_open(Q_I2C2);
    uint8_t data[10];
    q_conf_i2c_t conf;

    conf.dev = 0x68;
    conf.reg = 0x75;
    q_ioctl(rd_gyro, (void*)&conf);

    q_read(rd_gyro, data, 1);
    q_snprintf(s, 100, "______TREAD_7::MPU9250 reply - %d, top stack - %d____\r\n", data[0], qos_threads_pool[2].stack_top);
    int len = q_strlen(s);

    q_delay(1);
    q_mutex_lock(&mutex);
    q_write(rd, s, len);
    q_mutex_unlock(&mutex);

    q_close(rd_gyro);
    q_close(rd);
}


void thread_4(void)
{
    int rd = q_open(Q_USART3);
    char s[100];

    int rd_gyro = q_open(Q_I2C2);
    uint8_t data[10];
    q_conf_i2c_t conf;

    conf.dev = 0x68;
    conf.reg = 0x75;
    q_ioctl(rd_gyro, (void*)&conf);

    q_read(rd_gyro, data, 1);
    q_snprintf(s, 100, "TREAD_4::MPU9250 reply - %d, top stack - %d____\r\n", data[0], qos_threads_pool[2].stack_top);
    int len = q_strlen(s);

//    q_mutex_lock(&mutex);
    q_write(rd, s, len);
//    q_mutex_unlock(&mutex);

    q_close(rd_gyro);
    q_close(rd);
}

void thread_5(void)
{
    int rd = q_open(Q_USART3);
    char s[100];
    int len;

    while (1) {

        for (uint32_t i = 0; i < 5; i++) {
            q_mutex_lock(&mutex);
            q_snprintf(s, 100, "%s - %d, dt - %d, top stack - %d\r\n", "_THREAD 5_", sysTickNum, sysTickNum - dt3,
                       qos_threads_pool[3].stack_top);
            dt3 = sysTickNum;
            len = q_strlen(s);
            q_write(rd, s, len);
            q_mutex_unlock(&mutex);
        }
    q_delay(20);
    }

    q_close(rd);

}

void thread_3(void)
{
    int rd = q_open(Q_USART3);
    int len;
    char s[100];
    while (1) {
        q_mutex_lock(&mutex);
        for (uint32_t i = 0; i < 100; i++) {

            q_snprintf(s, 100, "%s - %d, dt - %d, top stack - %d\r\n", "Simple string", sysTickNum, sysTickNum - dt3,
                       qos_threads_pool[3].stack_top);
            dt3 = sysTickNum;
            len = q_strlen(s);
            q_write(rd, s, len);
        }
        q_mutex_unlock(&mutex);
    }
    q_close(rd);

}
*/
//void high_lev_thread(void)
//{
//    q_unset_pin_lev(_PB_PIN14);

//}
/*
void uart_print(void)
{

    int rd_gyro = q_open(Q_I2C2);
    uint8_t data[10];
    q_conf_i2c_t conf;

//    q_set_pin_lev(_PB_PIN14);
    conf.dev = 0x68;
    conf.reg = 65;
    q_ioctl(rd_gyro, (void*)&conf);

    q_read(rd_gyro, data, 2);


    int rd = q_open(Q_USART1);
    int len;
    char s[100];

    q_snprintf(s, 100, "TREAD_4::MPU9250 temp high - %d, temp low - %d ____\r\n", data[0], data[1]);
    len = q_strlen(s);
    q_write(rd, (uint8_t*)s, len);
    q_close(rd);
    q_close(rd_gyro);

}
*/
/*
void lora_test(void)
{
    q_set_pin_lev(LORA_RESET_PIN);
    q_set_pin_lev(LORA_SPI_NSS);

//    uint8_t data_spi[2];
    char s[100];
    int len;

    uint8_t LR_RegOpMode = 0x01;
    int lora = q_open(Q_SPI2);
    int console = q_open(Q_USART1);
    q_conf_spi_t conf = {LR_RegOpMode, LORA_SPI_NSS};
    q_ioctl(lora, (void*)&conf);

    while (1) {
        conf.reg = LR_RegOpMode;
        q_read(lora, data_spi, 3);
        q_snprintf(s,
                   100,
                   "LORA LR_RegOpMode and ... content - %d, %d, %d\r\n",
                   data_spi[0], data_spi[1], data_spi[2]);
        len = q_strlen(s);
        q_write(console, (uint8_t*)s, len);
        q_delay(500);

        data_spi[0] = 8;
        conf.reg = LR_RegOpMode | 0x80;
        q_write(lora, data_spi, 1);
        q_delay(10);
        conf.reg = LR_RegOpMode;
        q_read(lora, data_spi, 1);
        q_delay(10);
        q_snprintf(s,
                   100,
                   "LORA LR_RegOpMode after write content - %d\r\n",
                   data_spi[0]);
        len = q_strlen(s);
        q_write(console, (uint8_t*)s, len);

        data_spi[0] = 9;
        conf.reg = LR_RegOpMode | 0x80;
        q_write(lora, data_spi, 1);
        q_delay(500);
        data_spi[0] = 0;
    }
    q_close(lora);
    q_close(console);

}

void lora_ro_test(void)
{
    q_set_pin_lev(LORA_RESET_PIN);
    q_set_pin_lev(LORA_SPI_NSS);

//    uint8_t data_spi[2];
    char s[100];
    int len;

    uint8_t LR_RegOpMode = 0x01;
    int lora = q_open(Q_SPI2);
    int console = q_open(Q_USART1);
    q_conf_spi_t conf = {LR_RegOpMode, LORA_SPI_NSS};
    q_ioctl(lora, (void*)&conf);

    while (1) {
        q_read(lora, data_spi, 3);
        q_snprintf(s,
                   100,
                   "LORA LR_RegOpMode and ... content - %d, %d, %d\r\n",
                   data_spi[0], data_spi[1], data_spi[2]);
        len = q_strlen(s);
        q_write(console, (uint8_t*)s, len);
        q_delay(500);

    }
    q_close(lora);
    q_close(console);

}
*/
/*
void vl_i2c_test(void)
{
    char s[100];
    int len;

//    init_console(q_open(Q_USART1));
    int vl = q_open(Q_I2C2);
    if (vl < 0) {
        qos_error_handler(vl);
    }
    int console = q_open(Q_USART1);
    if (console < 0) {
        qos_error_handler(vl);
    }
    q_conf_i2c_t conf;
    conf.dev = 0x52 >> 1;
    conf.reg = 0xC0;
    uint8_t data[10];
    q_ioctl(vl, &conf);

    q_read(vl, data, 3);

    q_snprintf (s, 100, "VL53L0X - %d, %d, %d\r\n",
               data[0], data[1], data[2]);
    len = q_strlen(s);
    q_write(console, (uint8_t*)s, len);

//    q_printf(  "VL53L0X - %d, %d, %d\r\n",
//               data[0], data[1], data[2]);
    q_close(vl);
    q_close(console);
}


void range_test(void)
{

    int rd = q_open(Q_USART1);
    if (rd < 0) {
        qos_error_handler(rd);
    }
    console_init(rd);

    lidar_init();
    while(1) {
//        rangingTest();
//        rangingTestContinues();
        rangingTestLong();
        q_delay(500);
    }

}
*/
/*
void icm_spi_test(void)
{
    uint8_t data[10];

    int rd = q_open(Q_USART1);
    if (rd < 0) {
        qos_error_handler(rd);
    }
    console_init(rd);
    q_printf("__CONSOLE INIT__\r\n");
    int gyro = q_open(Q_SPI1);
    if (gyro < 0) {
        qos_error_handler(gyro);
    }

    q_delay(10);
    q_conf_spi_t conf = {107, ICM20689_NSS};
    q_ioctl(gyro, (void*)&conf);
    data[0] = 1 << 7;
    q_write(gyro, data, 1);
    q_delay(50);

    conf.reg = 107;
    data[0] = 0;
    q_write(gyro, data, 1);
    q_delay(20);

    conf.reg = 108;
    data[0] = 0;
    q_write(gyro, data, 1);
    q_delay(10);

    conf.reg = 106;
    data[0] = 1 << 4;
    q_write(gyro, data, 1);
    q_delay(10);
    
    while (1) {
        conf.reg = 65 | 0x80;
        q_read(gyro, data, 2);
//        q_printf("ICM20689 Who Am I? reg - %d\r\n", data[0]);
        q_printf("ICM20689 temp high - %d, temp low - %d ____\r\n", data[0], data[1]);

        conf.reg = 117 | 0x80;
        q_read(gyro, data, 1);
        q_printf("ICM20689 Who Am I? reg - %d\r\n", data[0]);
        q_delay(500);
    }
    q_close(rd);
    q_close(gyro);
}
*/
void icm_spi_test(void)
{

    int rd = q_open(Q_USART1);
    int len;
    char s[100];


    uint8_t data[10];
    uint8_t trans_data[10];
    for (uint8_t i=0; i < 10; i++) {
        trans_data[i] = 0;
    }


    int gyro = q_open(Q_SPI1);
    if (gyro < 0) {
        qos_error_handler(gyro);
    }

    q_conf_spi_t conf = {ICM20689_NSS};
    q_ioctl(gyro, (void*)&conf);

    data[0] = 107;
    data[1] = 1 << 7;
    q_rawwrite(gyro, data, 2);
    q_delay(50);

    data[0] = 107;
    data[1] = 0;
    q_rawwrite(gyro, data, 2);
    q_delay(20);

    data[0] = 108;
    data[1] = 0;
    q_rawwrite(gyro, data, 2);
    q_delay(10);

    data[0] = 106;
    data[1] = 1 << 4;
    q_rawwrite(gyro, data, 2);
    q_delay(10);

    while (1) {
        trans_data[0] = 65 | 0x80;
        q_ioswap(gyro, trans_data, data, 3);
        q_snprintf(s, 100, "ICM20689 temp high - %d, temp low - %d ____\r\n", data[1], data[2]);
        len = q_strlen(s);
        q_rawwrite(rd, (uint8_t*)s, len);

        trans_data[0] = 117 | 0x80;
        q_ioswap(gyro, trans_data, data, 2);
        q_snprintf(s, 100, "ICM20689 Who Am I? reg - %d\r\n", data[1]);
        len = q_strlen(s);
        q_rawwrite(rd, (uint8_t*)s, len);
        q_delay(500);
    }
    q_close(rd);
}

void mag3110_test(void)
{
    int rd = q_open(Q_USART1);
    int len;
    char s[100];

    uint8_t data[10];

    int mag = q_open(Q_I2C1);
    if (mag< 0) {
        qos_error_handler(mag);
    }
    q_conf_i2c_t conf;
    conf.dev = 0x0E;
    q_ioctl(mag, (void*)&conf);

    uint8_t WHO_AM_I = 0x7;
    uint8_t CTRL_REG1 = 0x10;
    uint8_t backupreg;

    q_read(mag, CTRL_REG1 , data, 1);
    backupreg = data[0];
    q_delay(100);

    while (1) {
        q_read(mag, WHO_AM_I , data, 1);
        q_snprintf(s, 100, "mag3110 Ident. regs WHO_AM_I - %d\r\n", data[0]);
        len = q_strlen(s);
        q_rawwrite(rd, (uint8_t*)s, len);

        q_read(mag, CTRL_REG1 , data, 1);
        q_snprintf(s, 100, "mag3110 CTRL_REG1 before write - %d\r\n", data[0]);
        len = q_strlen(s);
        q_rawwrite(rd, (uint8_t*)s, len);

        data[0] = CTRL_REG1;
        data[1] = 1 << 3;
        q_rawwrite(mag, data, 2);
        q_delay(100);

        q_read(mag, CTRL_REG1, data, 1);
        q_snprintf(s, 100, "mag3110 CTRL_REG1 after write - %d\r\n", data[0]);
        len = q_strlen(s);
        q_rawwrite(rd, (uint8_t*)s, len);

        q_delay(100);
        data[0] = CTRL_REG1;
        data[1] = backupreg;
        q_rawwrite(mag, data, 2);

        q_delay(500);
    }

    q_close(rd);
    q_close(mag);

}

float chf(float a, float b)
{
    if (a < 0.1F)
    {
        a = 125.6848F;
    }
    for (int i=0; i < 5; i++) {
        a /= 0.1F*b;
    }
    return a * b;
}

void uart_print_1(void)
{

    float f = 1.08646F;
    void *ptr = q_alloc(256);
    void *ptr1 = q_alloc(1024);


    while (1) {

        f = chf(f, 45.2);
        q_delay(100);
        q_delay(100);

        f *= 2.2 ;


        f /= 3 - 0.01;
        q_delay(100);

        q_printf("float - %f, UART Print thread 1 \r\n", (double)f);
        q_printf("ptr - %d\r\n", (u32)ptr);
        q_printf("ptr - %d\r\n", (u32)ptr1);

    }
//    q_close(rd);
    q_delay(400);

}

void uart_print_2(void)
{
    static float f;
    f += 3.;

    q_mutex_lock(&mutex);

    q_printf("float - %f, UART Print _____thread 2 \r\n", (double)chf(f, 35.2));


    q_delay(100);
//    q_close(rd);
    q_mutex_unlock(&mutex);

}

void uart_print_3(void)
{

    static float f;


    while (1) {

    f += 1.;
    q_printf("float - %f, UART Print _____________thread 3 \r\n", (double)f);

    q_delay(200);
    }

}
/*
void ms5611_read_prom(void)
{
    uint8_t data[10];

    int baro = q_open(Q_SPI1);
    if (baro < 0) {
        qos_error_handler(baro);
    }
    q_delay(1000);

    q_conf_spi_t conf = {107, MS5611_NSS};
    q_ioctl(baro, (void*)&conf);
}
*/

int main(void)
{
//    scb = (SCB_Type*)SCB_BASE;
    ff = (RCC_TypeDef *)RCC ;

    rcc_ = RCC;
    port_b = GPIOB;
    port_a = GPIOA;
    i2c2 = I2C2;

//    exc_acc_res[0] = exc_access();
//    exc_acc_res[1] = exc_access_fail();
//    exc_read();

    //**************debug**********************
    q_th_t* thread;
    q_init_io_hw();
//    exc_acc_res[1] = exc_write();

    qos_system_init();
    _qos_close_item_lns(&mutex.lnlist);
/*
    thread = qos_get_new_thread();
    ERR_CHECK();
    thread->period = 10;
    qos_thread_init(thread, 1000, 6, thread_6);
    th2 = thread;

    thread = qos_get_new_thread();
    ERR_CHECK();
    thread->period = 10;
    qos_thread_init(thread, 1000, 6, thread_7);
    th2 = thread;
*/
//    thread = qos_get_new_thread();
//    ERR_CHECK();
//    thread->period = 500;
//    qos_thread_init(thread, 1000, 5, thread_1);
//    th1 = thread;
/*
    thread = qos_get_new_thread();
    ERR_CHECK();
    thread->period = 10;
    qos_thread_init(thread, 1000, 6, thread_2);
    th2 = thread;

    thread = qos_get_new_thread();
    ERR_CHECK();
    thread->period = 0;
    qos_thread_init(thread, 1000, 7, thread_3);
    th3 = thread;

//    thread = qos_get_new_thread();
//    ERR_CHECK();
//    thread->period = 20;
//    qos_thread_init(thread, 1000, 6, thread_4);
//    th2 = thread;

//    thread = qos_get_new_thread();
//    ERR_CHECK();
//    thread->period = 0;
//    qos_thread_init(thread, 1000, 6, thread_5);
//    th2 = thread;
*/

//    thread = qos_get_new_thread();
//    ERR_CHECK();
//    thread->period = 1100;
//    qos_thread_init(thread, 300, 6, gyro_i2c_test_thread);

//    thread = qos_get_new_thread();
//    ERR_CHECK();
//    thread->period = 500;
//    qos_thread_init(thread, 1000, 7, high_lev_thread);


//    thread = qos_get_new_thread();
//    ERR_CHECK();
//    thread->period = 1000;
//    qos_thread_init(thread, 1000, 7, uart_print);

//    thread = qos_get_new_thread();
//    ERR_CHECK();
//    thread->period = 0;
//    qos_thread_init(thread, 1000, 3, lora_test);

//    thread = qos_get_new_thread();
//    ERR_CHECK();
//    thread->period = 1000;
//    qos_thread_init(thread, 1000, 3, vl_i2c_test);


//    thread = qos_get_new_thread();
//    ERR_CHECK();
//    thread->period = 0;
//    qos_thread_init(thread, 1000, 3, lora_ro_test);

//        thread = qos_get_new_thread();
//        ERR_CHECK();
//        thread->period = 0;
//        qos_thread_init(thread, 1000, 3, range_test);

//    th0 = &qos_threads_pool[0];
/*
    thread = qos_get_new_thread();
    th0 = thread;
    ERR_CHECK();
    thread->period = 0;
    qos_thread_init(thread, 1000, 10, icm_spi_test);
    //debug

*/
    /*
    thread = qos_get_new_thread();
    th0 = thread;
    ERR_CHECK();
    thread->period = 0;
    qos_thread_init(thread, 1000, 10, mag3110_test);

*/

//    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
//    DWT->COMP0 = (uint32_t)&next_run_thread;
//    DWT->MASK0 = 0;
//    DWT->FUNCTION0 = (1 << 11) | 0b0110;

    //debug end/
    thread = qos_get_new_thread();
    th1 = thread;
    ERR_CHECK();
    thread->period = 0;
    qos_thread_init(thread, 1000, 20, uart_print_1);


    thread = qos_get_new_thread();
    th2 = thread;
    ERR_CHECK();
    thread->period = 100;
    qos_thread_init(thread, 300, 9, uart_print_2);
    thread = qos_get_new_thread();
    th3 = thread;
    ERR_CHECK();
    thread->period = 500;
    qos_thread_init(thread, 300, 7, uart_print_3);



    qos_system_run();
    while (1);

    return 0;
}

