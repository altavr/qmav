#include "qmav_math.h"
#include "stm32f4xx.h"
#include "qos_debug.h"

PROFILING_VAR;

#pragma GCC push_options
#pragma GCC optimize ("-O3")

#define DT 0.02f
#define _180_DIV_PI 57.2957795131f


arm_status q_rotate_vector(arm_matrix_instance_f32 *vector, arm_matrix_instance_f32 *new_vector, arm_matrix_instance_f32 *q)
{
    arm_status arm_math_status = 0;
    float *qD = q->pData;

    float quat_left_matrix_data[] = {qD[0],   -qD[1], -qD[2], -qD[3],
                                  qD[1],    qD[0],  -qD[3], qD[2],
                                  qD[2],    qD[3],  qD[0],  -qD[1],
                                  qD[3],    -qD[2], qD[1],  qD[0]};

    arm_matrix_instance_f32 quat_left_matrix = {.numRows = 4, .numCols = 4, .pData = quat_left_matrix_data};

//    q_printf("quat left:\n\r");
//    matrix_print(&quat_left_matrix);

    float quat_right_matrix_data[] = {qD[0],   qD[1], qD[2], qD[3],
                                   -qD[1],    qD[0],  -qD[3], qD[2],
                                   -qD[2],    qD[3],  qD[0],  -qD[1],
                                   -qD[3],    -qD[2], qD[1],  qD[0]};

    arm_matrix_instance_f32 quat_right_matrix = {.numRows = 4, .numCols = 4, .pData = quat_right_matrix_data};

//    q_printf("quat right:\n\r");
//    matrix_print(&quat_right_matrix);

    float sVector_data[] = {0, vector->pData[0], vector->pData[1], vector->pData[2]};
    arm_matrix_instance_f32 sVector = {.numRows = 4, .numCols = 1, .pData = sVector_data};

    float dVector_data[4];
    arm_matrix_instance_f32 dVector = {.numRows = 4, .numCols = 1, .pData = dVector_data};

    CHECK_ARM_MATH(arm_mat_mult_f32(&quat_left_matrix, &sVector, &dVector));
//    q_printf("one mult:\n\r");
//    matrix_print(&dVector);

    CHECK_ARM_MATH(arm_mat_mult_f32(&quat_right_matrix, &dVector, &sVector));

    new_vector->pData[0] = sVector.pData[1];
    new_vector->pData[1] = sVector.pData[2];
    new_vector->pData[2] = sVector.pData[3];

    return arm_math_status;
}

arm_status mult_quternions(arm_matrix_instance_f32 *qLeft, arm_matrix_instance_f32 *qRight, arm_matrix_instance_f32 *q)
{
    float qL_data[] = {qLeft->pData[0],   -qLeft->pData[1], -qLeft->pData[2], -qLeft->pData[3],
                        qLeft->pData[1],    qLeft->pData[0],  -qLeft->pData[3], qLeft->pData[2],
                        qLeft->pData[2],    qLeft->pData[3],  qLeft->pData[0],  -qLeft->pData[1],
                        qLeft->pData[3],    -qLeft->pData[2], qLeft->pData[1],  qLeft->pData[0]};

    arm_matrix_instance_f32 qL = {.numRows = 4, .numCols = 4, .pData = qL_data};

    return arm_mat_mult_f32(&qL, qRight, q);
}

void q2R(arm_matrix_instance_f32 *srcq, arm_matrix_instance_f32 *dstR)
{
    float *q = srcq->pData;
    float *R = dstR->pData;

    float ww = q[0] * q[0];
    float wx = q[0] * q[1];
    float wy = q[0] * q[2];
    float wz = q[0] * q[3];
    float xx = q[1] * q[1];
    float xy = q[1] * q[2];
    float xz = q[1] * q[3];
    float yy = q[2] * q[2];
    float yz = q[2] * q[3];
    float zz = q[3] * q[3];


    R[0] = ww + xx - yy - zz;
    R[1] = 2 * (xy - wz);
    R[2] = 2 * (wy + xz);
    R[3] = 2 * (xy + wz);
    R[4] = ww - xx + yy - zz;
    R[5] = 2 * (yz - wx);
    R[6] = 2 * (xz - wy);
    R[7] = 2 * (wx + yz);
    R[8] = ww - xx - yy + zz;
}

arm_status integrate_mid_point(arm_matrix_instance_f32 *w_vect, arm_matrix_instance_f32 *w_prev_vect, arm_matrix_instance_f32 *q_delta)
{
    arm_status arm_math_status = 0;
    float w_mid[3];
    float w_sum = 0;
    float w_mod;
    float *q_img = q_delta->pData + 1;

    for (int i = 0; i < 3; i++) {
        w_mid[i] = (w_vect->pData[i] + w_prev_vect->pData[i]) / 2;
        w_sum += w_mid[i] * w_mid[i];
    }
    CHECK_ARM_MATH(arm_sqrt_f32(w_sum, &w_mod));
    q_delta->pData[0] = arm_cos_f32(w_mod * DT / 2);

    float m = (1 / w_mod) * arm_sin_f32(w_mod * DT / 2);
    for (int i = 0; i < 3; i++) {
        q_img[i] = w_mid[i] * m;
     }

    return arm_math_status;
}

arm_status normalize(arm_matrix_instance_f32 *vSrc, arm_matrix_instance_f32 *vNorm)
{
    arm_status arm_math_status = 0;
    float dot_product;
    float k;

    arm_dot_prod_f32(vSrc->pData, vSrc->pData, vSrc->numRows, &dot_product);
    CHECK_ARM_MATH(arm_sqrt_f32(dot_product, &k));
    k = (k != 0) ? 1 / k : 1;
    CHECK_ARM_MATH(arm_mat_scale_f32(vSrc, k, vNorm));
    return arm_math_status;
}

arm_status normalize_in_place(arm_matrix_instance_f32 *v)
{
    arm_status arm_math_status = 0;
    float dot_product;
    float k;

    arm_dot_prod_f32(v->pData, v->pData, v->numRows, &dot_product);
    CHECK_ARM_MATH(arm_sqrt_f32(dot_product, &k));
    k = (k != 0) ? 1 / k : 1;

    for (uint8_t i = 0; i < v->numRows; i++) {
        v->pData[i] *= k;
    }
    return arm_math_status;
}

void q_from_vectors(arm_matrix_instance_f32 *u, arm_matrix_instance_f32 *v, arm_matrix_instance_f32 *q)
{
    float a[3], b[3];
    float dot_product;
    float t_vector[4];
    arm_matrix_instance_f32 q_non_norm = {.numRows = 4, .numCols = 1, .pData = t_vector};

    arm_dot_prod_f32(u->pData, v->pData, 3, &dot_product);

    t_vector[0] = v->pData[1];
    t_vector[1] = v->pData[2];
    t_vector[2] = v->pData[0];
    arm_mult_f32(u->pData, t_vector, a, 3);

    t_vector[0] = u->pData[1];
    t_vector[1] = u->pData[2];
    t_vector[2] = u->pData[0];
    arm_mult_f32(v->pData, t_vector, b, 3);

    t_vector[0] = 1 + dot_product;
    t_vector[1] = a[1] - b[1];
    t_vector[2] = a[2] - b[2];
    t_vector[3] = a[0] - b[0];
    normalize(&q_non_norm, q);

}

__attribute__ ((optimize(2))) void euler_to_quaternion(arm_matrix_instance_f32 *eu_angles, arm_matrix_instance_f32 *q)
{
    float half_angles[3];

    arm_scale_f32(eu_angles->pData, 0.5f, half_angles, 3);
    float cr = arm_cos_f32(half_angles[0]);
    float sr = arm_sin_f32(half_angles[0]);
    float cp = arm_cos_f32(half_angles[1]);
    float sp = arm_sin_f32(half_angles[1]);
    float cy = arm_cos_f32(half_angles[2]);
    float sy = arm_sin_f32(half_angles[2]);

    q->pData[0] = cy * cr * cp + sy * sr * sp;
    q->pData[1] = cy * sr * cp - sy * cr * sp;
    q->pData[2] = cy * cr * sp + sy * sr * cp;
    q->pData[3] = sy * cr * cp - cy * sr * sp;

}

void quternion_to_euler(arm_matrix_instance_f32 *q, arm_matrix_instance_f32 *eu_angles)
{
    float *q_ = q->pData;

    eu_angles->pData[0] = atan2f(2 * (q_[0] * q_[1] + q_[2] * q_[3]), 1 -  2 * (q_[1] * q_[1] + q_[2] * q_[2]));
    eu_angles->pData[1] = asinf(2 * (q_[0] * q_[2] - q_[3] * q_[1]));
    eu_angles->pData[2] = atan2f(2 * (q_[0] * q_[3] + q_[1] * q_[2]), 1 - 2 * (q_[2] * q_[2] + q_[3] * q_[3]));
}

void vector_to_skew_symm_mat(arm_matrix_instance_f32 *v, arm_matrix_instance_f32 *mat)
{
    mat->pData[0] = 0;
    mat->pData[1] = -v->pData[2];
    mat->pData[2] = v->pData[1];
    mat->pData[3] = v->pData[2];
    mat->pData[4] = 0;
    mat->pData[5] = -v->pData[0];
    mat->pData[6] = -v->pData[1];
    mat->pData[7] = v->pData[0];
    mat->pData[8] = 0;
}


void rot_mat_to_q(arm_matrix_instance_f32 *R, arm_matrix_instance_f32 *q)
{
    float *m = R->pData;
    float qt[4];
    float t, t_sqrt;

    if (m[8] < 0) {
        if (m[0] > m[4]) {
//            q_printf("x-form\r\n");
            t = 1.0f + m[0] - m[4] - m[8];
            float p[] = {m[7] - m[5], fabs(t), m[1] + m[3], m[6] + m[2]};
            arm_copy_f32(p, qt, 4);
        }
        else {
//            q_printf("y-form\r\n");
            t = 1.0f - m[0] + m[4] - m[8];
            float p[] = { m[2] - m[6], m[1]+m[3], fabs(t), m[5]+m[7]};
            arm_copy_f32(p, qt, 4);
        }
    }
    else {
        if (m[0] < -m[4]) {
//            q_printf("z-form\r\n");
            t = 1.0f - m[0] - m[4] + m[8];
            float p[] = {m[3] - m[1], m[6]+m[2], m[5]+m[7], fabs(t)};
            arm_copy_f32(p, qt, 4);
        }
        else {
//            q_printf("w-form\r\n");
            t = 1.0f + m[0] + m[4] + m[8];
            float p[] = {fabs(t), m[7] - m[5], m[2]-m[6], m[3]-m[1]};
            arm_copy_f32(p, qt, 4);
        }
    }
    arm_sqrt_f32(t, &t_sqrt);
    arm_scale_f32(qt, 0.5f / t_sqrt, q->pData, 4);
}

#pragma GCC pop_options

void matrix_insert(arm_matrix_instance_f32 *srcMat, uint8_t row, uint8_t col, arm_matrix_instance_f32 *dstMat)
{
    float *pDE = &dstMat->pData[row * dstMat->numCols + col];
    float *pSE = srcMat->pData;
    for (int i = 0; i < srcMat->numRows; i++) {
        for (int j = 0; j < srcMat->numCols; j++) {
            pDE[j] = pSE[j];
        }
        pDE += dstMat->numCols;
        pSE += srcMat->numCols;
    }
}

void jacobian_qxvxq(arm_matrix_instance_f32 *q, arm_matrix_instance_f32 *v, arm_matrix_instance_f32 *J)
{
    float *pq = q->pData;
    float *pv = v->pData;

    float a1 = 2 * (pq[0] * pv[0] - pq[3] * pv[1] + pq[2] * pv[2]);
    float a2 = 2 * (pq[1] * pv[0] + pq[2] * pv[1] + pq[3] * pv[2]);
    float a3 = 2 * (-pq[2] * pv[0] + pq[1] * pv[1] + pq[0] * pv[2]);
    float a4 = 2 * (-pq[3] * pv[0] - pq[0] * pv[1] + pq[1] * pv[2]);

    J->pData[0] = a1;
    J->pData[1] = a2;
    J->pData[2] = a3;
    J->pData[3] = a4;
    J->pData[4] = -a4;
    J->pData[5] = -a3;
    J->pData[6] = a2;
    J->pData[7] = a1;
    J->pData[8] = a3;
    J->pData[9] = -a4;
    J->pData[10] = -a1;
    J->pData[11] = a2;
}

void fill_diagonal_matrix(arm_matrix_instance_f32 *mat, float val)
{
    arm_fill_f32(0.0f, mat->pData, mat->numCols * mat->numRows);
    for (uint8_t i = 0; i < mat->numRows; i++) {
        mat->pData[i + mat->numCols * i] = val;
    }
}

void matrix_print(arm_matrix_instance_f32 *m)
{
    char buf[100];
    float *p = m->pData;

    for (int i = 0; i < m->numRows; i++) {
        for (int j = 0; j < m->numCols; j++) {
            q_snprintf(buf, 100, "%f\t", (double)*p++);
            q_printf(buf);
        }
        q_printf("\n\r");
    }
}

void ekf_math_func_test(void)
{


    q_printf("____EKF MATH TEST____\r\n");


    float v1_raw_data[] = {3, -5, 7};
    arm_matrix_instance_f32 v1_raw = {.numRows = 3, .numCols = 1, .pData = v1_raw_data};
    float v1_data[3];
    arm_matrix_instance_f32 v1 = {.numRows = 3, .numCols = 1, .pData = v1_data};

    PROFILING(normalize(&v1_raw, &v1));
    q_printf("cycles %d __ v1:\n\r", cycle_num);
    matrix_print(&v1);

    float q_raw_data[] = {34, -53, 76, 19};
    arm_matrix_instance_f32 q_raw = {.numRows = 4, .numCols = 1, .pData = q_raw_data};
    float q_data[4];
    arm_matrix_instance_f32 q = {.numRows = 4, .numCols = 1, .pData = q_data};

    PROFILING(normalize(&q_raw, &q));

    q_printf("cycles %d __ norm q:\n\r", cycle_num);
    matrix_print(&q);

    float v2_data[3];
    arm_matrix_instance_f32 v2 = {.numRows = 3, .numCols = 1, .pData = v2_data};

    PROFILING(PRINT_ARM_M_STATUS(q_rotate_vector(&v1, &v2, &q)));

    q_printf("cycles %d __ rotate v1 to v2 q:\n\r", cycle_num);
    matrix_print(&v2);

    float q0_data[4] = {25, 57, -3, 67};
    arm_matrix_instance_f32 q0 = {.numRows = 4, .numCols = 1, .pData = q0_data};
    float q1_data[4];
    arm_matrix_instance_f32 q1 = {.numRows = 4, .numCols = 1, .pData = q1_data};
    PROFILING(normalize(&q0, &q1));
    q_printf("cycles %d __ normalize q0 ->  q1:\n\r", cycle_num);
    matrix_print(&q1);

    PROFILING(PRINT_ARM_M_STATUS(mult_quternions(&q, &q1, &q0)));
    q_printf("cycles %d __ mult q, q1 -> q0:\n\r", cycle_num);
    matrix_print(&q0);

    float R_data[9];
    arm_matrix_instance_f32 R = {.numRows = 3, .numCols = 3, .pData = R_data};

    q_printf("q for R matrix\r\n");
    matrix_print(&q);

    PROFILING(q2R(&q, &R));
    q_printf("cycles %d __ q -> R:\n\r", cycle_num);
    matrix_print(&R);

    v1_raw.pData[0] = 0.34;
    v1_raw.pData[1] = -0.54;
    v1_raw.pData[2] = 0.75;

    normalize(&v1_raw, &v2);

    q_printf("v2:\n\r");
    matrix_print(&v2);

    PROFILING(PRINT_ARM_M_STATUS(integrate_mid_point(&v1, &v2, &q1)));
    q_printf("cycles %d __ integrated q1:\n\r", cycle_num);
    matrix_print(&q1);

    normalize(&q1, &q);
    q_printf("norm integrated q1:\n\r");
    matrix_print(&q);

    PROFILING(q_from_vectors(&v1, &v2, &q1));
    q_printf("cycles %d __ q_from_vectors q1:\n\r", cycle_num);
    matrix_print(&q1);

    float eu_data[] = {0.43, -1.01, 0.123};
    arm_matrix_instance_f32 eu = {.numRows = 3, .numCols = 1, .pData = eu_data};

    PROFILING(euler_to_quaternion(&eu, &q));
    q_printf("cycles %d __ euler_to_quaternion, q:\n\r", cycle_num);
    matrix_print(&q);

    PROFILING(quternion_to_euler(&q, &eu));
    q_printf("cycles %d __ quternion_to_euler, eu:\n\r", cycle_num);
    matrix_print(&eu);

    float Rt_data[9];
    arm_matrix_instance_f32 Rt = {.numRows = 3, .numCols = 3, .pData = Rt_data};
    arm_mat_trans_f32(&R, &Rt);

    PROFILING(rot_mat_to_q(&R, &q));
    q_printf("cycles %d __ rot_mat_to_q, q:\n\r", cycle_num);
    matrix_print(&q);

    vector_to_skew_symm_mat(&v1, &R);
    q_printf("vector_to_skew_symm_mat, R\n\r");
    matrix_print(&R);

    //w-form transform R->q
    q_printf("w-form transform R->q0\r\n");
    q1.pData[0] = 12;
    q1.pData[1] = -8;
    q1.pData[2] = 4;
    q1.pData[3] = 1;

    normalize(&q1, &q);
    matrix_print(&q);
    q2R(&q, &R);
    matrix_print(&R);
    rot_mat_to_q(&R, &q0);
    matrix_print(&R);

    matrix_print(&q0);


    //x-form transform R->q
    q_printf("x-form transform R->q0\r\n");
    q1.pData[0] = 12;
    q1.pData[1] = -80;
    q1.pData[2] = 4;
    q1.pData[3] = 1;

    normalize(&q1, &q);
    q2R(&q, &R);
    matrix_print(&R);
    rot_mat_to_q(&R, &q0);

    matrix_print(&q0);

    //z-form transform R->q
    q_printf("z-form transform R->q0\r\n");
    q1.pData[0] = 12;
    q1.pData[1] = -80;
    q1.pData[2] = 4;
    q1.pData[3] = -120;

    normalize(&q1, &q);
    q2R(&q, &R);
    rot_mat_to_q(&R, &q0);

    matrix_print(&q0);

    q2R(&q0, &Rt);
    matrix_print(&Rt);
    quternion_to_euler(&q0, &eu);
    matrix_print(&eu);

    q_printf("q x q0:\r\n");

    mult_quternions(&q, &q0, &q1);
    matrix_print(&q1);

    q_printf("filling R\r\n");
    arm_fill_f32(0.f, R.pData, 9);
    q_printf("inserting in R\r\n");
    matrix_insert(&v1, 0, 1, &R);

    q_printf("matrix_insert, v1 -> R\r\n");
    matrix_print(&R);
}

float invSqrt(float x) {

    float y;
    arm_sqrt_f32(x, &y);
    return 1.0f / y;
}
