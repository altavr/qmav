#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include "qos_memory.h"
#include "binheap_test_libs.h"

#define DATA_ARRAY_SIZE 11

#define DD__mem_heap_test

MemCB_t mcb;


void print_chunk (mb_head_t* chunk)
{
    printf("CHUNK %p, len - %d,\t used - %d\n", chunk, chunk->len, chunk->used);
}


void print_chunk_list(void) {
    
        printf("________________CHunk LIST :______________________\n");

    q_lnim_t *list = mcb.chunklist;
    q_lnim_t *p = list->next;
    for (; p != list; p = p->next) {
        mb_head_t* c = container_of(p, mb_head_t, lle);
        print_chunk(c);
    }
    
}


#ifdef DD__mem_heap_test
#undef _PROXY
#define _PROXY(f, ...) A_##f(__VA_ARGS__)
#endif

void mem_heap_test(void)
{
    
    void *data[DATA_ARRAY_SIZE];
    for (int i = 0; i < DATA_ARRAY_SIZE; i++) {
        data[i] = qos_malloc(sizeof(int) * i + 4);
    }
    
     PRINT_DDBG_("_____\n______\n__________FREE START \n_________\n_______\n");
    PRINT_LINE_NUMBER();
    PRINT_CHUNK_LIST();
    PRINT_HEAP(mcb.free_heap.root);
    
    for (int i = 0; i < DATA_ARRAY_SIZE; i++) {
        if (i % 4 == 1) {
            PRINT_DDBG("_____\n______\n__________i - %d \n_________\n_______\n", i);
            qos_free(data[i]);
            PRINT_LINE_NUMBER();
            PRINT_CHUNK_LIST();
            PRINT_HEAP(mcb.free_heap.root);
            qos_free(data[i-1]);
            PRINT_LINE_NUMBER();
            PRINT_CHUNK_LIST();
            PRINT_HEAP(mcb.free_heap.root);
            qos_free(data[i+1]);
            PRINT_LINE_NUMBER();
            PRINT_CHUNK_LIST();
            PRINT_HEAP(mcb.free_heap.root);
        }
    }
    
   
    PRINT_LINE_NUMBER();
    PRINT_CHUNK_LIST();
    PRINT_HEAP(mcb.free_heap.root);
    
    for (int i = 0; i < DATA_ARRAY_SIZE; i++) {
        if (i % 4 == 0) {
            data[i] = qos_malloc(sizeof(int) * i + 4);
        }
    }
    
    PRINT_LINE_NUMBER();
    PRINT_CHUNK_LIST();
    PRINT_HEAP(mcb.free_heap.root);
    
}

#ifdef DD__mem_heap_test
#undef _PROXY
#define _PROXY(f, ...) NO_##f(__VA_ARGS__)
#endif


int main(void)
{

    void *data[DATA_ARRAY_SIZE];
    void *ptr;
    
    ptr = malloc(40*1024);
    if (ptr == NULL) {
        int er = errno;
        printf("ERROR CODE _%d\n", er);
        exit(EXIT_FAILURE);
    }
    mcb.low_border =  (uint64_t)ptr;
    mcb.high_border = mcb.low_border + 40*1024;
    
    printf("low - 0x%lx, high - 0x%lx\n", mcb.low_border, mcb.high_border);
//     print_heap(mcb.free_heap.root, 0);
    qos_dyn_mem_enable();
    
//     print_heap(mcb.free_heap.root, 0);
    
    int *r = qos_malloc(sizeof(int));
    
    *r = 1488;
    printf("r before changing - %d, address - %p \n", *r, r);
    
//     print_heap(mcb.free_heap.root, 0);
    
    printf("r free  \n");
    qos_free(r);
    
//     for (int i = 0; i < DATA_ARRAY_SIZE; i++) {
//         data[i] = qos_malloc(sizeof(int));
//     }
//     
//     for (int i = 0; i < DATA_ARRAY_SIZE; i++) {
//         qos_free(data[i]);
//     }
    
    mem_heap_test();
    
    return 0;
}
