 
#include <stdio.h>
#include "ll_test_libs.h"

void print_list_item(q_lnim_t *list)
{
    printf("List item pointers: %p %p %p\n", list, list->prev, list->next);
}



void print_list(q_lnim_t* list)
{
    printf("________________LIST :______________________\n");

    q_lnim_t *p = list->next;
//     print_list_item(list);
    for (; p != list; p = p->next) {
        print_list_item(p);
    }
}
