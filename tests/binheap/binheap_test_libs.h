 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "misc/qos_bin_heap.h"


#define ARRAY_SIZE 2000

void check_heap(bheap_t* heap);
void print_node(bh_node_t* node);
void print_heap(bh_node_t* node, int offset);
int check_subtree(bh_node_t* node, int level, int rowsum, u8 list_nodes[]);


void print_array(bh_node_t nodes[], int num);
void diff_nodes(bh_node_t* node1, bh_node_t* node2);
void diff_array_nodes(bh_node_t array[], bh_node_t tmp[], int num);
void init_node_array(bh_node_t nodes[], int num);
void add_node_array(bh_node_t nodes[], int num, bheap_t* heap);
void rem_root_heap(bheap_t* heap);
void print_heap_nodes(bheap_t* heap);
void rem_0123 (int count, bheap_t* heap);
void heap_test(bh_node_t nodes[], bheap_t* heap);
