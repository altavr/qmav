//var mcu_def = ""
function mcu_def(mcu) {
    var splRoot = ""
    var incPaths = ["inc/",
                    "rtos/inc/",
                    "rtos/",
                    "/home/alex/documents/c/rc/rc_sx1278/",
                    "/home/alex/documents/c/control/optical/laser/VL53L0X_1.0.2/Api/core/inc/",
            "sensors/inc/"]
    var libPaths = ""
    var defines = ["USE_STDPERIPH_DRIVER",
                   "ARM_MATH_CM4",
//                   "__FPU_PRESENT = 1",
                   "__MPU_PRESENT = 1",
                   "ARM_MATH_MATRIX_CHECK"]
    var startup = ""
    var perifDrvRoot = ""
    var systemRoot = ""
    var sys_c
    var paths
    var drivers
    if (mcu === "f303") {
        splRoot = "/home/alex/documents/c/stm32/STM32F30x_DSP_StdPeriph_Lib_V1.2.3/Libraries/"
        incPaths = incPaths.concat([splRoot + "CMSIS/Device/ST/STM32F30x/Include",
                                    splRoot + "STM32F30x_StdPeriph_Driver/inc/",
                                    splRoot + "CMSIS_4.5/Include"])

        libPaths = libPaths.concat([splRoot + "CMSIS_4.5/Lib/GCC"])

        defines = defines.concat(["STM32F303xC",
                                 "STM32F303_"])

        perifDrvRoot = splRoot + "STM32F30x_StdPeriph_Driver/src/"


        drivers = [
                    "stm32f30x_adc.c",
                    "stm32f30x_can.c",
                    "stm32f30x_comp.c",
                    "stm32f30x_crc.c",
                    "stm32f30x_dac.c",
                    "stm32f30x_dbgmcu.c",
                    "stm32f30x_dma.c",
                    "stm32f30x_exti.c",
                    "stm32f30x_flash.c",
                    "stm32f30x_fmc.c",
                    "stm32f30x_gpio.c",
                    "stm32f30x_hrtim.c",
                    "stm32f30x_i2c.c",
                    "stm32f30x_iwdg.c",
                    "stm32f30x_misc.c",
                    "stm32f30x_opamp.c",
                    "stm32f30x_pwr.c",
                    "stm32f30x_rcc.c",
                    "stm32f30x_rtc.c",
                    "stm32f30x_spi.c",
                    "stm32f30x_syscfg.c",
                    "stm32f30x_tim.c",
                    "stm32f30x_usart.c",
                    "stm32f30x_wwdg.c"
                ]

        systemRoot = splRoot + "CMSIS/Device/ST/STM32F30x/Source/Templates/"

        sys_c = ["system_stm32f30x.c"]

        paths = {root: splRoot,
            perifRoot: perifDrvRoot,
            sysRoot: systemRoot,
        ls: ["ld_script/STM32F303VC_FLASH.ld"],
        inc: incPaths,
        lib: libPaths,
        def: defines,
        startup: ["CMSIS/Device/ST/STM32F30x/Source/Templates/gcc_ride7/startup_stm32f30x.s"],
        spl: drivers,
        sys: sys_c}
    }




    if (mcu === "f407") {
        splRoot = "/home/alex/documents/c/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/"
        incPaths = incPaths.concat([splRoot + "CMSIS/Device/ST/STM32F4xx/Include",
                                    splRoot + "STM32F4xx_StdPeriph_Driver/inc/",
                                    splRoot + "CMSIS/Include"])

        libPaths = libPaths.concat([splRoot + "CMSIS/Lib/GCC"])

        defines = defines.concat(["STM32F40_41xxx",
                                 "STM32F407_",
                                 "HSE_VALUE=8000000"])

        perifDrvRoot = splRoot + "STM32F4xx_StdPeriph_Driver/src/"

        drivers = [
                    "misc.c" ,
                    "stm32f4xx_adc.c" ,
                    "stm32f4xx_can.c" ,
                    "stm32f4xx_cec.c" ,
                    "stm32f4xx_crc.c" ,
                    "stm32f4xx_cryp.c" ,
                    "stm32f4xx_cryp_aes.c" ,
                    "stm32f4xx_cryp_des.c" ,
                    "stm32f4xx_cryp_tdes.c" ,
                    "stm32f4xx_dac.c" ,
                    "stm32f4xx_dbgmcu.c" ,
                    "stm32f4xx_dcmi.c" ,
                    "stm32f4xx_dfsdm.c" ,
                    "stm32f4xx_dma.c" ,
                    "stm32f4xx_dma2d.c" ,
                    "stm32f4xx_dsi.c" ,
                    "stm32f4xx_exti.c" ,
                    "stm32f4xx_flash.c" ,
                    "stm32f4xx_flash_ramfunc.c" ,
                    "stm32f4xx_fmpi2c.c" ,
                    "stm32f4xx_fsmc.c" ,
                    "stm32f4xx_gpio.c" ,
                    "stm32f4xx_hash.c" ,
                    "stm32f4xx_hash_md5.c" ,
                    "stm32f4xx_hash_sha1.c" ,
                    "stm32f4xx_i2c.c" ,
                    "stm32f4xx_iwdg.c" ,
                    "stm32f4xx_lptim.c" ,
                    "stm32f4xx_ltdc.c" ,
                    "stm32f4xx_pwr.c" ,
                    "stm32f4xx_qspi.c" ,
                    "stm32f4xx_rcc.c" ,
                    "stm32f4xx_rng.c" ,
                    "stm32f4xx_rtc.c" ,
                    "stm32f4xx_sai.c" ,
                    "stm32f4xx_sdio.c" ,
                    "stm32f4xx_spdifrx.c" ,
                    "stm32f4xx_spi.c" ,
                    "stm32f4xx_syscfg.c" ,
                    "stm32f4xx_tim.c" ,
                    "stm32f4xx_usart.c" ,
                    "stm32f4xx_wwdg.c" ,
                ]

        systemRoot = splRoot + "CMSIS/Device/ST/STM32F4xx/Source/Templates/"
        sys_c = ["system_stm32f4xx.c"]

        paths = {root: splRoot,
            perifRoot: perifDrvRoot,
            sysRoot: systemRoot,
        ls: ["ld_script/STM32F417IG_FLASH.ld"],
        inc: incPaths,
        lib: libPaths,
        def: defines,
        startup: ["CMSIS/Device/ST/STM32F4xx/Source/Templates/gcc_ride7/startup_stm32f40xx.s"],
        spl: drivers,
        sys: sys_c}
    }

    return paths
}

function gen_includes(paths, base)
{
    var includes = []
    for (var i = 0; i < paths.length; i++) {
        includes.push("-I" +  paths[i])
    }
    return includes
}
