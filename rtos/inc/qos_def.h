#ifndef _QOS_DEF_H
#define _QOS_DEF_H


//#include "qos_debug.h"

#define NVIC_PRIOR_GROUP 4

#define Q_MCU STM32F303_C_B
#define MCU_NUM_PORTS 3
#define MCU_DMA_CONTRL 2
#define MCU_DMA_CHANNEL_MAX 8
#define MCU_DMA_STREAM_MAX 8
//#define MPU_ENABLE
#define FPU_ENABLE
#define TIME_M_ENABLE
#define TIMERS_Q_MAX 2

#ifdef STM32F303_
#define MCU_DMA_CONTRL_OFFSET (-1)
#define MCU_DMA_CH_OFFSET (-1)
#endif
#ifdef STM32F407_
#define MCU_DMA_CONTRL_OFFSET (1)
#endif


#define RES_DESCRIPTORS_MAX 16 //power of 2
//#define USART_BUS_NUMBER    3


//define hw ********************************************
#define RES_MAX 3

#define Q_USART1_EN
//#define USART1_DMA_RX_EN
#define USART1_DMA_TX_EN


//#define Q_USART3_EN
//#define USART3_DMA_RX_EN
//#define USART3_DMA_TX_EN

//#define Q_I2C2_EN
//#define I2C2_DMA_RX_EN
//#define I2C2_DMA_TX_EN


#define Q_I2C1_EN
#define I2C1_DMA_RX_EN
#define I2C1_DMA_TX_EN

//#define Q_SPI2_EN
//#define SPI2_DMA_RX_EN
//#define SPI2_DMA_TX_EN

#define Q_SPI1_EN
#define SPI1_DMA_RX_EN
#define SPI1_DMA_TX_EN

//define hw ********************************************

#ifdef STM32F407_
#if defined(I2C2_DMA_RX_EN) || defined(I2C2_DMA_TX_EN)
#define I2C2_DMA_EN
#endif
#if defined(I2C1_DMA_RX_EN) || defined(I2C1_DMA_TX_EN)
#define I2C1_DMA_EN
#endif

#endif //I2C2_DMA_EN

#ifdef STM32F407_
#if defined(SPI2_DMA_RX_EN) || defined(SPI2_DMA_TX_EN)
#define SPI2_DMA_EN
#endif
#endif //SPI2_DMA_EN

#ifdef STM32F407_
#if defined(SPI1_DMA_RX_EN) || defined(SPI1_DMA_TX_EN)
#define SPI1_DMA_EN
#endif
#endif //SPI1_DMA_EN


#define VARS_CHECK
#define DEBUG_RETURN_VAL

#define _FORCE_INLINE   __attribute__((always_inline)) inline
#define _PRIV_DATA        __attribute__((section(".priv_data")))
#define _PRIV_BSS         __attribute__((section(".priv_bss")))



#ifdef DESKTOP_TEST
#define ADDRVAR(var) var
#else
#define ADDRVAR(var) ((memaddr_t)&var)
#endif

#define SET_PRIOR_FLAG(prior) runnable_prior_threads |= 0x80000000 >> prior
#define CLEAN_PRIOR_FLAG(prior) runnable_prior_threads &= (~(0x80000000 >> prior))

//#define SET_ACTIVE_FLAG(pos, store) store |= 0x80000000 >> prior
//#define UNSET_ACTIVE_FLAG(pos, store) store &= (~(0x80000000 >> prior))
//#define GET_STATUS_FLAG (pos, store) ((0x80000000 >> pos) & store);

#define Q_ENABLE    1UL
#define Q_DISABLE   0

#define BUS_READ_MODE_FLAG     0x20
#define BUS_RAW_MODE_FLAG       0x40
#define BUS_SWAPMODE_FLAG       0x200





#ifndef __ASSEMBLY__

#include <stdint.h>
#include <stddef.h>
#include <limits.h>

typedef unsigned char uc;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;
typedef int32_t i32;
typedef int16_t i16;
typedef int8_t i8;
typedef unsigned int uint;
typedef long unsigned  int luint;

#if !defined(container_of)
/* given a pointer @ptr to the field @member embedded into type (usually
 * struct) @type, return pointer to the embedding instance of @type. */
#define container_of(ptr, type, member) \
   ((type *)((char *)(ptr)-(char *)(&((type *)0)->member)))
#endif

#define offset_of(type, member) (size_t)&(((type *)0)->member)



typedef struct {
    u32 sec;
    u32 us;
} q_time_t;

typedef struct {
    u32 tick;
    u32 counter;
} q_tickstamp_t;

typedef enum {
    Q_SUCCESS = -1,
    Q_LIB_INIT_ERROR = -2,
    Q_OUT_OF_MEMORY = -3,
    Q_TIMEOUT = -4,
    Q_NONE_ITEM = -5,
    Q_EXCEED_NUM_ITEMS = -6,
    Q_PIN_CONF_CONFLICT = -7,
    Q_HW_NOT_CONF = -8,
    Q_RD_NONE_OPEN = -9,
    Q_MPU_NOT_PRESENT = -10,
    Q_UNDEF_ERR = -11,
    Q_MEM_ACCESS_VIOLATE = -12,
    Q_WRONG_ITEM = -13,
    Q_NOT_IMPLEMENTED = -14,
    Q_QUEUE_EMPTY = -15,
    Q_QUEUE_FULL = -16,
    Q_NONE_FREE_SPACE = -17

} qos_err_code;


typedef enum  {
#ifdef Q_USART1_EN
    Q_USART1,
#endif
#ifdef Q_USART3_EN
    Q_USART3,
#endif
#ifdef Q_I2C2_EN
  Q_I2C2,
#endif
#ifdef Q_I2C1_EN
  Q_I2C1,
#endif
#ifdef Q_SPI2_EN
    Q_SPI2,
#endif
#ifdef Q_SPI1_EN
    Q_SPI1
#endif
} io_res_t;

typedef enum {
    Q_IO_HW = 0
} q_hw_type;

#define USART3_NUM 0

//#ifndef DESKTOP_TEST

//_FORCE_INLINE uint8_t __builtin_clz(luint flags)
//{
//    uint8_t first_bit;
//    asm volatile ("clz %[out], %[in]    \n"
//                  :[out] "=r" (first_bit)
//                  :[in] "r" (flags)
//                  :);
//    return first_bit;
//}
//#else

//_FORCE_INLINE uint8_t __builtin_clz(uint flags)
//{
//    return __builtin_clzl(flags);

//}

//#endif //DESKTOP_TEST


//_FORCE_INLINE uint32_t ceil_round_to_8(uint32_t v)
//{
//    if (v & 0x7) {
//        v = (v & 0xFFFFFFF8) + 0x8;
//    }
//    else {
//        v &= 0xFFFFFFF8;
//    }
//    return v;
//}

//_FORCE_INLINE uint32_t ceil_round_to_32(uint32_t v)
//{
//    if (v & 0x1F) {
//        v = (v & 0xFFFFFFE0) + 0x20;
//    }
//    else {
//        v &= 0xFFFFFFE0;
//    }
//    return v;
//}

_FORCE_INLINE uint ceil_round_to_x(uint v, uint x)
{
    if (v & (UINT_MAX  >> (__builtin_clz(x)+1))) {
        return (v & (~(UINT_MAX  >> (__builtin_clz(x)+1)))) + x;
    }
    else {
        return v;
    }
}

_FORCE_INLINE uint floor_round_to_x(uint v, uint x)
{
    return ~(UINT_MAX >> (__builtin_clz(x)+1)) & v;
}

_FORCE_INLINE uint ceil_round_to_2_degree(uint val)
{
    if (val == (~(UINT_MAX >> 1) >> (__builtin_clz(val)))) {
        return ~(UINT_MAX >> 1) >> (__builtin_clz(val));
    }
    else {
        return ~(UINT_MAX >> 1) >> (__builtin_clz(val) - 1);
    }

}

_FORCE_INLINE uint floor_round_to_2_degree(uint val)
{
    return ~(UINT_MAX >> 1) >> __builtin_clz(val);

}

_FORCE_INLINE uint ceil_round_to_pow2_x(uint v, uint pow)
{
    if (v & (UINT_MAX  >> (32U - pow))) {
        return (v & (~(UINT_MAX  >> (32U - pow)))) + (1U << pow);
    }
    else {
        return v;
    }
}

//_FORCE_INLINE uint32_t floor_round_to_8(uint32_t v)
//{
//    v &= 0xFFFFFFF8;
//    return v;
//}

//_FORCE_INLINE uint32_t floor_round_to_32(uint32_t v)
//{
//    v &= 0xFFFFFFE0;
//    return v;
//}




//#ifdef MPU_ENABLE
#define ROUND_LOW_BORD_STACK(x)     ceil_round_to_x(x, 256)
#define ROUND_STACK_SIZE(x)         ceil_round_to_x(x, 256)
//#else
//#define ROUND_LOW_BORD_STACK(x)     ceil_round_to_8(x)
//#define ROUND_STACK_SIZE(x)         ceil_round_to_8(x)
//#endif




#ifdef DESKTOP_TEST
typedef size_t memaddr_t;
#else
typedef uint32_t memaddr_t;
#endif

#endif //ASSEMBLY
#endif // _QOS_DEF_H
