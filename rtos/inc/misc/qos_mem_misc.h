#ifndef QOS_MEM_MISC_H
#define QOS_MEM_MISC_H

#include <stddef.h>
#include "qos_def.h"

//void* memcpy(void* dest, const void* src, size_t count);

_FORCE_INLINE void _q_memswap(void *ptr1, void* ptr2, size_t len )
{

    u32 *ptr1_ptr32, *ptr2_ptr32, temp_32;
    char *ptr1_ptr_c,  *ptr2_ptr_c, temp_c;
    size_t i;

    ptr1_ptr32 = (u32*)ptr1;
    ptr2_ptr32 = (u32*)ptr2;
    i = len / 4;
    while (i > 0) {
        temp_32 = *ptr1_ptr32;
        *ptr1_ptr32++ = *ptr2_ptr32;
        *ptr2_ptr32++ = temp_32;
        i--;
    }
    ptr1_ptr_c = (char*)ptr1_ptr32;
    ptr2_ptr_c = (char*)ptr2_ptr32;
    i = len % 4;
    while (i > 0) {
        temp_c = *ptr1_ptr_c;
        *ptr1_ptr_c++ = *ptr2_ptr_c;
        *ptr2_ptr_c++ = temp_c;
        i--;
    }
}

_FORCE_INLINE void _q_memcp(void *dst, void* src, size_t len )
{


    u32 *src_ptr32, *dst_ptr32;
    char *src_ptr_c,  *dst_ptr_c;
    size_t i;

    src_ptr32 = (u32*)src;
    dst_ptr32 = (u32*)dst;
    i = len / 4;
    while (i > 0) {
        *dst_ptr32++ = *src_ptr32++;
        i--;
    }
    src_ptr_c = (char*)src_ptr32;
    dst_ptr_c = (char*)dst_ptr32;
    i = len % 4;
    while (i > 0) {
        *dst_ptr_c++ = *src_ptr_c++;
        i--;
    }

}

#endif // QOS_MEM_MISC_H
