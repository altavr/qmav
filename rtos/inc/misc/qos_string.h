#ifndef QOS_STRING_H
#define QOS_STRING_H



char * q_strcpy(char *strDest, const char *strSrc);
//void *malloc(size_t size);

int q_strlen(char* s);

char* q_snprintf(char* str, unsigned int num, char* fmt, ...);

void console_init(int rd);
void q_printf(char* fmt, ...);
#endif // QOS_STRING_H
