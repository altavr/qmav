#ifndef QOS_BIN_HEAP_H
#define QOS_BIN_HEAP_H

#include "qos_def.h"
#include "stddef.h"
#include "qos_debug.h"

typedef struct bhnode {
    u32 key;
//    u32 value;
#ifdef DESKTOP_DEBUG_BH
    u32 id;
    u32 intree;
#endif // DESKTOP_DEBUG_BH
    struct bhnode* parent;
    struct bhnode* lchild;
    struct bhnode* rchild;

} bh_node_t;

//typedef struct bhnode bh_node_t;

typedef struct {
    bh_node_t* root;
    bh_node_t* last;
} bheap_t;




void _q_bh_min_init(bheap_t* bheap);
void _q_bh_node_init(bh_node_t* node, u32 key);
void _q_bh_min_add(bh_node_t* node, bheap_t* heap);
void _q_bh_remove(bh_node_t* node, bheap_t* heap);

bh_node_t* _bh_next_node(bh_node_t* node);




#ifdef DESKTOP_DEBUG_BH

extern int array_size;
extern bh_node_t nodes[];
extern int levsum[50];
extern u8 list_nodes[];
extern int idcount;
extern int gcount;
    void print_node(bh_node_t* node);
    void print_array(bh_node_t nodes[], int num, int all);
    void print_heap(bh_node_t* node, int offset);
#define PRINT_NODE(node) _PROXY(PRINT_NODE, node)
 #define PRINT_HEAP(root) _PROXY(PRINT_HEAP, root)
#define PRINT_ARRAY_NODE()  _PROXY(PRINT_ARRAY_NODE)

#else
#define PRINT_NODE(node)
#define PRINT_HEAP(root)
#define PRINT_ARRAY_NODE()

#endif // DESKTOP_TEST


#define A_PRINT_NODE(node) print_node(node)
#define A_PRINT_ARRAY_NODE() print_array(nodes, gcount, 0)
#define A_PRINT_HEAP(root) print_heap(root, 0)


#define NO_PRINT_NODE(node)
#define NO_PRINT_ARRAY_NODE()
#define NO_PRINT_HEAP(root)



#define _PROXY(f, ...) NO_##f(__VA_ARGS__)



//#define  DD___q_bh_remove
//#define DD___bh_next_node
//#define DD___q_bh_min_add



#ifdef DESKTOP_TEST
#include <stdlib.h>
#define DEXIT exit(1)
#endif

enum node_ref {
    PARENT_REF = sizeof(bh_node_t) - 12,
    LCHILD_REF = sizeof(bh_node_t) - 8,
    RCHILD_REF = sizeof(bh_node_t) - 4,
};




_FORCE_INLINE void _bh_replace_ext_links1(bh_node_t* const target, bh_node_t** src)
{

    bh_node_t** node = &((*src)->lchild);
//    DDBG("target %p src %p\n", target, *src);
    for (uint i=0; i<2; i++) {
//        DDBG("offset________________- %d __ node %p\n", i, *node);
        if ((*node != NULL) && (*node != target)) {
//            DDBG("offset__replace________- %d pointer - %p\n", i, *node);
            (*node)->parent = target;
//            PRINT_ARRAY_NODE();
        }
        node++;
    }
}


_FORCE_INLINE void _bh_replace_ext_links2(bh_node_t* const target, bh_node_t** src)
{

    bh_node_t** node = &((*src)->lchild);
//    DDBG("target %p src %p\n", target, *src);
    for (uint i=0; i<2; i++) {
//        DDBG("offset________________- %d __ node %p\n", i, *node);
        if ((*node != NULL)) {
//            DDBG("offset__replace________- %d pointer - %p\n", i, *node);
            (*node)->parent = target;
//            PRINT_ARRAY_NODE();
        }
        node++;
    }
}


_FORCE_INLINE void _bh_swap_refs(bh_node_t** n1, bh_node_t** n2)
{
    bh_node_t*  temp = *n1;
    *n1 = *n2;
    *n2 = temp;
}

//_FORCE_INLINE void _bh_adjust_links(bh_node_t* const node)
//{
//    if (node->parent != NULL) {
//        if (node->parent->lchild == node) {
//            node->parent->lchild = node;
//        }
//        else {
//            node->parent->rchild = node;
//        }
//    }
//    bh_node_t **ptr = &node->lchild;
//    for (u32 i=0; i < 2; i++) {
//        if (ptr[i] != NULL) {
//            ptr[i]->parent = node;
//        }
//    }
//}

#endif
