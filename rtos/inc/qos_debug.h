#ifndef __QOS_DEBUG_H
#define __QOS_DEBUG_H
#include "qos_def.h"

#define DISABLE_WRITE_BUFFER


//#define Q_DEBUG_IO_SW_TH
//#define Q_DEBUG_VARS
//#define Q_STACK_BOUNDARIES_CHECK
#define Q_IS_THREAD_P_CHECK_
#define Q_CORTEX_FAULT_STATUS

#ifdef Q_STACK_BOUNDARIES_CHECK
#define Q_THREAD_STACK_CHECK() thread_stack_check()
#else
#define Q_THREAD_STACK_CHECK()
#endif

#ifdef Q_IS_THREAD_P_CHECK_
#define Q_IS_THREAD_P_CHECK(x) q_is_thread(x)
#else
#define Q_IS_THREAD_P_CHECK()
#endif

#ifdef Q_CORTEX_FAULT_STATUS
#define Q_GET_FAULT_STATUS() q_get_fault_stat()
#elif
#define Q_GET_FAULT_STATUS()
#endif



#ifdef DESKTOP_DEBUG
#define _PROXY(f, ...) NO_##f(__VA_ARGS__)
#define _DESKTOP_DEBUG
#include <stdio.h>
#define DDBG(fmt, ...) printf(fmt, __VA_ARGS__)
#define DDBG_(fmt) printf(fmt)
#define PRT_LST_I_DBG(list) print_list_item(list)
#define PRT_THREAD_DBG(th) print_thread(th)
#define LINE_NUMBER printf("file - %s, line - %d\n", __FILE__, __LINE__)
#define PRINT_LINE_NUMBER() _PROXY(PRINT_LINE_NUMBER)
#define PRINT_DDBG(...)      _PROXY(PRINT_DDBG, __VA_ARGS__)
#define PRINT_DDBG_(x)      _PROXY(PRINT_DDBG_, x)
#define A_PRINT_LINE_NUMBER() LINE_NUMBER
#define A_PRINT_DDBG(...) DDBG(__VA_ARGS__)
#define A_PRINT_DDBG_(x) DDBG_(x)
#define NO_PRINT_LINE_NUMBER()
#define NO_PRINT_DDBG(...)
#define NO_PRINT_DDBG_(x)
#else
#define DDBG(fmt, ...)
#define PRT_LST_I_DBG(list)
#define PRT_THREAD_DBG(th)
#define DDBG_(fmt)
#define LINE_NUMBER
#define PRINT_LINE_NUMBER()
#define PRINT_DDBG(...)
#define PRINT_DDBG_(x)
#endif


#ifdef VARS_CHECK
#define ERR_DEF _q_error = 0
#ifdef DESKTOP_TEST
#define ERR_RAISE(err) printf("ERROR %d\n", err);
#else
#define ERR_RAISE(err) qos_error_handler(err)
#endif //DESKTOP_TEST

#define ERR_CHECK() if (_q_error) {qos_error_handler(_q_error);}
#else
#define ERR_DEF
#define ERR_RAISE(err)
#define ERR_CHECK()

#endif

#ifdef DEBUG_RETURN_VAL
#define CHECK_RETURN(v) int e = v; if ((e < 0)) {qos_error_handler(e);}

#else
#define CHECK_RETURN(v) v;
#endif


#define PROFILING_ENABLE CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk
#define PROFILING_DISABLE CoreDebug->DEMCR &= (~CoreDebug_DEMCR_TRCENA_Msk)
#define PROFILING_VAR extern uint32_t cycle_num
#define PROFILING(f) DWT->CYCCNT = 0; DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk; f; cycle_num = DWT->CYCCNT; DWT->CTRL &= (~DWT_CTRL_CYCCNTENA_Msk)




extern qos_err_code _q_error;
void qos_error_handler(qos_err_code err);

#endif // _QOS_DEBUF_H
