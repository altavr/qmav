#ifndef _QOS_CORE_H
#define _QOS_CORE_H

#define _THREAD_EXIT_SYSCALL    0
#define _DELAY_SYSCALL          1
#define _IOCTL_SYSCALL          2
#define _READ_SYSCALL           3
#define _WRITE_SYSCALL          4
#define _OPEN_SYSCALL           5
#define _CLOSE_SYSCALL          6
#define _RAWREAD_SYSCALL        7
#define _RAWWRITE_SYSCALL       8
#define _IOSWAP_SYSCALL         9
#define _MUTEX_LOCK_SIGNAL      10
#define _MUTEX_UNLOCK_SIGNAL    11
#define _EXT_ITR_SYSCALL        12
#define _QALLOC_SYSCALL         13
#define _QFREE_SYSCALL          14
#define _EXEC_SYSCALL           15
#define _NO_SYSCALL             0xFF


//#define RETURN_TO_ISR           0xFFFFFFF1UL
//#define RETURN_TO_MAIN_THREAD   0xFFFFFFF9UL
//#define RETURN_TO_PROG_THREAD   0xFFFFFFFDUL

#define _MUTEX_LOCKED   1
#define _MUTEX_UNLOCKED  0

#ifdef  __ASSEMBLY__
#define THREAD_EXIT_SYSCALL     #_THREAD_EXIT_SYSCALL
#define DELAY_SYSCALL           #_DELAY_SYSCALL
#define IOCTL_SYSCALL           #_IOCTL_SYSCALL
#define READ_SYSCALL            #_READ_SYSCALL
#define WRITE_SYSCALL           #_WRITE_SYSCALL
#define OPEN_SYSCALL            #_OPEN_SYSCALL
#define CLOSE_SYSCALL           #_CLOSE_SYSCALL
#define MUTEX_LOCK_SIGNAL       #_MUTEX_LOCK_SIGNAL
#define MUTEX_UNLOCK_SIGNAL     #_MUTEX_UNLOCK_SIGNAL
#define EXT_ITR_SYSCALL         #_EXT_ITR_SYSCALL
#define RAWREAD_SYSCALL         #_RAWREAD_SYSCALL
#define RAWWRITE_SYSCALL        #_RAWWRITE_SYSCALL
#define IOSWAP_SYSCALL          #_IOSWAP_SYSCALL
#define NO_SYSCALL              #_NO_SYSCALL
#define QALLOC_SYSCALL          #_QALLOC_SYSCALL
#define QFREE_SYSCALL           #_NO_SYSCALL
#define EXEC_SYSCALL            #_EXEC_SYSCALL
#else


#include "qos_def.h"
#include "qos_debug.h"
#include "qos_threads.h"
#include "qos_drivers/general.h"

//#include "qos_threads.h"


#define SYSTICK_PERIOD 1000UL //in microsecends
#define STACK_FRAME_SIZE 16


#define SAVE_CONTEXT 1
#define NO_SAVE_CONTEXT 0
#define RAISE_SVPend SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk

typedef enum {
    Q_THREAD_EXIT = _THREAD_EXIT_SYSCALL,
    Q_DELAY =  _DELAY_SYSCALL,
    Q_IOCTL = _IOCTL_SYSCALL,
    Q_READ = _READ_SYSCALL,
    Q_WRITE = _WRITE_SYSCALL,
    Q_OPEN = _OPEN_SYSCALL,
    Q_CLOSE = _CLOSE_SYSCALL,
    Q_RAWREAD = _RAWREAD_SYSCALL,
    Q_RAWWRITE = _RAWWRITE_SYSCALL,
    Q_IOSWAP = _IOSWAP_SYSCALL,
    Q_MEX_LOCK_SIGNAL = _MUTEX_LOCK_SIGNAL,
    Q_MEX_UNLOCK_SIGNAL = _MUTEX_UNLOCK_SIGNAL,
    Q_EXT_ITR_SYSCALL = _EXT_ITR_SYSCALL,
    Q_ALLOC_SYSCALL = _QALLOC_SYSCALL,
    Q_FREE_SYSCALL = _QFREE_SYSCALL,
    Q_EXEC_SYSCALL = _EXEC_SYSCALL,
    Q_NO_SYSCALL = _NO_SYSCALL

} syscall_t;


typedef struct {
    memaddr_t R4;
    memaddr_t R5;
    memaddr_t R6;
    memaddr_t R7;
    memaddr_t R8;
    memaddr_t R9;
    memaddr_t R10;
    memaddr_t R11;

    memaddr_t R0;
    memaddr_t R1;
    memaddr_t R2;
    memaddr_t R3;
    memaddr_t R12;
    memaddr_t LR;
    memaddr_t PC;
    memaddr_t xPSR;

} hstackframe_t;


typedef struct {

    memaddr_t R4;
    memaddr_t R5;
    memaddr_t R6;
    memaddr_t R7;
    memaddr_t R8;
    memaddr_t R9;
    memaddr_t R10;
    memaddr_t R11;

    memaddr_t R0;
    memaddr_t R1;
    memaddr_t R2;
    memaddr_t R3;
    memaddr_t R12;
    memaddr_t LR;
    memaddr_t PC;
    memaddr_t xPSR;

    memaddr_t S0;
    memaddr_t S1;
    memaddr_t S2;
    memaddr_t S3;
    memaddr_t S4;
    memaddr_t S5;
    memaddr_t S6;
    memaddr_t S7;
    memaddr_t S8;
    memaddr_t S9;
    memaddr_t S10;
    memaddr_t S11;
    memaddr_t S12;
    memaddr_t S13;
    memaddr_t S14;
    memaddr_t S15;
    memaddr_t FPSCR;

}   hstackframe_fpu_t;



typedef struct {
    q_tickstamp_t th_start;
    q_tickstamp_t th_end;
    q_time_t sys;
    q_time_t kernel;
} q_runtime_t;




void qos_timers_service(void);
uint8_t qos_switch_threads(uint_fast8_t save_context);
void qos_run_thread(void);
void qos_clean_thread_context(q_th_t* thread);

void qos_system_init(void);
void qos_system_run(void);

int q_end_waiting_io_hw(io_res_obj_t *ifc, int val);

void _q_ext_itr_wait_handler(uint32_t pin);
void _q_ext_itr_end_wait_handler(uint8_t pin);

void *_q_alloc_handler(u32 len);

//int  rrrrr(int a, int b);

#ifdef Q_STACK_BOUNDARIES_CHECK
void thread_stack_check(void);
#endif

#ifdef Q_IS_THREAD_P_CHECK_
void q_is_thread(q_th_t* thread);
#endif

#ifdef Q_CORTEX_FAULT_STATUS

typedef enum {

    MMEMNOERR = 0,

    IACCVIOL = 1 << 0,
    /*Instruction access violation flag:
0 = no instruction access violation fault
1 = the processor attempted an instruction fetch from a location that does not permit execution.
This fault occurs on any access to an XN region, even when the MPU is disabled or not present.
When this bit is 1, the PC value stacked for the exception return points to the faulting instruction. The processor has not written a fault address to the MMAR.
     * */
    DACCVIOL = 1 << 1,
    /*Data access violation flag:

0 = no data access violation fault
1 = the processor attempted a load or store at a location that does not permit the operation.
When this bit is 1, the PC value stacked for the exception return points to the faulting instruction. The processor has loaded the MMAR with the address of the attempted access.
     * */

    MUNSTKERR = 1 << 3,
    /*MemManage fault on unstacking for a return from exception:
0 = no unstacking fault
1 = unstack for an exception return has caused one or more access violations.
This fault is chained to the handler. This means that when this bit is 1, the original return stack is still present. The processor has not adjusted the SP from the failing return, and has not performed a new save. The processor has not written a fault address to the MMAR.
     * */

    MSTKERR = 1 << 4,
    /*MemManage fault on stacking for exception entry:
0 = no stacking fault
1 = stacking for an exception entry has caused one or more access violations.
When this bit is 1, the SP is still adjusted but the values in the context area on the stack might be incorrect. The processor has not written a fault address to the MMAR.
*/

    MLSPERR = 1 << 5,
    /*
     * 0 = no MemManage fault occurred during floating-point lazy state preservation
1 = a MemManage fault occurred during floating-point lazy state preservation.
*/

    MMARVALID = 1 << 7,
    /*MemManage Fault Address Register (MMFAR) valid flag:
0 = value in MMAR is not a valid fault address
1 = MMAR holds a valid fault address.
If a MemManage fault occurs and is escalated to a HardFault because of priority, the HardFault handler must set this bit to 0. This prevents problems on return to a stacked active MemManage fault handler whose MMAR value has been overwritten.
     * */

} mmem_fault_t;


typedef enum {

    BUSNOERR = 0,

    IBUSERR = 1 << 0,
    /*Instruction bus error:
0 = no instruction bus error
1 = instruction bus error.
The processor detects the instruction bus error on prefetching an instruction, but it sets the IBUSERR flag to 1 only if it attempts to issue the faulting instruction.
When the processor sets this bit is 1, it does not write a fault address to the BFAR.
*/
    PRECISERR = 1 << 1,
    /*Precise data bus error:
0 = no precise data bus error
1 = a data bus error has occurred, and the PC value stacked for the exception return points to the instruction that caused the fault.
When the processor sets this bit is 1, it writes the faulting address to the BFAR.
*/
    IMPRECISERR = 1 << 2,
    /*
Imprecise data bus error:
0 = no imprecise data bus error
1 = a data bus error has occurred, but the return address in the stack frame is not related to the instruction that caused the error.
When the processor sets this bit to 1, it does not write a fault address to the BFAR.
This is an asynchronous fault. Therefore, if it is detected when the priority of the current process is higher than the BusFault priority, the BusFault becomes pending and becomes active only when the processor returns from all higher priority processes. If a precise fault occurs before the processor enters the handler for the imprecise BusFault, the handler detects both IMPRECISERR set to 1 and one of the precise fault status bits set to 1.
*/

    UNSTKERR = 1 << 3,
    /*
     * BusFault on unstacking for a return from exception:
0 = no unstacking fault
1 = unstack for an exception return has caused one or more BusFaults.
This fault is chained to the handler. This means that when the processor sets this bit to 1, the original return stack is still present. The processor does not adjust the SP from the failing return, does not performed a new save, and does not write a fault address to the BFAR.
*/
    STKERR = 1 << 4,
    /*
     * BusFault on stacking for exception entry:
0 = no stacking fault
1 = stacking for an exception entry has caused one or more BusFaults.
When the processor sets this bit to 1, the SP is still adjusted but the values in the context area on the stack might be incorrect. The processor does not write a fault address to the BFAR.
*/

    LSPERR = 1 << 5,
    /*
     *

0 = no bus fault occurred during floating-point lazy state preservation
1 = a bus fault occurred during floating-point lazy state preservation.
*/
    BFARVALID = 1 << 7
    /*
     * BusFault Address Register (BFAR) valid flag:
0 = value in BFAR is not a valid fault address
1 = BFAR holds a valid fault address.
The processor sets this bit to 1 after a BusFault where the address is known. Other faults can set this bit to 0, such as a MemManage fault occurring later.
If a BusFault occurs and is escalated to a hard fault because of priority, the hard fault handler must set this bit to 0. This prevents problems if returning to a stacked active BusFault handler whose BFAR value has been overwritten.
*/

} bus_fault_t;

typedef enum {

    USNOERR = 0,

    UNDEFINSTR = 1 << 0,
    /*
     * Undefined instruction UsageFault:
0 = no undefined instruction UsageFault
1 = the processor has attempted to execute an undefined instruction.
When this bit is set to 1, the PC value stacked for the exception return points to the undefined instruction.
An undefined instruction is an instruction that the processor cannot decode.
*/

    INVSTATE = 1 << 1,
    /*
     * Invalid state UsageFault:
0 = no invalid state UsageFault
1 = the processor has attempted to execute an instruction that makes illegal use of the EPSR.
When this bit is set to 1, the PC value stacked for the exception return points to the instruction that attempted the illegal use of the EPSR.
This bit is not set to 1 if an undefined instruction uses the EPSR.
*/

    INVPC = 1 << 2,
    /*
     * Invalid PC load UsageFault, caused by an invalid PC load by EXC_RETURN:
0 = no invalid PC load UsageFault
1 = the processor has attempted an illegal load of EXC_RETURN to the PC, as a result of an invalid context, or an invalid EXC_RETURN value.
When this bit is set to 1, the PC value stacked for the exception return points to the instruction that tried to perform the illegal load of the PC.
*/

    NOCP = 1 << 3,
    /*
No coprocessor UsageFault. The processor does not support coprocessor instructions:
0 = no UsageFault caused by attempting to access a coprocessor
1 = the processor has attempted to access a coprocessor.
*/

    UNALIGNED = 1 << 8,
    /*
     * Unaligned access UsageFault:
0 = no unaligned access fault, or unaligned access trapping not enabled
1 = the processor has made an unaligned memory access.
Enable trapping of unaligned accesses by setting the UNALIGN_TRP bit in the CCR to 1, see Configuration and Control Register.
Unaligned LDM, STM, LDRD, and STRD instructions always fault irrespective of the setting of UNALIGN_TRP.
*/

    DIVBYZERO = 1 << 9
    /*
Divide by zero UsageFault:
0 = no divide by zero fault, or divide by zero trapping not enabled
1 = the processor has executed an SDIV or UDIV instruction with a divisor of 0.
When the processor sets this bit to 1, the PC value stacked for the exception return points to the instruction that performed the divide by zero.
Enable trapping of divide by zero by setting the DIV_0_TRP bit in the CCR to 1, see Configuration and Control Register.
*/

} usage_fault_t;

typedef struct {

    memaddr_t R0;
    memaddr_t R1;
    memaddr_t R2;
    memaddr_t R3;
    memaddr_t R12;
    memaddr_t LR;
    memaddr_t PC;
    memaddr_t xPSR;

} hw_stackframe_t;


#endif


#endif //__ASSEMBLY__
#endif // _QOS_CORE_H
