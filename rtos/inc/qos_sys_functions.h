#ifndef __QOS_SYS_FUNCTIONS_H
#define __QOS_SYS_FUNCTIONS_H

#include "stdint.h"
#include "qos_drivers/general.h"
#include "qos_drivers/uart.h"
#include "qos_itc.h"

void q_exit(void);
void q_delay(uint32_t num);

/**
 * @brief Control io interfaces
 * @param rd    Resource descriptor
 * @param conf  Pointer to conf struct, unique for each type of bus
 */

void q_ioctl(int rd, void *conf);


int q_read(int rd, uint32_t reg, uint8_t *buf, uint32_t nbyte);
int q_write(int rd, uint32_t reg, uint8_t* buf, uint32_t nbyte);
int q_open(io_res_t res);
qos_err_code q_close(int rd);
void q_mutex_lock(q_mutex_t *mutex);
void q_mutex_unlock(q_mutex_t *mutex);
void q_ext_itr_wait(uint32_t pin);
int q_rawread(int rd, uint8_t *buf, uint32_t nbyte);
int q_rawwrite(int rd, uint8_t* buf, uint32_t nbyte);
int q_ioswap(int rd, uint8_t* tx_buf, uint8_t* r0x_buf, uint32_t nbyte);
void* q_alloc(u32 len);
void q_free(void *ptr);
u32 q_exec(void* func_ptr, u32 stack_size, u32 prio);

//debug
void q_mpu_test(uint32_t size);
//debug


//#define q_ioctl(rd, conf) _Generic((conf), \
//                q_conf_usart_t*: q_ioctl_usart, \
//                q_conf_i2c_t*: q_ioctl_i2c \
//    )(rd, conf)

#endif // __QOS_SYS_FUNCTIONS_H
