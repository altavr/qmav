#ifndef __QOS_THREADS_H
#define __QOS_THREADS_H


#include "inttypes.h"
#include "qos_def.h"
#include "qos_ln_list.h"
#include "qos_itc.h"
#include "qos_memory.h"
#include <stddef.h>
//#include "qos_core.h"

#define TIMER_QUE 1
#define PRIOR_MAX 32
//#define RESOURCE_MAX 2

#define THREADS_MAX 32
#define MUTEX_MAX   2
#define EXT_ITR_LINE_MAX 16


//#define RAM_ORIGIN 0x20000000


#define IDLE_PRIOR 32



typedef enum  {
    RUNNABLE,
    WAIT,
    SUSPEND,
    DORMANT
} thread_stat_t;

typedef enum {
    Q_TIMEOUT_WAIT,
    Q_HW_WAIT,
    Q_MX_WAIT,
    Q_EXT_ITR_WAIT
} wait_reason_t;


struct q_th_st {

    memaddr_t stack_top;
    memaddr_t base;
#ifdef FPU_ENABLE
    uint8_t use_fpu;
#endif
    uint8_t prior;
    uint8_t original_prio;
    uint8_t rd_active;
#ifdef FPU_ENABLE
    uint32_t fpu_frame_stack[33];
#endif
    q_lnim_t list_item;
    uint32_t stack_size;
    thread_stat_t stat;
    wait_reason_t reason;
    memaddr_t thread_code;

    uint32_t id;
    uint32_t timeout;
    uint32_t period;
    struct q_mutex_st* mutex;
    //    uint32_t wait_mutex_p;
    uint32_t temp;

    sbcb_t  stack_cb[_Q_MPU_STACK_REGIONS];
#ifdef MPU_ENABLE
    mpu_rp_t mpu_conf[_Q_MPU_STACK_REGIONS + _Q_MPU_DYNAMIC_REGIONS];
#endif //MPU_ENABLE
    q_lnim_t memblocks;
#ifdef TIME_M_ENABLE
    q_time_t cpu_time;
    q_tickstamp_t runtime;
#endif //TIME_M_ENABLE
};

typedef struct q_th_st q_th_t;

extern uint32_t runnable_prior_threads;
extern q_th_t qos_threads_pool[THREADS_MAX];
extern q_lnim_t qos_prior_thread_list[PRIOR_MAX + 1];
extern q_lnim_t qos_timer_thread_list;
extern q_th_t* qos_idle_thread;
extern q_lnim_t qos_ext_itr_wait_list;
extern uint32_t qos_threads_num;



void qos_init_all_lists(void);
void qos_empty_list_init(q_lnim_t *list_item, uint8_t num);

q_th_t *qos_first_thread_waiting_ext_itr(uint32_t ext_itr, q_lnim_t* queue);
q_th_t *qos_first_thread_waiting_mutex(uint32_t mutex_id, q_lnim_t* queue);
void qos_thread_set_wait_prio_order(q_th_t* thread, q_lnim_t* queue, wait_reason_t reason);
void qos_thread_set_time_wait_state(q_th_t* thread, q_lnim_t* queue, uint32_t timeout);
void qos_thread_set_runnable(q_th_t* thread);
void qos_thread_set_suspend(q_th_t* thread);
void qos_thread_set_dormant(q_th_t* thread);
void qos_thread_set_prio(q_th_t* thread, uint8_t prior);

q_th_t* qos_get_new_thread(void);
int qos_thread_init(q_th_t* thread, uint32_t stack_size, uint8_t prior, void* func);
void qos_init_idle_thread(void);

void idle_func(void);

size_t  _qos_round_stack_size_mpu(size_t size);

#endif // __QOS_THREADS_H
