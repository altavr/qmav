#ifndef __DRV_GENERAL_H
#define __DRV_GENERAL_H

#include <stdint.h>
#include "qos_def.h"
#include "qos_ln_list.h"

//#include "qos_drivers/mcu_gpio.h"

#ifndef DESKTOP_TEST
#include "qos_drivers/stm_types.h"
//    #ifdef STM32F303_
//        #include "stm32f30x.h"
//    #endif
//    #ifdef STM32F407_
//        #include "stm32f4xx.h"
//    #endif
//#include "qos_drivers/uart.h"
//#include "qos_drivers/i2c.h"


//#include "stm32f30x_usart.h"
//#include "stm32f30x_i2c.h"
//#include "stm32f30x_gpio.h"
//#include "stm32f30x_rcc.h"

#endif //DESKTOP_TEST



#define GPIOA_ID    0
#define GPIOB_ID    1
#define GPIOC_ID    2
#define GPIOD_ID    3
#define GPIOE_ID    4

#ifdef STM32F303_
#define _DMA_CTRL_FROM_ZERO(ctrl) (ctrl + MCU_DMA_CONTRL_OFFSET)
#define _DMA_CH_FROM_ZERO(ch) (ch + MCU_DMA_CH_OFFSET)
#define DMA_IN_ORDER_NUM(ctrl, ch) (_DMA_CTRL_FROM_ZERO(ctrl) * MCU_DMA_CHANNEL_MAX + _DMA_CH_FROM_ZERO(ch))
#endif  //STM32F303_

#ifdef STM32F407_
#define _DMA_CTRL_FROM_ZERO(ctrl) (ctrl)
#define _DMA_STREAM_FROM_ZERO(ch) (ch)
#define DMA_IN_ORDER_NUM(ctrl, ch) (_DMA_CTRL_FROM_ZERO(ctrl) * MCU_DMA_STREAM_MAX + _DMA_STREAM_FROM_ZERO(ch))
#endif  //STM32F407_

#define _PORT_PIN_2_COMB_FMT(port_id, pin) ((port_id << 8) & pin)
#define _PORT_FROM_COMB_FMT(comb) (comb >> 8)
#define _PIN_FROM_COMB_FMT(comb) (comb & 0xFF)

typedef enum {
    Q_USART_T,
    Q_I2C_T,
    Q_SPI_T
} q_io_res_type;

struct io_res_obj;

enum __q_io_mode {
    Q_READMODE = BUS_READ_MODE_FLAG,
    Q_WRITEMODE = 0,
    Q_RAWREADMODE = BUS_READ_MODE_FLAG | BUS_RAW_MODE_FLAG,
    Q_RAWWRITEMODE = BUS_RAW_MODE_FLAG,
    Q_SWAPMODE = BUS_SWAPMODE_FLAG
};

typedef enum  __q_io_mode q_io_mode_t;


typedef struct q_descriptor_i
{
    uint8_t* buf;
    uint8_t* aux_buf;
    uint8_t status;
    uint16_t nb;
    uint16_t count;
    uint16_t reg;
    q_io_mode_t mode;
    struct io_res_obj *ifc;
    void *conf;
} q_descriptor_i_t;

//#include "qos_drivers/uart.h"
//#include "qos_drivers/i2c.h"

struct  io_res_obj{
    uint32_t base;
    io_res_t resnum;
    q_io_res_type type;
    uint16_t flags;
    uint32_t port_conf;
    uint32_t pins_conf;
    uint8_t iface_itp_no;
    uint8_t dma_rx_itp_no;
    uint8_t dma_tx_itp_no;

    uint8_t iface_prio_preempt;
    uint8_t iface_prio_sub;
    uint8_t dma_rx_prio_preempt;
    uint8_t dma_rx_prio_sub;
    uint8_t dma_tx_prio_preempt;
    uint8_t dma_tx_prio_sub;


    struct q_descriptor_i* rd;
    DMA_Ch_TD* dma_rx;
    DMA_Ch_TD* dma_tx;
    void (*read_h)(q_descriptor_i_t* rd_i);
    void (*write_h)(q_descriptor_i_t* rd_i);
    void (*ioswap_h)(q_descriptor_i_t* rd_i);
    void (*iface_init)(struct io_res_obj* res_obj);
    q_lnim_t lnlist;
    uint8_t prio;
    uint8_t in_process;
};

typedef struct  io_res_obj io_res_obj_t;

typedef struct
{
    uint16_t dev;
//    uint16_t reg;
} q_conf_i2c_t;

typedef struct
{
    void* pref;
} q_conf_usart_t;
/*
typedef struct
{
    uint8_t port_id;
    uint8_t pin;
} q_spi_dev_if_t;
*/
typedef struct
{
//    uint8_t reg;
    uint8_t nss_port_id;
    uint16_t nss_pin;

} q_conf_spi_t;

#ifndef DESKTOP_TEST


extern q_descriptor_i_t res_desc_table[RES_DESCRIPTORS_MAX];
extern io_res_obj_t io_handlers[];
extern io_res_obj_t* dma_res_links[];

void q_pin_itr_init(uint32_t port_id, uint32_t pin, uint8_t no_alt_func, uint8_t mode);
void q_pin_out_init(uint16_t port_id, uint16_t pin, uint8_t pull, uint8_t num_alt_func);

void q_set_pin_lev(uint32_t port_id, uint16_t pin);
void q_unset_pin_lev(uint32_t port_id, uint32_t pin);

void q_iface_gpio_init(io_res_t res);
void q_init_io_hw(void);
void q_iface_dma_init(io_res_obj_t* res, uint32_t iface_tx_reg, uint32_t iface_rx_reg);

int _q_open(io_res_t res);
qos_err_code _q_close(int rd);
void _q_read(q_descriptor_i_t* rd_i);
void _q_write(q_descriptor_i_t* rd_i);
void _q_rawread(q_descriptor_i_t* rd_i);
void _q_rawwrite(q_descriptor_i_t* rd_i);
void _q_ioswap(q_descriptor_i_t* rd_i);

void _q_ioctl(int rd, void *conf);


void gpio_conf_default(GPIO_InitTypeDef* s);
void dma_conf_default(DMA_InitTypeDef* s);
void q_set_using(uint32_t part, uint8_t n, uint32_t flags[]);
void q_unset_using(uint32_t part, uint8_t n, uint32_t flags[]);



//***************debug*************
typedef enum {
    NONE,
    START,
    TXE,
    RXNE,
    ADDR,
    END,
    ITR,
    BTF,
    EV_DMA_RX,
    EV_DMA_TX
} call_ev_t;

void clean_seq(void);


//***************debug*************


#endif //DESKTOP_TEST
#endif // __DRV_GENERAL_H
#
