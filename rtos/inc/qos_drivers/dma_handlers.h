#ifndef DMA_HANDLERS_H
#define DMA_HANDLERS_H
#include "qos_drivers/general.h"
#ifdef STM32F303_
    #include "qos_drivers/mcu_f303_gpio.h"
#endif
#ifdef STM32F407_
    #include "qos_drivers/mcu_f407_gpio.h"
#endif

#define _DISABLE_USART_DMA(ro, dir) USART_DMACmd((USART_TypeDef*)ro, dir, DISABLE)
#define _DISABLE_I2C_DMA(ro, dir) I2C_DMACmd((I2C_TypeDef*) ro , DISABLE)
#define _DISABLE_SPI_DMA(ro, dir) ((SPI_TypeDef*)( ro ->base))->CR2 &= (~ dir )


#ifdef STM32F303_
#ifdef _DMA_1_STREAM_3
#if _DMA_1_STREAM_3 == USART_DEF
#define DISABLE_BUS_DMA1_2(ro) _DISABLE_USART_DMA(ro, USART_DMAReq_Tx)
//#define DISABLE_BUS_DMA1_2(ro) USART_DMACmd((USART_TypeDef*)ro, USART_DMAReq_Tx, DISABLE)
//#define BUS_ITRP(ro) USART_ITConfig((USART_TypeDef*)ro, USART_IT_TC, ENABLE );
#elif _DMA_1_CH_2 == I2C_DEF
#define DISABLE_DMA1_2(ro)
#endif

#endif

#ifdef _DMA_1_CH_5
#if _DMA_1_CH_5 == USART_DEF
#define DISABLE_BUS_DMA1_2(ro)
#elif _DMA_1_CH_5 == I2C_DEF
#define DISABLE_BUS_DMA1_5(ro) _DISABLE_I2C_DMA(ro, I2C_DMAReq_Rx)
#endif

#endif
#endif //STM32F303_


#ifdef STM32F407_
#ifdef _DMA_1_STREAM_3
#if _DMA_1_STREAM_3 == USART_DEF
#define DISABLE_BUS_DMA1_2(ro) _DISABLE_USART_DMA(ro, USART_DMAReq_Tx)
#define _DMA_1_STREAM_3_USART3
#endif

#endif
/*
#ifdef _DMA_1_CH_5
#if _DMA_1_CH_5 == USART_DEF
#define DISABLE_BUS_DMA1_2(ro)
#elif _DMA_1_CH_5 == I2C_DEF
#define DISABLE_BUS_DMA1_5(ro) _DISABLE_I2C_DMA(ro, I2C_DMAReq_Rx)
#endif

#endif
*/

//USART1 TX
#ifdef _DMA_2_STREAM_7
#if _DMA_2_STREAM_7 == USART_DEF
#define DISABLE_BUS_DMA2_7(ro) _DISABLE_USART_DMA(ro, USART_DMAReq_Tx)
#endif //_DMA_2_STREAM_7 == USART_DEF
#endif //_DMA_2_STREAM_7


//I2C1 TX
#ifdef _DMA_1_STREAM_6
#if _DMA_1_STREAM_6 == I2C_DEF
#define DISABLE_BUS_DMA1_6(ro) _DISABLE_I2C_DMA(ro, ro)
#endif //_DMA_1_STREAM_6 == I2C_DEF
#endif //_DMA_1_STREAM_6
//I2C1 RX
#ifdef _DMA_1_STREAM_0
#if _DMA_1_STREAM_0 == I2C_DEF
#define DISABLE_BUS_DMA1_0(ro) _DISABLE_I2C_DMA(ro, ro)
#endif //_DMA_1_STREAM_0 == I2C_DEF
#endif //_DMA_1_STREAM_0

//I2C2 TX
#ifdef _DMA_1_STREAM_7
#if _DMA_1_STREAM_7 == I2C_DEF
#define DISABLE_BUS_DMA1_7(ro) _DISABLE_I2C_DMA(ro, ro)
#endif //_DMA_1_STREAM_7 == I2C_DEF
#endif //_DMA_1_STREAM_7
//I2C2 RX
#ifdef _DMA_1_STREAM_2
#if _DMA_1_STREAM_2 == I2C_DEF
#define DISABLE_BUS_DMA1_2(ro) _DISABLE_I2C_DMA(ro, ro)
#endif //_DMA_1_STREAM_2 == I2C_DEF
#endif //_DMA_1_STREAM_2


//SPI2 TX
#ifdef _DMA_1_STREAM_4
#if _DMA_1_STREAM_4 == SPI_DEF
#define DISABLE_BUS_DMA1_4(ro) _DISABLE_SPI_DMA(ro, SPI_CR2_TXDMAEN)
#endif //_DMA_1_STREAM_7 == SPI_DEF
#endif //_DMA_1_STREAM_7
//SPI2 RX
#ifdef _DMA_1_STREAM_3
#if _DMA_1_STREAM_3 == SPI_DEF
#define DISABLE_BUS_DMA1_3(ro) _DISABLE_SPI_DMA(ro, SPI_CR2_RXDMAEN)
#endif //_DMA_1_STREAM_2 == SPI_DEF
#endif //_DMA_1_STREAM_2

//SPI1 TX
#ifdef _DMA_2_STREAM_5
#if _DMA_2_STREAM_5 == SPI_DEF
#define DISABLE_BUS_DMA2_5(ro) _DISABLE_SPI_DMA(ro, SPI_CR2_TXDMAEN)
#endif
#endif
//SPI1 RX
#ifdef _DMA_2_STREAM_2
#if _DMA_2_STREAM_2 == SPI_DEF
#define DISABLE_BUS_DMA2_2(ro) _DISABLE_SPI_DMA(ro, SPI_CR2_RXDMAEN)
#endif
#endif

#endif //STM32F407_


#endif // DMA_HANDLERS_H
