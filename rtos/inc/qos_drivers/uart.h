#ifndef __DRV_UART_H
#define __DRV_UART_H

#include "qos_def.h"
#include "qos_drivers/general.h"




void q_read_usart_handler(q_descriptor_i_t* rd_i);
void q_write_usart_handler(q_descriptor_i_t* rd_i);
void q_read_usart_dma_handler(q_descriptor_i_t* rd_i);
void q_write_usart_dma_handler(q_descriptor_i_t* rd_i);
void q_ioswap_usart_handler(q_descriptor_i_t *rd_i);
void q_ioswap_usart_dma_handler(q_descriptor_i_t *rd_i);
void q_usart_init(io_res_obj_t *res);
void q_usart_dma_init(io_res_obj_t *res);
void q_ioctl_usart_handler(q_descriptor_i_t* rd_i);
#endif // __DRV_UART_H
