#ifndef I2C_H
#define I2C_H

#include "qos_def.h"
#include "qos_drivers/general.h"



void q_read_i2c_handler(q_descriptor_i_t *rd_i);
void q_read_i2c_dma_handler(q_descriptor_i_t* rd_i);
void q_write_i2c_handler(q_descriptor_i_t* rd_i);
void q_write_i2c_dma_handler(q_descriptor_i_t* rd_i);
void q_ioswap_i2c_handler(q_descriptor_i_t* rd_i);
void q_ioswap_i2c_dma_handler(q_descriptor_i_t* rd_i);
void q_i2c_init(io_res_obj_t* res_obj);

void q_i2c_dma_init(io_res_obj_t* res);
#endif // I2C_H
