#ifndef SPI_H
#define SPI_H
#include "qos_def.h"
#include "qos_drivers/general.h"

void q_read_spi_handler(q_descriptor_i_t *rd_i);
void q_read_spi_dma_handler(q_descriptor_i_t* rd_i);
void q_write_spi_handler(q_descriptor_i_t* rd_i);
void q_write_spi_dmi_handler(q_descriptor_i_t* rd_i);
void q_ioswap_spi_handler(q_descriptor_i_t* rd_i);
void q_ioswap_spi_dma_handler(q_descriptor_i_t* rd_i);
void q_spi_init(io_res_obj_t* res_obj);

void q_spi_dma_init(io_res_obj_t* res_obj);
//void q_ioctl_spi_handler(q_descriptor_i_t* rd_i, void* conf);

#endif // SPI_H
