#ifndef MCU_GPIO_H
#define MCU_GPIO_H

#include <stdint.h>
#include "qos_def.h"
#include "qos_drivers/uart.h"
#include "qos_drivers/i2c.h"
#include "qos_drivers/spi.h"
#include "stm32f4xx.h"

#ifndef DESKTOP_TEST
#include "stm32f4xx_rcc.h"
#endif


#define SF_SET(what, offset, n) (what << (offset + n))
#define SF_GET(val, offset, mask, n) ((val >> (offset + n)) & mask)
//******************************************************

#define EMPTY_FLAG          0
//#define BUS_DMA_RX           0x10
//#define BUS_DMA_TX           0x20


#define BUS_GPIO_EN_FLAG      0x1
#define BUS_EN_FLAG          0x2
#define BUS_DMA_RX_EN_FLAG     0x4
#define BUS_DMA_TX_EN_FLAG     0x8
#define BUS_SLAVE_FLAG         0x10
#define BUS_I2C_ADDR_FLAG       0x80
#define BUS_I2C_START_FLAG    0x100

#define USART_DEF 1
#define I2C_DEF 2
#define SPI_DEF 3


//******************************************************

#define DMA_C_MASK  0b1
#define DMA_C_OFFSET  0
#define DMA_C_SET(c) SF_SET(c, DMA_C_OFFSET, 0)
#define DMA_C_GET(val) SF_GET(val, DMA_C_OFFSET, \
    DMA_C_MASK, 0)

#define DMA1_C  0
#define DMA2_C  1

//define DMA for interfaces
#define DMA_USART1  DMA2_C
#define DMA_USART3  DMA1_C
#define DMA_I2C1    DMA1_C
#define DMA_I2C2    DMA1_C
#define DMA_SPI2    DMA1_C
#define DMA_SPI1    DMA2_C

//for substitution in struct definition
#define DMA_USART1_NUM  2
#define DMA_USART3_NUM  1
#define DMA_I2C1_NUM    1
#define DMA_I2C2_NUM    1
#define DMA_SPI2_NUM    1
#define DMA_SPI1_NUM    2


//******************************************************
#define DMA_TX_STREAM_MASK     0b111
#define DMA_TX_STREAM_OFFSET   1
#define DMA_TX_STREAM_SET(stream) SF_SET(stream, DMA_TX_STREAM_OFFSET, 0)
#define DMA_TX_STREAM_GET(val) SF_GET(val, DMA_TX_STREAM_OFFSET, \
    DMA_TX_STREAM_MASK, 0)

//define tx DMA STREAMs for interfaces
#define DMA_TX_STREAM_USART1    7
#define DMA_TX_STREAM_USART3    3
#define DMA_TX_STREAM_I2C1      6
#define DMA_TX_STREAM_I2C2      7
#define DMA_TX_STREAM_SPI2      4
#define DMA_TX_STREAM_SPI1      5

//******************************************************
#define DMA_RX_STREAM_MASK     0b111
#define DMA_RX_STREAM_OFFSET   4
#define DMA_RX_STREAM_SET(stream) SF_SET(stream, DMA_RX_STREAM_OFFSET, 0)
#define DMA_RX_STREAM_GET(val) SF_GET(val, DMA_RX_STREAM_OFFSET, \
    DMA_RX_STREAM_MASK, 0)

//define RX DMA STREAMs for interfaces
#define DMA_RX_STREAM_USART1    5
#define DMA_RX_STREAM_USART3    1
#define DMA_RX_STREAM_I2C1      0
#define DMA_RX_STREAM_I2C2      2
#define DMA_RX_STREAM_SPI2      3
#define DMA_RX_STREAM_SPI1      2


//******************************************************
#define DMA_TX_CHANNEL_MASK     0b111
#define DMA_TX_CHANNEL_OFFSET   7
#define DMA_TX_CHANNEL_SET(ch) SF_SET(ch, DMA_TX_CHANNEL_OFFSET, 0)
#define DMA_TX_CHANNEL_GET(val) SF_GET(val, DMA_TX_CHANNEL_OFFSET, \
    DMA_TX_CHANNEL_MASK, 0)

//define tx DMA channels for interfaces
#define DMA_TX_CH_USART1    4
#define DMA_TX_CH_USART3    4
#define DMA_TX_CH_I2C1      1
#define DMA_TX_CH_I2C2      7
#define DMA_TX_CH_SPI2      0
#define DMA_TX_CH_SPI1      3

//******************************************************

#define DMA_RX_CHANNEL_MASK     0b111
#define DMA_RX_CHANNEL_OFFSET   10
#define DMA_RX_CHANNEL_SET(ch) SF_SET(ch, DMA_RX_CHANNEL_OFFSET, 0)
#define DMA_RX_CHANNEL_GET(val) SF_GET(val, DMA_RX_CHANNEL_OFFSET, \
    DMA_RX_CHANNEL_MASK, 0)

////define rx DMA channels for interfaces
#define DMA_RX_CH_USART1    4
#define DMA_RX_CH_USART3    4
#define DMA_RX_CH_I2C1      1
#define DMA_RX_CH_I2C2      7
#define DMA_RX_CH_SPI2      0
#define DMA_RX_CH_SPI1      3

//******************************************************


#define GPIO_CLOCK_EN_MASK     0b11111
#define GPIO_CLOCK_EN_OFFSET_OFFSET   13
#define GPIO_CLOCK_EN_OFFSET_SET(bus) SF_SET(bus, GPIO_CLOCK_EN_OFFSET_OFFSET, 0)
#define GPIO_CLOCK_EN_OFFSET_GET(val) SF_GET(val, GPIO_CLOCK_EN_OFFSET_OFFSET, \
    GPIO_CLOCK_EN_MASK, 0)

//
#define GPIO_CLOCK_EN_BIT_A 0
#define GPIO_CLOCK_EN_BIT_B 1
#define GPIO_CLOCK_EN_BIT_C 2
#define GPIO_CLOCK_EN_BIT_D 3
#define GPIO_CLOCK_EN_BIT_E 4
#define GPIO_CLOCK_EN_BIT_F 5
#define GPIO_CLOCK_EN_BIT_G 6
#define GPIO_CLOCK_EN_BIT_H 7
#define GPIO_CLOCK_EN_BIT_J 8
#define GPIO_CLOCK_EN_BIT_K 9
//
#define GPIO_CLOCK_BIT_PORT_ID_0    GPIO_CLOCK_EN_BIT_A
#define GPIO_CLOCK_BIT_PORT_ID_1    GPIO_CLOCK_EN_BIT_B
#define GPIO_CLOCK_BIT_PORT_ID_2    GPIO_CLOCK_EN_BIT_C
#define GPIO_CLOCK_BIT_PORT_ID_3    GPIO_CLOCK_EN_BIT_D
#define GPIO_CLOCK_BIT_PORT_ID_4    GPIO_CLOCK_EN_BIT_E
#define GPIO_CLOCK_BIT_PORT_ID_5    GPIO_CLOCK_EN_BIT_F

//******************************************************

#define RCC_EN_BIT_MASK     0b11111
#define RCC_EN_BIT_OFFSET_OFFSET   18
#define RCC_EN_BIT_OFFSET_SET(bus) SF_SET(bus, RCC_EN_BIT_OFFSET_OFFSET, 0)
#define RCC_EN_BIT_OFFSET_GET(val) SF_GET(val, RCC_EN_BIT_OFFSET_OFFSET, \
    RCC_EN_BIT_MASK, 0)

//
#define RCC_EN_BIT_USART1   4
#define RCC_EN_BIT_USART2   17
#define RCC_EN_BIT_USART3   18
#define RCC_EN_BIT_USART4   19
#define RCC_EN_BIT_USART5   20
#define RCC_EN_BIT_USART6   5
#define RCC_EN_BIT_USART7   30
#define RCC_EN_BIT_USART8   31
#define RCC_EN_BIT_I2C1     21
#define RCC_EN_BIT_I2C2     22
#define RCC_EN_BIT_I2C3     23
#define RCC_EN_BIT_SPI1     12
#define RCC_EN_BIT_SPI2     14
#define RCC_EN_BIT_SPI3     15
#define RCC_EN_BIT_SPI4     13
#define RCC_EN_BIT_SYSCFG   14

//******************************************************

#define ALTER_FUNC_MASK     0b1111
#define ALTER_FUNC_OFFSET   23
#define ALTER_USART123         7
#define ALTER_USART456         8
#define ALTER_I2C           4
#define ALTER_SPI12           5
#define ALTER_FUNC_SET(t)    SF_SET(t, ALTER_FUNC_OFFSET, 0)
#define ALTER_FUNC_GET(val)  SF_GET(val, ALTER_FUNC_OFFSET, ALTER_FUNC_MASK, 0)

//******************************************************

#define RCC_MASK    0b11
#define RCC_OFFSET  27
#define RCC_AHB1_ID     0
//#define RCC_AHB2_ID     1
//#define RCC_AHB3_ID     2
#define RCC_APB1_ID     1
#define RCC_APB2_ID     2
#define RCC_IFACE_IDX_SET(port) SF_SET(port, RCC_OFFSET, 0)
#define RCC_IFACE_IDX_GET(val) SF_GET(val, RCC_OFFSET, RCC_MASK, 0)

//
#define RCC_BUS_USART1  RCC_APB2_ID
#define RCC_BUS_USART2   RCC_APB1_ID
#define RCC_BUS_USART3   RCC_APB1_ID
#define RCC_BUS_USART4   RCC_APB1_ID
#define RCC_BUS_USART5   RCC_APB1_ID
#define RCC_BUS_USART6  RCC_APB2_ID
#define RCC_BUS_USART7   RCC_APB1_ID
#define RCC_BUS_USART8   RCC_APB1_ID
#define RCC_BUS_I2C1    RCC_APB1_ID
#define RCC_BUS_I2C2    RCC_APB1_ID
#define RCC_BUS_I2C3    RCC_APB1_ID
#define RCC_BUS_SPI1    RCC_APB2_ID
#define RCC_BUS_SPI2    RCC_APB1_ID
#define RCC_BUS_SPI3    RCC_APB1_ID
#define RCC_BUS_SPI4    RCC_APB2_ID
#define RCC_SYSCFG      RCC_APB2_ID
//


#define GPIO_MASK   0b111
#define GPIO_OFFSET 29

#define GPIO_PORT_IDX_SET(port) SF_SET(port, GPIO_OFFSET, 0)
#define GPIO_PORT_IDX_GET(val) SF_GET(val, GPIO_OFFSET, GPIO_MASK, 0)
//******************************************************


//define RCC enable for type dev

#define RCC_DMA_BUS_ID  RCC_AHB1_ID
#define GPIO_RCC_BUS_ID  RCC_AHB1_ID
//******************************************************
//NVIC priorities

//USART1
#define PRIO_PREEMPT_USART1         2
#define PRIO_SUB_USART1             0
#define PRIO_PREEMPT_USART1_DMA_RX  5
#define PRIO_SUB_USART1_DMA_RX      0
#define PRIO_PREEMPT_USART1_DMA_TX  5
#define PRIO_SUB_USART1_DMA_TX      0


//USART3
#define PRIO_PREEMPT_USART3         2
#define PRIO_SUB_USART3             0
#define PRIO_PREEMPT_USART3_DMA_RX  5
#define PRIO_SUB_USART3_DMA_RX      0
#define PRIO_PREEMPT_USART3_DMA_TX  5
#define PRIO_SUB_USART3_DMA_TX      0

//I2C1
#define PRIO_PREEMPT_I2C1         5
#define PRIO_SUB_I2C1             0
#define PRIO_PREEMPT_I2C1_DMA_RX  5
#define PRIO_SUB_I2C1_DMA_RX      0
#define PRIO_PREEMPT_I2C1_DMA_TX  5
#define PRIO_SUB_I2C1_DMA_TX      0

//I2C2
#define PRIO_PREEMPT_I2C2         5
#define PRIO_SUB_I2C2             0
#define PRIO_PREEMPT_I2C2_DMA_RX  5
#define PRIO_SUB_I2C2_DMA_RX      0
#define PRIO_PREEMPT_I2C2_DMA_TX  5
#define PRIO_SUB_I2C2_DMA_TX      0

//SPI2
#define PRIO_PREEMPT_SPI2         5
#define PRIO_SUB_SPI2             0
#define PRIO_PREEMPT_SPI2_DMA_RX  5
#define PRIO_SUB_SPI2_DMA_RX      0
#define PRIO_PREEMPT_SPI2_DMA_TX  5
#define PRIO_SUB_SPI2_DMA_TX      0

//SPI1
#define PRIO_PREEMPT_SPI1         5
#define PRIO_SUB_SPI1             0
#define PRIO_PREEMPT_SPI1_DMA_RX  5
#define PRIO_SUB_SPI1_DMA_RX      0
#define PRIO_PREEMPT_SPI1_DMA_TX  5
#define PRIO_SUB_SPI1_DMA_TX      0


//pins definitions

#define PIN_NUM_IFACE_MAX   4
#define PIN_FIELD_LEN       8

#define Q_GPIO_OutType_MASK     1
#define Q_GPIO_OutType_OFFSET   0
#define Q_GPIO_OutType_PP       0x00
#define Q_GPIO_OutType_OD       0x01
#define OTYPE_PIN_SET(pin, t)    SF_SET(t, Q_GPIO_OutType_OFFSET,\
    pin * PIN_FIELD_LEN)
#define OTYPE_PIN_GET(pin, val)      SF_GET(val, Q_GPIO_OutType_OFFSET,\
    Q_GPIO_OutType_MASK, pin * PIN_FIELD_LEN)

#define Q_GPIO_PuPd_MASK    0b11
#define Q_GPIO_PuPd_OFFSET  1
#define Q_GPIO_PuPd_NOPULL  0x00
#define Q_GPIO_PuPd_UP      0x01
#define Q_GPIO_PuPd_DOWN    0x02
#define PuPd_PIN_SET(pin, t)    SF_SET(t, Q_GPIO_PuPd_OFFSET,\
    pin * PIN_FIELD_LEN)
#define PuPd_PIN_GET(pin, val)      SF_GET(val, Q_GPIO_PuPd_OFFSET,\
    Q_GPIO_PuPd_MASK, pin * PIN_FIELD_LEN)


#define PIN_ACT_FLAG_MASK   1
#define PIN_ACT_FLAG_OFFSET 3
#define PIN_EN_VAL          1
#define PIN_DE_VAL          0
#define PIN_SET_EN(pin, ...)  SF_SET(PIN_EN_VAL, PIN_ACT_FLAG_OFFSET, \
    pin * PIN_FIELD_LEN)
#define PIN_IS_EN(pin, val)  SF_GET(val, PIN_ACT_FLAG_OFFSET,\
    PIN_ACT_FLAG_MASK, pin * PIN_FIELD_LEN)

#define PIN_NUM_MASK    0b1111
#define PIN_NUM_OFFSET  4
#define PIN_NUM_SET(pin, t)    SF_SET(t, PIN_NUM_OFFSET,\
    pin * PIN_FIELD_LEN)
#define PIN_NUM_GET(pin, val)      SF_GET(val, PIN_NUM_OFFSET,\
    PIN_NUM_MASK, pin * PIN_FIELD_LEN)



//****************************************************************************
// Bus DMA functions definition

//USART1
#if  defined(USART1_DMA_RX_EN) || defined(USART1_DMA_TX_EN)
#define USART1_CONF_FUNC q_usart_dma_init
#else
#define USART1_CONF_FUNC q_usart_init
#endif

#ifndef USART1_DMA_RX_EN
#define _USART1_DMA_RX EMPTY_FLAG
#else
#define _USART1_DMA_RX BUS_DMA_RX_EN_FLAG
#define _DMA_2_STREAM_5 USART_DEF
#endif
#ifndef USART1_DMA_TX_EN
#define _USART1_DMA_TX EMPTY_FLAG
#else
#define _USART1_DMA_TX BUS_DMA_TX_EN_FLAG
#define _DMA_2_STREAM_7 USART_DEF
#endif


//USART3
//#if  defined(USART3_DMA_RX_EN) || defined(USART3_DMA_TX_EN)
//#define USART3_CONF_FUNC q_usart_dma_init
//#else
//#define USART3_CONF_FUNC q_usart_init
//#endif

//#ifndef USART3_DMA_RX_EN
//#define _USART3_DMA_RX EMPTY_FLAG
//#else
//#define _USART3_DMA_RX BUS_DMA_RX_EN_FLAG
////#define _DMA_1_STREAM_3 USART_DEF
//#endif
//#ifndef USART3_DMA_TX_EN
//#define _USART3_DMA_TX EMPTY_FLAG
//#else
//#define _USART3_DMA_TX BUS_DMA_TX_EN_FLAG
//#define _DMA_1_STREAM_3 USART_DEF
//#endif

//I2C1
//#if  defined(I2C1_DMA_RX_EN) || defined(I2C1_DMA_TX_EN)
//#define I2C1_CONF_FUNC q_i2c_dma_init
//#else
//#define I2C1_CONF_FUNC q_i2c_init
//#endif


//I2C1
#if  defined(I2C1_DMA_EN)
#define I2C1_CONF_FUNC q_i2c_dma_init
#define _DMA_1_STREAM_0 I2C_DEF
#define _DMA_1_STREAM_6 I2C_DEF
#define _I2C1_DMA_TX BUS_DMA_TX_EN_FLAG
#define _I2C1_DMA_RX BUS_DMA_RX_EN_FLAG
#else
#define I2C1_CONF_FUNC q_i2c_init
#define _I2C1_DMA_RX EMPTY_FLAG
#define _I2C1_DMA_TX EMPTY_FLAG
#endif



//I2C2
#if  defined(I2C2_DMA_EN)
#define I2C2_CONF_FUNC q_i2c_dma_init
#define _DMA_1_STREAM_2 I2C_DEF
#define _DMA_1_STREAM_7 I2C_DEF
#define _I2C2_DMA_TX BUS_DMA_TX_EN_FLAG
#define _I2C2_DMA_RX BUS_DMA_RX_EN_FLAG
#else
#define I2C2_CONF_FUNC q_i2c_init
#define _I2C2_DMA_RX EMPTY_FLAG
#define _I2C2_DMA_TX EMPTY_FLAG
#endif


//SPI2

#if  defined(SPI2_DMA_EN)
#define SPI2_CONF_FUNC q_spi_dma_init
#define _DMA_1_STREAM_3 SPI_DEF
#define _DMA_1_STREAM_4 SPI_DEF
#define _SPI2_DMA_TX BUS_DMA_TX_EN_FLAG
#define _SPI2_DMA_RX BUS_DMA_RX_EN_FLAG
#else
#define SPI2_CONF_FUNC q_spi_init
#define _SPI2_DMA_RX EMPTY_FLAG
#define _SPI2_DMA_TX EMPTY_FLAG
#endif


//SPI1

#if  defined(SPI1_DMA_EN)
#define SPI1_CONF_FUNC q_spi_dma_init
#define _DMA_2_STREAM_2 SPI_DEF
#define _DMA_2_STREAM_5 SPI_DEF
#define _SPI1_DMA_TX BUS_DMA_TX_EN_FLAG
#define _SPI1_DMA_RX BUS_DMA_RX_EN_FLAG
#else
#define SPI1_CONF_FUNC q_spi_init
#define _SPI1_DMA_RX EMPTY_FLAG
#define _SPI1_DMA_TX EMPTY_FLAG
#endif


//********************************************************************
//Bus flags expansion
#define _INIT_USART_FLAGS_CONF(num) (_USART##num##_DMA_RX | _USART##num##_DMA_TX)
#define _INIT_I2C_FLAGS_CONF(num) (_I2C##num##_DMA_RX | _I2C##num##_DMA_TX)
#define _INIT_SPI_FLAGS_CONF(num) (_SPI##num##_DMA_RX | _SPI##num##_DMA_TX)

//****************************************************************************

//For DMA streams substitution
#define _DMA_IRQ_EXP(no, stream) DMA##no##_Stream##stream##_IRQn
#define DMA_IRQ_EXP(no, stream) _DMA_IRQ_EXP(no, stream)

#define _DMA_STREAM_EXP(no, stream)  DMA##no##_Stream##stream
#define DMA_STREAM_EXP(no, stream)  _DMA_STREAM_EXP(no, stream)


//USART configuration
#define _INIT_USART_PORT_CONF(num, port_id) \
    (DMA_C_SET(DMA_USART##num) | \
    DMA_RX_STREAM_SET(DMA_RX_STREAM_USART##num) | \
    DMA_TX_STREAM_SET(DMA_TX_STREAM_USART##num) | \
    DMA_RX_CHANNEL_SET(DMA_RX_CH_USART##num) | \
    DMA_TX_CHANNEL_SET(DMA_TX_CH_USART##num) | \
    GPIO_CLOCK_EN_OFFSET_SET(GPIO_CLOCK_BIT_PORT_ID_##port_id) | \
    RCC_EN_BIT_OFFSET_SET(RCC_EN_BIT_USART##num) | \
    ALTER_FUNC_SET(ALTER_USART123) | \
    GPIO_PORT_IDX_SET(port_id) | \
    RCC_IFACE_IDX_SET(RCC_BUS_USART##num))


// pin0 - tx, pin1 - rx
#define _INIT_USART_PINS_CONF(pin0, pin1)  \
    (PIN_SET_EN(0) | PIN_SET_EN(1) | \
    PIN_NUM_SET(0, pin0) | PIN_NUM_SET(1, pin1) | \
    OTYPE_PIN_SET(0, Q_GPIO_OutType_PP) | OTYPE_PIN_SET(1, Q_GPIO_OutType_PP) | \
    PuPd_PIN_SET(0, Q_GPIO_PuPd_UP) | PuPd_PIN_SET(1, Q_GPIO_PuPd_UP) )


#define _INIT_USART_H(num, port_id, pin0, pin1) \
    {USART##num##_BASE, \
    Q_USART##num, \
    Q_USART_T, \
    _INIT_USART_FLAGS_CONF(num), \
    _INIT_USART_PORT_CONF(num, port_id), \
    _INIT_USART_PINS_CONF(pin0, pin1), \
    USART##num##_IRQn, \
    DMA_IRQ_EXP(DMA_USART##num##_NUM, DMA_RX_STREAM_USART##num), \
    DMA_IRQ_EXP(DMA_USART##num##_NUM, DMA_TX_STREAM_USART##num), \
    PRIO_PREEMPT_USART##num, \
    PRIO_SUB_USART##num, \
    PRIO_PREEMPT_USART##num##_DMA_RX, \
    PRIO_SUB_USART##num##_DMA_RX, \
    PRIO_PREEMPT_USART##num##_DMA_TX, \
    PRIO_SUB_USART##num##_DMA_TX, \
    0, \
    DMA_STREAM_EXP(DMA_USART##num##_NUM, DMA_RX_STREAM_USART##num), \
    DMA_STREAM_EXP(DMA_USART##num##_NUM, DMA_TX_STREAM_USART##num), \
    q_read_usart_handler, \
    q_write_usart_handler, \
    q_ioswap_usart_handler, \
    USART##num##_CONF_FUNC, \
    {(q_lnim_t*)0, (q_lnim_t*)0}, \
    0, 0}

#define INIT_USART_H(conf) _INIT_USART_H(conf)

//I2C configuration
#define _INIT_I2C_PORT_CONF(num, port_id) \
    (DMA_C_SET(DMA_I2C##num) | \
    DMA_RX_STREAM_SET(DMA_RX_STREAM_I2C##num) | \
    DMA_TX_STREAM_SET(DMA_TX_STREAM_I2C##num) | \
    DMA_RX_CHANNEL_SET(DMA_RX_CH_I2C##num) | \
    DMA_TX_CHANNEL_SET(DMA_TX_CH_I2C##num) | \
    GPIO_CLOCK_EN_OFFSET_SET(GPIO_CLOCK_BIT_PORT_ID_##port_id) | \
    RCC_EN_BIT_OFFSET_SET(RCC_EN_BIT_I2C##num) | \
    ALTER_FUNC_SET(ALTER_I2C) | \
    GPIO_PORT_IDX_SET(port_id) | \
    RCC_IFACE_IDX_SET(RCC_BUS_I2C##num))


// pin0 - sck, pin1 - sda
#define _INIT_I2C_PINS_CONF(pin0, pin1)  \
    (PIN_SET_EN(0) | PIN_SET_EN(1) | \
    PIN_NUM_SET(0, pin0) | PIN_NUM_SET(1, pin1) | \
    OTYPE_PIN_SET(0, Q_GPIO_OutType_OD) | OTYPE_PIN_SET(1, Q_GPIO_OutType_OD) | \
    PuPd_PIN_SET(0, Q_GPIO_PuPd_UP) | PuPd_PIN_SET(1, Q_GPIO_PuPd_UP) )


#define _INIT_I2C_H(num, port_id, pin0, pin1) \
    {I2C##num##_BASE, \
    Q_I2C##num,\
    Q_I2C_T, \
    _INIT_I2C_FLAGS_CONF(num), \
    _INIT_I2C_PORT_CONF(num, port_id), \
    _INIT_I2C_PINS_CONF(pin0, pin1), \
    I2C##num##_EV_IRQn, \
    DMA_IRQ_EXP(DMA_I2C##num##_NUM, DMA_RX_STREAM_I2C##num), \
    DMA_IRQ_EXP(DMA_I2C##num##_NUM, DMA_TX_STREAM_I2C##num), \
    PRIO_PREEMPT_I2C##num, \
    PRIO_SUB_I2C##num, \
    PRIO_PREEMPT_I2C##num##_DMA_RX, \
    PRIO_SUB_I2C##num##_DMA_RX, \
    PRIO_PREEMPT_I2C##num##_DMA_TX, \
    PRIO_SUB_I2C##num##_DMA_TX, \
    0, \
    DMA_STREAM_EXP(DMA_I2C##num##_NUM, DMA_RX_STREAM_I2C##num), \
    DMA_STREAM_EXP(DMA_I2C##num##_NUM, DMA_TX_STREAM_I2C##num), \
    q_read_i2c_handler, \
    q_write_i2c_handler, \
    q_ioswap_i2c_handler, \
    I2C##num##_CONF_FUNC, \
    {(q_lnim_t*)0, (q_lnim_t*)0}, \
    0, 0}


#define INIT_I2C_H(conf) _INIT_I2C_H(conf)

//SPI configuration
#define _INIT_SPI_PORT_CONF(num, port_id) \
    (DMA_C_SET(DMA_SPI##num) | \
    DMA_RX_STREAM_SET(DMA_RX_STREAM_SPI##num) | \
    DMA_TX_STREAM_SET(DMA_TX_STREAM_SPI##num) | \
    DMA_RX_CHANNEL_SET(DMA_RX_CH_SPI##num) | \
    DMA_TX_CHANNEL_SET(DMA_TX_CH_SPI##num) | \
    GPIO_CLOCK_EN_OFFSET_SET(GPIO_CLOCK_BIT_PORT_ID_##port_id) | \
    RCC_EN_BIT_OFFSET_SET(RCC_EN_BIT_SPI##num) | \
    ALTER_FUNC_SET(ALTER_SPI12) | \
    GPIO_PORT_IDX_SET(port_id) | \
    RCC_IFACE_IDX_SET(RCC_BUS_SPI##num))


// pin0 - sck, pin1 - miso, pin2 - mosi
#define _INIT_SPI_PINS_CONF(pin0, pin1, pin2)  \
    (PIN_SET_EN(0) | PIN_SET_EN(1) | PIN_SET_EN(2) |\
    PIN_NUM_SET(0, pin0) | PIN_NUM_SET(1, pin1) | PIN_NUM_SET(2, pin2) | \
    OTYPE_PIN_SET(0, Q_GPIO_OutType_PP) | PuPd_PIN_SET(0, Q_GPIO_PuPd_NOPULL) |\
    OTYPE_PIN_SET(1, Q_GPIO_OutType_OD) | PuPd_PIN_SET(1, Q_GPIO_PuPd_NOPULL) |\
    OTYPE_PIN_SET(2, Q_GPIO_OutType_PP) | PuPd_PIN_SET(2, Q_GPIO_PuPd_NOPULL))


#define _INIT_SPI_H(num, port_id, pin0, pin1, pin2) \
    {SPI##num##_BASE, \
    Q_SPI##num,\
    Q_SPI_T, \
    _INIT_SPI_FLAGS_CONF(num), \
    _INIT_SPI_PORT_CONF(num, port_id), \
    _INIT_SPI_PINS_CONF(pin0, pin1, pin2), \
    SPI##num##_IRQn, \
    DMA_IRQ_EXP(DMA_SPI##num##_NUM, DMA_RX_STREAM_SPI##num), \
    DMA_IRQ_EXP(DMA_SPI##num##_NUM, DMA_TX_STREAM_SPI##num), \
    PRIO_PREEMPT_SPI##num, \
    PRIO_SUB_SPI##num, \
    PRIO_PREEMPT_SPI##num##_DMA_RX, \
    PRIO_SUB_SPI##num##_DMA_RX, \
    PRIO_PREEMPT_SPI##num##_DMA_TX, \
    PRIO_SUB_SPI##num##_DMA_TX, \
    0, \
    DMA_STREAM_EXP(DMA_SPI##num##_NUM, DMA_RX_STREAM_SPI##num), \
    DMA_STREAM_EXP(DMA_SPI##num##_NUM, DMA_TX_STREAM_SPI##num), \
    q_read_spi_handler, \
    q_write_spi_handler, \
    q_ioswap_spi_handler, \
    SPI##num##_CONF_FUNC, \
    {(q_lnim_t*)0, (q_lnim_t*)0}, \
    0, 0}


#define INIT_SPI_H(conf) _INIT_SPI_H(conf)



#define USART1_B_TX_6_RX_7              1, GPIOB_ID, 6, 7
#define I2C2_B_CLK_10_SDA_11            2, GPIOB_ID, 10, 11
#define I2C1_B_CLK_8_SDA_9              1, GPIOB_ID, 8, 9
#define SPI2_B_CLK_13_MISO_14_MOSI_15   2, GPIOB_ID, 13, 14, 15
//#define SPI1_B_CLK_3_MISO_4_MOSI_5      1, GPIOB_ID, 3, 4, 5
#define SPI1_A_CLK_5_MISO_6_MOSI_7      1, GPIOA_ID, 5, 6, 7

#endif // MCU_GPIO_H
