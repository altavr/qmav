#ifndef STM_TYPES_H
#define STM_TYPES_H

#ifdef STM32F303_
    #include "stm32f30x.h"
#endif
#ifdef STM32F407_
#include "stm32f4xx.h"

typedef DMA_Stream_TypeDef DMA_Ch_TD;

#endif



#endif // STM_TYPES_H
