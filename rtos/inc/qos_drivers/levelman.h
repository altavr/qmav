#ifndef LEVELMAN_H
#define LEVELMAN_H

//#include "qos_def.h"
//#include "qos_drivers/general.h"
#ifdef STM32F303_
    #include "mcu_f303_gpio.h"
#endif
#ifdef STM32F407_
    #include "mcu_f407_gpio.h"
#endif

#ifdef STM32F303_
#define _RCC_PORT_EN_OFFSET 17
#endif
#ifdef STM32F407_
#define _RCC_PORT_EN_OFFSET 0
#endif

#define _GPIO_NOPULL 0x00
#define _GPIO_PULLUP 0x01
#define _GPIO_PULLDOWN 0x02

#define EXTI_TRIG_RISING    0x08
#define EXTI_TRIG_FALLING   0x0c
#define EXTI_TRIG_TOGGLE    0x10

#define PRIO_PREEMPT_EXTI   4
#define PRIO_SUB_EXTI       0

//port : pin : alternative function : mode
#define LORA_DIO0_PIN  GPIOB_ID, (1 << 1)
#define VL_GPIO01_PIN   GPIOA_ID, (1 << 11)
#define ICM20689_INT_PIN GPIOE_ID, (1 << 2)

//active external interrupt line

#define _EXT_ITR_LINE_0
//#define _EXT_ITR_LINE_1
//#define _EXT_ITR_LINE_11


//output pins
//port : pin : alternative function


#define LORA_SPI_NSS        GPIOB_ID, (1 << 0)
#define LORA_RESET_PIN      GPIOB_ID, (1 << 8)
//#define VL_XSHUT            GPIOA_ID, (1 << 12)
#define ICM20689_NSS        GPIOC_ID, (1 << 0)
#define MS5611_NSS          GPIOC_ID, (1 << 1)

#define LED1                GPIOD_ID, (1 << 4)
#define LED2                GPIOD_ID, (1 << 7)
#define LED3                GPIOD_ID, (1 << 10)
#define LED4                GPIOD_ID, (1 << 11)

#endif // LEVELMAN_H
