#ifndef QOS_LN_LIST_H
#define QOS_LN_LIST_H

#include "qos_def.h"
#include "qos_debug.h"


struct _qos_ln_list_item {
    struct _qos_ln_list_item *next;
    struct _qos_ln_list_item *prev;
};

typedef struct _qos_ln_list_item q_lnim_t;

#if !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)

_FORCE_INLINE void _qos_rem_item_from_ln_list(q_lnim_t* item)
{
    item->prev->next = item->next;
    item->next->prev = item->prev;
}

_FORCE_INLINE void _qos_close_item_lns(q_lnim_t* item)
{
    item->next = item;
    item->prev = item;
}

_FORCE_INLINE void _qos_ins_to_ln_before_item(q_lnim_t* item, q_lnim_t* base_item)
{
    base_item->prev->next = item;
    item->prev = base_item->prev;
    base_item->prev = item;
    item->next = base_item;
}

_FORCE_INLINE void _qos_ins_to_ln_after_item(q_lnim_t* item, q_lnim_t* base_item)
{
    base_item->next->prev = item;
    item->next = base_item->next;
    base_item->next = item;
    item->prev = base_item;
}

_FORCE_INLINE uint8_t _qos_ln_empty(q_lnim_t* list)
{
    if (list->next != list) {
        return 1;
    }
    else {
        return 0;
    }
}

_FORCE_INLINE q_lnim_t* _qos_get_first_item_ln(q_lnim_t* list)
{
    return list->next;
}

_FORCE_INLINE uint8_t _qos_item_in_ln(q_lnim_t* item, q_lnim_t* list)
{
    for (q_lnim_t* p = list->next; p != list; p = p->next) {
        if (p == item) {
            return 1;
        }
        else {
            return 0;
        }
    }
}

_FORCE_INLINE uint8_t _qos_item_pos_in_ln(q_lnim_t* item, q_lnim_t* list)
{
    uint8_t i = 0;
    for (q_lnim_t* p = list->next; p != list; p = p->next) {
        i++;
        if (p == item) {
            return i;
        }
    }
    return i;
}

_FORCE_INLINE uint8_t _qos_ins_item_to_pos_ln(q_lnim_t* item, uint8_t idx, q_lnim_t* list)
{
    uint8_t i = 0;
    q_lnim_t* p = list;
    do {
        p = p->next;
        if (p == list) {
            return i;
        }
        i++;
    } while (i < idx);

    _qos_ins_to_ln_before_item(item, p);

    return i;
}
#else
void _qos_rem_item_from_ln_list(q_lnim_t* item);
void _qos_close_item_lns(q_lnim_t* item);
void _qos_ins_to_ln_before_item(q_lnim_t* item, q_lnim_t* b_item);
void _qos_ins_to_ln_after_item(q_lnim_t* item, q_lnim_t* b_item);
uint8_t _qos_ln_empty(q_lnim_t* list);
q_lnim_t* _qos_get_first_item_ln(q_lnim_t* list);
uint8_t _qos_item_in_ln(q_lnim_t* item, q_lnim_t* list);
uint8_t _qos_item_pos_in_ln(q_lnim_t* item, q_lnim_t* list);
uint8_t _qos_ins_item_to_pos_ln(q_lnim_t* item, uint8_t idx, q_lnim_t* list);



#endif


#ifdef DESKTOP_DEBUG_LL
void print_list_item(q_lnim_t *list);
void print_list(q_lnim_t* list);
#define PRINT_LLIST(list) _PROXY(PRINT_LLIST, list)
#else
#define PRINT_LLIST(list)
#endif

#define A_PRINT_LLIST(list) print_list(list)

#define NO_PRINT_LLIST(list)

#endif // QOS_LN_LIST_H
