#ifndef __QOS_TIMERS_H
#define __QOS_TIMERS_H
#include "stdint.h"
#include "qos_ln_list.h"

#define TIMERS_MAX 32

uint8_t q_timers_tick(q_lnim_t* queue);

#endif // QOS_TIMERS_H
