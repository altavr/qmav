/**
 * @file
 * @brief Memory management
 */


#ifndef __QOS_MEMORY_H
#define __QOS_MEMORY_H

//#ifndef DESKTOP_TEST
//#include "qos_threads.h"
#include "stm32f4xx.h"
//#endif //DDESKTOP_TEST



#include <stddef.h>
#include "qos_def.h"
#include "qos_debug.h"
#include "misc/qos_bin_heap.h"
#include "qos_ln_list.h"


#define _MPU_EXEC_NEVER     0b1
#define _MPU_EXEC           0b0

#define _MPU_AP_P_NA_UP_NA  0b000
#define _MPU_AP_P_RW_UP_NA  0b001
#define _MPU_AP_P_RW_UP_RO  0b010
#define _MPU_AP_P_RW_UP_RW  0b011
#define _MPU_AP_P_RO_UP_NA  0b101
#define _MPU_AP_P_RO_UP_RO  0b110

#ifdef DISABLE_WRITE_BUFFER
#define _MPU_IN_SRAM_T      0b000100 // Write through, no write allocate, shareable
#define _MPU_IN_FLASH_T     0b000000 // Write through, no write allocate, no shareable
#else
#define _MPU_IN_SRAM_T      0b000110 // Write through, no write allocate, shareable
#define _MPU_IN_FLASH_T     0b000010 // Write through, no write allocate, no shareable

#endif

#define _MPU_DEF_XN(x)  (x << MPU_RASR_XN_Pos)
#define _MPI_DEF_AP(x)  (x << MPU_RASR_AP_Pos)
#define _MPU_DEF_TP(x)  (x << MPU_RASR_B_Pos)
#define _MPU_DEF_SRD(x) (x << MPU_RASR_SRD_Pos)
#define _MPU_DEF_B_REGION(x) (x << MPU_RNR_REGION_Pos)
#define _MPU_DEF_SIZE(x) ((( (-1 * (int16_t)__builtin_clz(x)) - 2) & 0b11111) << MPU_RASR_SIZE_Pos)
//#define _MPU_DEF_SIZE(x) ((30 - __builtin_clz(x))<< MPU_RASR_SIZE_Pos)

#define _MPU_USER_MPU_PREF1 (_MPU_DEF_XN(_MPU_EXEC_NEVER) | _MPI_DEF_AP(_MPU_AP_P_RW_UP_RW) | _MPU_DEF_TP(_MPU_IN_SRAM_T))
#define _MPU_NO_ACCESS      (_MPU_DEF_XN(_MPU_EXEC_NEVER) | _MPI_DEF_AP(_MPU_AP_P_NA_UP_NA) | _MPU_DEF_TP(_MPU_IN_SRAM_T))

#define _RAM_SIZE               0x20000UL
#ifndef DESKTOP_TEST
#define _IN_SRAM_ADDR_ORIGIN    0x20000000UL
#define _RAM_SIZE               0x20000UL
#else
#define _IN_SRAM_ADDR_ORIGIN    ram_origin
//#define _RAM_SIZE               ram_size
extern size_t ram_origin;
extern size_t ram_size;
extern MPU_Type mpu_struct;

#undef MPU
#define MPU (&mpu_struct)
#endif //DESKTOP_TEST

#define _FLASH_ADDR_ORIGIN      0x8000000UL
#define _FLASH_SIZE             0x100000UL

//MPU preferences block start
#define _Q_MPU_REGS         2U
#define _Q_MPU_REGIONS      8U
#define _Q_MPU_SUBREGIONS   8U
#define _Q_MPU_STACK_REGIONS    1U
#define _Q_MPU_DYNAMIC_REGIONS  4U
#define _Q_MPU_STATIC_REGIONS   (_Q_MPU_REGIONS - _Q_MPU_STACK_REGIONS - _Q_MPU_DYNAMIC_REGIONS)
#define _Q_MPU_DYN_BLOCK_NUM   (_Q_MPU_DYNAMIC_REGIONS * _Q_MPU_SUBREGIONS)
//#define _Q_MPU_STACK0_REG_IDX   4UL
////MPU preferences block end

#define _Q_MPU_STACK0_REGION     3U
#define _Q_MPU_STACK_PREF_IDX   0U
#define _Q_MPU_HEAP_REGION     4U
#define _Q_MPU_HEAP_PREF_IDX   1U

#define _Q_STACK_MEMBLOCK_NUM  2U
#define _Q_MMAP_CHUNKLEN       256U
#define _Q_MMAP_CH_IN_BL        8U
#define _Q_MMAP_GR_BLOCKNUM      (_RAM_SIZE / (_Q_MMAP_CHUNKLEN * _Q_MMAP_CH_IN_BL))
#define _Q_MMAP_BLOCKNUM       (_Q_MMAP_GR_BLOCKNUM * _Q_MMAP_CH_IN_BL)

typedef struct   {
    u32 base_addr;
    u32 attr_size;
} mpu_rp_t;


typedef struct {
    u8 free;  //bit set means block is free and can be used
    q_lnim_t lle;
    union { bh_node_t node;
            u32 len; };
} mch_head_t;

struct mbcb_st {
    mch_head_t* head;
    size_t size;
    q_lnim_t ll_item_thread;
//    q_lnim_t ll_chunks;
    bheap_t bheap;
//    u8 free;  //bit set means block is free and can be used
} ;

typedef struct mbcb_st mbcb_t;

typedef struct {
    size_t base;
    size_t len;
} sbcb_t;

typedef struct {
    memaddr_t low_border;
    memaddr_t high_border;
    u8 memmap[_Q_MMAP_GR_BLOCKNUM];    //bit set means block is free and can be used
    uint res_mem_startblock;

    size_t mpu_area_start;
    size_t mb_mpu_start;
    size_t mb_mpu_end;
    mbcb_t mb_mpu[_Q_MPU_DYN_BLOCK_NUM];
    mbcb_t *mem2mb[_Q_MPU_DYN_BLOCK_NUM];                 //pointer to mb, which own memory
    size_t mb_raw_blocksize;
//    u8 memblocks_in_use[_Q_MPU_DYNAMIC_REGIONS]; //bit set means block is free and can be used

} MemCB_t ;

void  _q_set_8bit_map(uint idx, uint num, u8 map[], u8 spacer);     //lev_0 V
void _q_unset_8bit_map(uint idx, uint num, u8 map[], u8 spacer);    //lev_0 V

uint _q_mem_start_mb_idx(void);                              //lev_0 V
uint _q_mem_end_mb_idx(void);                               //lev_0 V
uint _q_mem_mb_len2blocknum(size_t len);                     //lev_0 V
size_t _q_mem_mb_idx2addr(uint idx);                        //lev_1 V
uint _q_mem_mb_addr2idx(size_t addr);                       //lev_1 V
int _q_mem_mb_find_free_blocks(uint num);                  //lev_1 V
void _q_mem_mb_set_free(uint idx, uint num);                //lev_0 V
void _q_mem_mb_set_used(uint idx, uint num, mbcb_t* mb);    //lev_0 V

uint _q_mem_res_len2blocknum(size_t len);                   //lev_0 V
uint _q_mem_res_addr2map_idx(size_t addr);                  //lev_0 V
size_t _q_mem_res_map_idx2addr(uint idx);                   //lev_0 V
void _q_mem_res_set_map(uint idx, uint num);                //N
void _q_mem_res_unset_map(uint idx, uint num);              //N
int _q_mem_res_find_seq_map(uint num);                     //lev_1 V
int _q_mem_res_find_seq_map_mpu(uint num);     //lev_1
size_t _q_mem_res_num_free_chunk_from_idx(size_t idx);      //lev_1 V

void _qos_mb_array_extend_up(size_t len);
size_t _qos_mb_array_extend_down(size_t len);               //lev_2

int _mem_res_find_seq_map_in_region(const uint num, const uint start, const uint end);


void qos_dyn_memory_init(void);                               //lev_2
uint _q_mem_mb_get_free_status(mbcb_t *mb);                     //lev_0     //V
/**
 * @brief Initialization memory control block - mcb
 */
void qos_memblock_init(memaddr_t base, size_t size, mbcb_t *memblock);          //lev_2
#ifdef MPU_ENABLE
void* qalloc_dynmem(size_t len, q_lnim_t *th_memblocks, mpu_rp_t *mpu_conf);        //lev_3
void* qalloc_uniqmem(size_t len, sbcb_t *stack_cb, mpu_rp_t *mpu_conf);         //lev_3
void _q_free_main_mem(void *ptr,  q_lnim_t *th_memblocks, mpu_rp_t *mpu_conf);       //lev_3
void _q_free_residual_mem(uint idx, sbcb_t *stack_cb, mpu_rp_t *mpu_conf);                          //lev_3
#else
void* qalloc_dynmem(size_t len, q_lnim_t *th_memblocks);        //lev_3
void* qalloc_uniqmem(size_t len, sbcb_t *stack_cb);         //lev_3
void _q_free_main_mem(void *ptr,  q_lnim_t *th_memblocks);       //lev_3
void _q_free_residual_mem(uint idx, sbcb_t *stack_cb);                          //lev_3
#endif



/**
* @brief Dynamic memory allocation
* @param   size             size bytes for allocate
* @return  Pointer to the allocated memory
*/
void* qos_memblock_malloc(size_t size, mbcb_t *memblock);


/**
* @brief Frees allocated memory
* @param  Pointer to thr allocated memory
*/
void qos_memblock_free(void* ptr, mbcb_t* memblock);



//#ifndef DESKTOP_TEST
void qos_thread_mem_init(void);
memaddr_t qos_mem_alloc(uint32_t size);

#ifdef MPU_ENABLE

void _q_mpu_add_heap_region_to_tread(uint idx, uint num, mpu_rp_t *mpu_conf);
void _q_mpu_remove_heap_region_to_tread(uint idx, uint num, mpu_rp_t *mpu_conf);
void _q_mpu_add_stack_region_to_tread(uint stack_idx, sbcb_t *stack_cb, mpu_rp_t *mpu_conf);
void _q_mpu_remove_stack_region_to_tread(uint stack_idx, sbcb_t *stq_mpu_encodeack_cb,  mpu_rp_t *mpu_conf);
void q_mpu_tread_init(sbcb_t *stack_cb, mpu_rp_t *mpu_conf);
void q_mpu_apply_thread_pref(mpu_rp_t *mpu_conf);


//debug start
void _mpu_region_access (uint32_t size);

//debug end
void q_mpu_encode_pref(mpu_rp_t* pref,
                      u8 region,
                      uint32_t addr,
                      uint32_t size,
                      uint32_t exec_never,
                      uint32_t access_perm,
                      uint32_t mem_type,
                      u32 subregion_disable);          //lev_0 V
void q_mpu_encode_pref_spec(mpu_rp_t* pref,
                         u8 region,
                         u32 addr,
                         u32 size,
                         u32 subregion_disable,
                         u32 spec_defs);                    //lev_0 0
void q_mpu_add_subreg_pref(mpu_rp_t* pref, u8 subregions);      //lev_0
void q_mpu_rem_subreg_pref(mpu_rp_t* pref, u8 subregions);      //lev_0
void q_mpu_set_base_region(u8 region);                     //lev_0
void q_mpu_apply_pref(mpu_rp_t* pref);          //lev_0
void q_mpu_apply_group_pref(mpu_rp_t* pref);                //lev_0
void q_mpu_region_unset(uint8_t region);                    //lev_0

void q_mpu_enable(void);
//void q_mpu_fill_stack_table(q_th_t* thread_pool);
void q_mpu_thread_set(uint32_t tid);
#endif //MPU_ENABLE
//#endif //DDESKTOP_TEST
#endif // __QOS_MEMORY_H
