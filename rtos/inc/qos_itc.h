#ifndef QOS_ITC_H
#define QOS_ITC_H

#include <stdint.h>
#include <stddef.h>
#include "qos_threads.h"
#include "qos_def.h"



struct  q_mutex_st{
    uint16_t val;
    struct q_th_st *own;
    q_lnim_t lnlist;
};

typedef struct q_mutex_st q_mutex_t;



struct q_qprop {
    u16 start;
    u16 len;
    u8 prio;
};


#define _Q_PRIO_QUEUE_TYPE_DEF(len, buf_size_) \
typedef struct  { \
    u16 length; \
    u16 buf_size; \
    u16 head; \
    u16 tail; \
    u16 start; \
    u16 end; \
    q_mutex_t mutex; \
    struct q_qprop item[len]; \
    u8 buf[buf_size_]; \
} q_queue_##len##_##buf_size_##_t;

#define Q_QUE_OFFSET_BUF_SIZE sizeof(u16)
#define Q_QUE_OFFSET_HEAD Q_QUE_OFFSET_BUF_SIZE + sizeof(u16)
#define Q_QUE_OFFSET_TAIL Q_QUE_OFFSET_HEAD + sizeof(u16)
#define Q_QUE_OFFSET_START Q_QUE_OFFSET_BUF_SIZE + sizeof(u16)
#define Q_QUE_OFFSET_END Q_QUE_OFFSET_START + sizeof(u16)
#define Q_QUE_OFFSET_MUTEX Q_QUE_OFFSET_END + sizeof(u16)
#define Q_QUE_OFFSET_ITEMS Q_QUE_OFFSET_MUTEX + sizeof(q_mutex_t)
#define Q_QUE_OFFSET_BUF(len) Q_QUE_OFFSET_ITEMS + sizeof(struct q_qprop) * (len + 1)

#define LEN_P_OF_QUEUE(queue)           queue
#define BUF_P_SIZE_OF_QUEUE(queue)     ((u8*)queue + Q_QUE_OFFSET_BUF_SIZE)
#define HEAD_P_SIZE_OF_QUEUE(queue)      ((u8*)queue + Q_QUE_OFFSET_HEAD)
#define TAIL_P_SIZE_OF_QUEUE(queue)     ((u8*)queue + Q_QUE_OFFSET_TAIL)
#define START_P_SIZE_OF_QUEUE(queue)      ((u8*)queue + Q_QUE_OFFSET_START)
#define END_P_SIZE_OF_QUEUE(queue)     ((u8*)queue + Q_QUE_OFFSET_END)
#define MUTEX_P_OF_QUEUE(queue)       ((u8 *)queue + Q_QUE_OFFSET_MUTEX)
#define ITEMS_P_OF_QUEUE(queue)       ((u8 *)queue + Q_QUE_OFFSET_ITEMS)
#define BUF_P_OF_QUEUE(queue, len)    ((u8*)queue + Q_QUE_OFFSET_BUF(len))


_Q_PRIO_QUEUE_TYPE_DEF(10, 256)



#define Q_QUEUE_INIT(QUEUE, LEN, BUF_SIZE) _q_queue_init((void*) QUEUE , LEN , BUF_SIZE)


void mutex_init(q_mutex_t* mutex);



#endif // QOS_ITC_H
