#ifndef QOS_TEST_GPIO_MACRO_H
#define QOS_TEST_GPIO_MACRO_H

#ifdef STM32F303_
    #include "stm32f30x.h"
#endif
#ifdef STM32F407_
    #include "stm32f4xx.h"
#endif
#include "inc/qos_drivers/general.h"
#include "stdio.h"

//#define DESKTOP_TEST



#define STRINGINIZE(t) #t
#define MACROCALL(x, p) x(p)
#define MACROCALLPIN(x, p) x(pin, p)
#define PRINT_VARS printf("t: %d\n", t); printf("temp_t: %d\n", temp_t);
#define PRINT_PARAM(p) printf("%s: %d\n", #p, MACROCALL(p##_GET, v))
#define PRINT_PIN_PARAM(p) printf("PIN%d %s: %d\n", pin, #p, MACROCALLPIN(p##_GET, v))

#define TEST_MACRO_FLAGS(set, get, val, t_) t = t_; \
    val = set(t_);\
    temp_t = get(val); \
    PRINT_VARS printf("val: %u\n", val); \
    if (t == temp_t) {printf("SUCCESS: "  #get  "\n");} \
    else {printf("FAIL: " #get  "\n");}

#define TEST_MACRO_PINS(set, get, pin, val, t_) t = t_; \
    val = set(pin, t_);\
    temp_t = get(pin, val); \
    PRINT_VARS printf("val: %u\n", val); \
    if (t == temp_t) {printf("SUCCESS: "  #get  "\n");} \
    else {printf("FAIL: " #get  "\n");}

//TEST_MACRO_FLAGS(GPIO_CLOCK_EN_OFFSET_SET, GPIO_CLOCK_EN_OFFSET_GET, port_conf, 3)






#endif // QOS_TEST_GPIO_MACRO_H


