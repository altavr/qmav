#pragma GCC diagnostic ignored "-Wunused-variable"

#include "qos_test_gpio_macro.h"

//#define DESKTOP_TEST


#ifdef DESKTOP_TEST


uint32_t _rcc_reg[] = {
    1000,
    1001,
    1002
};

uint32_t _gpio_reg[] = {
    2000,
    2001,
    2002,
    2003
};



uint32_t port_conf;
uint32_t pins_conf;
uint16_t flags;

uint8_t temp_t, t;



int q_read_usart_handler(int rd, uint8_t* buf, uint32_t nbyte){}
int q_write_usart_handler(int rd, uint8_t* buf, uint32_t nbyte){}
void q_usart_init(int rd){}


io_res_obj_t io_handlers[RES_MAX] = {
    INIT_USART_H(USART3_B_TX_10_RX_11)
};

void print_port_conf(uint32_t v)
{
    PRINT_PARAM(DMA_C);
    PRINT_PARAM(DMA_RX_CHANNEL);
    PRINT_PARAM(DMA_TX_CHANNEL);
    PRINT_PARAM(GPIO_CLOCK_EN_OFFSET);
    PRINT_PARAM(RCC_EN_BIT_OFFSET);
    PRINT_PARAM(ALTER_FUNC);
    PRINT_PARAM(GPIO_PORT_IDX);
    PRINT_PARAM(RCC_IFACE_IDX);

}

void print_pins_conf(uint32_t v)
{
    for (uint32_t pin = 0; pin < PIN_NUM_IFACE_MAX; pin++) {
        if (PIN_IS_EN(pin, v)) {
            PRINT_PIN_PARAM(OTYPE_PIN);
            PRINT_PIN_PARAM(PuPd_PIN);
            PRINT_PIN_PARAM(PIN_NUM);
        }
    }
}


int main (void)
{
    TEST_MACRO_FLAGS(GPIO_CLOCK_EN_OFFSET_SET, GPIO_CLOCK_EN_OFFSET_GET, port_conf, GPIO_CLOCK_EN_BIT_B)
    TEST_MACRO_FLAGS(RCC_EN_BIT_OFFSET_SET, RCC_EN_BIT_OFFSET_GET, port_conf, RCC_EN_BIT_USART2)
    TEST_MACRO_FLAGS(ALTER_FUNC_SET, ALTER_FUNC_GET, port_conf, ALTER_USART)
    TEST_MACRO_FLAGS(GPIO_PORT_IDX_SET, GPIO_PORT_IDX_GET, port_conf, GPIOB_ID)
    TEST_MACRO_FLAGS(RCC_IFACE_IDX_SET, RCC_IFACE_IDX_GET, port_conf, RCC_BUS_USART3)
    TEST_MACRO_PINS(OTYPE_PIN_SET, OTYPE_PIN_GET, 3, pins_conf, Q_GPIO_OutType_OD)
    TEST_MACRO_PINS(PuPd_PIN_SET, PuPd_PIN_GET, 3, pins_conf, Q_GPIO_PuPd_DOWN)
    TEST_MACRO_PINS(PIN_SET_EN, PIN_IS_EN, 3, pins_conf, PIN_EN_VAL)
    TEST_MACRO_PINS(PIN_NUM_SET, PIN_NUM_GET, 3, pins_conf, 15)

    print_port_conf(io_handlers[0].port_conf);
    print_pins_conf(io_handlers[0].pins_conf);
    return 0;

}

#endif
