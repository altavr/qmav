#include <stdlib.h>
#include <assert.h>
#include "qos_debug.h"
#include "qos_def.h"
#include "qos_memory.h"
#ifndef DESKTOP_TEST
//#include "qos_threads.h"


//#include <stdarg.h>
//#include <limits.h>
//#include <stdint.h>
//#include <assert.h>
//#include <math.h>

extern const uint32_t _sbss;
extern const uint32_t _estack; /* end of 40K RAM */
extern const uint32_t _Min_Stack_Size; /* required amount of stack */
extern const uint32_t _Min_Heap_Size; /* required amount of heap  */

extern const uint32_t _Priv_Data_Size;
extern const uint32_t _Priv_Bss_Size;
extern const uint32_t _p_sdata;

extern const uint32_t _Data_Size;
extern const uint32_t _Bss_Size;
extern const uint32_t _sdata;

_PRIV_BSS static  MemCB_t mcb;

#ifdef MPU_ENABLE


_PRIV_BSS static uint32_t mpu_stack_table[32][_Q_MPU_REGS];

#endif //MPU_ENABLE

//debug
//uint32_t _mpu_prefs[8][_Q_MPU_REGS];
//debug


#if  defined(__QOS_THREAD_MEM_INIT__UNIT_TESTS) || !defined(DESKTOP_TEST)
void qos_thread_mem_init(void)
{
    mcb.low_border = ROUND_LOW_BORD_STACK(ADDRVAR(_sdata) + ADDRVAR(_Bss_Size) + ADDRVAR(_Data_Size));
    mcb.high_border = floor_round_to_x(ADDRVAR(_estack) - ADDRVAR(_Min_Stack_Size) - ADDRVAR(_Min_Heap_Size), 8);
}
#endif //qos_thread_mem_init_INIT_TESTS


#endif //DDESKTOP_TEST

#ifdef DD__qos_dyn_mem_enable
#undef _PROXY
#define _PROXY(f, ...) A_##f(__VA_ARGS__)
#endif

#ifdef DESKTOP_TEST
#include <stdio.h>
extern MemCB_t mcb;

//extern size_t _sbss;
extern size_t _Bss_Size;
extern size_t _estack;
extern size_t _Min_Stack_Size;
extern size_t _Min_Heap_Size;
extern size_t _Priv_Data_Size;
extern size_t _Priv_Bss_Size;
extern size_t _p_sdata;
extern size_t _sdata;
extern size_t _Data_Size;
#endif





#if  defined(___Q_MEM_START_MB_IDX__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
uint _q_mem_start_mb_idx(void)
{
    return  (uint)((mcb.mb_mpu_start - mcb.mpu_area_start) / mcb.mb_raw_blocksize);
}
#endif //_q_mem_start_mb_idx_INIT_TESTS

#if  defined(___Q_MEM_END_MB_IDX__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
uint _q_mem_end_mb_idx(void)
{
    return  (uint)((mcb.mb_mpu_end - mcb.mpu_area_start) / mcb.mb_raw_blocksize);
}
#endif //_q_mem_end_mb_idx_INIT_TESTS

#if  defined(___Q_MEM_MB_LEN2BLOCKNUM__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
uint _q_mem_mb_len2blocknum(size_t len)
{
    uint need_blocks = 0;

    if (len > 0) {
        size_t remainder_len = len  + sizeof(mch_head_t);
        need_blocks += remainder_len % mcb.mb_raw_blocksize ?
                    (uint)(remainder_len / mcb.mb_raw_blocksize) + 1U : (uint)(remainder_len / mcb.mb_raw_blocksize);
    }
    return need_blocks;
}
#endif //_q_mem_mb_len2blocknum_INIT_TESTS

//#define DESKTOP_TEST



#if  defined(___Q_MEM_MB_IDX2ADDR__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
size_t _q_mem_mb_idx2addr(uint idx)
{
    return mcb.mb_mpu_start + idx * mcb.mb_raw_blocksize;
}
#endif //_q_mem_mb_idx2addr_INIT_TESTS

#if  defined(___Q_MEM_MB_ADDR2IDX__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
uint _q_mem_mb_addr2idx(size_t addr)
{
    return (uint)((addr - mcb.mpu_area_start) / mcb.mb_raw_blocksize);
}
#endif //_q_mem_mb_addr2idx_INIT_TESTS


#if  defined(___Q_MEM_MB_FIND_FREE_BLOCKS__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
int _q_mem_mb_find_free_blocks(uint num)
{
    uint curent_num = 0;
    for (uint i = _q_mem_start_mb_idx(); i < _q_mem_end_mb_idx(); i++) {
        if (mcb.mem2mb[i] == NULL) {
            curent_num++;
            if (curent_num == num) {
                return (int)i + 1 - (int)num;
            }
        }
        else {
            curent_num = 0;
        }
    }
    return -1;
}
#endif //_q_mem_mb_find_free_blocks_INIT_TESTS

#if  defined(___Q_MEM_MB_SET_FREE__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void _q_mem_mb_set_free(uint idx, uint num)
{
    for (uint i = idx; i < idx + num; i++) {
        mcb.mem2mb[i] = NULL;
    }
}
#endif //_q_mem_mb_set_free_INIT_TESTS

#if  defined(___Q_MEM_MB_SET_USED__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void _q_mem_mb_set_used(uint idx, uint num, mbcb_t* mb)
{
    for (uint i = idx; i < idx + num; i++) {
        mcb.mem2mb[i] = mb;
    }
}
#endif //_q_mem_mb_set_used_INIT_TESTS


#if  defined(___Q_SET_8BIT_MAP__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void _q_set_8bit_map(uint idx, uint num, u8 map[], u8 spacer)
{
    uint p1 = ceil_round_to_x(idx, 8U);
    uint p2 = floor_round_to_x(idx + num, 8U);
    if (p2 > p1) {
        if (idx != p1) {
            uint elem = (idx / 8U) << spacer;
            map[elem] = map[elem] | (u8)(0xFF << (idx - (p1 - 8U)));
        }
        for (uint i = p1 / 8U; i < p2 / 8U; i++) {
            map[i << spacer] = 0xFF;
        }
        if (p2 != (idx + num)) {
          uint elem = (p2 / 8U) << spacer;
          map[elem] = map[elem] | (u8)(0xFF >> ((p2 + 8U) - (idx + num)));
        }

    }
    else if (p2 < p1) {
        for (uint i = idx; i < idx + num; i++) {
            map[(idx / 8U) << spacer] |= (u8)(1 << i % 8U);
        }
    }
    else {

        if (idx != p1) {
            uint elem = (idx / 8U) << spacer;
            map[elem] = map[elem] | (u8)(0xFF << (idx - (p1 - 8U)));
        }
        if (p2 != (idx + num)) {
          uint elem = (p2 / 8U) << spacer;
          map[elem] = map[elem] | (u8)(0xFF >> ((p2 + 8U) - (idx + num)));
        }

    }
}
#endif //_q_set_8bit_map_UNIT_TESTS


#if  defined(___Q_UNSET_8BIT_MAP__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void _q_unset_8bit_map(uint idx, uint num, u8 map[], u8 spacer)
{

    uint p1 = ceil_round_to_x(idx, 8U);
    uint p2 = floor_round_to_x(idx + num, 8U);

    if (p2 > p1) {
        if (idx != p1) {
            uint elem = idx / 8U << spacer;
            map[elem] = map[elem] & ~(0xFF << (idx - (p1 - 8U)));
        }
        for (uint i = p1 / 8U; i < p2 / 8U; i++) {
            map[i << spacer] = 0;
        }
        if (p2 != (idx + num)) {
          uint elem = p2 / 8U << spacer;
          map[elem] = map[elem] & ~(0xFF >> ((p2 + 8U) - (idx + num)));
        }

    }
    else if (p2 < p1) {
        for (uint i = idx; i < idx + num; i++) {
            map[idx / 8U << spacer] &= ~(1U << i % 8U);
        }
    }
    else {

        if (idx != p1) {
            uint elem = idx / 8U << spacer;
            map[elem] = map[elem] & ~(0xFF << (idx - (p1 - 8U)));
        }
        if (p2 != (idx + num)) {
          uint elem = p2 / 8U << spacer;
          map[elem] = map[elem] & ~(0xFF >> ((p2 + 8U) - (idx + num)));
        }
    }
}
#endif //_q_unset_8bit_map_UNIT_TESTS



#if  defined(___Q_MEM_RES_SET_MAP__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void _q_mem_res_set_map(uint idx, uint num)
{
    _q_set_8bit_map(idx, num, mcb.memmap, 0);
}
#endif //_q_mem_res_set_map_INIT_TESTS

//add test variant when num = 0
#if  defined(___Q_MEM_RES_UNSET_MAP__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void _q_mem_res_unset_map(uint idx, uint num)
{
    _q_unset_8bit_map(idx, num, mcb.memmap, 0);
}
#endif //_q_mem_res_unset_map_INIT_TESTS


void __mem_rsfsmir_check_region_in_block(const uint num, const uint down, const uint up,
                                        uint* const len_block_seq, int* const idx)
{
    uint lim = up % _Q_MMAP_CH_IN_BL;
    for (uint i = down % _Q_MMAP_CH_IN_BL; i < (lim ? lim : _Q_MMAP_CH_IN_BL); i++) {
        if (mcb.memmap[down / _Q_MMAP_CH_IN_BL] & (1 << i)) {
            (*len_block_seq)++;
            if (*len_block_seq == num) {
                return;
            }
        }
        else {
            *len_block_seq = 0;
            *idx = (int)down + (int)i + 1;
        }
    }
}

int _mem_res_find_seq_map_in_region(const uint num, const uint start, const uint end)
{
    uint len_block_seq = 0;
    int idx = (int)start;

    if (start % _Q_MMAP_CH_IN_BL) {
        __mem_rsfsmir_check_region_in_block(num, start, ceil_round_to_x(start, _Q_MMAP_CH_IN_BL), &len_block_seq, &idx);
        if (len_block_seq == num) {
            return idx;
        }
    }

//    if (start % _Q_MMAP_CH_IN_BL) {
//        for (uint i = start; i < ceil_round_to_x(start, _Q_MMAP_CH_IN_BL); i++) {
//            if (mcb.memmap[start / _Q_MMAP_CH_IN_BL] & (1 << i)) {
//                len_block_seq++;
//                if (len_block_seq == num) {
//                    return idx;
//                }
//            }
//            else {
//                len_block_seq = 0;
//                idx = i + 1;
//            }
//        }
//    }
    for (uint i = ceil_round_to_x(start, _Q_MMAP_CH_IN_BL) / _Q_MMAP_CH_IN_BL; i < end / _Q_MMAP_CH_IN_BL; i++) {
        if (mcb.memmap[i] == 0xFF) {
            len_block_seq += _Q_MMAP_CH_IN_BL;
            if (len_block_seq >= num) {
                return idx;
            }
        }
        else if (mcb.memmap[i] > 0) {
            __mem_rsfsmir_check_region_in_block(num, i * _Q_MMAP_CH_IN_BL, (i + 1) * _Q_MMAP_CH_IN_BL, &len_block_seq, &idx);
            if (len_block_seq == num) {
                return idx;
            }
        }
        else {
            len_block_seq = 0;
            idx = ((int)i  + 1) * _Q_MMAP_CH_IN_BL;
        }
    }
    if (end % _Q_MMAP_CH_IN_BL) {
        __mem_rsfsmir_check_region_in_block(num, floor_round_to_x(start, _Q_MMAP_CH_IN_BL), end, &len_block_seq, &idx);
//        if (len_block_seq == num) {
//            return idx;
//        }
    }



//    if (end % _Q_MMAP_CH_IN_BL) {
//        for (uint i = floor_round_to_x(start, _Q_MMAP_CH_IN_BL); i < end; i++) {
//            if (mcb.memmap[end / _Q_MMAP_CH_IN_BL] & (1 << i)) {
//                len_block_seq++;
//                if (len_block_seq == num) {
//                    return (int)num;
//                }
//            }
//            else {
//                len_block_seq = 0;
//            }
//        }
//    }
    if (len_block_seq < num) {
        idx = -1;
    }
    return idx;
}



//return idx
#if  defined(___Q_MEM_RES_FIND_SEQ_MAP__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
int _q_mem_res_find_seq_map(uint num)
{

    return _mem_res_find_seq_map_in_region(num,
                                           mcb.res_mem_startblock,
                                           _q_mem_res_addr2map_idx(mcb.high_border));

//    uint len_block_seq = 0;
//    for (uint i = mcb.res_mem_startblock / _Q_MMAP_CH_IN_BL; i < _q_mem_res_addr2map_idx(mcb.high_border) /
//         _Q_MMAP_CH_IN_BL; i++) {
//        if (mcb.memmap[i] != 0) {
//            for (uint j = 0; j < _Q_MMAP_CH_IN_BL; j++) {
//                if (mcb.memmap[i] & (1 << j)) {

//                    len_block_seq++;
//                    if (len_block_seq == num) {
//                        return i * 8 + j + 1 - num;
//                    }
//                }
//                else {
//                    len_block_seq = 0;
//                }
//            }
//        }
//        else {
//            len_block_seq = 0;
//        }
//    }
//    return -1;
}
#endif //_q_mem_res_find_seq_map_INIT_TESTS

#if  defined(___Q_MEM_RES_FIND_SEQ_MAP_MPU__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
// для использования MPU размер учатка памяти д.б. кратен адресу начала этого участка
int _q_mem_res_find_seq_map_mpu(uint num)
{
//    uint num = num / _Q_MMAP_CH_IN_BL;
//    uint reminder = num % _Q_MMAP_CH_IN_BL;
//    uint num = ceil_round_to_2_degree(num);
    for (uint i = ceil_round_to_x(mcb.res_mem_startblock, num);
         i < floor_round_to_x(_q_mem_res_addr2map_idx(mcb.high_border), num); i += num) {
        int idx = _mem_res_find_seq_map_in_region(num, i, i + num);
        if (idx >= 0) {
            return idx;
        }

    }
    return -1;
}
#endif //_q_mem_res_find_seq_map_mpu_INIT_TESTS


#if  defined(___Q_MEM_RES_ADDR2MAP_IDX__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
uint _q_mem_res_addr2map_idx(size_t addr)
{
    return (addr - _IN_SRAM_ADDR_ORIGIN) / _Q_MMAP_CHUNKLEN;
}
#endif //_q_mem_res_addr2map_idx_INIT_TESTS

#if  defined(___Q_MEM_RES_LEN2BLOCKNUM__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
uint _q_mem_res_len2blocknum(size_t len)
{
    return len % _Q_MMAP_CHUNKLEN ? len / _Q_MMAP_CHUNKLEN + 1 : len / _Q_MMAP_CHUNKLEN;
}
#endif //_q_mem_res_len2blocknum_INIT_TESTS

#if  defined(___Q_MEM_RES_MAP_IDX2ADDR__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
size_t _q_mem_res_map_idx2addr(uint idx)
{
    return _IN_SRAM_ADDR_ORIGIN + idx * _Q_MMAP_CHUNKLEN;
}
#endif //_q_mem_res_map_idx2addr_INIT_TESTS


#if  defined(___Q_MEM_MB_GET_FREE_STATUS__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
uint _q_mem_mb_get_free_status(mbcb_t *mb)
{
    uint status = 0;
    if ((mb->size == mb->head->len + sizeof(*(mb->head))) && mb->head->free) {
        status = 1;
    }
    return status;
}
#endif //_q_mem_mb_get_free_status_INIT_TESTS


#if  defined(__QOS_DYN_MEMORY_INIT__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void qos_dyn_memory_init(void)
{
    mcb.low_border = ceil_round_to_x(ADDRVAR(_sdata) + ADDRVAR(_Bss_Size) +
                                     ADDRVAR(_Data_Size), _Q_MMAP_CHUNKLEN);
    mcb.high_border = floor_round_to_x(ADDRVAR(_estack) - ADDRVAR(_Min_Stack_Size) -
                                   ADDRVAR(_Min_Heap_Size), _Q_MMAP_CHUNKLEN);

    //init mpu protected area of heap
    size_t mpu_protected_area_size = floor_round_to_2_degree(_RAM_SIZE / 2);
//    size_t mpu_heap_origin;
    if (mpu_protected_area_size * 2 < _RAM_SIZE) {
        mcb.mpu_area_start =_IN_SRAM_ADDR_ORIGIN +  mpu_protected_area_size;
    }
    else {
        mcb.mpu_area_start =_IN_SRAM_ADDR_ORIGIN + mpu_protected_area_size / 2;
    }
    mcb.mb_mpu_end = mcb.mpu_area_start + mpu_protected_area_size;
    mcb.mb_raw_blocksize = mpu_protected_area_size / _Q_MPU_DYN_BLOCK_NUM;

    uint exclude_block_num = 0;
    if (mcb.low_border > mcb.mpu_area_start) {
        mcb.mb_mpu_start = ceil_round_to_x((mcb.low_border - mcb.mpu_area_start), mcb.mb_raw_blocksize) + mcb.mpu_area_start;
        exclude_block_num = (mcb.mb_mpu_start - mcb.mpu_area_start) / _Q_MMAP_GR_BLOCKNUM;
    }
    else {
        mcb.mb_mpu_start = mcb.mpu_area_start;
    }

    _q_mem_mb_set_free(exclude_block_num, _Q_MPU_DYN_BLOCK_NUM - exclude_block_num);

    //fill memmap
    if (mcb.low_border < mcb.mb_mpu_start) {
        uint num_blocks = (mcb.mb_mpu_start - mcb.low_border) / _Q_MMAP_CHUNKLEN;
        _q_mem_res_set_map(_q_mem_res_addr2map_idx(mcb.low_border), num_blocks);
        mcb.res_mem_startblock = _q_mem_res_addr2map_idx(mcb.low_border);
    }
    else {
        mcb.res_mem_startblock = _q_mem_res_addr2map_idx(mcb.mb_mpu_end);
    }
    uint num_blocks = (mcb.high_border - mcb.mb_mpu_end) / _Q_MMAP_CHUNKLEN;
    _q_mem_res_set_map(_q_mem_res_addr2map_idx(mcb.mb_mpu_end), num_blocks);


}
#endif //qos_dyn_memory_init_INIT_TESTS



#if  defined(__QOS_MEMBLOCK_INIT__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void qos_memblock_init(memaddr_t base, size_t size, mbcb_t* memblock)
{
//    memblock->free = 1;

    _q_bh_min_init(&memblock->bheap);
//    _qos_close_item_lns(&memblock->ll_chunks);
    _qos_close_item_lns(&memblock->ll_item_thread);
    memblock->size = size;

    mch_head_t* chunk = (mch_head_t*)base;
    memblock->head = chunk;
    chunk->free = 1;
    _qos_close_item_lns(&chunk->lle);
    _q_bh_node_init(&chunk->node, memblock->size - sizeof(*chunk));

//    _qos_ins_to_ln_after_item(&chunk->lle, &memblock->ll_chunks);
    _q_bh_min_add(&chunk->node, &memblock->bheap);
}
#endif //qos_memblock_init_INIT_TESTS


#if  defined(__QALLOC_HANDLER__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
#ifdef MPU_ENABLE
void* qalloc_dynmem(size_t len, q_lnim_t *th_memblocks, mpu_rp_t *mpu_conf)
#else
void* qalloc_dynmem(size_t len, q_lnim_t *th_memblocks)
#endif
{

    void *ptr = NULL;

    for (q_lnim_t *p = th_memblocks->next; p != th_memblocks; p = p->next) {
        mbcb_t *mb = container_of(p, mbcb_t, ll_item_thread);
        ptr = qos_memblock_malloc(len, mb);
        if (ptr != NULL) {
            return ptr;
        }
    }
    uint need_blocks = _q_mem_mb_len2blocknum(len);

    int free_block_idx = _q_mem_mb_find_free_blocks(need_blocks);
    if (free_block_idx >= 0) {
        qos_memblock_init(_q_mem_mb_idx2addr(free_block_idx),
                          need_blocks * mcb.mb_raw_blocksize,
                          &mcb.mb_mpu[free_block_idx]);
        _q_mem_mb_set_used(free_block_idx, need_blocks, &mcb.mb_mpu[free_block_idx]);
        ptr = qos_memblock_malloc(len, &mcb.mb_mpu[free_block_idx]);
        if (ptr != NULL) {
            _qos_ins_to_ln_after_item(&mcb.mb_mpu[free_block_idx].ll_item_thread, th_memblocks);
#ifdef MPU_ENABLE
            _q_mpu_add_heap_region_to_tread(free_block_idx, need_blocks, mpu_conf);
#endif // MPU_ENABLE
            return ptr;
        }
    }

    return ptr;
}
#endif //qalloc_handler_INIT_TESTS



#if  defined(__QALLOC_UNIQ_HANDLER__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
#ifdef MPU_ENABLE
void* qalloc_uniqmem(size_t len, sbcb_t *stack_cb, mpu_rp_t *mpu_conf)
#else
void* qalloc_uniqmem(size_t len, sbcb_t *stack_cb)
#endif
{
    void *ptr = NULL;
    uint stack_idx = 0;
    for (; stack_idx < _Q_MPU_STACK_REGIONS; stack_idx++) {
        if (stack_cb[stack_idx].base == 0) {
            break;
        }
    }
    if (stack_cb[stack_idx].base != 0) {
        return ptr;
    }

    uint blocknum = _q_mem_res_len2blocknum(len);
    int empty_block_seq_idx = _q_mem_res_find_seq_map(blocknum);

    if (empty_block_seq_idx < 0) {
        //проверяем, можно ли расширить пространство для стека за счет кучи
        size_t available_blocks = _q_mem_res_num_free_chunk_from_idx(_q_mem_res_addr2map_idx(mcb.mb_mpu_end));
        size_t add_space = _qos_mb_array_extend_down(len - available_blocks * _Q_MMAP_CHUNKLEN);
        if (add_space) {
            uint add_blocks_num = _q_mem_res_len2blocknum(add_space);
            empty_block_seq_idx = _q_mem_res_addr2map_idx(mcb.mb_mpu_end);
            _q_mem_res_set_map(empty_block_seq_idx, add_blocks_num);
            empty_block_seq_idx += (add_blocks_num  - (blocknum - available_blocks));
        }
    }
    if (empty_block_seq_idx >= 0) {
        _q_mem_res_unset_map(empty_block_seq_idx, blocknum);
        ptr = (uc*)_q_mem_res_map_idx2addr(empty_block_seq_idx);
#ifdef MPU_ENABLE
        _q_mpu_add_stack_region_to_tread(stack_idx, stack_cb, mpu_conf);
#endif // MPU_ENABLE
        stack_cb[stack_idx].base = (size_t)ptr;
        stack_cb[stack_idx].len = len;
    }
    return ptr;
}
#endif //qalloc_uniq_handler_INIT_TESTS


#if  defined(___QOS_MB_ARRAY_EXTEND_UP__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void _qos_mb_array_extend_up(size_t len)
{

}
#endif //_qos_mb_array_extend_up_INIT_TESTS

#if  defined(___QOS_MB_ARRAY_EXTEND_DOWN__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
size_t _qos_mb_array_extend_down(size_t len)
{
   uint need_blocks = len % mcb.mb_raw_blocksize ? len / mcb.mb_raw_blocksize + 1 : len / mcb.mb_raw_blocksize;
   if (need_blocks > (_q_mem_end_mb_idx() - _q_mem_start_mb_idx())) {
       return 0;
   }
    for (uint i = _q_mem_end_mb_idx() - need_blocks; (i < _q_mem_end_mb_idx()); i++) {
        if (mcb.mem2mb[i] != NULL) {
            return 0;
        }
    }
    mcb.mb_mpu_end -= mcb.mb_raw_blocksize * need_blocks;

    return mcb.mb_raw_blocksize * need_blocks;
}
#endif //_qos_mb_array_extend_down_INIT_TESTS


#if  defined(___Q_FREE_MAIN_MEM__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
#ifdef MPU_ENABLE
void _q_free_main_mem(void *ptr,  q_lnim_t *th_memblocks, mpu_rp_t *mpu_conf)
#else
void _q_free_main_mem(void *ptr,  q_lnim_t *th_memblocks)
#endif
{
    size_t memaddr = (size_t)ptr;

    if ((memaddr >= mcb.mb_mpu_start) && (memaddr < mcb.mb_mpu_end)) {
        q_lnim_t *p = th_memblocks->next;
        for (; p != th_memblocks; p = p->next) {
            mbcb_t *mb = container_of(p, mbcb_t, ll_item_thread);
            if ((memaddr >= ((size_t)mb->head + sizeof(mb->head))) &&
                    (memaddr < ((size_t)mb->head + sizeof(mb->head) + mb->size))) {
                qos_memblock_free(ptr, mb);
                if (_q_mem_mb_get_free_status(mb)) {
                    _qos_rem_item_from_ln_list(&mb->ll_item_thread);
                    uint num = mb->size / mcb.mb_raw_blocksize;
                    uint idx = _q_mem_mb_addr2idx((size_t)mb->head);
                    _q_mem_mb_set_free(idx, num);
                    #ifdef MPU_ENABLE
                    _q_mpu_remove_heap_region_to_tread(idx, num, mpu_conf);
                    #endif // MPU_ENABLE
                }
                return;
            }
        }
    }

    ERR_RAISE(Q_MEM_ACCESS_VIOLATE);

}
#endif //_q_free_main_mem_INIT_TESTS




#if  defined(___Q_FREE_RESIDUAL_MEM__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
#ifdef MPU_ENABLE
void _q_free_residual_mem(uint idx, sbcb_t *stack_cb, mpu_rp_t *mpu_conf)
#else
void _q_free_residual_mem(uint idx, sbcb_t *stack_cb)
#endif
{
    if ((stack_cb[idx].base < mcb.low_border) ||
         ((stack_cb[idx].base >= mcb.mpu_area_start) && (stack_cb[idx].base < mcb.mb_mpu_end))  ||
            (stack_cb[idx].base > mcb.high_border)) {
        ERR_RAISE(Q_MEM_ACCESS_VIOLATE);
        return;
    }
    _q_mem_res_set_map(_q_mem_res_addr2map_idx(stack_cb[idx].base),
                       _q_mem_res_len2blocknum(stack_cb[idx].len));
    stack_cb[idx].base = 0;
    #ifdef MPU_ENABLE
    _q_mpu_remove_stack_region_to_tread(idx, stack_cb, mpu_conf);
    #endif // MPU_ENABLE

}
#endif //_q_free_residual_mem_INIT_TESTS

#if  defined(___Q_MEM_RES_NUM_FREE_CHUNK_FROM_IDX) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
size_t _q_mem_res_num_free_chunk_from_idx(size_t idx)
{
    uint count = 0;
    uint i = idx / _Q_MMAP_CH_IN_BL ;
    if (mcb.memmap[i] != 0xFF) {
        for (uint j = idx % _Q_MMAP_CH_IN_BL; j < _Q_MMAP_CH_IN_BL; j++) {
            if ((mcb.memmap[i] & (1 << j)) == 0) {

                break;
            }
            else {
                count++;
            }
        }
        i++;
    }
    for (; i <= _q_mem_res_addr2map_idx(mcb.high_border) /
         _Q_MMAP_CH_IN_BL; i++) {
        if (mcb.memmap[i] != 0xFF) {
            for (uint j = 0; j < _Q_MMAP_CH_IN_BL; j++) {
                if ((mcb.memmap[i] & (1 << j)) == 0) {
                    break;
                }
                else {
                    count++;
                }
            }
            break;
        }
        count += 8;
    }
    return count;
}
#endif //_q_mem_res_num_free_chunk_from_idx

#ifdef DD__qos_dyn_mem_enable
#undef _PROXY
#define _PROXY(f, ...) NO_##f(__VA_ARGS__)
#endif

#ifdef DD__qos_malloc
#undef _PROXY
#define _PROXY(f, ...) A_##f(__VA_ARGS__)
#endif

#if  defined(__QOS_MEMBLOCK_MALLOC__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void* qos_memblock_malloc(size_t size, mbcb_t* memblock)
{
    bh_node_t* node = memblock->bheap.root;
    while(node != NULL) {
        if (node->key >= size) {
            break;
        }
        node = _bh_next_node(node);
    }
    if (node == NULL) {
        return NULL;
    }
    mch_head_t* chunk = container_of(node, mch_head_t, node);
    if (chunk->len >= size + sizeof(mch_head_t) + 4) {
        mch_head_t *new_free_chunk = (mch_head_t*)((size_t)chunk + sizeof(mch_head_t) + size);
        _q_bh_node_init(&new_free_chunk->node, chunk->len - (sizeof(mch_head_t) + size));
        _qos_ins_to_ln_after_item(&new_free_chunk->lle, &chunk->lle);
        new_free_chunk->free = 1;
        _q_bh_min_add(&new_free_chunk->node, &memblock->bheap);

        chunk->len = size;
    }
    chunk->free = 0;
    _q_bh_remove(node, &memblock->bheap);
    return (void*)((size_t)chunk + sizeof(mch_head_t));
}
#endif //qos_memblock_malloc_INIT_TESTS

#ifdef DD__qos_malloc
#undef _PROXY
#define _PROXY(f, ...) NO_##f(__VA_ARGS__)
#endif


#ifdef DD__qos_free
#undef _PROXY
#define _PROXY(f, ...) A_##f(__VA_ARGS__)
#endif

#if  defined(__QOS_MEMBLOCK_FREE__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void qos_memblock_free(void* ptr, mbcb_t* memblock)
{
    mch_head_t *chunk = (mch_head_t*)((size_t)ptr - sizeof(mch_head_t));

    //union chunk with neighbour chunks
    //go up

    u32 len = chunk->len;
    if (chunk->lle.next != &memblock->head->lle) {
        mch_head_t *tc = container_of(chunk->lle.next, mch_head_t, lle);
        if ((size_t)tc + sizeof(tc) + tc->len > (size_t)memblock->head + memblock->size) {
            ERR_RAISE(Q_MEM_ACCESS_VIOLATE);
            return;
        }
        if (tc->free == 1) {
            _qos_rem_item_from_ln_list(&tc->lle);
            _q_bh_remove(&tc->node, &memblock->bheap);
            len += tc->len + sizeof(*tc);
        }
    }
    else {
        len = (size_t)memblock->head + memblock->size - (size_t)(chunk) - sizeof(*chunk);
    }

    //go down
    if (chunk != memblock->head) {
        mch_head_t *tc = container_of(chunk->lle.prev, mch_head_t, lle);
        if ((size_t)tc < (size_t)memblock->head) {
            ERR_RAISE(Q_MEM_ACCESS_VIOLATE);
            return;
        }
        if (tc->free == 1) {
            _qos_rem_item_from_ln_list(&chunk->lle);
            _q_bh_remove(&tc->node, &memblock->bheap);
            len += tc->len + sizeof(*tc);
            chunk = tc;
        }
    }
    chunk->free = 1;
    _q_bh_node_init(&chunk->node, len);
    _q_bh_min_add(&chunk->node, &memblock->bheap);
}
#endif //qos_memblock_free_INIT_TESTS

#ifdef DD__qos_free
#undef _PROXY
#define _PROXY(f, ...) NO_##f(__VA_ARGS__)
#endif


//#ifndef DESKTOP_TEST
#if  defined(__QOS_MEM_ALLOC__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
memaddr_t qos_mem_alloc(uint32_t size)
{

    uint32_t base;
    //base address of MPU region aligh to region size
    base = ceil_round_to_x(mcb.low_border, size);


    if (base + size >= mcb.high_border)  {
        ERR_RAISE(Q_OUT_OF_MEMORY);
    }
    mcb.low_border = base + size;
    return base;
}
#endif //qos_mem_alloc_INIT_TESTS

//void *malloc(size_t size)
//{


//}


 #ifdef MPU_ENABLE
#if  defined(___Q_MPU_ADD_HEAP_REGION_TO_TREAD__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void _q_mpu_add_heap_region_to_tread(uint idx, uint num, mpu_rp_t *mpu_conf)
{
    _q_unset_8bit_map(idx, num, &((u8*)&mpu_conf[_Q_MPU_HEAP_PREF_IDX].attr_size)[1], 3);

}
#endif //_q_mpu_add_heap_region_to_tread_INIT_TESTS

#if  defined(___Q_MPU_REMOVE_HEAP_REGION_TO_TREAD__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void _q_mpu_remove_heap_region_to_tread(uint idx, uint num, mpu_rp_t *mpu_conf)
{
    _q_set_8bit_map(idx, num, &((u8*)&mpu_conf[_Q_MPU_HEAP_PREF_IDX].attr_size)[1], 3);
}
#endif //_q_mpu_remove_heap_region_to_tread_INIT_TESTS

#if  defined(___Q_MPU_ADD_STACK_REGION_TO_TREAD__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void _q_mpu_add_stack_region_to_tread(uint stack_idx, sbcb_t *stack_cb, mpu_rp_t *mpu_conf)
{

    q_mpu_encode_pref_spec(&mpu_conf[_Q_MPU_STACK_PREF_IDX + stack_idx],
                           _Q_MPU_STACK0_REGION + stack_idx,
            stack_cb[stack_idx].base,
            stack_cb[stack_idx].len, 0, _MPU_USER_MPU_PREF1);

}
#endif //_q_mpu_add_stack_region_to_tread_INIT_TESTS

#if  defined(___Q_MPU_REMOVE_STACK_REGION_TO_TREAD__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void _q_mpu_remove_stack_region_to_tread(uint stack_idx, sbcb_t *stack_cb,  mpu_rp_t *mpu_conf)
{

    q_mpu_encode_pref_spec(&mpu_conf[_Q_MPU_STACK_PREF_IDX + stack_idx],
                           _Q_MPU_STACK0_REGION + stack_idx,
            stack_cb[stack_idx].base,
            stack_cb[stack_idx].len, 0, _MPU_NO_ACCESS);

}
#endif //_q_mpu_remove_stack_region_to_tread_INIT_TESTS


//init all regions (and for stack too)
#if  defined(__Q_MPU_TREAD_INIT__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void q_mpu_tread_init(sbcb_t *stack_cb, mpu_rp_t *mpu_conf)
{
    _q_mpu_add_stack_region_to_tread(0, stack_cb, mpu_conf);
    for (uint i = 0; i < _Q_MPU_DYNAMIC_REGIONS; i++) {
        q_mpu_encode_pref_spec(&mpu_conf[_Q_MPU_HEAP_PREF_IDX + i], _Q_MPU_HEAP_REGION + i,
                _q_mem_mb_idx2addr(i * _Q_MPU_SUBREGIONS), mcb.mb_raw_blocksize, 0xFF, _MPU_USER_MPU_PREF1);
    }
//    q_mpu_apply_thread_pref(mpu_conf);
}
#endif //q_mpu_tread_init_INIT_TESTS

#if  defined(__Q_MPU_APPLY_THREAD_PREF__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void q_mpu_apply_thread_pref(mpu_rp_t *mpu_conf)
{
    q_mpu_apply_pref(&mpu_conf[_Q_MPU_STACK_PREF_IDX]);
    q_mpu_apply_group_pref(&mpu_conf[_Q_MPU_HEAP_PREF_IDX]);
}
#endif //q_mpu_apply_thread_pref_INIT_TESTS


/*
 * size - value from row: 32; 64; 128; 256; 512; 1024, 2048 ...
 */

#if  defined(__Q_MPU_ENCODE_PREF__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
_FORCE_INLINE void q_mpu_encode_pref(mpu_rp_t* pref,
                      u8 region,
                      uint32_t addr,
                      uint32_t size,
                      uint32_t exec_never,
                      uint32_t access_perm,
                      uint32_t mem_type,
                      u32 subregion_disable)
{
    pref->base_addr = (addr & MPU_RBAR_ADDR_Msk) | MPU_RBAR_VALID_Msk| (region & MPU_RBAR_REGION_Msk);
    pref->attr_size = ((_MPU_DEF_XN(exec_never) & MPU_RASR_XN_Msk) |
                       (_MPI_DEF_AP(access_perm) & MPU_RASR_AP_Msk) |
                       (_MPU_DEF_TP(mem_type) & (MPU_RASR_TEX_Msk | MPU_RASR_S_Msk| MPU_RASR_C_Msk| MPU_RASR_B_Msk)) |
                       (_MPU_DEF_SRD(subregion_disable) & MPU_RASR_SRD_Msk) |
                       (_MPU_DEF_SIZE(size) & MPU_RASR_SIZE_Msk) |
                       (MPU_RASR_ENABLE_Msk));
}
#endif //q_mpu_encode_pref_INIT_TESTS


#if  defined(__Q_MPU_ENCODE_PREF_SPEC__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void q_mpu_encode_pref_spec(mpu_rp_t* pref,
                         u8 region,
                         u32 addr,
                         u32 size,
                         u32 subregion_disable,
                        u32 spec_defs)
{
    pref->base_addr = (addr & MPU_RBAR_ADDR_Msk) | MPU_RBAR_VALID_Msk| (region & MPU_RBAR_REGION_Msk);
    pref->attr_size = (_MPU_DEF_SRD(subregion_disable) & MPU_RASR_SRD_Msk) |
                (_MPU_DEF_SIZE(size) & MPU_RASR_SIZE_Msk) |
                spec_defs |
                MPU_RASR_ENABLE_Msk;
}
#endif //q_mpu_encode_pref_spec_INIT_TESTS

//u8 q_mpu_get_subregs_pref(u32 pref[_Q_MPU_REGS])
//{
//    return (pref[1] & MPU_RASR_SRD_Msk) >> MPU_RASR_SRD_Pos;
//}


#if  defined(__Q_MPU_ADD_SUBREG_PREF__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
/*_FORCE_INLINE */void q_mpu_add_subreg_pref(mpu_rp_t* pref, u8 subregions)
{
    pref->attr_size |= (_MPU_DEF_SRD(subregions));
}
#endif //q_mpu_add_subreg_pref_INIT_TESTS

#if  defined(__Q_MPU_REM_SUBREG_PREF__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
/*_FORCE_INLINE */void q_mpu_rem_subreg_pref(mpu_rp_t* pref, u8 subregions)
{
    pref->attr_size &= ~(_MPU_DEF_SRD(subregions));
}
#endif //q_mpu_rem_subreg_pref_INIT_TESTS


#if  defined(__Q_MPU_SET_BASE_REGION__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void q_mpu_set_base_region(u8 region)
{
    MPU->RNR = MPU_RNR_REGION_Msk | _MPU_DEF_B_REGION(region);
}
#endif //q_mpu_set_base_region_INIT_TESTS


#if  defined(__Q_MPU_APPLY_PREF__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
_FORCE_INLINE void q_mpu_apply_pref(mpu_rp_t* pref)
{
    MPU->RBAR = pref->base_addr;
    MPU->RNR = pref->attr_size;
}
#endif //q_mpu_apply_pref_INIT_TESTS

#if  defined(__Q_MPU_APPLY_GROUP_PREF__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void q_mpu_apply_group_pref(mpu_rp_t* pref)
{
    volatile uint32_t* pReg = &MPU->RBAR;
    u32* pref_ptr = (u32*)pref;
    for (uint8_t i = 0; i < _Q_MPU_REGS * 4; i++) {
        pReg[i] = pref_ptr[i];
    }
}
#endif //q_mpu_apply_group_pref_INIT_TESTS

#if  defined(__Q_MPU_REGION_UNSET__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
_FORCE_INLINE void q_mpu_region_unset(uint8_t region)
{
    MPU->RBAR = MPU_RBAR_VALID_Msk | (region & MPU_RBAR_REGION_Msk);
    MPU->RASR = 0;
}
#endif //q_mpu_region_unset_INIT_TESTS


#if  defined(__Q_MPU_ENABLE__UNIT_TESTS) || !defined(DESKTOP_TEST) || defined(__QOS_MEM_ITGR_TEST)
void q_mpu_enable(void)
{
    mpu_rp_t pref;
    //check mpu
    if (!(MPU->TYPE & MPU_TYPE_DREGION_Msk)) {
        ERR_RAISE(Q_MPU_NOT_PRESENT);
    }
    //turn off mpu
    MPU->CTRL = 0;

    //initial code here (while pass)
    q_mpu_region_unset(0);
    //Flash is read only for all
    q_mpu_encode_pref(&pref, 0, _FLASH_ADDR_ORIGIN, _FLASH_SIZE, _MPU_EXEC, _MPU_AP_P_RO_UP_RO, _MPU_IN_FLASH_T, 0);
    q_mpu_apply_pref(&pref);
    //access to priveleged data
    q_mpu_encode_pref(&pref, 1, ADDRVAR(_p_sdata), ADDRVAR(_Priv_Data_Size) + ADDRVAR(_Priv_Bss_Size), _MPU_EXEC_NEVER, _MPU_AP_P_RW_UP_RO, _MPU_IN_SRAM_T, 0);
    q_mpu_apply_pref(&pref);
    //RW, NOEXEC permissions to user data
    q_mpu_encode_pref(&pref, 2, ADDRVAR(_sdata), (ADDRVAR(_Data_Size) + ADDRVAR(_Bss_Size)), _MPU_EXEC_NEVER, _MPU_AP_P_RW_UP_RW, _MPU_IN_SRAM_T, 0);
    q_mpu_apply_pref(&pref);

//    reset left regions
    q_mpu_region_unset(3);
    q_mpu_region_unset(4);
    q_mpu_region_unset(5);
    q_mpu_region_unset(6);
    q_mpu_region_unset(7);

    q_mpu_set_base_region(_Q_MPU_HEAP_REGION);
    //turn on mpu
    MPU->CTRL = MPU_CTRL_ENABLE_Msk | MPU_CTRL_PRIVDEFENA_Msk;
}
#endif //q_mpu_enable_INIT_TESTS

//obcolete
#if  defined(__Q_MPU_THREAD_SET__UNIT_TESTS) || !defined(DESKTOP_TEST)
void q_mpu_thread_set(uint32_t tid)
{
    q_mpu_apply_pref(mpu_stack_table[tid]);
}
#endif //q_mpu_thread_set_INIT_TESTS






//debug
#define _MPU_TEST_ADDRESS 0x20010000UL

/*

static uint32_t _mpu_debug_pref[2];
volatile uint32_t mpu_d_counter;

#if  defined(___MPU_REGION_ACCESS__UNIT_TESTS) || !defined(DESKTOP_TEST)
void _mpu_region_access (uint32_t size)
{
    volatile uint32_t* pMem = (uint32_t*)_MPU_TEST_ADDRESS;
    volatile uint32_t mem_c;
    for (uint32_t i = 0; i < size/4; i++) {
        mpu_d_counter = i;
        mem_c = *pMem++;
    }
}
#endif //_mpu_region_access_INIT_TESTS


#if  defined(___MPU_TEST_REGION_SIZE__UNIT_TESTS) || !defined(DESKTOP_TEST)
void _mpu_test_region_size(uint32_t size)
{

    q_mpu_encode_pref(_mpu_debug_pref, 7, _MPU_TEST_ADDRESS, size, _MPU_EXEC_NEVER, _MPU_AP_P_RW_UP_RW, _MPU_IN_SRAM_T, 0);
    q_mpu_apply_pref(_mpu_debug_pref);

}
#endif //_mpu_test_region_size_INIT_TESTS
*/
//debug
#endif //MPU_ENABLE
//#endif //DDESKTOP_TEST

