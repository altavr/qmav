#include "misc/qos_bin_heap.h"
#include "qos_def.h"
#include "misc/qos_mem_misc.h"
#include "qos_debug.h"

void _bh_move_up(bh_node_t* node);
void _bh_replace_node(bh_node_t* old, bh_node_t* new);


void _q_bh_min_init(bheap_t* bheap)
{
    bheap->root = NULL;
    bheap->last = NULL;
}

void _q_bh_node_init(bh_node_t* node, u32 key)
{
    node->lchild = NULL;
    node->rchild = NULL;
    node->parent = NULL;
    node->key = key;
//    node->value = value;

}

#ifdef DD___q_bh_min_add
#undef _PROXY
#define _PROXY(f, ...) A_##f(__VA_ARGS__)
#endif

void _q_bh_min_add(bh_node_t* node, bheap_t* heap)
{
    #ifdef DESKTOP_DEBUG_BH
    node->id = idcount;
    list_nodes[idcount] = 1;
    idcount++;
    gcount++;
#endif


    if (heap->root == NULL) {
        heap->root = node;
        heap->last = node;
        return;
    }
    //перебираем узлы пока не дойдем до корня кучи
    bh_node_t* cur = heap->last;
    while (cur->parent != NULL) {
        //если потомок левый - общий предок найден
//        LINE_NUMBER;
        if (cur->parent->lchild == cur) {
            PRINT_LINE_NUMBER();
            break;
        }
        //если нет, поднимаемся на следующий уровень
        cur = cur->parent;

    }
    if (cur->parent != NULL) {
        //если есть правый потомок у родителя спускаемся по левой стороне этого потомка
        PRINT_LINE_NUMBER();
        if (cur->parent->rchild != NULL) {
            PRINT_LINE_NUMBER();
            cur = cur->parent->rchild;
            while (cur->lchild != NULL) {
                cur = cur->lchild;
            }
            cur->lchild = node;
        }
        else {
            PRINT_LINE_NUMBER();
            cur = cur->parent;
            cur->rchild = node;
        }
    }
    else {
        //текущий узел это корень, уровень заполнен, спускаемся до левого нижнего узла
//        uint i = 0;
        while (cur->lchild != NULL) {
            cur = cur->lchild;
            PRINT_LINE_NUMBER();
//            if (i++ > 10) {
//                break;
//            }
        }
        cur->lchild = node;
    }
    node->parent = cur;

    heap->last = node;
    //восстановить свойство кучи
    if ((node->parent != NULL) && (node->key < node->parent->key)) {
        heap->last = node->parent;
        PRINT_LINE_NUMBER();
    }
    while ((node->parent != NULL) && (node->key < node->parent->key)) {
        PRINT_LINE_NUMBER();
        _bh_move_up(node);
        if (node->parent == NULL) {
            heap->root = node;
            PRINT_LINE_NUMBER();
        }
        PRINT_LINE_NUMBER();
    }
//    PRINT_NODE(node);

}

#ifdef DD___q_bh_min_add
#undef _PROXY
#define _PROXY(f, ...) NO_##f(__VA_ARGS__)
#endif



#ifdef DD___q_bh_remove
#undef _PROXY
#define _PROXY(f, ...) A_##f(__VA_ARGS__)
#endif

void _q_bh_remove(bh_node_t* node, bheap_t* heap)
{

#ifdef DESKTOP_DEBUG_BH
    list_nodes[node->id] = 0;
    gcount--;
#endif

    if ((heap->root == heap->last) && (node->parent == NULL)) {
        heap->root = NULL;
        heap->last = NULL;
        return;
    }
    //find node before last node

    bh_node_t* cur = heap->last;
    while ((cur->parent != NULL) && (cur->parent->lchild == cur)) {
        cur = cur->parent;
    }
    if (cur->parent != NULL) {
        //cur is not root
        cur = cur->parent->lchild;
    }
    while (cur->rchild != NULL) {
        cur = cur->rchild;
    }
    //disconnect last node
    if (heap->last->parent->lchild == heap->last) {
        heap->last->parent->lchild = NULL;
    }
    else {
        heap->last->parent->rchild = NULL;
    }

    //remove node
    if (node == heap->last) {
        heap->last = cur;
    }
    else {
        bh_node_t *srcnode = heap->last;
        if (node == heap->root) {
            heap->root = srcnode;
        }
        _bh_replace_node(node, srcnode);
        if (node != cur) {
            heap->last = cur;
        }
        // Restore the heap propert
        if ((srcnode->parent != NULL) && (srcnode->key < srcnode->parent->key)) {
            do {
                _bh_move_up(srcnode);
            } while ((srcnode->parent != NULL) && (srcnode->key < srcnode->parent->key));
            if (srcnode->parent == NULL) {
                heap->root = srcnode;
            }
        }

        else {

            if ((srcnode->lchild != NULL) ) {
                bh_node_t *r;
                if ((srcnode->rchild != NULL) && (srcnode->lchild->key > srcnode->rchild->key)){
                    r = srcnode->rchild;
                }
                else {
                    r = srcnode->lchild;
                }
                if (srcnode->key > r->key) {
                    if (srcnode->parent == NULL) {
                        heap->root = r;
                    }
                    if (r == heap->last) {
                        heap->last = srcnode;
                    }
                    _bh_move_up(r);
                }

                while ((srcnode->lchild != NULL) ) {
                    if ((srcnode->rchild != NULL) && (srcnode->lchild->key > srcnode->rchild->key)){
                        r = srcnode->rchild;
                    }
                    else {
                        r = srcnode->lchild;
                    }
                    if (srcnode->key > r->key) {
                        if (r == heap->last) {
                            heap->last = srcnode;
                        }
                        _bh_move_up(r);

                    }
                    else {
                        break;
                    }

                }
            }
        }
    }
}


#ifdef DD___q_bh_remove
#undef _PROXY
#define _PROXY(f, ...) NO_##f(__VA_ARGS__)
#endif



#ifdef   DD___bh_next_node
#undef _PROXY
#define _PROXY(f, ...) A_##f(__VA_ARGS__)
#endif



bh_node_t* _bh_next_node(bh_node_t* node)
{
    PRINT_DDBG_("\nstart _bh_next_node ");
    uint diff_levels = 0;
    while ((node->parent != NULL) && (node->parent->rchild == node)) {
        PRINT_LINE_NUMBER();
        PRINT_NODE(node);
        node = node->parent;
        diff_levels++;
        PRINT_LINE_NUMBER();
        PRINT_NODE(node);
    }
    if (node->parent != NULL) {
        PRINT_LINE_NUMBER();
        PRINT_NODE(node);
        if (node->parent->rchild != NULL) {
            node = node->parent->rchild;
        }
        else
        {
            return NULL;
        }
        PRINT_LINE_NUMBER();
        PRINT_NODE(node);
    }
    else {
        diff_levels++;
        PRINT_LINE_NUMBER();
    }
    while ((diff_levels > 0)) {
        PRINT_LINE_NUMBER();
        PRINT_NODE(node);
        if (node->lchild != NULL) {
            node = node->lchild;
            diff_levels--;
        }
        else {
            return NULL;
        }
        PRINT_LINE_NUMBER();
        PRINT_NODE(node);
    }
    return node;
}

#ifdef   DD___bh_next_node
#undef _PROXY
#define _PROXY(f, ...) NO_##f(__VA_ARGS__)
#endif


void _bh_move_up(bh_node_t* node)

{
    bh_node_t* old_parent = node->parent;
//    LINE_NUMBER;
//    PRINT_ARRAY_NODE();

    _bh_replace_ext_links1(node, &old_parent);
//    LINE_NUMBER;
//    PRINT_ARRAY_NODE();
    _bh_replace_ext_links1(old_parent, &node);


//    LINE_NUMBER;
//    PRINT_ARRAY_NODE();
//    DDBG("old parent pointer - %p\n", old_parent);
//    DDBG("node - %p\n", node);
    if (old_parent->parent != NULL) {
//        DDBG("old_parent->parent->lchild - %p\n", old_parent->parent->lchild);
        if (old_parent->parent->lchild == old_parent) {
            old_parent->parent->lchild = node;
        }
        else {
            old_parent->parent->rchild = node;
        }
    }

//    LINE_NUMBER;
//    PRINT_ARRAY_NODE();
    if (old_parent->lchild == node) {
        _bh_swap_refs(&node->rchild, &old_parent->rchild);
        old_parent->lchild = node->lchild;
        node->lchild = old_parent;

//        LINE_NUMBER;
//        PRINT_ARRAY_NODE();

    }
    else {
        _bh_swap_refs(&node->lchild, &old_parent->lchild);
        old_parent->rchild = node->rchild;
        node->rchild = old_parent;
//        LINE_NUMBER;
//        PRINT_ARRAY_NODE();
    }

    node->parent = old_parent->parent;
    old_parent->parent = node;

//    LINE_NUMBER;
//    PRINT_ARRAY_NODE();


}

void _bh_replace_node(bh_node_t* old, bh_node_t* new)
{

    if (old->parent != NULL) {
        PRINT_DDBG("old->parent->lchild - %p\n", old->parent->lchild);
        if (old->parent->lchild == old) {
            old->parent->lchild = new;
        }
        else {
            old->parent->rchild = new;
        }
    }
    _bh_replace_ext_links2(new, &old);
    _q_memcp(&new->parent, &old->parent, sizeof(bh_node_t*) * 3);
}
