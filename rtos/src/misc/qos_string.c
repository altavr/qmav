#include "misc/qos_string.h"
#include <stdarg.h>
#include <limits.h>
#include <stdint.h>
#include <assert.h>
#include "qos_sys_functions.h"
//#include "stdlib.h"
#include <math.h>

#define CONSOLE_BUF_SIZE 200
static int console_rd;
static q_mutex_t console_mutex;

// Buffer size for a decimal string of a signed integer, 10/33 > log10(2)
#define SIGNED_PRINT_SIZE(object)  (sizeof(object) * CHAR_BIT * 10 / 33 + 3)
#define UNSIGNED_PRINT_SIZE(object)  (sizeof(object) * CHAR_BIT * 10 / 33 + 2)

const uint32_t powers_of_10[] = {
    1,
    10,
    100 ,
    1000,
    10000,
    100000,
    1000000,
    10000000,
    100000000,
    1000000000
};

char *itoa_x(int number, char *dest, size_t dest_size);
char *uitoa_x(unsigned int number, char *dest, size_t dest_size);
void q_ftoa(float Value, char* Buffer);
char* _q_snprintf(char* str, unsigned int num, char* fmt, va_list argp);


char * q_strcpy(char *strDest, const char *strSrc)
{
//    assert(strDest!=NULL && strSrc!=NULL);
    char *temp = strDest;
    while((*strDest++ = *strSrc++)); // or while((*strDest++=*strSrc++) != '\0');
    return temp;
}

//void *malloc(size_t size)
//{
//    uint16_t data[3200];
//    return (void *)data;
//}


int q_strlen(char* s)
{
    int i;
    for (i = 0; s[i] != '\0'; i++);
    return i;
}

void reverse(char s[])
{
    int i, j;
    char c;

    for (i = 0, j = q_strlen(s)-1; i<j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}




char *itoa_x(int number, char *dest, size_t dest_size) {
    if (dest == NULL) {
        return NULL;
    }

    char buf[SIGNED_PRINT_SIZE(number)];
    char *p = &buf[sizeof(buf) - 1];

    // Work with negative absolute value
    int neg_num = number < 0 ? number : -number;

    // Form string
    *p = '\0';
    do {
        *--p = (char) ('0' - neg_num % 10);
        neg_num /= 10;
    } while (neg_num);
    if (number < 0) {
        *--p = '-';
    }

    // Copy string
    size_t src_size = (size_t) (&buf[sizeof buf] - p);
    if (src_size > dest_size) {
        // Not enough room
        return NULL;
    }
    for (unsigned int i = 0; i < src_size; i++) {
        dest[i] = p[i];
    }
    //  return memcpy(dest, p, src_size);
    return dest;
}

char *uitoa_x(unsigned int number, char *dest, size_t dest_size) {
    if (dest == NULL) {
        return NULL;
    }

    char buf[UNSIGNED_PRINT_SIZE(number)];
    char *p = &buf[sizeof(buf) - 1];



    // Form string
    *p = '\0';
    do {
        *--p = (char) ('0' + number % 10);
        number /= 10;
    } while (number);


    // Copy string
    size_t src_size = (size_t) (&buf[sizeof buf] - p);
    if (src_size > dest_size) {
        // Not enough room
        return NULL;
    }
    for (unsigned int i = 0; i < src_size; i++) {
        dest[i] = p[i];
    }
    //  return memcpy(dest, p, src_size);
    return dest;
}


void q_ftoa(float Value, char* Buffer)
{
    union
    {
        float f;

        struct
        {
            unsigned int    mantissa_lo : 16;
            unsigned int    mantissa_hi : 7;
            unsigned int     exponent : 8;
            unsigned int     sign : 1;
        };
    } helper;

    unsigned long mantissa;
    signed char exponent;
    unsigned int int_part;
    char frac_part[3];
    int i, count = 0;

    helper.f = Value;
    //mantissa is LS 23 bits
    mantissa = helper.mantissa_lo;
    mantissa += ((unsigned long) helper.mantissa_hi << 16);
    //add the 24th bit to get 1.mmmm^eeee format
    mantissa += 0x00800000;
    //exponent is biased by 127
    exponent = (signed char) helper.exponent - 127;

    //too big to shove into 8 chars
    if (exponent > 18)
    {
        Buffer[0] = 'I';
        Buffer[1] = 'n';
        Buffer[2] = 'f';
        Buffer[3] = '\0';
        return;
    }

    //too small to resolve (resolution of 1/8)
    if (exponent < -3)
    {
        Buffer[0] = '0';
        Buffer[1] = '\0';
        return;
    }

    count = 0;

    //add negative sign (if applicable)
    if (helper.sign)
    {
        Buffer[0] = '-';
        count++;
    }

    //get the integer part
    int_part = mantissa >> (23 - exponent);
    //convert to string
    itoa_x(int_part, &Buffer[count], 8);

    //find the end of the integer
    for (i = 0; i < 8; i++)
        if (Buffer[i] == '\0')
        {
            count = i;
            break;
        }

    //not enough room in the buffer for the frac part
    if (count > 5)
        return;

    //add the decimal point
    Buffer[count++] = '.';

    //use switch to resolve the fractional part
    switch (0x7 & (mantissa  >> (20 - exponent)))
    {
        case 0:
            frac_part[0] = '0';
            frac_part[1] = '0';
            frac_part[2] = '0';
            break;
        case 1:
            frac_part[0] = '1';
            frac_part[1] = '2';
            frac_part[2] = '5';
            break;
        case 2:
            frac_part[0] = '2';
            frac_part[1] = '5';
            frac_part[2] = '0';
            break;
        case 3:
            frac_part[0] = '3';
            frac_part[1] = '7';
            frac_part[2] = '5';
            break;
        case 4:
            frac_part[0] = '5';
            frac_part[1] = '0';
            frac_part[2] = '0';
            break;
        case 5:
            frac_part[0] = '6';
            frac_part[1] = '2';
            frac_part[2] = '5';
            break;
        case 6:
            frac_part[0] = '7';
            frac_part[1] = '5';
            frac_part[2] = '0';
            break;
        case 7:
            frac_part[0] = '8';
            frac_part[1] = '7';
            frac_part[2] = '5';
            break;
    }

    //add the fractional part to the output string
    for (i = 0; i < 3; i++)
        if (count < 7)
            Buffer[count++] = frac_part[i];

    //make sure the output is terminated
    Buffer[count] = '\0';
}

static void strreverse(char* begin, char* end)
{
    char aux;
    while (end > begin)
        aux = *end, *end-- = *begin, *begin++ = aux;
}


int itoa_prim(int num, char *str, int count)
{
    char *begin = &str[count];
    do {
        str[count++] = '0' + (char)(num % 10);
        num /= 10;
    } while (num);

    strreverse(begin, &str[count - 1]);

    return count;
}

void ftoexpa(float num, char* str, int prec)
{

    int count = 0;
//     int dec_exp = exponent * REV_LOG_BASE_2_10;

    if (num < 0) {
        num = -num;
        str[count++] = '-';
    }

    int exp = floor(log10(num));
    float mantissa = num / powf(10, exp);
    int whole = (int)mantissa;

    int frac = (mantissa - whole) * powers_of_10[prec];
    str[count++] = '0' + (char)whole;
    str[count++] = '.';

    count = itoa_prim(frac, str, count);
    str[count++] = 'e';

    if (exp < 0) {
        exp = -exp;
        str[count++] = '-';
    }
    else {
        str[count++] = '+';
    }
    if (exp < 10) {
        str[count++] = '0';
    }
    count = itoa_prim(exp, str, count);
    str[count] = '\0';


}

size_t modp_ftoa(float value, char* str, int prec)
{
    /* Hacky test for NaN
     * under -fast-math this won't work, but then you also won't
     * have correct nan values anyways.  The alternative is
     * to link with libmath (bad) or hack IEEE float bits (bad)
     */
    if (! (value == value)) {
        str[0] = 'n'; str[1] = 'a'; str[2] = 'n'; str[3] = '\0';
        return (size_t)3;
    }
    /* if input is larger than thres_max, revert to exponential */
    const float thres_max = (float)(0x7FFFFFFF);

    /* for very large numbers switch back to native sprintf for exponentials.
       anyone want to write code to replace this? */
    /*
      normal printf behavior is to print EVERY whole number digit
      which can be 100s of characters overflowing your buffers == bad
    */
    if (value > thres_max) {
//        ftoexpa(value, str, prec);
        str[0] = '*';
        str[1] = '*';
        str[2] = '*';
        str[3] = '\0';
        return q_strlen(str);
    }



    float diff = 0.0;
    char* wstr = str;

    if (prec < 0) {
        prec = 0;
    } else if (prec > 9) {
        /* precision of >= 10 can lead to overflow errors */
        prec = 9;
    }


    /* we'll work in positive values and deal with the
       negative sign issue later */
    int neg = 0;
    if (value < 0) {
        neg = 1;
        value = -value;
    }


    int whole = (int) value;
    float tmp = (value - whole) * powers_of_10[prec];
    uint32_t frac = (uint32_t)(tmp);
    diff = tmp - frac;

    if (diff > 0.5) {
        ++frac;
        /* handle rollover, e.g.  case 0.99 with prec 1 is 1.0  */
        if (frac >= powers_of_10[prec]) {
            frac = 0;
            ++whole;
        }
    } else if (diff == 0.5 && ((frac == 0) || (frac & 1))) {
        /* if halfway, round up if odd, OR
           if last digit is 0.  That last part is strange */
        ++frac;
    }


    if (prec == 0) {
        diff = value - whole;
        if (diff > 0.5) {
            /* greater than 0.5, round up, e.g. 1.6 -> 2 */
            ++whole;
        } else if (diff == 0.5 && (whole & 1)) {
            /* exactly 0.5 and ODD, then round up */
            /* 1.5 -> 2, but 2.5 -> 2 */
            ++whole;
        }
    } else {
        int count = prec;
        // now do fractional part, as an unsigned number
        do {
            --count;
            *wstr++ = (char)('0' + (frac % 10));
        } while (frac /= 10);
        // add extra 0s
        while (count-- > 0) *wstr++ = '0';
        // add decimal
        *wstr++ = '.';
    }

    // do whole part
    // Take care of sign
    // Conversion. Number is reversed.
    do *wstr++ = (char)(48 + (whole % 10)); while (whole /= 10);
    if (neg) {
        *wstr++ = '-';
    }
    *wstr='\0';
    strreverse(str, wstr-1);
    return (size_t)(wstr - str);
}


char* q_snprintf(char* str, unsigned int num, char* fmt, ...)
{
    va_list argp;
    va_start(argp, fmt);
    char *s = _q_snprintf(str, num, fmt,  argp);
    va_end(argp);
    return s;
}


char* _q_snprintf(char* str, unsigned int num, char* fmt, va_list argp)
{
    int ia;
//    unsigned int uia;
    float fa;
    char *sa;
    char buf[16];
    uint32_t len;

    num -= 1;
    uint32_t j = 0, i = 0;
//    va_list argp;
//    va_start(argp, fmt);
    for (; fmt[i] != '\0'; i++) {
        if (fmt[i] == '%') {
            i++;
            switch (fmt[i]) {
            case '\0':
                break;
            case 'd':
                ia = va_arg(argp, int);
                itoa_x(ia, buf, 16);
                len = q_strlen(buf);
                for (uint32_t k = 0; (k < len) && (j < num); k++, j++) {
                    str[j] = buf[k];
                }
                break;
            case 'f':
                fa = va_arg(argp, double);

                modp_ftoa(fa, buf, 5);
                len = q_strlen(buf);
                for (uint32_t k = 0; (k < len) && (j < num); k++, j++) {
                    str[j] = buf[k];
                }
                break;
            case 's':
                sa = va_arg(argp, char*);
                len = q_strlen(sa);
                for (uint32_t k = 0; (k < len) && (j < num); k++, j++) {
                    str[j] = sa[k];
                }
                break;
            default:
                break;
            }
        }
        else {
            if (j < num) {
                str[j] = fmt[i];
            }
            else {
                break;
            }
            j++;
        }
    }
//    va_end(argp);
    str[j] = '\0';
    return str;

}

void console_init(int rd)
{
    console_rd = rd;
    mutex_init(&console_mutex);
}

void q_printf(char* fmt, ...)
{
    static char buf[CONSOLE_BUF_SIZE];
    uint16_t len;
    q_mutex_lock(&console_mutex);


    va_list argp;
    va_start(argp, fmt);
    _q_snprintf(buf, CONSOLE_BUF_SIZE, fmt, argp);
    va_end(argp);
    len = q_strlen(buf);

/*
    static char ibuf[CONSOLE_BUF_SIZE/2];
    static uint16_t st_str;
    char* st_buf;

    q_mutex_lock(&console_mutex);
    va_list argp;
    va_start(argp, fmt);
    _q_snprintf(ibuf, CONSOLE_BUF_SIZE/2, fmt, argp);
    va_end(argp);

    len = q_strlen(ibuf);
    if ((st_str + len) > CONSOLE_BUF_SIZE) {
        st_str = 0;
    }
    st_buf = &buf[st_str];
    uint32_t i=0;
    for (; i<len; i++) {
        st_buf[i] = ibuf[i];
    }

*/
    q_rawwrite(console_rd, (uint8_t*)buf, len);
    q_mutex_unlock(&console_mutex);
//    st_str += len;

}
