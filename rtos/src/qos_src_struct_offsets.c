#include "qos_threads.h"

#define DECLARE(SYM,VAL) \
__asm("__AS_DEFINE__ " SYM "\t%0" : : "n" ((unsigned long)(VAL)))

void foo(void)
{
    // mystruct.h
    DECLARE("Q_TH_T__USE_FPU",  offsetof(q_th_t, use_fpu));
    DECLARE("Q_TH_T_FPU_FRAME_STACK",  offsetof(q_th_t, fpu_frame_stack));

}
