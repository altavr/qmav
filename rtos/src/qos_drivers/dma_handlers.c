#include "qos_drivers/dma_handlers.h"
#ifdef STM32F303_
    #include "stm32f30x_dma.h"
#endif
#ifdef STM32F407_
    #include "stm32f4xx_dma.h"
    #include "qos_core.h"
#endif //STM32F407_

//********debug******
extern call_ev_t ev_seq[20];
extern uint32_t ev_count;
//********debug******


#ifdef STM32F303_
//USART3 TX
#ifdef _DMA_1_STREAM_3
void DMA1_Channel2_IRQHandler(void)
{
    io_res_obj_t* ro = dma_res_links[DMA_IN_ORDER_NUM(1, 2)];
    if (DMA_GetITStatus(DMA1_IT_TC2), ENABLE) {
        DMA_ClearITPendingBit(DMA1_IT_TC2);
        DISABLE_BUS_DMA1_2(ro);
        DMA_Cmd(DMA1_Channel2, DISABLE);
        q_end_waiting_io_hw(ro, ro->rd->nb);
//        BUS_ITRP(ro);
    }
}
#endif



//I2C2 RX
#ifdef _DMA_1_CH_5
void DMA1_Channel5_IRQHandler(void)
{
    io_res_obj_t* ro = dma_res_links[DMA_IN_ORDER_NUM(1, 5)];
    if (DMA_GetITStatus(DMA1_IT_TC5), ENABLE) {
        DMA_ClearITPendingBit(DMA1_IT_TC5);
        DISABLE_BUS_DMA1_5(ro);
        DMA_Cmd(DMA1_Channel5, DISABLE);
        q_end_waiting_io_hw(ro, ro->rd->nb);
    }
}

#endif
#endif //STM32F303_

#ifdef STM32F407_
//USART3 TX
#ifdef _DMA_1_STREAM_3_USART3
void DMA1_Stream3_IRQHandler(void)
{
    io_res_obj_t* ro = dma_res_links[DMA_IN_ORDER_NUM(DMA1_C, 3)];
    if (DMA1->LISR & DMA_FLAG_TCIF3) {
        DMA1->LIFCR = DMA_FLAG_TCIF3;
        DISABLE_BUS_DMA1_2(ro);
        DMA_Cmd(DMA1_Stream3, DISABLE);
        q_end_waiting_io_hw(ro, ro->rd->nb);
//        BUS_ITRP(ro);
    }
}
#endif //_DMA_1_STREAM_3



//I2C1 TX
#ifdef _DMA_1_STREAM_6
void DMA1_Stream6_IRQHandler(void)
{
    io_res_obj_t* ro = dma_res_links[DMA_IN_ORDER_NUM(DMA1_C, 6)];
    if (DMA1->HISR & DMA_FLAG_TCIF6)  {

        //debug****************************
        ev_seq[ev_count++] = EV_DMA_TX;
        //debug****************************

        DMA1->HIFCR = DMA_FLAG_TCIF6;
        DISABLE_BUS_DMA1_6(ro);
        DMA_Cmd(DMA1_Stream6, DISABLE);
    }
}

#endif

//I2C1 RX
#ifdef _DMA_1_STREAM_0
void DMA1_Stream0_IRQHandler(void)
{
    io_res_obj_t* ro = dma_res_links[DMA_IN_ORDER_NUM(DMA1_C, 0)];
    if (DMA1->LISR & DMA_FLAG_TCIF0)  {

        //debug****************************
        ev_seq[ev_count++] = EV_DMA_RX;
        //debug****************************

        DMA1->LIFCR = DMA_FLAG_TCIF0;
        ((I2C_TypeDef*)ro->base)->CR1 |= I2C_CR1_STOP;
        DISABLE_BUS_DMA1_0(ro);
        DMA_Cmd(DMA1_Stream0, DISABLE);
        q_end_waiting_io_hw(ro, ro->rd->nb);
    }
}

#endif



//I2C2 TX
#ifdef _DMA_1_STREAM_7
void DMA1_Stream7_IRQHandler(void)
{
    io_res_obj_t* ro = dma_res_links[DMA_IN_ORDER_NUM(DMA1_C, 7)];
    if (DMA1->HISR & DMA_FLAG_TCIF7)  {

        //debug****************************
        ev_seq[ev_count++] = EV_DMA_TX;
        //debug****************************

        DMA1->HIFCR = DMA_FLAG_TCIF7;
        DISABLE_BUS_DMA1_7(ro);
        DMA_Cmd(DMA1_Stream7, DISABLE);
    }
}

#endif

//I2C2 RX
#ifdef _DMA_1_STREAM_2
void DMA1_Stream2_IRQHandler(void)
{
    io_res_obj_t* ro = dma_res_links[DMA_IN_ORDER_NUM(DMA1_C, 2)];
    if (DMA1->LISR & DMA_FLAG_TCIF2)  {

        //debug****************************
        ev_seq[ev_count++] = EV_DMA_RX;
        //debug****************************

        DMA1->LIFCR = DMA_FLAG_TCIF2;
        ((I2C_TypeDef*)ro->base)->CR1 |= I2C_CR1_STOP;
        DISABLE_BUS_DMA1_2(ro);
        DMA_Cmd(DMA1_Stream2, DISABLE);

        q_end_waiting_io_hw(ro, ro->rd->nb);
    }
}

#endif

//USART1 TX
#ifdef _DMA_2_STREAM_7
void DMA2_Stream7_IRQHandler(void)
{
    io_res_obj_t* ro = dma_res_links[DMA_IN_ORDER_NUM(DMA2_C, 7)];
    if (DMA2->HISR & DMA_FLAG_TCIF7) {
        DMA2->HIFCR = DMA_FLAG_TCIF7;
        DISABLE_BUS_DMA2_7(ro);
        DMA_Cmd(DMA2_Stream7, DISABLE);
        q_end_waiting_io_hw(ro, ro->rd->nb);
//        BUS_ITRP(ro);
    }
}
#endif


//SPI2 TX
#ifdef _DMA_1_STREAM_4
void DMA1_Stream4_IRQHandler(void)
{
    io_res_obj_t* ro = dma_res_links[DMA_IN_ORDER_NUM(DMA1_C, 4)];
    if (DMA1->HISR & DMA_FLAG_TCIF4)  {

        DMA1->HIFCR = DMA_FLAG_TCIF4;
//        DISABLE_BUS_DMA1_4(ro);
        SPI2->CR2 &= ~SPI_CR2_TXDMAEN;
        DMA1_Stream4->CR &= ~DMA_SxCR_EN;
//        DMA_Cmd(DMA1_Stream4, DISABLE);
    }
}

#endif

//SPI2 RX
#ifdef _DMA_1_STREAM_3
void DMA1_Stream3_IRQHandler(void)
{
    io_res_obj_t* ro = dma_res_links[DMA_IN_ORDER_NUM(DMA1_C, 3)];
    if (DMA1->LISR & DMA_FLAG_TCIF3)  {
        DMA1->LIFCR = DMA_FLAG_TCIF3;
//        DISABLE_BUS_DMA1_3(ro);
        SPI2->CR2 &= ~SPI_CR2_RXDMAEN;
        DMA1_Stream3->CR &= ~DMA_SxCR_EN;
//        DMA_Cmd(DMA1_Stream2, DISABLE);
        q_set_pin_lev(((q_conf_spi_t*)(ro->rd->conf))->nss_port_id, ((q_conf_spi_t*)(ro->rd->conf))->nss_pin);
        q_end_waiting_io_hw(ro, ro->rd->nb);
    }
}

#endif


//SPI1 TX
#ifdef _DMA_2_STREAM_2
void DMA2_Stream2_IRQHandler(void)
{
    io_res_obj_t* ro = dma_res_links[DMA_IN_ORDER_NUM(DMA2_C, 2)];
    if (DMA2->LISR & DMA_FLAG_TCIF2)  {

        DMA2->LIFCR = DMA_FLAG_TCIF2;
        DISABLE_BUS_DMA2_2(ro);
        DMA2_Stream2->CR &= ~DMA_SxCR_EN;
    }
}

#endif

//SPI2 RX
#ifdef _DMA_2_STREAM_5
void DMA2_Stream5_IRQHandler(void)
{
    io_res_obj_t* ro = dma_res_links[DMA_IN_ORDER_NUM(DMA2_C, 5)];
    if (DMA2->HISR & DMA_FLAG_TCIF5)  {
        DMA2->HIFCR = DMA_FLAG_TCIF5;
        DISABLE_BUS_DMA2_5(ro);
        DMA2_Stream5->CR &= ~DMA_SxCR_EN;
        q_set_pin_lev(((q_conf_spi_t*)(ro->rd->conf))->nss_port_id, ((q_conf_spi_t*)(ro->rd->conf))->nss_pin);
        q_end_waiting_io_hw(ro, ro->rd->nb);
    }
}

#endif


#endif //STM32F303_
