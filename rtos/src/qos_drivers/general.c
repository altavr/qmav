#include "qos_drivers/general.h"
#include"qos_debug.h"
#include "qos_drivers/uart.h"
#include "qos_drivers/i2c.h"
#include "qos_drivers/levelman.h"
//#include "qos_def.h"
//#include <stdarg.h>
//#include "stm32f30x_rcc.h"
//#include "qos_utils.h"
#ifdef STM32F303_
    #include "qos_drivers/mcu_f303_gpio.h"
    #include "stm32f30x_gpio.h"
    #include "stm32f30x_dma.h"
#endif
#ifdef STM32F407_
    #include "qos_drivers/mcu_f407_gpio.h"
    #include "stm32f4xx_gpio.h"
    #include "stm32f4xx_dma.h"
#endif


//debug************************************************
_PRIV_BSS static RCC_TypeDef *_rcc;
_PRIV_BSS GPIO_TypeDef *_gpio_b;
_PRIV_BSS DMA_TypeDef* _dma1;
_PRIV_BSS DMA_TypeDef* _dma2;
_PRIV_BSS EXTI_TypeDef *_exti;
//end debug************************************************

static uint fd;

_PRIV_DATA GPIO_TypeDef* _gpio_reg[] = {
    GPIOA,
    GPIOB,
    GPIOC,
    GPIOD,
    GPIOE
};


//DMA_TypeDef * _dma_ptr[] = {
//    DMA1,
//    DMA2
//};

_PRIV_DATA uint32_t res_descriptor_count = 0;

_PRIV_DATA io_res_obj_t io_handlers[RES_MAX] = {
    #ifdef Q_USART1_EN
        INIT_USART_H(USART1_B_TX_6_RX_7 ),
    #endif
    #ifdef Q_I2C1_EN
        INIT_I2C_H(I2C1_B_CLK_8_SDA_9),
    #endif
    #ifdef Q_I2C2_EN
        INIT_I2C_H(I2C2_B_CLK_10_SDA_11),
    #endif
    #ifdef Q_SPI2_EN
        INIT_SPI_H(SPI2_B_CLK_13_MISO_14_MOSI_15),
    #endif
    #ifdef Q_SPI1_EN
        INIT_SPI_H(SPI1_A_CLK_5_MISO_6_MOSI_7)
    #endif
};

_PRIV_BSS io_res_obj_t* dma_res_links[MCU_DMA_CONTRL * MCU_DMA_CHANNEL_MAX];

_PRIV_BSS static uint32_t _q_pins_using[MCU_NUM_PORTS];
_PRIV_BSS static uint32_t _q_dma_ch_using[MCU_DMA_CONTRL];

_PRIV_BSS q_descriptor_i_t res_desc_table[RES_DESCRIPTORS_MAX];
//uint32_t res_descriptor_count = 0;

//extern uint32_t hw_active_flags[RES_MAX / 32 + 1];
//extern uint32_t hw_dma_using_flags[RES_MAX / 32 + 1];


#ifdef STM32F303_

_PRIV_DATA uint32_t _rcc_reg[] = {
    (uint32_t)&RCC->AHBENR,
    (uint32_t)&RCC->APB1ENR,
    (uint32_t)&RCC->APB2ENR
};

uint8_t rcc_dma_bit[] = {
    0, 1
};

void q_gpio_pin_init(GPIO_InitTypeDef* stm_gpio, GPIO_TypeDef* port, uint8_t pin, uint32_t pins_conf)
{
    stm_gpio->GPIO_Pin = 1 <<  PIN_NUM_GET(pin, pins_conf);
    stm_gpio->GPIO_OType = OTYPE_PIN_GET(pin, pins_conf);
    stm_gpio->GPIO_PuPd = PuPd_PIN_GET(pin, pins_conf);
    GPIO_Init(port, stm_gpio);
}

_FORCE_INLINE void q_gpio_rcc_enable(uint32_t bit_offset)
{
    volatile uint32_t* rcc_en_reg;
    rcc_en_reg = (uint32_t*)_rcc_reg[GPIO_RCC_BUS_ID];
    *rcc_en_reg |= 1 << bit_offset;
}

void q_pin_itr_init(uint32_t port_id, uint32_t pin, uint8_t no_alt_func, uint8_t mode)
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
    q_gpio_rcc_enable(_RCC_PORT_EN_OFFSET + port_id);
    GPIO_PinAFConfig(_gpio_reg[port_id], pin, no_alt_func);

    GPIO_InitTypeDef stm_gpio_Struct;
    EXTI_InitTypeDef stm_exti_struct;

    stm_gpio_Struct.GPIO_Mode = GPIO_Mode_IN;
    stm_gpio_Struct.GPIO_OType = GPIO_OType_PP;
    stm_gpio_Struct.GPIO_Pin = 1 << pin;
    stm_gpio_Struct.GPIO_Speed = GPIO_Speed_50MHz;
    stm_gpio_Struct.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(_gpio_reg[port_id], &stm_gpio_Struct);

    SYSCFG_EXTILineConfig(port_id, pin);
    stm_exti_struct.EXTI_Line = pin;
    stm_exti_struct.EXTI_Mode = EXTI_Mode_Interrupt;
    stm_exti_struct.EXTI_Trigger = mode;
    stm_exti_struct.EXTI_LineCmd = ENABLE;
    EXTI_Init(&stm_exti_struct);

    uint8_t itr_num;
    if (pin < 5) {
        itr_num = EXTI0_IRQn + pin;
    }
    else {
        itr_num = EXTI9_5_IRQn;
    }

    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, PRIO_PREEMPT_EXTI, PRIO_SUB_EXTI);
    NVIC_SetPriority(itr_num, prio);
    NVIC_ClearPendingIRQ(itr_num);
    NVIC_EnableIRQ(itr_num);

}
void q_pin_out_init(uint32_t port_id, uint32_t pin, uint8_t no_alt_func)
{
    q_gpio_rcc_enable(_RCC_PORT_EN_OFFSET + port_id);
    GPIO_PinAFConfig(_gpio_reg[port_id], pin, no_alt_func);

    GPIO_InitTypeDef stm_gpio_Struct;

    stm_gpio_Struct.GPIO_Mode = GPIO_Mode_OUT;
    stm_gpio_Struct.GPIO_OType = GPIO_OType_PP;
    stm_gpio_Struct.GPIO_Pin = 1 << pin;
    stm_gpio_Struct.GPIO_Speed = GPIO_Speed_50MHz;
    stm_gpio_Struct.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(_gpio_reg[port_id], &stm_gpio_Struct);

}

void q_set_pin_lev(uint32_t port_id, uint32_t pin)
{
    _gpio_reg[port_id]->BSRR = pin;
//    GPIOx->BSRR = GPIO_Pin;
}

void q_unset_pin_lev(uint32_t port_id, uint32_t pin)
{
    _gpio_reg[port_id]->BRR = pin;
//    GPIOx->BRR = GPIO_Pin;
}



void q_iface_gpio_init(io_res_t res)
{
    io_res_obj_t* ro = &io_handlers[res];
    uint32_t conf = ro->port_conf;
    uint32_t tmp_var;
    volatile uint32_t* rcc_en_reg;

    //enable RCC bus for GPIO port
    tmp_var = GPIO_CLOCK_EN_OFFSET_GET(conf);
//    rcc_en_reg = (uint32_t*)_rcc_reg[GPIO_RCC_BUS_ID];
//    *rcc_en_reg |= 1 << tmp_var;
    q_gpio_rcc_enable(tmp_var);


    //enable RCC bus for iface
    tmp_var = RCC_IFACE_IDX_GET(conf);
    rcc_en_reg = (uint32_t*)_rcc_reg[tmp_var];
    tmp_var = RCC_EN_BIT_OFFSET_GET(conf);
    *rcc_en_reg |= 1 << tmp_var;

    //pins configuration
    GPIO_InitTypeDef stm_gpio;
    gpio_conf_default(&stm_gpio);
    uint32_t port_id = GPIO_PORT_IDX_GET(conf);
    uint8_t pin_num;
    for (uint32_t pin = 0; pin < PIN_NUM_IFACE_MAX; pin++) {
        if (PIN_IS_EN(pin, ro->pins_conf)) {
            pin_num = PIN_NUM_GET(pin, ro->pins_conf);
            q_gpio_pin_init(&stm_gpio, _gpio_reg[port_id],
                            pin, ro->pins_conf);
            GPIO_PinAFConfig(_gpio_reg[port_id],
                             pin_num,
                             ALTER_FUNC_GET(ro->port_conf));
            q_set_using(port_id, pin_num, _q_pins_using);
        }
    }
    ro->flags |= BUS_GPIO_EN_FLAG;

}

void q_iface_dma_init(io_res_obj_t* ro, uint32_t iface_tx_reg, uint32_t iface_rx_reg)
{
    uint32_t conf = ro->port_conf;
    DMA_InitTypeDef stm_dma;
    uint32_t dma_contr = DMA_C_GET(conf);

    //******************debug***************
//    DMA_Channel_TypeDef* ch2 = DMA1_Channel2;
//    DMA_Channel_TypeDef* ch3 = DMA1_Channel3;
    //******************debug***************
    //enable DMA in RCC
    volatile uint32_t* rcc_en_reg  = (uint32_t*)_rcc_reg[RCC_DMA_BUS_ID];
    *rcc_en_reg |= 1 << rcc_dma_bit[dma_contr + MCU_DMA_CONTRL_OFFSET];

    dma_conf_default(&stm_dma);
    if (ro->flags & BUS_DMA_RX_EN_FLAG) {
        stm_dma.DMA_DIR = DMA_DIR_PeripheralSRC;
        stm_dma.DMA_PeripheralBaseAddr = iface_rx_reg;
        DMA_DeInit(ro->dma_rx_ch);
        DMA_Init(ro->dma_rx_ch, &stm_dma);

        uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, ro->dma_rx_prio_preempt, ro->dma_rx_prio_sub);
        NVIC_SetPriority(ro->dma_rx_itp_no, prio);
        NVIC_ClearPendingIRQ(ro->dma_rx_itp_no);
        NVIC_EnableIRQ(ro->dma_rx_itp_no);

        DMA_ITConfig(ro->dma_rx_ch, DMA_IT_TC, ENABLE);

        uint32_t dma_ch = DMA_RX_CHANNEL_GET(conf);
        q_set_using(dma_contr, dma_ch, _q_dma_ch_using);
        ro->flags |= BUS_DMA_RX_EN_FLAG;
        dma_res_links[DMA_IN_ORDER_NUM(dma_contr, dma_ch)] = ro;
    }
    if (ro->flags & BUS_DMA_TX_EN_FLAG) {
        stm_dma.DMA_DIR = DMA_DIR_PeripheralDST;
        stm_dma.DMA_PeripheralBaseAddr = iface_tx_reg;
        DMA_DeInit(ro->dma_tx_ch);
        DMA_Init(ro->dma_tx_ch, &stm_dma);

        uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, ro->dma_tx_prio_preempt, ro->dma_tx_prio_sub);
        NVIC_SetPriority(ro->dma_tx_itp_no, prio);
        NVIC_ClearPendingIRQ(ro->dma_tx_itp_no);
        NVIC_EnableIRQ(ro->dma_tx_itp_no);

        DMA_ITConfig(ro->dma_tx_ch, DMA_IT_TC, ENABLE);

        uint32_t dma_ch = DMA_TX_CHANNEL_GET(conf);
        q_set_using(dma_contr, dma_ch, _q_dma_ch_using);
        ro->flags |= BUS_DMA_TX_EN_FLAG;
        dma_res_links[DMA_IN_ORDER_NUM(dma_contr, dma_ch)] = ro;
    }
}


void gpio_conf_default(GPIO_InitTypeDef* s)
{
    s->GPIO_Mode = GPIO_Mode_AF;
    s->GPIO_Speed = GPIO_Speed_50MHz;
}

void dma_conf_default(DMA_InitTypeDef* s)
{
    s->DMA_PeripheralInc  = DMA_PeripheralInc_Disable;
    s->DMA_MemoryInc = DMA_MemoryInc_Enable;
    s->DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    s->DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    s->DMA_Mode = DMA_Mode_Normal;
    s->DMA_Priority = DMA_Priority_Medium;
    s->DMA_M2M = DMA_M2M_Disable;
}


#endif //STM32F303_********************************************************

#ifdef STM32F407_

_PRIV_DATA uint32_t _rcc_reg[] = {
    (uint32_t)&RCC->AHB1ENR,
//    (uint32_t)&RCC->AHB2ENR,
//    (uint32_t)&RCC->AHB3ENR,
    (uint32_t)&RCC->APB1ENR,
    (uint32_t)&RCC->APB2ENR
};

_PRIV_DATA uint8_t rcc_dma_bit[] = {21, 22};

void q_gpio_pin_init(GPIO_InitTypeDef* stm_gpio, GPIO_TypeDef* port, uint8_t pin, uint32_t pins_conf)
{
    stm_gpio->GPIO_Pin = 1 <<  PIN_NUM_GET(pin, pins_conf);
    stm_gpio->GPIO_OType = OTYPE_PIN_GET(pin, pins_conf);
    stm_gpio->GPIO_PuPd = PuPd_PIN_GET(pin, pins_conf);
    GPIO_Init(port, stm_gpio);
}

void gpio_conf_default(GPIO_InitTypeDef* s)
{
    s->GPIO_Mode = GPIO_Mode_AF;
    s->GPIO_Speed = GPIO_Speed_100MHz;
}

_FORCE_INLINE void q_gpio_rcc_enable(uint32_t bit_offset)
{
    volatile uint32_t* rcc_en_reg;
    rcc_en_reg = (uint32_t*)_rcc_reg[GPIO_RCC_BUS_ID];
    *rcc_en_reg |= 1 << bit_offset;
}

void q_pin_itr_init(uint32_t port_id, uint32_t pin, uint8_t no_alt_func, uint8_t mode)
{
    //debug********************************
    _exti = EXTI;
    //debug**************************


    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
    q_gpio_rcc_enable(_RCC_PORT_EN_OFFSET + port_id);
    uint8_t pin_bit = 31 - __builtin_clz(pin);
    GPIO_PinAFConfig(_gpio_reg[port_id], pin_bit, no_alt_func);

    GPIO_InitTypeDef stm_gpio_Struct;
    EXTI_InitTypeDef stm_exti_struct;

    stm_gpio_Struct.GPIO_Mode = GPIO_Mode_IN;
    stm_gpio_Struct.GPIO_OType = GPIO_OType_PP;
    stm_gpio_Struct.GPIO_Pin = pin;
    stm_gpio_Struct.GPIO_Speed = GPIO_Speed_100MHz;
    stm_gpio_Struct.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(_gpio_reg[port_id], &stm_gpio_Struct);

    SYSCFG_EXTILineConfig(port_id, pin_bit);
    stm_exti_struct.EXTI_Line = pin ;
    stm_exti_struct.EXTI_Mode = EXTI_Mode_Interrupt;
    stm_exti_struct.EXTI_Trigger = mode;
    stm_exti_struct.EXTI_LineCmd = ENABLE;
    EXTI_Init(&stm_exti_struct);

    uint8_t itr_num;
    if (pin_bit < 5) {
        itr_num = EXTI0_IRQn + pin;
    }
    else if (pin_bit < 10) {
        itr_num = EXTI9_5_IRQn;
    }
    else {
        itr_num = EXTI15_10_IRQn;
    }

    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, PRIO_PREEMPT_EXTI, PRIO_SUB_EXTI);
    NVIC_SetPriority(itr_num, prio);
    NVIC_ClearPendingIRQ(itr_num);
    NVIC_EnableIRQ(itr_num);

}

void q_pin_out_init(uint16_t port_id, uint16_t pin, uint8_t pull, uint8_t num_alt_func)
{
    q_gpio_rcc_enable(_RCC_PORT_EN_OFFSET + port_id);
    uint8_t pin_bit = 31 - __builtin_clz(pin);
    GPIO_PinAFConfig(_gpio_reg[port_id], pin_bit, num_alt_func);

    GPIO_InitTypeDef stm_gpio_Struct;

    stm_gpio_Struct.GPIO_Mode = GPIO_Mode_OUT;
    stm_gpio_Struct.GPIO_OType = GPIO_OType_PP;
    stm_gpio_Struct.GPIO_Pin = pin;
    stm_gpio_Struct.GPIO_Speed = GPIO_Speed_100MHz;
    stm_gpio_Struct.GPIO_PuPd = pull;
    GPIO_Init(_gpio_reg[port_id], &stm_gpio_Struct);

}

void q_set_pin_lev(uint32_t port_id, uint16_t pin)
{
    _gpio_reg[port_id]->BSRRL = pin;
}

void q_unset_pin_lev(uint32_t port_id, uint32_t pin)
{
    _gpio_reg[port_id]->BSRRH = pin;
}

void dma_conf_default(DMA_InitTypeDef* s)
{
    DMA_StructInit(s);
    s->DMA_PeripheralInc  = DMA_PeripheralInc_Disable;
    s->DMA_MemoryInc = DMA_MemoryInc_Enable;
    s->DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    s->DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    s->DMA_Mode = DMA_Mode_Normal;
    s->DMA_Priority = DMA_Priority_Medium;
//    s->DMA_M2M = DMA_M2M_Disable;
}

void q_iface_dma_init(io_res_obj_t* ro, uint32_t iface_tx_reg, uint32_t iface_rx_reg)
{
    uint32_t conf = ro->port_conf;
    DMA_InitTypeDef stm_dma;
    uint32_t dma_contr = DMA_C_GET(conf);

    //******************debug***************
    _dma1 = DMA1;
    _dma2 = DMA2;
    //******************debug***************
    //enable DMA in RCC
    volatile uint32_t* rcc_en_reg  = (uint32_t*)_rcc_reg[RCC_DMA_BUS_ID];
    *rcc_en_reg |= 1 << rcc_dma_bit[dma_contr];

    dma_conf_default(&stm_dma);
    if (ro->flags & BUS_DMA_RX_EN_FLAG) {
        stm_dma.DMA_DIR = DMA_DIR_PeripheralToMemory;
        stm_dma.DMA_PeripheralBaseAddr = iface_rx_reg;
        stm_dma.DMA_Channel = DMA_RX_CHANNEL_GET(conf) << 25;
        DMA_DeInit(ro->dma_rx);
        DMA_Init(ro->dma_rx, &stm_dma);

        uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, ro->dma_rx_prio_preempt, ro->dma_rx_prio_sub);
        NVIC_SetPriority(ro->dma_rx_itp_no, prio);
        NVIC_ClearPendingIRQ(ro->dma_rx_itp_no);
        NVIC_EnableIRQ(ro->dma_rx_itp_no);

        DMA_ITConfig(ro->dma_rx, DMA_IT_TC, ENABLE);

        uint32_t dma_stream = DMA_RX_STREAM_GET(conf);
        q_set_using(dma_contr, dma_stream, _q_dma_ch_using);
        ro->flags |= BUS_DMA_RX_EN_FLAG;
        dma_res_links[DMA_IN_ORDER_NUM(dma_contr, dma_stream)] = ro;
    }
    if (ro->flags & BUS_DMA_TX_EN_FLAG) {
        stm_dma.DMA_DIR = DMA_DIR_MemoryToPeripheral;
        stm_dma.DMA_PeripheralBaseAddr = iface_tx_reg;
        stm_dma.DMA_Channel = DMA_TX_CHANNEL_GET(conf) << 25;
        DMA_DeInit(ro->dma_tx);
        DMA_Init(ro->dma_tx, &stm_dma);

        uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, ro->dma_tx_prio_preempt, ro->dma_tx_prio_sub);
        NVIC_SetPriority(ro->dma_tx_itp_no, prio);
        NVIC_ClearPendingIRQ(ro->dma_tx_itp_no);
        NVIC_EnableIRQ(ro->dma_tx_itp_no);

        DMA_ITConfig(ro->dma_tx, DMA_IT_TC, ENABLE);

        uint32_t dma_stream = DMA_TX_STREAM_GET(conf);
        q_set_using(dma_contr, dma_stream, _q_dma_ch_using);
        ro->flags |= BUS_DMA_TX_EN_FLAG;
        dma_res_links[DMA_IN_ORDER_NUM(dma_contr, dma_stream)] = ro;
    }
}


#endif //STM32F407_*********************************************************

void q_iface_gpio_init(io_res_t res)
{
    io_res_obj_t* ro = &io_handlers[res];
    uint32_t conf = ro->port_conf;
    uint32_t tmp_var;
    volatile uint32_t* rcc_en_reg;

    //enable RCC bus for GPIO port
    tmp_var = GPIO_CLOCK_EN_OFFSET_GET(conf);
//    rcc_en_reg = (uint32_t*)_rcc_reg[GPIO_RCC_BUS_ID];
//    *rcc_en_reg |= 1 << tmp_var;
    q_gpio_rcc_enable(tmp_var);


    //enable RCC bus for iface
    tmp_var = RCC_IFACE_IDX_GET(conf);
    rcc_en_reg = (uint32_t*)_rcc_reg[tmp_var];
    tmp_var = RCC_EN_BIT_OFFSET_GET(conf);
    *rcc_en_reg |= 1 << tmp_var;

    //pins configuration
    GPIO_InitTypeDef stm_gpio;
    gpio_conf_default(&stm_gpio);
    uint32_t port_id = GPIO_PORT_IDX_GET(conf);
    uint8_t pin_num;
    for (uint32_t pin = 0; pin < PIN_NUM_IFACE_MAX; pin++) {
        if (PIN_IS_EN(pin, ro->pins_conf)) {
            pin_num = PIN_NUM_GET(pin, ro->pins_conf);
            q_gpio_pin_init(&stm_gpio, _gpio_reg[port_id],
                            pin, ro->pins_conf);
            GPIO_PinAFConfig(_gpio_reg[port_id],
                             pin_num,
                             ALTER_FUNC_GET(ro->port_conf));
            q_set_using(port_id, pin_num, _q_pins_using);
        }
    }
    ro->flags |= BUS_GPIO_EN_FLAG;

}

void q_set_using(uint32_t part, uint8_t n, uint32_t flags[])
{
    if (flags[part] & (1 << n)) {
        ERR_RAISE(Q_PIN_CONF_CONFLICT);
    }
    flags[part] |= 1 << n;
}

void q_unset_using(uint32_t part, uint8_t n, uint32_t flags[])
{
    flags[part] &= ~(1 << n);
}


void q_init_io_hw(void)
{
    //debug************************************************
    _rcc = RCC;
    _gpio_b = GPIOB;

    //end debug************************************************

    io_res_obj_t* res;
    for (uint32_t res_no = 0; res_no < RES_MAX; res_no++) {
        res = &io_handlers[res_no];
        _qos_close_item_lns(&res->lnlist);
        q_iface_gpio_init(res_no);
        res->iface_init(res);
    }
//    q_pin_out_init(LORA_SPI_NSS, _GPIO_PULLDOWN, 0);
//    q_pin_out_init(LORA_RESET_PIN, _GPIO_PULLDOWN, 0);
    q_pin_itr_init(LORA_DIO0_PIN, 0, EXTI_TRIG_RISING);

//    q_pin_out_init(VL_XSHUT, _GPIO_PULLUP, 0);
//    q_pin_itr_init(VL_GPIO01_PIN, 0, EXTI_TRIG_FALLING);

    q_pin_out_init(ICM20689_NSS, _GPIO_PULLUP, 0);
    q_set_pin_lev(ICM20689_NSS);
    q_pin_itr_init(ICM20689_INT_PIN, 0, EXTI_TRIG_RISING);

    q_pin_out_init(MS5611_NSS, _GPIO_PULLUP, 0);
    q_set_pin_lev(MS5611_NSS);

    q_pin_out_init(LED1, _GPIO_NOPULL, 0);
    q_pin_out_init(LED2, _GPIO_NOPULL, 0);
    q_pin_out_init(LED3, _GPIO_NOPULL, 0);
    q_pin_out_init(LED4, _GPIO_NOPULL, 0);
}


int _q_open(io_res_t res)
{
//    static uint32_t res_descriptor_count = 0;
    uint32_t i = 0;
    for (; i < RES_DESCRIPTORS_MAX; i++) {
        if (res_desc_table[res_descriptor_count].status == 0) {
            res_desc_table[res_descriptor_count].status = 1;
            res_desc_table[res_descriptor_count].ifc = &io_handlers[res];
            break;
        }
        else {
            res_descriptor_count = (res_descriptor_count + 1) & (RES_DESCRIPTORS_MAX - 1);
        }
    }
    if (i == RES_DESCRIPTORS_MAX) {
        return Q_EXCEED_NUM_ITEMS;
    }

    //********debug******
//    if (res_descriptor_count > 125) {
//        res_desc_table[res_descriptor_count].h->nb = 1;
//    }
    //********debug******
    return res_descriptor_count;
    }


qos_err_code _q_close(int rd)
{
    qos_err_code status = Q_SUCCESS;
    if (res_desc_table[rd].status == 0) {
        status = Q_RD_NONE_OPEN;
    }
    if (rd > RES_DESCRIPTORS_MAX) {
        status = Q_EXCEED_NUM_ITEMS;
    }
    if (status == Q_SUCCESS) {
        res_desc_table[rd].status = 0;
    }
    return status;

}

void _q_read(q_descriptor_i_t* rd_i)
{
 /*    q_descriptor_i_t* rd_i = &res_desc_table[rd];

    io_res_obj_t* res_obj = rd_i->ifc;
    res_obj->rd = rd_i;
    res_obj->read_h(rd_i);
    ifc->read_h(ifc->rd);
*/
    rd_i->ifc->read_h(rd_i);

}

void _q_write(q_descriptor_i_t* rd_i)
{
/*    q_descriptor_i_t* rd_i = &res_desc_table[rd];
    io_res_obj_t* res_obj = rd_i->ifc;
    res_obj->rd = rd_i;
    ifc->write_h(ifc->rd);
    res_obj->write_h(rd_i);
    */
    rd_i->ifc->write_h(rd_i);
}

void _q_rawread(q_descriptor_i_t* rd_i)
{
    rd_i->ifc->read_h(rd_i);
}

void _q_rawwrite(q_descriptor_i_t* rd_i)
{
    rd_i->ifc->write_h(rd_i);
}

void _q_ioswap(q_descriptor_i_t *rd_i)
{
    rd_i->ifc->ioswap_h(rd_i);
}

void _q_ioctl(int rd, void* conf)
{
    q_descriptor_i_t* rd_i = &res_desc_table[rd];
    rd_i->conf = conf;
//    io_res_obj_t* res_obj = rd_i->h;
//    res_obj->ioctl_h(res_obj);
}

//***************debug*************

_PRIV_BSS call_ev_t ev_seq[20];
_PRIV_BSS uint32_t ev_count;

void clean_seq(void)
{
    for(uint32_t i=0; i < 20; i++) {
        ev_seq[i] = NONE;
    }
    ev_count = 0;
}

//***************debug*************
