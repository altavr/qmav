#include "qos_drivers/levelman.h"
#include "qos_core.h"



#ifdef STM32F303_
#if defined(_EXT_ITR_LINE_8)

void EXTI9_5_IRQHandler(void)
{
#ifdef _EXT_ITR_LINE_8
    if (EXTI_GetITStatus(EXTI_Line8) == SET) {
        EXTI_ClearITPendingBit(EXTI_Line8);
        //your code here
        _q_ext_itr_end_wait_handler(8);

    }
#endif
}


#endif
#endif //STM32F303_


#ifdef STM32F407_

#ifdef _EXT_ITR_LINE_0
void EXTI0_IRQHandler(void)
{
    if (EXTI->PR & EXTI_Line0) {
        EXTI->PR = EXTI_Line0;
    }
    _q_ext_itr_end_wait_handler(1);
}

#endif

#ifdef _EXT_ITR_LINE_1
void EXTI1_IRQHandler(void)
{
    if (EXTI->PR & EXTI_Line1) {
        EXTI->PR = EXTI_Line1;
    }
    _q_ext_itr_end_wait_handler(1);
}

#endif

void EXTI15_10_IRQHandler(void)
{

#ifdef _EXT_ITR_LINE_11
    if (EXTI->PR & EXTI_Line11) {
        EXTI->PR = EXTI_Line11;
    }
    _q_ext_itr_end_wait_handler(11);
#endif

#ifdef _EXT_ITR_LINE_12
    if (EXTI->PR & EXTI_Line12) {
        EXTI->PR = EXTI_Line12;
    }
    _q_ext_itr_end_wait_handler(12);
#endif

}
#endif //STM32F407_
