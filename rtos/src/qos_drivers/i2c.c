﻿#include "qos_drivers/i2c.h"
#ifdef STM32F303_
    #include "qos_drivers/mcu_f303_gpio.h"
#endif
#ifdef STM32F407_
    #include "qos_drivers/mcu_f407_gpio.h"
#endif
#include "qos_drivers/general.h"
#include "qos_core.h"


//********debug******


extern call_ev_t ev_seq[20];
extern uint32_t ev_count;



//void check_txit(void)
//{
//    if (I2C2->CR2 & I2C_IT_BUF) {
//        while (1);
//    }
//}

_PRIV_DATA uint32_t txe_call = 0;
_PRIV_DATA uint32_t tc_call = 0;
_PRIV_DATA uint32_t rxne_call = 0;
_PRIV_DATA uint32_t end_trans = 0;
_PRIV_BSS uint32_t start_evt;
_PRIV_BSS uint32_t addr_evt;
_PRIV_BSS uint8_t trans_count;

_PRIV_BSS uint32_t tc_read;
_PRIV_BSS uint32_t tc_write;
_PRIV_BSS I2C_TypeDef *_i2c;
//********debug******


#ifdef STM32F303_
void q_read_i2c_handler(q_descriptor_i_t* rd_i)
{
    io_res_obj_t* res_obj = rd_i->h;
    I2C_TypeDef* i2c = (I2C_TypeDef*)res_obj->base;
    q_conf_i2c_t* conf = (q_conf_i2c_t*)rd_i->conf;

    rd_i->count = 0;
    res_obj->flags |= (BUS_READ_MODE_FLAG | BUS_I2C_ADDR_FLAG);

    while(I2C_GetFlagStatus(i2c, I2C_ISR_BUSY) != RESET);
    I2C_TransferHandling(i2c, conf->dev << 1, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
    I2C_ITConfig(i2c, I2C_IT_TXI, ENABLE);
    I2C_ITConfig(i2c, I2C_IT_TCI, ENABLE);

}

void q_read_i2c_dma_handler(q_descriptor_i_t* rd_i)
{
    io_res_obj_t* res_obj = rd_i->h;
    I2C_TypeDef* i2c = (I2C_TypeDef*)res_obj->base;
    q_conf_i2c_t* conf = (q_conf_i2c_t*)rd_i->conf;

    res_obj->dma_rx_ch->CNDTR = rd_i->nb;
    res_obj->dma_rx_ch->CMAR = (uint32_t)rd_i->buf;
    res_obj->flags |= (BUS_READ_MODE_FLAG | BUS_I2C_ADDR_FLAG);

    DMA_Cmd(res_obj->dma_rx_ch, ENABLE);
    I2C_DMACmd(i2c, I2C_DMAReq_Rx, ENABLE);

    while(I2C_GetFlagStatus(i2c, I2C_ISR_BUSY) != RESET);
    I2C_TransferHandling(i2c, conf->dev << 1, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
    I2C_ITConfig(i2c, I2C_IT_TXI, ENABLE);
    I2C_ITConfig(i2c, I2C_IT_TCI, ENABLE);
}

void q_write_i2c_handler(q_descriptor_i_t* rd_i)
{
    io_res_obj_t* res_obj = rd_i->h;
    I2C_TypeDef* i2c = (I2C_TypeDef*)res_obj->base;
    q_conf_i2c_t* conf = (q_conf_i2c_t*)rd_i->conf;

    rd_i->count = 0;
    res_obj->flags = (res_obj->flags | BUS_I2C_ADDR_FLAG) & ~(BUS_READ_MODE_FLAG);

    while(I2C_GetFlagStatus(i2c, I2C_ISR_BUSY) != RESET);
    I2C_TransferHandling(i2c, conf->dev << 1, rd_i->nb + 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
    I2C_ITConfig(i2c, I2C_IT_TXI, ENABLE);
    I2C_ITConfig(i2c, I2C_IT_TCI, ENABLE);

}

void q_write_i2c_dma_handler(q_descriptor_i_t* rd_i)
{

}

void q_i2c_init(io_res_obj_t* res)
{
//    io_res_obj_t* res_obj = rd_i->h;
    I2C_TypeDef* i2c = (I2C_TypeDef*)res->base;
    I2C_InitTypeDef init_struct;

    I2C_StructInit(&init_struct);
    init_struct.I2C_Timing = 0x00310309;
    init_struct.I2C_Ack = I2C_Ack_Enable;
    I2C_Init(i2c, &init_struct);

    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, res->iface_prio_preempt, res->iface_prio_sub);
    NVIC_SetPriority(res->iface_itp_no, prio);
    NVIC_ClearPendingIRQ(res->iface_itp_no);
    NVIC_EnableIRQ(res->iface_itp_no);

    I2C_Cmd(i2c, ENABLE);
}

void q_i2c_dma_init(io_res_obj_t* res)
{
    I2C_TypeDef* i2c = (I2C_TypeDef*)res->base;
    I2C_InitTypeDef init_struct;

    if (res->flags & BUS_DMA_RX_EN_FLAG) {
        res->read_h = q_read_i2c_dma_handler;
    }
    if (res->flags & BUS_DMA_TX_EN_FLAG) {
        res->write_h = q_write_i2c_dma_handler;
    }
    q_iface_dma_init(res, (uint32_t)&i2c->TXDR, (uint32_t)&i2c->RXDR);

    I2C_StructInit(&init_struct);
    init_struct.I2C_Timing = 0x00310309;
    init_struct.I2C_Ack = I2C_Ack_Enable;
    I2C_Init(i2c, &init_struct);

    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, res->iface_prio_preempt, res->iface_prio_sub);
    NVIC_SetPriority(res->iface_itp_no, prio);
    NVIC_ClearPendingIRQ(res->iface_itp_no);
    NVIC_EnableIRQ(res->iface_itp_no);

    I2C_Cmd(i2c, ENABLE);

}

void q_ioctl_i2c_handler(q_descriptor_i_t* rd_i, void* conf)
{

}


void I2C2_EV_IRQHandler(void)
{
    io_res_obj_t* i2c_res = &io_handlers[Q_I2C2];
    uint16_t* flags = &i2c_res->flags;
    q_descriptor_i_t* rd_i = i2c_res->rd;

    if (I2C_GetITStatus(I2C2, I2C_IT_TXIS) == SET) {
        I2C_ClearITPendingBit(I2C2, I2C_IT_TXIS);
        //********debug******
        txis_call++;
        //********debug******
        if (*flags & BUS_READ_MODE_FLAG) {
//            if (*flags & BUS_DMA_RX_EN_FLAG) {
//                I2C_ITConfig(I2C2, I2C_IT_TC, DISABLE);
//            }
            I2C_ITConfig(I2C2, I2C_IT_TXI, DISABLE);
            //I2C_ITConfig(I2C2, I2C_IT_TC, ENABLE);
            I2C_SendData(I2C2, ((q_conf_i2c_t*)rd_i->conf)->reg);

        }
        else {
            if (*flags & BUS_I2C_ADDR_FLAG) {
                *flags &= ~BUS_I2C_ADDR_FLAG;
                I2C_SendData(I2C2, ((q_conf_i2c_t*)rd_i->conf)->reg);
            }
            else {
                I2C_SendData(I2C2, rd_i->buf[rd_i->count]);
                rd_i->count++;
            }
        }
        return;
    }
    //end of transmission

    //if receive register empty:
    if (I2C_GetITStatus(I2C2, I2C_IT_RXNE) == SET) {
        I2C_ClearITPendingBit(I2C2, I2C_IT_RXNE);

        //********debug******
        rxne_call++;
        //********debug******

        rd_i->buf[rd_i->count] = I2C_ReceiveData(I2C2);
        rd_i->count++;
//        if (rd_i->count == rd_i->nb) {
//            I2C_ITConfig(I2C2, I2C_IT_RXNE, DISABLE);
//            q_end_waiting_io_hw(i2c_res, i2c_res->rd->count);
//        }
    }
    if (I2C_GetITStatus(I2C2, I2C_IT_TC) == SET) {
        I2C_ClearITPendingBit(I2C2, I2C_IT_TC);
        //********debug******
        tc_call++;
        //********debug******
//        I2C_ITConfig(I2C2, I2C_IT_TC, DISABLE);
        if (*flags & BUS_READ_MODE_FLAG) {
            //********debug******
            tc_read++;
            //********debug******
            if (*flags & BUS_I2C_ADDR_FLAG) {
                *flags &= ~BUS_I2C_ADDR_FLAG;
                if ((*flags & BUS_DMA_RX_EN_FLAG)) {
                    I2C_TransferHandling(I2C2,
                                         ((q_conf_i2c_t*)rd_i->conf)->dev << 1,
                                         rd_i->nb,
                                         I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
                    I2C_ITConfig(I2C2, I2C_IT_TC, DISABLE);
                }
                else {
                    I2C_TransferHandling(I2C2,
                                         ((q_conf_i2c_t*)rd_i->conf)->dev << 1,
                                         rd_i->nb,
                                         I2C_SoftEnd_Mode, I2C_Generate_Start_Read);
                    I2C_ITConfig(I2C2, I2C_IT_RXNE, ENABLE);
                }
            }
            else {
                I2C_GenerateSTOP(I2C2, ENABLE);
                I2C_ITConfig(I2C2, I2C_IT_RXI, DISABLE);
                q_end_waiting_io_hw(i2c_res, i2c_res->rd->count);
            }
        }
        else {
            //********debug******
            tc_write++;
            //********debug******
            I2C_GenerateSTOP(I2C2, ENABLE);
            I2C_ITConfig(I2C2, I2C_IT_TXI, DISABLE);
            q_end_waiting_io_hw(i2c_res, i2c_res->rd->count);
        }
    }
}
#endif // STM32F303_


#ifdef STM32F407_

void q_read_i2c_handler(q_descriptor_i_t* rd_i)
{
    ERR_RAISE(Q_NOT_IMPLEMENTED);
    //debug****************************
    txe_call = 0;
    rxne_call = 0;
    end_trans = 0;
    start_evt = 0;
    addr_evt = 0;
    clean_seq();
    //debug****************************

    io_res_obj_t* res_obj = rd_i->ifc;
    I2C_TypeDef* i2c = (I2C_TypeDef*)res_obj->base;
    q_conf_i2c_t* conf = (q_conf_i2c_t*)rd_i->conf;

    rd_i->count = 0;
    res_obj->flags |= (BUS_READ_MODE_FLAG | BUS_I2C_ADDR_FLAG);

    I2C_ITConfig(i2c, I2C_IT_EVT, ENABLE);
    I2C_ITConfig(i2c, I2C_IT_BUF, DISABLE);

    while(I2C_GetFlagStatus(i2c, I2C_FLAG_BUSY) != RESET);
    I2C_GenerateSTART(i2c, ENABLE);

}

void q_read_i2c_dma_handler(q_descriptor_i_t* rd_i)
{

    io_res_obj_t* res_obj = rd_i->ifc;
    I2C_TypeDef* i2c = (I2C_TypeDef*)res_obj->base;
    q_conf_i2c_t* conf = (q_conf_i2c_t*)rd_i->conf;

    //debug****************************
    txe_call = 0;
    rxne_call = 0;
    end_trans = 0;
    start_evt = 0;
    addr_evt = 0;
    clean_seq();
    //debug****************************

    //Define number of byte for recevieve
    res_obj->dma_rx->NDTR = rd_i->nb;
    //Memory address
    res_obj->dma_rx->M0AR = (uint32_t)rd_i->buf;

    //Define number of byte for transmit register address
//    res_obj->dma_tx->NDTR = 1;
    //Memory address
//    res_obj->dma_tx->M0AR = (uint32_t)&conf->reg;

    res_obj->flags |= (BUS_I2C_ADDR_FLAG | BUS_READ_MODE_FLAG);

//    DMA_Cmd(res_obj->dma_rx, ENABLE);
    res_obj->dma_rx->CR |= DMA_SxCR_EN;
//    res_obj->dma_tx->CR |= DMA_SxCR_EN;
    i2c->CR2 = (i2c->CR2 | I2C_IT_EVT) & ~I2C_IT_BUF;
//    i2c->CR2 |= I2C_CR2_DMAEN;
    while(I2C_GetFlagStatus(i2c, I2C_FLAG_BUSY) != RESET);
    I2C_GenerateSTART(i2c, ENABLE);

}

void q_write_i2c_handler(q_descriptor_i_t* rd_i)
{
    ERR_RAISE(Q_NOT_IMPLEMENTED);
    io_res_obj_t* res_obj = rd_i->ifc;
    I2C_TypeDef* i2c = (I2C_TypeDef*)res_obj->base;
//    q_conf_i2c_t* conf = (q_conf_i2c_t*)rd_i->conf;

    //debug****************************
    txe_call = 0;
    rxne_call = 0;
    end_trans = 0;
    start_evt = 0;
    addr_evt = 0;
    clean_seq();
    //debug****************************


    rd_i->count = 0;
    res_obj->flags = (res_obj->flags | BUS_I2C_ADDR_FLAG) & ~(BUS_READ_MODE_FLAG);

    i2c->CR2 = (i2c->CR2 | I2C_IT_EVT) & ~I2C_IT_BUF;
    while(I2C_GetFlagStatus(i2c, I2C_FLAG_BUSY) != RESET);
    I2C_GenerateSTART(i2c, ENABLE);

}

void q_write_i2c_dma_handler(q_descriptor_i_t* rd_i)
{

    //debug****************************


    clean_seq();
    //debug****************************

    io_res_obj_t* res_obj = rd_i->ifc;
    I2C_TypeDef* i2c = (I2C_TypeDef*)res_obj->base;
    q_conf_i2c_t* conf = (q_conf_i2c_t*)rd_i->conf;

    //Define number of byte for transmition
    res_obj->dma_tx->NDTR = rd_i->nb;
    //Memory address
    res_obj->dma_tx->M0AR = (uint32_t)rd_i->buf;

    res_obj->flags = (res_obj->flags | BUS_I2C_ADDR_FLAG) & ~(BUS_READ_MODE_FLAG);

    DMA_Cmd(res_obj->dma_tx, ENABLE);
    i2c->CR2 = (i2c->CR2 | I2C_IT_EVT) & ~I2C_IT_BUF;

    i2c->CR2 |= I2C_CR2_DMAEN;

    while(I2C_GetFlagStatus(i2c, I2C_FLAG_BUSY) != RESET);
    I2C_GenerateSTART(i2c, ENABLE);

}

void q_ioswap_i2c_handler(q_descriptor_i_t *rd_i)
{
    ERR_RAISE(Q_NOT_IMPLEMENTED);
}

void q_ioswap_i2c_dma_handler(q_descriptor_i_t *rd_i)
{
    ERR_RAISE(Q_NOT_IMPLEMENTED);

}

void q_i2c_init(io_res_obj_t* res)
{
    //debug****************************
    _i2c = I2C1;
    //debug*************

//    io_res_obj_t* res_obj = rd_i->h;
    I2C_TypeDef* i2c = (I2C_TypeDef*)res->base;
    I2C_InitTypeDef init_struct;

    I2C_StructInit(&init_struct);
    init_struct.I2C_ClockSpeed = 400000;
    init_struct.I2C_Mode = I2C_Mode_I2C;
    init_struct.I2C_Ack = I2C_Ack_Enable;
    init_struct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
    I2C_Init(i2c, &init_struct);

    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, res->iface_prio_preempt, res->iface_prio_sub);
    NVIC_SetPriority(res->iface_itp_no, prio);
    NVIC_ClearPendingIRQ(res->iface_itp_no);
    NVIC_EnableIRQ(res->iface_itp_no);

    I2C_Cmd(i2c, ENABLE);
}

void q_i2c_dma_init(io_res_obj_t* res)
{

    //debug****************************
    _i2c = I2C1;
    //debug*************

    I2C_TypeDef* i2c = (I2C_TypeDef*)res->base;
    I2C_InitTypeDef init_struct;

    if (res->flags & BUS_DMA_RX_EN_FLAG) {
        res->read_h = q_read_i2c_dma_handler;
    }
    if (res->flags & BUS_DMA_TX_EN_FLAG) {
        res->write_h = q_write_i2c_dma_handler;
    }
    q_iface_dma_init(res, (uint32_t)&i2c->DR, (uint32_t)&i2c->DR);

    I2C_StructInit(&init_struct);
    init_struct.I2C_ClockSpeed = 400000;
    init_struct.I2C_Mode = I2C_Mode_I2C;
    init_struct.I2C_Ack = I2C_Ack_Enable;
    init_struct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
    I2C_Init(i2c, &init_struct);


    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, res->iface_prio_preempt, res->iface_prio_sub);
    NVIC_SetPriority(res->iface_itp_no, prio);
    NVIC_ClearPendingIRQ(res->iface_itp_no);
    NVIC_EnableIRQ(res->iface_itp_no);

    I2C_Cmd(i2c, ENABLE);
}

#ifdef Q_I2C1_EN

#ifdef I2C1_DMA_EN
void I2C1_EV_IRQHandler(void)
{
    io_res_obj_t* i2c_res = &io_handlers[Q_I2C1];
    uint16_t* flags = &i2c_res->flags;
    q_descriptor_i_t* rd_i = i2c_res->rd;

    //debug****************************
    ev_seq[ev_count++] = ITR;
    //debug****************************

    //read SR1 reset SB flag in SR1
    if (I2C1->SR1 & I2C_FLAG_SB) {

        //debug****************************
        start_evt++;
        ev_seq[ev_count++] = START;
        //debug****************************
        if (*flags & BUS_I2C_ADDR_FLAG) {
            I2C1->DR = ((q_conf_i2c_t*)rd_i->conf)->dev << 1;
        }
        else {
            I2C1->DR = (((q_conf_i2c_t*)rd_i->conf)->dev << 1) + 1;
        }
        return;
    }
    if (I2C1->SR1 & I2C_FLAG_ADDR) {
        //debug****************************
        ev_seq[ev_count++] = ADDR;
        //debug****************************

        if (*flags & BUS_I2C_ADDR_FLAG) {
            *flags &= ~BUS_I2C_ADDR_FLAG;
            //read SR2 after read SR1 - reset ADDR flag in SR1
            volatile uint16_t tmp = I2C1->SR2;

            if (*flags & BUS_READ_MODE_FLAG) {
                I2C1->CR2 |= I2C_IT_BUF;
            }
        }
        else {
            I2C1->CR2 |= I2C_CR2_DMAEN;
            if (rd_i->nb == 1) {
                I2C1->CR2 &= ~I2C_CR2_LAST;
                I2C1->CR1 &= ~I2C_CR1_ACK;
                volatile uint16_t tmp = I2C1->SR2;
            }
            else {
                I2C1->CR2 |= I2C_CR2_LAST;
                I2C1->CR1 |= I2C_CR1_ACK;
                volatile uint16_t tmp = I2C1->SR2;
            }
        }
        return;
    }
    if (I2C1->SR1 & I2C_FLAG_BTF) {
        //********debug******
        ev_seq[ev_count++] = BTF;
        //********debug******
        if ((*flags & BUS_READ_MODE_FLAG) == 0) {

            I2C1->CR1 |= I2C_CR1_STOP;
            q_end_waiting_io_hw(i2c_res, i2c_res->rd->count);
        }
//        else {
//            I2C1->CR1 |= I2C_CR1_START;
//        }
    }
    if (I2C1->SR1 & I2C_FLAG_TXE) {
        //********debug******
        ev_seq[ev_count++] = TXE;
        //********debug******
        volatile uint16_t tmp = I2C1->DR;
        I2C1->CR2 &= ~I2C_IT_BUF;

        I2C1->DR = rd_i->reg;
        I2C1->CR1 |= I2C_CR1_START;
    }
}
#else//I2C1_DMA_EN

#endif //I2C1_DMA_EN

#endif //Q_I2C1_EN



#ifdef Q_I2C2_EN
#ifdef I2C2_DMA_EN

void I2C2_EV_IRQHandler(void)
{
    io_res_obj_t* i2c_res = &io_handlers[Q_I2C2];
    uint16_t* flags = &i2c_res->flags;
    q_descriptor_i_t* rd_i = i2c_res->rd;

    //debug****************************
    ev_seq[ev_count++] = ITR;
    //debug****************************

    //read SR1 reset SB flag in SR1
    if (I2C2->SR1 & I2C_FLAG_SB) {

        //debug****************************
        start_evt++;
        ev_seq[ev_count++] = START;
        //debug****************************
        if (*flags & BUS_I2C_ADDR_FLAG) {
            I2C2->DR = ((q_conf_i2c_t*)rd_i->conf)->dev << 1;
        }
        else {
            I2C2->DR = (((q_conf_i2c_t*)rd_i->conf)->dev << 1) + 1;
        }
        return;
    }
    if (I2C2->SR1 & I2C_FLAG_ADDR) {
        //debug****************************
        ev_seq[ev_count++] = ADDR;
        //debug****************************

        if (*flags & BUS_I2C_ADDR_FLAG) {
            *flags &= ~BUS_I2C_ADDR_FLAG;
            //read SR2 after read SR1 - reset ADDR flag in SR1
            uint16_t tmp = I2C2->SR2;

            if (*flags & BUS_READ_MODE_FLAG) {
                I2C2->CR2 |= I2C_IT_BUF;
            }
        }
        else {
            I2C2->CR2 |= I2C_CR2_DMAEN;
            if (rd_i->nb == 1) {
                I2C2->CR2 &= ~I2C_CR2_LAST;
                I2C2->CR1 &= ~I2C_CR1_ACK;
                uint16_t tmp = I2C2->SR2;
            }
            else {
                I2C2->CR2 |= I2C_CR2_LAST;
                I2C2->CR1 |= I2C_CR1_ACK;
                uint16_t tmp = I2C2->SR2;
            }
        }
        return;
    }
    if (I2C2->SR1 & I2C_FLAG_BTF) {
        //********debug******
        ev_seq[ev_count++] = BTF;
        //********debug******
        if ((*flags & BUS_READ_MODE_FLAG) == 0) {

            I2C2->CR1 |= I2C_CR1_STOP;
            q_end_waiting_io_hw(i2c_res, i2c_res->rd->count);
        }
//        else {
//            I2C2->CR1 |= I2C_CR1_START;
//        }
    }
    if (I2C2->SR1 & I2C_FLAG_TXE) {
        //********debug******
        ev_seq[ev_count++] = TXE;
        //********debug******
        I2C2->CR2 &= ~I2C_IT_BUF;
        I2C2->DR = rd_i->reg;
        I2C2->CR1 |= I2C_CR1_START;
    }
}

#else


void I2C2_EV_IRQHandler(void)
{
    io_res_obj_t* i2c_res = &io_handlers[Q_I2C2];
    uint16_t* flags = &i2c_res->flags;
    q_descriptor_i_t* rd_i = i2c_res->rd;

    //debug****************************
    ev_seq[ev_count++] = ITR;
    //debug****************************

    if (I2C2->SR1 & I2C_FLAG_SB) {

        //debug****************************
        start_evt++;
        ev_seq[ev_count++] = START;
//        check_txit();
        //debug****************************
        if (*flags & BUS_I2C_ADDR_FLAG) {
//            I2C2->CR2 &= ~I2C_IT_BUF;
            I2C2->DR = ((q_conf_i2c_t*)rd_i->conf)->dev << 1;

        }
        else {
            I2C2->DR = (((q_conf_i2c_t*)rd_i->conf)->dev << 1) + 1;
        }
        //        I2C_ITConfig(I2C2, I2C_IT_EVT, DISABLE);
        //        I2C_ITConfig(I2C2, I2C_IT_BUF, ENABLE);
        return;
    }
    if (I2C2->SR1 & I2C_FLAG_ADDR) {
        //debug****************************
        addr_evt++;
        ev_seq[ev_count++] = ADDR;
//        check_txit();
        //debug****************************

        if (*flags & BUS_I2C_ADDR_FLAG) {
            *flags &= ~BUS_I2C_ADDR_FLAG;
            uint16_t tmp = I2C2->SR2;
            //            uint8_t reg = rd_i->reg;
            //            I2C2->DR = rd_i->reg;
            if (*flags & BUS_READ_MODE_FLAG) {
                //                I2C2->CR2 &= ~I2C_IT_BUF;
                I2C2->CR2 |= I2C_IT_BUF;


            }
            else {
                I2C_ITConfig(I2C2, I2C_IT_BUF, ENABLE);
//                I2C2->DR = rd_i->reg;

            }
        }
        else {
            I2C_ITConfig(I2C2, I2C_IT_BUF, ENABLE);
            if (rd_i->nb == 1) {
                I2C2->CR1 &= ~I2C_CR1_ACK;
                uint16_t tmp = I2C2->SR2;
                I2C2->CR1 |= I2C_CR1_STOP;
            }
            else {
                I2C2->CR1 |= I2C_CR1_ACK;
                uint16_t tmp = I2C2->SR2;
                //                rd_i->count++;
            }
        }
        return;
    }

    if (I2C2->SR1 & I2C_FLAG_TXE) {
        //********debug******
        txe_call++;
        ev_seq[ev_count++] = TXE;
        //********debug******
        if (*flags & BUS_READ_MODE_FLAG) {
            I2C2->CR2 &= ~I2C_IT_BUF;
//            I2C_ITConfig(I2C2, I2C_IT_EVT, DISABLE);
            I2C2->DR = rd_i->reg;


            I2C2->CR1 |= I2C_CR1_START;

//            I2C2->CR2 &= ~I2C_IT_BUF;
//            I2C2->CR1 |= I2C_CR1_START;
//            I2C_ITConfig(I2C2, I2C_IT_BUF, DISABLE);

        }
        else {
            if (rd_i->count == rd_i->nb) {
                I2C_ITConfig(I2C2, I2C_IT_BUF, DISABLE);
                I2C_ITConfig(I2C2, I2C_IT_EVT, DISABLE);
                I2C2->CR1 |= I2C_CR1_STOP;

                //debug****************************
                end_trans++;
                ev_seq[ev_count++] = END;
                //debug****************************
                q_end_waiting_io_hw(i2c_res, i2c_res->rd->count);

            }
            else {
                I2C2->DR = rd_i->buf[rd_i->count];
                rd_i->count++;
                //debug****************************
                trans_count = rd_i->count;
                //debug****************************
            }
        }
    }
    if (I2C2->SR1 & I2C_FLAG_RXNE) {
        //********debug******
        rxne_call++;
        ev_seq[ev_count++] = RXNE;
        //********debug******
        rd_i->buf[rd_i->count] = I2C2->DR;
        rd_i->count++;
        if (rd_i->count == (rd_i->nb - 1)) {
            I2C2->CR1 &= ~I2C_CR1_ACK;
            I2C2->CR1 |= I2C_CR1_STOP;
            return;
        }
        if (rd_i->count == rd_i->nb) {
            q_end_waiting_io_hw(i2c_res, i2c_res->rd->count);
            //********debug******
            ev_seq[ev_count++] = END;
            //********debug******
        }
    }

}

#endif //I2C2_DMA_EN
#endif //Q_I2C2_EN

#endif //STM32F407_
