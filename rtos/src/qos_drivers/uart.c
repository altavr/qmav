#include "qos_drivers/uart.h"
//#include "stm32f30x_usart.h"
//#include "stm32f30x_dma.h"
#include "qos_core.h"

#ifdef STM32F303_
#include "qos_drivers/mcu_f303_gpio.h"
#endif
#ifdef STM32F407_
    #include "qos_drivers/mcu_f407_gpio.h"
#endif

//debug************************************************
_PRIV_BSS USART_TypeDef *_uart3;


//end debug************************************************


//static io_res_obj_t* usart3_res = &io_handlers[Q_USART3];


#ifdef STM32F303_
void q_read_usart_dma_handler(q_descriptor_i_t* rd_i)
{
}

void q_read_usart_handler(q_descriptor_i_t* rd_i)
{
}



void q_write_usart_handler(q_descriptor_i_t* rd_i)
{
    io_res_obj_t* res = rd_i->h;
    USART_TypeDef * usart = (USART_TypeDef *)res->base;
    USART_SendData(usart, (uint16_t)rd_i->buf[0]);
    rd_i->count = 1;
    USART_ITConfig(usart, USART_IT_TXE, ENABLE );

}


void q_write_usart_dma_handler(q_descriptor_i_t* rd_i)
{
    io_res_obj_t* res = rd_i->h;
    USART_TypeDef * usart = (USART_TypeDef *)res->base;

    //Define number of byte for transmition
    res->dma_tx_ch->CNDTR = rd_i->nb;
    //Memory address
    res->dma_tx_ch->CMAR = (uint32_t)rd_i->buf;
    USART_ClearFlag(usart, USART_FLAG_TC);
//    USART_ITConfig(usart, USART_IT_TC, ENABLE );

    DMA_Cmd(res->dma_tx_ch, ENABLE);
    USART_DMACmd(usart, USART_DMAReq_Tx, ENABLE);


}


void q_usart_init(io_res_obj_t* res)
{

//    io_res_obj_t* res = &io_handlers[res_no];
    USART_InitTypeDef init_struct;
    USART_TypeDef * usart = (USART_TypeDef *)res->base;

    USART_StructInit(&init_struct);
    init_struct.USART_BaudRate = 230400;
    USART_Init(usart, &init_struct);

    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, res->iface_prio_preempt, res->iface_prio_sub);
    NVIC_SetPriority(res->iface_itp_no, prio);
    NVIC_ClearPendingIRQ(res->iface_itp_no);
    NVIC_EnableIRQ(res->iface_itp_no);

    USART_Cmd(usart, ENABLE);

}

void q_usart_dma_init(io_res_obj_t* res)
{
//    io_res_obj_t* res = &io_handlers[res_no];
    USART_InitTypeDef init_struct;
    USART_TypeDef * usart = (USART_TypeDef *)res->base;

    if (res->flags & BUS_DMA_RX_EN_FLAG) {
        res->read_h = q_read_usart_dma_handler;
    }
    if (res->flags & BUS_DMA_TX_EN_FLAG) {
        res->write_h = q_write_usart_dma_handler;
    }
    q_iface_dma_init(res, (uint32_t)&usart->TDR, (uint32_t)&usart->RDR);

    USART_StructInit(&init_struct);
    init_struct.USART_BaudRate = 230400;
    USART_Init(usart, &init_struct);

//    USART_ITConfig(usart, USART_IT_TC, ENABLE );
//    USART_ClearFlag(usart, USART_FLAG_TC);
//    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, res->iface_prio_preempt, res->iface_prio_sub);
//    NVIC_SetPriority(res->iface_itp_no, prio);
//    NVIC_ClearPendingIRQ(res->iface_itp_no);
//    NVIC_EnableIRQ(res->iface_itp_no);

    USART_Cmd(usart, ENABLE);

}

void q_ioctl_usart_handler(q_descriptor_i_t* rd_i)
{

}

void USART3_IRQHandler(void)
{
    io_res_obj_t* usart3_res = &io_handlers[Q_USART3];
    if (USART_GetITStatus(USART3, USART_IT_TXE) == SET) {
        USART_ClearITPendingBit(USART3, USART_IT_TXE);
        if (usart3_res->rd->count < usart3_res->rd->nb) {
            USART_SendData(USART3, (uint16_t)usart3_res->rd->buf[usart3_res->rd->count]);
            usart3_res->rd->count++;
        }
        else {
            USART_ITConfig(USART3, USART_IT_TXE, DISABLE );
            q_end_waiting_io_hw(usart3_res, usart3_res->rd->nb);

        }
    }
//    else if (USART_GetITStatus(USART3, USART_IT_TC) == SET) {
//        USART_ClearITPendingBit(USART3, USART_IT_TC);
//        USART_ITConfig(USART3, USART_IT_TC, DISABLE);
//        q_end_waiting_hw(Q_IO_HW, usart3_res->res, usart3_res->nb);
//    }
}
#endif //STM32F303_

void q_read_usart_handler(q_descriptor_i_t* rd_i)
{
    ERR_RAISE(Q_NOT_IMPLEMENTED);
}

void q_write_usart_handler(q_descriptor_i_t* rd_i)
{
    io_res_obj_t* res = rd_i->ifc;
    USART_TypeDef * usart = (USART_TypeDef *)res->base;
    USART_SendData(usart, (uint16_t)rd_i->buf[0]);
    rd_i->count = 1;
    USART_ITConfig(usart, USART_IT_TXE, ENABLE );

}


void q_usart_init(io_res_obj_t* res)
{

//    io_res_obj_t* res = &io_handlers[res_no];
    USART_InitTypeDef init_struct;
    USART_TypeDef * usart = (USART_TypeDef *)res->base;

    USART_StructInit(&init_struct);
    init_struct.USART_BaudRate = 230400;
    USART_Init(usart, &init_struct);

    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, res->iface_prio_preempt, res->iface_prio_sub);
    NVIC_SetPriority(res->iface_itp_no, prio);
    NVIC_ClearPendingIRQ(res->iface_itp_no);
    NVIC_EnableIRQ(res->iface_itp_no);

    USART_Cmd(usart, ENABLE);

}

void q_read_usart_dma_handler(q_descriptor_i_t* rd_i)
{
    ERR_RAISE(Q_NOT_IMPLEMENTED);
}

void q_write_usart_dma_handler(q_descriptor_i_t* rd_i)
{
    io_res_obj_t* res = rd_i->ifc;
    USART_TypeDef * usart = (USART_TypeDef *)res->base;

    //Define number of byte for transmition
    res->dma_tx->NDTR = rd_i->nb;
    //Memory address
    res->dma_tx->M0AR = (uint32_t)rd_i->buf;
    USART_ClearFlag(usart, USART_FLAG_TC);
//    USART_ITConfig(usart, USART_IT_TC, ENABLE );

    DMA_Cmd(res->dma_tx, ENABLE);
    USART_DMACmd(usart, USART_DMAReq_Tx, ENABLE);


}

void q_ioswap_usart_handler(q_descriptor_i_t *rd_i)
{
    ERR_RAISE(Q_NOT_IMPLEMENTED);
}

void q_ioswap_usart_dma_handler(q_descriptor_i_t *rd_i)
{
    ERR_RAISE(Q_NOT_IMPLEMENTED);

}



void q_usart_dma_init(io_res_obj_t* res)
{
    //debug************************************************
    _uart3 = (USART_TypeDef *)res->base;
    //end debug************************************************

//    io_res_obj_t* res = &io_handlers[res_no];
    USART_InitTypeDef init_struct;
    USART_TypeDef * usart = (USART_TypeDef *)res->base;

    if (res->flags & BUS_DMA_RX_EN_FLAG) {
        res->read_h = q_read_usart_dma_handler;
    }
    if (res->flags & BUS_DMA_TX_EN_FLAG) {
        res->write_h = q_write_usart_dma_handler;
    }
    q_iface_dma_init(res, (uint32_t)&usart->DR, (uint32_t)&usart->DR);

    USART_StructInit(&init_struct);
    init_struct.USART_BaudRate = 230400;
    USART_Init(usart, &init_struct);

//    USART_ITConfig(usart, USART_IT_TC, ENABLE );
//    USART_ClearFlag(usart, USART_FLAG_TC);
//    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, res->iface_prio_preempt, res->iface_prio_sub);
//    NVIC_SetPriority(res->iface_itp_no, prio);
//    NVIC_ClearPendingIRQ(res->iface_itp_no);
//    NVIC_EnableIRQ(res->iface_itp_no);

    USART_Cmd(usart, ENABLE);

}


#ifdef Q_USART1_EN
void USART1_IRQHandler(void)
{
    io_res_obj_t* usart1_res = &io_handlers[Q_USART1];
    if (USART_GetITStatus(USART1, USART_IT_TXE) == SET) {
        USART_ClearITPendingBit(USART1, USART_IT_TXE);
        if (usart1_res->rd->count < usart1_res->rd->nb) {
            USART_SendData(USART1, (uint16_t)usart1_res->rd->buf[usart1_res->rd->count]);
            usart1_res->rd->count++;
        }
        else {
            USART_ITConfig(USART1, USART_IT_TXE, DISABLE );
            q_end_waiting_io_hw(usart1_res, usart1_res->rd->nb);
        }
    }
}
#endif //Q_USART1_EN


#ifdef Q_USART3_EN
void USART3_IRQHandler(void)
{
    io_res_obj_t* usart3_res = &io_handlers[Q_USART3];
    if (USART_GetITStatus(USART3, USART_IT_TXE) == SET) {
        USART_ClearITPendingBit(USART3, USART_IT_TXE);
        if (usart3_res->rd->count < usart3_res->rd->nb) {
            USART_SendData(USART3, (uint16_t)usart3_res->rd->buf[usart3_res->rd->count]);
            usart3_res->rd->count++;
        }
        else {
            USART_ITConfig(USART3, USART_IT_TXE, DISABLE );
            q_end_waiting_io_hw(usart3_res, usart3_res->rd->nb);

        }
    }
//    else if (USART_GetITStatus(USART3, USART_IT_TC) == SET) {
//        USART_ClearITPendingBit(USART3, USART_IT_TC);
//        USART_ITConfig(USART3, USART_IT_TC, DISABLE);
//        q_end_waiting_hw(Q_IO_HW, usart3_res->res, usart3_res->nb);
//    }
}
#endif //Q_USART3_EN
