#include "qos_drivers/spi.h"
#include "qos_core.h"
#ifdef STM32F303_
    #include "qos_drivers/mcu_f303_gpio.h"
#endif
#ifdef STM32F407_
    #include "qos_drivers/mcu_f407_gpio.h"
#endif
#include "qos_drivers/general.h"

#define DUMMY_BYTE 0x0

_PRIV_BSS static uint16_t dev_null;
_PRIV_BSS static uint16_t dev_zero;

//#ifdef Q_SPI2_EN
_PRIV_BSS int s2t_count;
_PRIV_BSS int s1t_count;
_PRIV_BSS int s3t_count;
//#endif

//********debug******
_PRIV_BSS SPI_TypeDef *_spi1;
_PRIV_BSS SPI_TypeDef *_spi2;

//typedef enum {
//    NONE,
//    START,
//    TXE,
//    RXNE,
//    ADDR,
//    END,
//    ITR,
//    BTF
//} spi_ev_t;

_PRIV_BSS call_ev_t sev_seq[20];
_PRIV_BSS uint32_t sev_count;

void cln_spi_seq(void)
{
    for(uint32_t i=0; i < 20; i++) {
        sev_seq[i] = NONE;
    }
    sev_count = 0;
}

uint8_t sss;

//********debug******


#ifdef STM32F303_
void q_read_spi_handler(q_descriptor_i_t *rd_i)
{

}
void q_read_spi_dma_handler(q_descriptor_i_t* rd_i)
{

}

void q_write_spi_handler(q_descriptor_i_t* rd_i)
{

}

void q_spi_init(io_res_obj_t* res_obj)
{
    SPI_TypeDef* spi = (SPI_TypeDef*)res_obj->base;
    SPI_InitTypeDef init_struct;

    SPI_StructInit(&init_struct);
    init_struct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    init_struct.SPI_Mode =  SPI_Mode_Master;
    init_struct.SPI_DataSize = SPI_DataSize_8b;
    init_struct.SPI_CPOL = SPI_CPOL_Low;
    init_struct.SPI_CPHA = SPI_CPHA_1Edge;
    init_struct.SPI_NSS = SPI_NSS_Soft;
    init_struct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
    SPI_Init(spi, &init_struct);

    SPI_RxFIFOThresholdConfig(spi, SPI_RxFIFOThreshold_QF);
//    SPI_DataSizeConfig(init_struct, SPI_DataSize_8b);
    SPI_Cmd(spi, ENABLE);


}

void q_spi_dma_init(io_res_obj_t* res)
{

}

void SPI2_IRQHandler(void)
{

}
#endif //STM32F303_

#ifdef STM32F407_

void q_read_spi_handler(q_descriptor_i_t *rd_i)
{

    //********debug******
    cln_spi_seq();
    //********debug******

    io_res_obj_t* res_obj = rd_i->ifc;
    SPI_TypeDef* spi = (SPI_TypeDef*)res_obj->base;
    q_conf_spi_t *conf = (q_conf_spi_t*)rd_i->conf;

    res_obj->flags |= BUS_READ_MODE_FLAG;

    q_unset_pin_lev(conf->nss_port_id, conf->nss_pin);
    rd_i->count = 0;
    switch ((uint32_t)spi) {
    case (uint32_t)SPI1:
        s1t_count = -1;
        break;
    case (uint32_t)SPI2:
        s2t_count = -1;
        break;
    case (uint32_t)SPI3:
        s3t_count = -1;
    }

    spi->CR2 |= (SPI_CR2_TXEIE | SPI_CR2_RXNEIE);
    while(spi->SR & SPI_SR_BSY);
    spi->DR = rd_i->reg;



}
void q_read_spi_dma_handler(q_descriptor_i_t* rd_i)
{
    io_res_obj_t* res_obj = rd_i->ifc;
    SPI_TypeDef* spi = (SPI_TypeDef*)res_obj->base;
    q_conf_spi_t *conf = (q_conf_spi_t*)rd_i->conf;

//    res_obj->flags |= BUS_READ_MODE_FLAG;
    q_unset_pin_lev(conf->nss_port_id, conf->nss_pin);

    res_obj->dma_tx->NDTR = rd_i->nb;
    res_obj->dma_tx->M0AR = (uint32_t)&dev_zero;   //(uint32_t)rd_i->buf;
    res_obj->dma_rx->NDTR = rd_i->nb;
    res_obj->dma_rx->M0AR = (uint32_t)rd_i->buf;
    res_obj->dma_rx->CR |= DMA_SxCR_MINC;
    res_obj->dma_tx->CR &= ~DMA_SxCR_MINC;
    res_obj->dma_tx->CR |= DMA_SxCR_EN;
    res_obj->dma_rx->CR |= DMA_SxCR_EN;


    if ((rd_i->mode | BUS_RAW_MODE_FLAG) == 0) {
        spi->CR2 |= (SPI_CR2_TXEIE | SPI_CR2_RXNEIE);
        spi->DR = rd_i->reg;
    }
    else {
        spi->CR2 |= (SPI_CR2_TXDMAEN | SPI_CR2_RXDMAEN);
    }

//    (SPI_TypeDef*)(res_obj->base)->CR2 &= ~SPI_CR2_TXDMAEN

}

void q_write_spi_handler(q_descriptor_i_t* rd_i)
{
    //********debug******
    cln_spi_seq();
    //********debug******

    io_res_obj_t* res_obj = rd_i->ifc;
    SPI_TypeDef* spi = (SPI_TypeDef*)res_obj->base;
    q_conf_spi_t *conf = (q_conf_spi_t*)rd_i->conf;

    res_obj->flags &= ~BUS_READ_MODE_FLAG;

    q_unset_pin_lev(conf->nss_port_id, conf->nss_pin);
    rd_i->count = 0;
    switch ((uint32_t)spi) {
    case (uint32_t)SPI1:
        s1t_count = -1;
        break;
    case (uint32_t)SPI2:
        s2t_count = -1;
        break;
    case (uint32_t)SPI3:
        s3t_count = -1;
    }
    spi->CR2 |= (SPI_CR2_TXEIE | SPI_CR2_RXNEIE);
    while(spi->SR & SPI_SR_BSY);
    spi->DR = rd_i->reg;

}

void q_write_spi_dma_handler(q_descriptor_i_t* rd_i)
{
    io_res_obj_t* res_obj = rd_i->ifc;
    SPI_TypeDef* spi = (SPI_TypeDef*)res_obj->base;
    q_conf_spi_t *conf = (q_conf_spi_t*)rd_i->conf;


//    res_obj->flags |= BUS_READ_MODE_FLAG;
    q_unset_pin_lev(conf->nss_port_id, conf->nss_pin);

    res_obj->dma_tx->NDTR = rd_i->nb;
    res_obj->dma_tx->M0AR = (uint32_t)rd_i->buf;
    res_obj->dma_rx->NDTR = rd_i->nb;
    res_obj->dma_rx->M0AR = (uint32_t)(&dev_null);
    res_obj->dma_rx->CR &= ~DMA_SxCR_MINC;
    res_obj->dma_tx->CR |= DMA_SxCR_MINC;
    res_obj->dma_tx->CR |= DMA_SxCR_EN;
    res_obj->dma_rx->CR |= DMA_SxCR_EN;


    if ((rd_i->mode | BUS_RAW_MODE_FLAG) == 0) {
        spi->CR2 |= (SPI_CR2_TXEIE | SPI_CR2_RXNEIE);
        spi->DR = rd_i->reg;
    }
    else {
        spi->CR2 |= (SPI_CR2_TXDMAEN | SPI_CR2_RXDMAEN);
    }

}

void q_ioswap_spi_handler(q_descriptor_i_t *rd_i)
{
    ERR_RAISE(Q_NOT_IMPLEMENTED);
}

void q_ioswap_spi_dma_handler(q_descriptor_i_t *rd_i)
{
    io_res_obj_t* res_obj = rd_i->ifc;
    SPI_TypeDef* spi = (SPI_TypeDef*)res_obj->base;
    q_conf_spi_t *conf = (q_conf_spi_t*)rd_i->conf;

    q_unset_pin_lev(conf->nss_port_id, conf->nss_pin);

    res_obj->dma_tx->NDTR = rd_i->nb;
    res_obj->dma_tx->M0AR = (uint32_t)rd_i->buf;
    res_obj->dma_rx->NDTR = rd_i->nb;
    res_obj->dma_rx->M0AR = (uint32_t)rd_i->aux_buf;
    res_obj->dma_rx->CR |= DMA_SxCR_MINC;
    res_obj->dma_tx->CR |= DMA_SxCR_MINC;
    res_obj->dma_tx->CR |= DMA_SxCR_EN;
    res_obj->dma_rx->CR |= DMA_SxCR_EN;

    spi->CR2 |= (SPI_CR2_TXDMAEN | SPI_CR2_RXDMAEN);
}


void q_spi_init(io_res_obj_t* res_obj)
{
    //********debug******
    _spi1 = SPI1;
    _spi2 = SPI2;
    //********debug******

    SPI_TypeDef* spi = (SPI_TypeDef*)res_obj->base;
    SPI_InitTypeDef init_struct;

    SPI_StructInit(&init_struct);
    init_struct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    init_struct.SPI_Mode =  SPI_Mode_Master;
    init_struct.SPI_DataSize = SPI_DataSize_8b;
    init_struct.SPI_CPOL = SPI_CPOL_High;
    init_struct.SPI_CPHA = SPI_CPHA_2Edge;
    init_struct.SPI_NSS = SPI_NSS_Soft;
    init_struct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;
    SPI_Init(spi, &init_struct);

    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, res_obj->iface_prio_preempt, res_obj->iface_prio_sub);
    NVIC_SetPriority(res_obj->iface_itp_no, prio);
    NVIC_ClearPendingIRQ(res_obj->iface_itp_no);
    NVIC_EnableIRQ(res_obj->iface_itp_no);

    spi->CR1 |= SPI_CR1_SPE;

}

void q_spi_dma_init(io_res_obj_t* res_obj)
{
    //********debug******
    _spi1 = SPI1;
    _spi2 = SPI2;
    //********debug******

    SPI_TypeDef* spi = (SPI_TypeDef*)res_obj->base;
    SPI_InitTypeDef init_struct;

    if (res_obj->flags & BUS_DMA_RX_EN_FLAG) {
        res_obj->read_h = q_read_spi_dma_handler;
    }
    if (res_obj->flags & BUS_DMA_TX_EN_FLAG) {
        res_obj->write_h = q_write_spi_dma_handler;
    }
    if (res_obj->flags & (BUS_DMA_RX_EN_FLAG | BUS_DMA_TX_EN_FLAG)) {
        res_obj->ioswap_h = q_ioswap_spi_dma_handler;
    }

    q_iface_dma_init(res_obj, (uint32_t)&spi->DR, (uint32_t)&spi->DR);

    SPI_StructInit(&init_struct);
    init_struct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    init_struct.SPI_Mode =  SPI_Mode_Master;
    init_struct.SPI_DataSize = SPI_DataSize_8b;
    init_struct.SPI_CPOL = SPI_CPOL_Low;
    init_struct.SPI_CPHA = SPI_CPHA_1Edge;
    init_struct.SPI_NSS = SPI_NSS_Soft;
    init_struct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
    SPI_Init(spi, &init_struct);

    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, res_obj->iface_prio_preempt, res_obj->iface_prio_sub);
    NVIC_SetPriority(res_obj->iface_itp_no, prio);
    NVIC_ClearPendingIRQ(res_obj->iface_itp_no);
    NVIC_EnableIRQ(res_obj->iface_itp_no);

    spi->CR1 |= SPI_CR1_SPE;
}


#ifdef Q_SPI1_EN
#ifdef SPI1_DMA_EN
void SPI1_IRQHandler(void)
{
//    io_res_obj_t* spi_res = &io_handlers[Q_SPI1];
//    q_descriptor_i_t *rd = spi_res->rd;
//    q_conf_spi_t *conf = (q_conf_spi_t*)rd->conf;
    //********debug******
//    sev_seq[sev_count++] = ITR;

    //********debug******


    if (SPI1->SR & SPI_SR_RXNE) {

        //********debug******
//        sev_seq[sev_count++] = RXNE;

        //********debug******

        uint8_t tmp = SPI1->DR;
        SPI1->CR2 &= ~SPI_CR2_RXNEIE;
        SPI1->CR2 |= SPI_CR2_RXDMAEN;
        return;
}

    if (SPI1->SR & SPI_SR_TXE) {

        //********debug******
//        sev_seq[sev_count++] = TXE;
        //********debug******

        SPI1->CR2 &= ~SPI_CR2_TXEIE;
        SPI1->CR2 |= SPI_CR2_TXDMAEN;
    }

}

#else


void SPI1_IRQHandler(void)
{
    io_res_obj_t* spi_res = &io_handlers[Q_SPI1];
    q_descriptor_i_t *rd = spi_res->rd;
    q_conf_spi_t *conf = (q_conf_spi_t*)rd->conf;

    //********debug******
    sev_seq[sev_count++] = ITR;

    //********debug******


    if (SPI1->SR & SPI_SR_RXNE) {

        //********debug******
        sev_seq[sev_count++] = RXNE;

        //********debug******

        if (s1t_count < 0) {
            uint8_t tmp = SPI1->DR;
            s1t_count++;
        }
        else {
            if (spi_res->flags & BUS_READ_MODE_FLAG) {
                rd->buf[s1t_count] = SPI1->DR;
            }
            else {
                uint8_t tmp = SPI1->DR;
            }
            s1t_count++;
            if (s1t_count == rd->nb) {
                q_set_pin_lev(conf->nss_port_id, conf->nss_pin);
                q_end_waiting_io_hw(spi_res, rd->count);
            }
        }
        return;
    }

    if (SPI1->SR & SPI_SR_TXE) {

        //********debug******
        sev_seq[sev_count++] = TXE;
        //********debug******


        SPI1->DR = rd->buf[rd->count];
        rd->count++;
        if (rd->count == rd->nb) {
            SPI1->CR2 &= ~SPI_CR2_TXEIE;
        }
    }
}

#endif //SPI1_DMA_EN
#endif // Q_SPI1_EN


#ifdef Q_SPI2_EN
#ifdef SPI2_DMA_EN
void SPI2_IRQHandler(void)
{
//    io_res_obj_t* spi_res = &io_handlers[Q_SPI2];
//    q_descriptor_i_t *rd = spi_res->rd;
//    q_conf_spi_t *conf = (q_conf_spi_t*)rd->conf;
    //********debug******
//    sev_seq[sev_count++] = ITR;

    //********debug******


    if (SPI2->SR & SPI_SR_RXNE) {

        //********debug******
//        sev_seq[sev_count++] = RXNE;

        //********debug******

        uint8_t tmp = SPI2->DR;
        SPI2->CR2 &= ~SPI_CR2_RXNEIE;
        SPI2->CR2 |= SPI_CR2_RXDMAEN;
        return;
}

    if (SPI2->SR & SPI_SR_TXE) {

        //********debug******
//        sev_seq[sev_count++] = TXE;
        //********debug******

        SPI2->CR2 &= ~SPI_CR2_TXEIE;
        SPI2->CR2 |= SPI_CR2_TXDMAEN;
    }

}

#else


void SPI2_IRQHandler(void)
{
    io_res_obj_t* spi_res = &io_handlers[Q_SPI2];
    q_descriptor_i_t *rd = spi_res->rd;
    q_conf_spi_t *conf = (q_conf_spi_t*)rd->conf;

    //********debug******
    sev_seq[sev_count++] = ITR;

    //********debug******


    if (SPI2->SR & SPI_SR_RXNE) {

        //********debug******
        sev_seq[sev_count++] = RXNE;

        //********debug******

        if (s2t_count < 0) {
            uint8_t tmp = SPI2->DR;
            s2t_count++;
        }
        else {
            if (spi_res->flags & BUS_READ_MODE_FLAG) {
                rd->buf[s2t_count] = SPI2->DR;
            }
            else {
                uint8_t tmp = SPI2->DR;
            }
            s2t_count++;
            if (s2t_count == rd->nb) {
                q_set_pin_lev(conf->nss_port_id, conf->nss_pin);
                q_end_waiting_io_hw(spi_res, rd->count);
            }
        }
        return;
    }


    if (SPI2->SR & SPI_SR_TXE) {

        //********debug******
        sev_seq[sev_count++] = TXE;
        //********debug******


//        SPI2->DR = rd->buf[rd->count];
        if (spi_res->flags & BUS_READ_MODE_FLAG) {
            SPI2->DR = DUMMY_BYTE;
        }
        else {
            SPI2->DR = rd->buf[rd->count];
        }
        rd->count++;
        if (rd->count == rd->nb) {
            SPI2->CR2 &= ~SPI_CR2_TXEIE;
        }
    }
}

#endif //SPI2_DMA_EN
#endif // Q_SPI2_EN

#endif //STM32F407_
