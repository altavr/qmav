
#include "qos_threads.h"
#include "qos_core.h"
#include "qos_debug.h"
#include "qos_memory.h"



#ifdef STM32F303_
    #include "stm32f30x_dbgmcu.h"
#endif

#ifdef DESKTOP_DEBUG_F
#include "stdio.h"
#include "qos_test.h"
#endif

_PRIV_BSS q_th_t qos_threads_pool[THREADS_MAX];
_PRIV_DATA uint32_t qos_threads_num = 0;


_PRIV_DATA uint32_t runnable_prior_threads = 0;
_PRIV_BSS q_lnim_t qos_prior_thread_list[PRIOR_MAX + 1];
_PRIV_BSS q_lnim_t qos_timer_thread_list;
_PRIV_BSS q_lnim_t qos_ext_itr_wait_list;


_PRIV_BSS q_th_t* qos_idle_thread;

//extern q_descriptor_i_t res_desc_table[];

q_th_t* qos_first_thread_waiting_ext_itr(uint32_t ext_itr, q_lnim_t* queue)
{
    q_th_t* th;
    q_lnim_t* li;
    for (li = queue->next; li != queue; li = li->next) {
        th = container_of(li, q_th_t, list_item);
        if (th->temp == ext_itr) {
            return th;
        }
    }
    return 0;
}


//q_th_t* qos_first_thread_waiting_mutex(uint32_t mutex_p, q_lnim_t* queue)
//{
//    q_th_t* th;
//    q_lnim_t* li;
//    for (li = queue->next; li != queue; li = li->next) {
//        th = container_of(li, q_th_t, list_item);
//        if (th->wait_mutex_p == mutex_p) {
//            return th;
////            _qos_rem_item_from_ln_list(li);
////            thread->wait_mutex_p = 0;
////            qos_thread_set_runnable(thread);
//        }
//    }
//    return 0;
//}

void qos_thread_set_wait_prio_order(q_th_t* thread, q_lnim_t* queue, wait_reason_t reason)
{
//    if (thread->stat != DORMANT) {
//        _qos_rem_item_from_ln_list(&thread->list_item);
//    }
    thread->stat = WAIT;
    thread->reason = reason;
    q_lnim_t* n_th = queue;
    for (q_lnim_t* p = queue->next->next; p != queue; p = p->next) {
        q_th_t* t  = container_of(p, q_th_t, list_item);
        if (t->prior > thread->prior) {
            n_th = p;
            break;
        }
    }
    _qos_ins_to_ln_before_item(&thread->list_item, n_th);
    return;

}

void qos_thread_set_time_wait_state(q_th_t* thread, q_lnim_t* queue, uint32_t timeout)
{
//    if (thread->stat != DORMANT) {
//        _qos_rem_item_from_ln_list(&thread->list_item);
//    }
    thread->stat = WAIT;
    thread->reason = Q_TIMEOUT_WAIT;
    thread->timeout = timeout;
//    _qos_rem_item_from_ln_list(&thread->list_item);
    for (q_lnim_t* p = queue->next; p != queue; p = p->next) {
        q_th_t* t  = container_of(p, q_th_t, list_item);
        if (t->timeout > thread->timeout) {
            _qos_ins_to_ln_before_item(&thread->list_item, p);
            return;
        }
    }
    _qos_ins_to_ln_before_item(&thread->list_item, queue);
    return;
}

void qos_thread_set_runnable(q_th_t* thread)
{

//    if (thread->stat != DORMANT) {
//        _qos_rem_item_from_ln_list(&thread->list_item);
//    }
    thread->stat = RUNNABLE;
    _qos_ins_to_ln_before_item(&thread->list_item, &qos_prior_thread_list[thread->prior]);
}


void qos_thread_set_suspend(q_th_t* thread)
{
//    if (thread->stat != DORMANT) {
//        _qos_rem_item_from_ln_list(&thread->list_item);
//    }
    thread->stat = SUSPEND;
    _qos_ins_to_ln_before_item(&thread->list_item, &qos_prior_thread_list[thread->prior]);
}

void qos_thread_set_dormant(q_th_t* thread)
{
    thread->stat = DORMANT;
    _qos_rem_item_from_ln_list(&thread->list_item);
}

void qos_thread_set_prio(q_th_t* thread, uint8_t prior)
{
    q_lnim_t *list;
    uint8_t old_prior = thread->prior;

    thread->prior = prior;
    if ((thread->stat == SUSPEND) || (thread->stat == RUNNABLE)) {
        _qos_rem_item_from_ln_list(&thread->list_item);
        list = &qos_prior_thread_list[prior];
        _qos_ins_to_ln_before_item(&thread->list_item, list);
        if (_qos_ln_empty(&qos_prior_thread_list[old_prior]) == 0) {
            CLEAN_PRIOR_FLAG(old_prior);
        }
        SET_PRIOR_FLAG(prior);
    }
    if (thread->stat == WAIT) {
        if (thread->reason != Q_TIMEOUT_WAIT) {
            switch (thread->reason) {
            case Q_HW_WAIT:
                list = &res_desc_table[thread->rd_active].ifc->lnlist;
                break;
            case Q_MX_WAIT:
                list = &thread->mutex->lnlist;
                break;
            case Q_EXT_ITR_WAIT:
                list = &qos_ext_itr_wait_list;
                break;
            default:
                ERR_RAISE(Q_UNDEF_ERR);
            }
            _qos_rem_item_from_ln_list(list);
            qos_thread_set_wait_prio_order(thread, list, thread->reason);
        }
    }

    //debug
    //debug

}

void  qos_empty_list_init(q_lnim_t* list_item, uint8_t num)
{
    for (uint8_t i=0; i < num; i++) {
        _qos_close_item_lns(&list_item[i]);
    }
}

void qos_init_all_lists(void)
{

    qos_empty_list_init(qos_prior_thread_list, PRIOR_MAX);
//    qos_empty_list_init(qos_wait_res_thread_list, RES_MAX);
    _qos_close_item_lns(&qos_timer_thread_list);
//    _qos_close_item_lns(&qos_mutex_wait_list);
    _qos_close_item_lns(&qos_ext_itr_wait_list);
}

q_th_t* qos_get_new_thread(void)
{
    ERR_DEF;
    q_th_t* thread;

    if (qos_threads_num >= THREADS_MAX) {
        ERR_RAISE(Q_EXCEED_NUM_ITEMS);
    }
    thread = &qos_threads_pool[qos_threads_num];
    return thread;
}

size_t  _qos_round_stack_size_mpu(size_t size)
{
    size_t rsize = _Q_MMAP_CHUNKLEN;;
    if (size > _Q_MMAP_CHUNKLEN)  {
        size_t subregsize = ceil_round_to_2_degree(size);
        rsize = ceil_round_to_x(size, subregsize);
    }
    return rsize;
}


int qos_thread_init(q_th_t* thread, uint32_t stack_size, uint8_t prior, void* func)
{


    stack_size = ROUND_STACK_SIZE(stack_size);
//    thread->base = qos_mem_alloc(stack_size);
    thread->base = (u32)qalloc_uniqmem(stack_size, thread->stack_cb);
    if (thread->base == 0)  {
        qos_error_handler(_q_error);
    }
    _qos_close_item_lns(&thread->memblocks);
    thread->stack_size = stack_size;
    thread->id = qos_threads_num;
    thread->prior = prior;
    thread->original_prio = prior;
    thread->thread_code = (memaddr_t)func;
    thread->stat = DORMANT;
    qos_threads_num++;

    _qos_close_item_lns(&thread->memblocks);
    #ifdef MPU_ENABLE
    q_mpu_tread_init(&thread->mpu_conf);
    #endif //MPU_ENABLE
    //clean context
    qos_clean_thread_context(thread);

    return qos_threads_num;
}


void idle_func(void)
{

    while (1);
}


void qos_init_idle_thread(void)
{
    q_th_t* thread = &qos_threads_pool[0];
    qos_idle_thread = thread;
    qos_thread_init(thread, 4 * 36, IDLE_PRIOR, idle_func);

}
