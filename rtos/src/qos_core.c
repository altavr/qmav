#include <inttypes.h>
#include "qos_core.h"
#include "qos_threads.h"
#include "qos_ln_list.h"
#include "qos_core.h"
#include "qos_memory.h"
#include "qos_sys_functions.h"
#include "qos_drivers/general.h"
#include "qos_debug.h"
#include "misc/qos_string.h"

#ifndef DESKTOP_TEST
#include "qos_timers.h"

#ifdef STM32F303_
    #include "system_stm32f30x.h"
    #include "stm32f30x.h"
#endif //STM32F303_
#ifdef STM32F407_
    #include "system_stm32f4xx.h"
    #include "stm32f4xx.h"
#endif //STM32F407_
#define TICK_PERIOD_US 1000000UL

#endif //DESKTOP_TEST

_PRIV_BSS uint32_t sysTickNum;
_PRIV_BSS static uint32_t st_count_us;

#ifdef TIME_M_ENABLE
_PRIV_DATA q_runtime_t systime;
#endif //TIME_M_ENABLE

_PRIV_BSS uint syscall_in_process;
_PRIV_BSS q_th_t *next_run_thread;
_PRIV_BSS q_th_t *p_run_thread;
_PRIV_DATA uint32_t thread_qu_count = 0;
_PRIV_DATA uint_fast8_t need_save_context = 1;
#ifdef FPU_ENABLE
_PRIV_DATA q_th_t* thread_last_used_fpu = &qos_threads_pool[0]; // this is idle thread on starup
#endif //FPU_ENABLE
//uint32_t _active_mutex = 0;

#ifdef Q_DEBUG_IO_SW_TH
_PRIV_BSS int32_t call_io_funcs[THREADS_MAX];
_PRIV_BSS int32_t sw_io_threads[THREADS_MAX];

#endif //Q_DEBUG_IO_SW_TH

#ifdef Q_CORTEX_FAULT_STATUS
__attribute__((naked))  void HardFault_Handler(void);
_PRIV_DATA hw_stackframe_t* fault_stack;
_PRIV_DATA usage_fault_t usage_f_reg;
_PRIV_DATA bus_fault_t bus_f_reg;
_PRIV_DATA mmem_fault_t mmem_f_reg;
_PRIV_DATA uint32_t mem_f_addr;
_PRIV_DATA uint32_t bus_f_addr;

#endif //Q_CORTEX_FAULT_STATUS



//for debug purpose
_PRIV_BSS hstackframe_t* next_tread_stack;
_PRIV_BSS hstackframe_t* prev_tread_stack;
_PRIV_BSS hstackframe_fpu_t* next_tread_fpu_stack;
_PRIV_BSS hstackframe_fpu_t* prev_tread_fpu_stack;
_PRIV_BSS uint32_t hw_rel_thread1;
_PRIV_BSS uint32_t hw_rel_thread2;

_PRIV_DATA SCB_Type* _scb = SCB;
_PRIV_DATA NVIC_Type* _nvic = NVIC;
_PRIV_DATA CoreDebug_Type *_cdeb = (CoreDebug_Type *)CoreDebug_BASE;
_PRIV_DATA MPU_Type *_mpu = MPU;
_PRIV_DATA FPU_Type *_fpu = FPU;

_PRIV_BSS uint32_t _bus_fault_stat_reg;
_PRIV_BSS uint32_t _mem_fault_stat_reg;
_PRIV_BSS uint32_t _usage_fault_stat_reg;

_PRIV_BSS uint32_t* bus_fault_adress;
_PRIV_BSS uint32_t* mem_fault_adress;

_PRIV_BSS uint8_t stack_access_violate;

//end debug purpose


//qos_err_code  qos_nvic_init(void);
//__attribute__((naked))  void PendSV_Handler(void);

#ifndef DESKTOP_TEST

#ifdef TIME_M_ENABLE
void q_tickstamp_get(q_tickstamp_t* ts)
{
    ts->tick = sysTickNum;
    ts->counter = SysTick->VAL;
}

void q_tickstamp_add(q_tickstamp_t* base, q_tickstamp_t* add)
{
    base->tick += add->tick;
    base->counter += add->counter;
}

_FORCE_INLINE void q_usaddtotime(u32 us, q_time_t* time)
{
    u32 sec;
    if ((sec = us / 1000000)) {
        time->sec += sec;
        time->us = us % 1000000;
    }
    else {
        time->us = us;
    }
}

void q_tickstampaddtotime(q_tickstamp_t* ts, q_time_t* time)
{
    u32 us = ts->tick * SYSTICK_PERIOD + ts->counter / st_count_us;
    q_usaddtotime(time->us + us, time);
}

void q_time_add(q_time_t* base, q_time_t* add)
{
    base->sec += add->sec;
    q_usaddtotime(base->us + add->us, base);
}


#endif //TIME_M_ENABLE

qos_err_code qos_systick_init(void)
{
    qos_err_code status = Q_SUCCESS;
    st_count_us = SystemCoreClock / TICK_PERIOD_US;
    status = SysTick_Config((st_count_us) * SYSTICK_PERIOD);
    return status;
}

//qos_err_code qos_pendSV_init(void)
//{

//}

qos_err_code  qos_nvic_init(void)
{
//    uint32_t prio_group = 4;
    /* STM32F303 have 4 bit for define priorit
    value 4 - 3 bits prio group, 1 bits prio subgroup */
    NVIC_SetPriorityGrouping(NVIC_PRIOR_GROUP);

    //PenSV interrupt, prio group - 7, prio subgroup - 0
    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, 7, 0);
    NVIC_SetPriority(PendSV_IRQn, prio);
    return SUCCESS;

}

void qos_cortex_excs_init(void)
{

//    SCB->SHCSR |= SCB_SHCSR_BUSFAULTENA_Msk;
//    SCB->SHCSR |= SCB_SHCSR_MEMFAULTENA_Msk;
#ifdef FPU_ENABLE
    SCB->SHCSR |= SCB_SHCSR_USGFAULTENA_Msk;
#endif //FPU_ENABLE
    //    uint32_t prio = NVIC_EncodePriority(NVIC_PRIOR_GROUP, 0, 0);
//    NVIC_SetPriority(BusFault_IRQn, prio);

}
#ifdef Q_CORTEX_FAULT_STATUS
void HardFault_Handler(void)
{
    register uint32_t *stack_top asm("r0");
    asm volatile(
                " tst lr, #4                                                \n"
                " ite eq                                                    \n"
                " mrseq r0, msp                                             \n"
                " mrsne r0, psp                                             \n"
                :[out] "=r" (stack_top)
                :
                :);
    fault_stack = (hw_stackframe_t*)stack_top;
    usage_f_reg = *((uint16_t*)(&SCB->CFSR) + 1);
    bus_f_reg = *((uint8_t*)(&SCB->CFSR) + 1);
    mmem_f_reg = *((uint16_t*)&SCB->CFSR);

    if (bus_f_reg & BFARVALID) {
        bus_f_addr = SCB->BFAR;
    }
    if (mmem_f_reg & MMARVALID) {
        mem_f_addr = SCB->MMFAR;
    }
    while (1) {}
}
#endif //Q_CORTEX_FAULT_STATUS


void MemManage_Handler(void)
{

//    register int *r1 __asm("r1");

    asm volatile(  "TST lr, #4\n"
            "ITE EQ\n"
            "MRSEQ r0, MSP\n"
            "MRSNE r0, PSP\n" // stack pointer now in r0
            "ldr r1, [r0, #0x18]\n" // stored pc now in r0
            //"add r0, r0, #6\n" // address to stored pc now in r0
                   :
                   :
                   :"r1"
         );
//        uint32_t PC = *r0;

//    _mem_fault_stat_reg = _scb->CFSR & 0x000000FF;
    uint32_t fa = SCB->MMFAR;
    q_th_t *thread = p_run_thread;
    if ((fa < thread->base) || (fa >= (thread->base + thread->stack_size))) {
        stack_access_violate = 1;
    }


    while (1);
}

void BusFault_Handler(void)
{
//    register int *r0 __asm("r0");

    asm volatile(  "TST lr, #4\n"
            "ITE EQ\n"
            "MRSEQ r0, MSP\n"
            "MRSNE r0, PSP\n" // stack pointer now in r0
            "ldr r0, [r0, #0x18]\n" // stored pc now in r0
            //"add r0, r0, #6\n" // address to stored pc now in r0
                   :
                   :
                   :"r0"
         );
//        uint32_t PC = *r0;
//    _bus_fault_stat_reg = (_scb->CFSR >> 8) & 0x000000FF;

    while (1);
}


#ifdef FPU_ENABLE
void UsageFault_Handler(void)
{
    usage_fault_t usage_fault_code = *((uint16_t*)(&SCB->CFSR) + 1);

    if (usage_fault_code  == NOCP) {
        RAISE_SVPend;
        *((uint16_t*)(&SCB->CFSR) + 1) = 0;
//        SCB->CPACR = 0b1111 << 20;
        p_run_thread->use_fpu = 1;
        return;
    }
    usage_f_reg = usage_fault_code;
    while (1);


}
#endif




void SysTick_Handler(void)
{
    ++sysTickNum;

    qos_timers_service();
    qos_switch_threads(SAVE_CONTEXT);

}


void qos_system_init(void)
{

#ifdef DISABLE_WRITE_BUFFER
    SCnSCB_Type* sc = SCnSCB;
    sc->ACTLR |= SCnSCB_ACTLR_DISDEFWBUF_Msk;
    _nvic = NVIC;
#endif //DISABLE_WRITE_BUFFER

    //Enables unaligned access traps
    SCB->CCR |= SCB_CCR_UNALIGN_TRP_Msk;
    FPU->FPCCR &= ~(FPU_FPCCR_LSPEN_Msk | FPU_FPCCR_ASPEN_Msk);


#ifdef MPU_ENABLE

    q_mpu_enable();
#endif //MPU_ENABLE

    //FPU turning

    //Denied access to FPU
    SCB->CPACR = 0;


    qos_init_all_lists();
//    qos_thread_mem_init();
    qos_dyn_memory_init();
    qos_init_idle_thread();
    qos_nvic_init();
    qos_cortex_excs_init();

#ifdef  Q_USART1_EN
    console_init(_q_open(Q_USART1));

#endif //Q_USART1_EN

}

void qos_system_run(void)
{

    for (uint8_t i = 1; i < qos_threads_num; i++) {
        qos_thread_set_runnable(&qos_threads_pool[i]);
        SET_PRIOR_FLAG(qos_threads_pool[i].prior);
    }
    p_run_thread = qos_idle_thread;

#ifdef MPU_ENABLE
    q_mpu_fill_stack_table(qos_threads_pool);
    q_mpu_thread_set(qos_idle_thread->id);
#endif //MPU_ENABLE
    asm volatile ("msr psp, %0 \n"
                  :
                  :"r" (qos_idle_thread->stack_top)
                  :);
    qos_systick_init();
    asm volatile ("mov r0, #3 \n"
                  "msr control, r0 \n"
                  "isb \n "
                  :
                  :
                  :"r1");
    idle_func();
}

void qos_timers_service(void)
{

    uint8_t st = q_timers_tick(&qos_timer_thread_list);
    for(uint8_t i = 1; i <= st; i++) {
        q_lnim_t* ln_item = _qos_get_first_item_ln(&qos_timer_thread_list);
        _qos_rem_item_from_ln_list(ln_item);
        q_th_t* thread = container_of(ln_item, q_th_t, list_item);
        qos_thread_set_runnable(thread);
        SET_PRIOR_FLAG(thread->prior);
    }
}

void qos_exit_thread_handler(void)
{
#ifdef FPU_ENABLE
    p_run_thread->use_fpu = 0;
#endif //FPU_ENABLE
    if (p_run_thread->period) {
        _qos_rem_item_from_ln_list(&p_run_thread->list_item);
        qos_thread_set_time_wait_state(p_run_thread,
                                       &qos_timer_thread_list,
                                       p_run_thread->period);
        qos_clean_thread_context(p_run_thread);
    }
    else {
        qos_thread_set_dormant(p_run_thread);
    }

    if (_qos_ln_empty(&qos_prior_thread_list[p_run_thread->prior]) == 0) {
        CLEAN_PRIOR_FLAG(p_run_thread->prior);
    }
    qos_switch_threads(NO_SAVE_CONTEXT);
}


void qos_delay_handler(uint32_t delay)
{
    _qos_rem_item_from_ln_list(&p_run_thread->list_item);
    qos_thread_set_time_wait_state(p_run_thread,
                                   &qos_timer_thread_list,
                                   delay);
    if (_qos_ln_empty(&qos_prior_thread_list[p_run_thread->prior]) == 0) {
        CLEAN_PRIOR_FLAG(p_run_thread->prior);
    }
    qos_switch_threads(SAVE_CONTEXT);
}

void q_open_handler(io_res_t res_no)
{
    _q_open(res_no);
}

void q_close_handler(int rd)
{
    _q_close(rd);
}

void q_ioctl_handler(int rd, void *conf)
{
    _q_ioctl(rd, conf);
}


_FORCE_INLINE unsigned int q_io_transfer(int rd, uint8_t* buf, uint32_t nbyte, q_descriptor_i_t* rd_i)
{
    unsigned int status;
//    q_descriptor_i_t* rd_i = &res_desc_table[rd];
    io_res_obj_t* ifc = rd_i->ifc;
    ifc->rd = rd_i;
//    io_res_t res_no = ifc->resnum;
    _qos_rem_item_from_ln_list(&p_run_thread->list_item);
    qos_thread_set_wait_prio_order(p_run_thread,
                                   &ifc->lnlist,
//                                  &qos_wait_res_thread_list[res_no],
                                  Q_HW_WAIT);
    if (_qos_ln_empty(&qos_prior_thread_list[p_run_thread->prior]) == 0) {
        CLEAN_PRIOR_FLAG(p_run_thread->prior);
    }
    p_run_thread->rd_active = rd;
    rd_i->buf = buf;
    rd_i->nb = nbyte;
    if (&p_run_thread->list_item == _qos_get_first_item_ln(&ifc->lnlist)) {
        status = 1;
    }
    else {
        status = 0;
    }

    return status;
}


void q_read_handler(int rd, uint32_t reg, uint8_t *buf, uint32_t nbyte)
{

    q_descriptor_i_t* rd_i = &res_desc_table[rd];
    rd_i->mode = Q_READMODE;
    rd_i->reg = reg;
    if (q_io_transfer(rd, buf, nbyte, rd_i)) {
        _q_read(rd_i);
    }
    qos_switch_threads(SAVE_CONTEXT);
}


void  q_write_handler(int rd, uint32_t reg, uint8_t *buf, uint32_t nbyte)
{

    q_descriptor_i_t* rd_i = &res_desc_table[rd];
    rd_i->mode = Q_WRITEMODE;
    rd_i->reg = reg;

    #ifdef Q_DEBUG_IO_SW_TH
    if (rd_i->h->res == Q_USART3) {
        call_io_funcs[present_running_thread->id]++;
    }

    #endif //Q_DEBUG_IO_SW_TH

    if (q_io_transfer(rd, buf, nbyte, rd_i)) {
        _q_write(rd_i);
        #ifdef Q_DEBUG_IO_SW_TH
        if (rd_i->h->res  == Q_USART3) {
            sw_io_threads[present_running_thread->id]++;
        }
        #endif //Q_DEBUG_IO_SW_TH
    }

    qos_switch_threads(SAVE_CONTEXT);

    //***********************debug******************
    if (p_run_thread->id == 1) {
        hw_rel_thread1++;
    }
    if (p_run_thread->id == 2) {
        hw_rel_thread2++;
    }
    //***********************debug******************

}

void q_rawread_handler(int rd, uint8_t *buf, uint32_t nbyte)
{
    q_descriptor_i_t* rd_i = &res_desc_table[rd];
    rd_i->mode = Q_RAWREADMODE;
    if (q_io_transfer(rd, buf, nbyte, rd_i)) {
        _q_rawread(rd_i);
    }
    qos_switch_threads(SAVE_CONTEXT);
}

void q_rawwrite_handler(int rd, uint8_t *buf, uint32_t nbyte)
{
    q_descriptor_i_t* rd_i = &res_desc_table[rd];
    rd_i->mode = Q_RAWWRITEMODE;
    if (q_io_transfer(rd, buf, nbyte, rd_i)) {
        _q_rawwrite(rd_i);
    }
    qos_switch_threads(SAVE_CONTEXT);
}

void q_ioswap_handler(int rd, uint8_t* tx_buf, uint8_t* rx_buf, uint32_t nbyte)
{
    q_descriptor_i_t* rd_i = &res_desc_table[rd];
    rd_i->mode = Q_SWAPMODE;
    rd_i->aux_buf = rx_buf;
    if (q_io_transfer(rd, tx_buf, nbyte, rd_i)) {
        _q_ioswap(rd_i);
    }
    qos_switch_threads(SAVE_CONTEXT);
}

int q_end_waiting_io_hw(io_res_obj_t *ifc, int val)
{
    q_lnim_t* ln_item;
    q_th_t* thread;

//    if (_qos_ln_empty(&qos_wait_res_thread_list[res_no]) == 0) {
//        return;
//    }

    ln_item = _qos_get_first_item_ln(&ifc->lnlist);
    thread = container_of(ln_item, q_th_t, list_item);
    Q_IS_THREAD_P_CHECK(thread);
    _qos_rem_item_from_ln_list(ln_item);
    qos_thread_set_runnable(thread);
    SET_PRIOR_FLAG(thread->prior);

    #ifdef Q_DEBUG_IO_SW_TH
    if (res_no  == Q_USART3) {
        call_io_funcs[thread->id]--;
        sw_io_threads[thread->id]--;
    }
    #endif //Q_DEBUG_IO_SW_TH


    if (_qos_ln_empty(&ifc->lnlist) != 0) {

        //if queue not empty

        ln_item = _qos_get_first_item_ln(&ifc->lnlist);
        thread = container_of(ln_item, q_th_t, list_item);
        Q_IS_THREAD_P_CHECK(thread);
        q_descriptor_i_t* rd_i = &res_desc_table[thread->rd_active];
        switch (rd_i->mode) {
        case Q_READMODE:
            _q_read(rd_i);
            break;
        case Q_WRITEMODE:
            _q_write(rd_i);
            break;
        case Q_RAWREADMODE:
            _q_rawread(rd_i);
            break;
        case Q_RAWWRITEMODE:
            _q_rawwrite(rd_i);
            break;
        case Q_SWAPMODE:
            _q_ioswap(rd_i);
        }

/*
        if (rd_i->mode == Q_WRITEMODE) {
            _q_write(thread->rd_active);
        }
        else {
            _q_read(thread->rd_active);
        }
*/
        #ifdef Q_DEBUG_IO_SW_TH
        if (res_no  == Q_USART3) {
            sw_io_threads[thread->id]++;
        }

        #endif //Q_DEBUG_IO_SW_TH
    }


    qos_switch_threads(SAVE_CONTEXT);

    return val;
}


void _q_lock_mutex_handler(q_mutex_t* mutex_p)
{

    _qos_rem_item_from_ln_list(&p_run_thread->list_item);
    qos_thread_set_wait_prio_order(p_run_thread,
                                  &mutex_p->lnlist,
                                  Q_MX_WAIT);
    if (_qos_ln_empty(&qos_prior_thread_list[p_run_thread->prior]) == 0) {
        CLEAN_PRIOR_FLAG(p_run_thread->prior);
    }
    if (mutex_p->own->prior > p_run_thread->prior) {
        //CHECK BOTTOM LINE!!!!!!!!!!!!
//        qos_thread_set_prio(mutex_p->own, p_run_thread->prior);
    }
    qos_switch_threads(SAVE_CONTEXT);
}

void _q_release_mutex_handler(q_mutex_t* mutex_p)
{
    q_lnim_t *thread_li = _qos_get_first_item_ln(&mutex_p->lnlist);
    q_th_t *thread = container_of(thread_li, q_th_t, list_item);
    Q_IS_THREAD_P_CHECK(thread);
    _qos_rem_item_from_ln_list(&thread->list_item);
    qos_thread_set_runnable(thread);
    SET_PRIOR_FLAG(thread->prior);
    //CHECK BOTTOM LINE!!!!!!!!!!!!
//    qos_thread_set_prio(mutex_p->own, mutex_p->own->original_prio);
    mutex_p->own = thread;
    mutex_p->val = 0;
    qos_switch_threads(SAVE_CONTEXT);
}

void _q_ext_itr_wait_handler(uint32_t pin)
{
//    uint32_t temp = _PORT_PIN_2_TEMP_FMT(port_id, pin);
    p_run_thread->temp = pin;
    _qos_rem_item_from_ln_list(&p_run_thread->list_item);
    qos_thread_set_wait_prio_order(p_run_thread,
                                  &qos_ext_itr_wait_list,
                                  Q_EXT_ITR_WAIT);
    if (_qos_ln_empty(&qos_prior_thread_list[p_run_thread->prior]) == 0) {
        CLEAN_PRIOR_FLAG(p_run_thread->prior);
    }
    qos_switch_threads(SAVE_CONTEXT);

}

void _q_ext_itr_end_wait_handler(uint8_t pin)
{
    q_th_t *thread = qos_first_thread_waiting_ext_itr(pin, &qos_ext_itr_wait_list);
    Q_IS_THREAD_P_CHECK(thread);
    if (thread) {
        _qos_rem_item_from_ln_list(&thread->list_item);
        qos_thread_set_runnable(thread);
        SET_PRIOR_FLAG(thread->prior);
    }
    qos_switch_threads(SAVE_CONTEXT);
}

void* _q_alloc_handler(u32 len)
{
#ifdef MPU_ENABLE
    return qalloc_dynmem(len, &p_run_thread->memblocks, &p_run_thread->mpu_conf);
#else
    return qalloc_dynmem(len, &p_run_thread->memblocks);
#endif

}

void _q_free_handler(void* ptr)
{

}

u32 _q_exec_handler(void* func_ptr, u32 stack_size, u32 prio)
{

}


#ifdef Q_STACK_BOUNDARIES_CHECK
void thread_stack_check(void)
{
    for (uint8_t i = 0; i < qos_threads_num; i++) {
        if(qos_threads_pool[i].stack_top <= qos_threads_pool[i].base) {
            ERR_RAISE(Q_MEM_ACCESS_VIOLATE);
        }
    }
}
#endif //Q_STACK_BOUNDARIES_CHECK


#ifdef Q_IS_THREAD_P_CHECK_
void q_is_thread(q_th_t* thread)
{
    uint32_t p = (uint32_t)thread;

    if ((p < (uint32_t)qos_threads_pool) && (p >= (sizeof(q_th_t) * qos_threads_num))) {
        ERR_RAISE(Q_WRONG_ITEM);
    }
    if ((p - (uint32_t)qos_threads_pool) % sizeof(q_th_t)) {
        ERR_RAISE(Q_WRONG_ITEM);
    }
}

#endif //Q_IS_THREAD_P_CHECK_


uint8_t qos_switch_threads(uint_fast8_t save_context)
{
    need_save_context = save_context;
    uint8_t status = 0;
    uint8_t transition_prio  = __builtin_clz(runnable_prior_threads);

    if (transition_prio < IDLE_PRIOR) {
        if ((p_run_thread->prior > transition_prio) || (p_run_thread->stat != RUNNABLE)) {
            if (p_run_thread->stat == RUNNABLE) {
                _qos_rem_item_from_ln_list(&p_run_thread->list_item);
                qos_thread_set_suspend(p_run_thread);
            }
            next_run_thread = container_of(_qos_get_first_item_ln(&qos_prior_thread_list[transition_prio]),
                                           q_th_t,
                                           list_item);
            Q_IS_THREAD_P_CHECK(next_run_thread);
            status = 1;
            Q_THREAD_STACK_CHECK();
#ifdef MPU_ENABLE
            q_mpu_thread_set(next_run_thread->id);
#endif
            RAISE_SVPend;
        }
    }
    else {
        if (p_run_thread->prior < IDLE_PRIOR) {
            next_run_thread = qos_idle_thread;
            status = 1;
            Q_THREAD_STACK_CHECK();
#ifdef MPU_ENABLE
            q_mpu_thread_set(next_run_thread->id);
#endif
            RAISE_SVPend;
        }
    }
    //for debug purpose
#ifdef FPU_ENABLE
    if (next_run_thread->use_fpu == 0) {
        next_tread_stack = (hstackframe_t*)next_run_thread->stack_top;
    }
    else {
        next_tread_fpu_stack = (hstackframe_fpu_t*)next_run_thread->stack_top;
    }

    if (p_run_thread->use_fpu ==  0) {
        prev_tread_stack = (hstackframe_t*)p_run_thread->stack_top;
    }
    else {
        prev_tread_fpu_stack = (hstackframe_fpu_t*)p_run_thread->stack_top;
    }

#endif
    //end debug purpose
    return status;
}


void qos_clean_thread_context(q_th_t* thread)
{
    uint32_t stack_size = sizeof(hstackframe_t) / sizeof(memaddr_t);
#ifdef FPU_ENABLE
  /*  if (thread->use_fpu) {
        thread->stack_top = thread->base + thread->stack_size - sizeof(hstackframe_fpu_t);
        hstackframe_fpu_t *stack_frame = (hstackframe_fpu_t*)thread->stack_top;
        stack_size = sizeof(hstackframe_fpu_t) / sizeof(memaddr_t);
        stack_frame->xPSR = 0x1000000;
        stack_frame->PC = thread->thread_code;
        stack_frame->LR = (memaddr_t)q_exit;

        uint32_t* sP = (uint32_t*)thread->stack_top;
        for (uint32_t i = 0; i < 16-3; i++) {
            *(sP + i) = 0;
        }

        stack_frame->S0 = 0 * (thread->id + 1);
        stack_frame->S1 = 1 * (thread->id + 1);
        stack_frame->S2 = 2 * (thread->id + 1);
        stack_frame->S3 = 3 * (thread->id + 1);
        stack_frame->S12 = 12 * (thread->id + 1);
        stack_frame->S13 = 13 * (thread->id + 1);
        stack_frame->S14 = 14 * (thread->id + 1);
        stack_frame->S15 = 15 * (thread->id + 1);

        stack_frame->R0 = 0 * (thread->id + 1);
        stack_frame->R1 = 1 * (thread->id + 1);
        stack_frame->R2 = 2 * (thread->id + 1);
        stack_frame->R3 = 3 * (thread->id + 1);
        stack_frame->R8 = 8 * (thread->id + 1);
        stack_frame->R9 = 9 * (thread->id + 1);
        stack_frame->R10 = 10 * (thread->id + 1);
        stack_frame->R11 = 11 * (thread->id + 1);

    }
    else {*/
        thread->stack_top = thread->base + thread->stack_size - sizeof(hstackframe_t);
        hstackframe_t *stack_frame = (hstackframe_t*)thread->stack_top;
        stack_frame->xPSR = 0x1000000;
        stack_frame->PC = thread->thread_code;
        stack_frame->LR = (memaddr_t)q_exit;

        uint32_t* sP = (uint32_t*)thread->stack_top;
        for (uint32_t i = 0; i < stack_size-3; i++) {
            *(sP + i) = 0;
        }

        stack_frame->R0 = 0 * (thread->id + 1);
        stack_frame->R1 = 1 * (thread->id + 1);
        stack_frame->R2 = 2 * (thread->id + 1);
        stack_frame->R3 = 3 * (thread->id + 1);
        stack_frame->R8 = 8 * (thread->id + 1);
        stack_frame->R9 = 9 * (thread->id + 1);
        stack_frame->R10 = 10 * (thread->id + 1);
        stack_frame->R11 = 11 * (thread->id + 1);

//    }
#else
    thread->stack_top = thread->base + thread->stack_size - sizeof(hstackframe_t);
    hstackframe_t *stack_frame = (hstackframe_t*)thread->stack_top;
    stack_frame->xPSR = 0x1000000;
    stack_frame->PC = thread->thread_code;
    stack_frame->LR = (memaddr_t)q_exit;
    uint32_t* sP = (uint32_t*)thread->stack_top;
    for (uint32_t i = 0; i < stack_size-3; i++) {
        *(sP + i) = 0;
    }

    stack_frame->R0 = 0 * (thread->id + 1);
    stack_frame->R1 = 1  * (thread->id + 1);
    stack_frame->R2 = 2  * (thread->id + 1);
    stack_frame->R3 = 3  * (thread->id + 1);
    stack_frame->R4 = 4  * (thread->id + 1);
    stack_frame->R5 = 5  * (thread->id + 1);
    stack_frame->R6 = 6  * (thread->id + 1);
    stack_frame->R7 = 7  * (thread->id + 1);
    stack_frame->R8 = 8  * (thread->id + 1);
    stack_frame->R9 = 9  * (thread->id + 1);
    stack_frame->R10 = 10  * (thread->id + 1);
    stack_frame->R11 = 11  * (thread->id + 1);
    stack_frame->R12 = 12  * (thread->id + 1);
#endif




}

#endif
