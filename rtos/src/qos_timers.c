#include "qos_timers.h"
#include "qos_def.h"
#include "qos_threads.h"


//static uint8_t active_timers_num = 0;

uint8_t q_timers_tick(q_lnim_t* queue)
{
    uint8_t pos_stopped_timer = 0;
    for (q_lnim_t* p = queue->next; p != queue; p = p->next) {
        q_th_t* t  = container_of(p, q_th_t, list_item);
        t->timeout--;
        if (t->timeout == 0) {
            pos_stopped_timer++;
        }
    }
    return pos_stopped_timer;
}
