#include "qos_debug.h"
#include "qos_def.h"
#include "stm32f4xx.h"

_PRIV_BSS qos_err_code _q_error;

//static hw_stackframe_t *process_stack;
_PRIV_DATA static enum IRQn interrupt_num;

uint32_t cycle_num;

void qos_error_handler(qos_err_code err)
{
    asm volatile  ("mov r3, #1       \n"
                  "msr primask, r3  ":::"r3");
    while (1);


    asm volatile  ("mov r3, #0       \n"
                  "msr primask, r3  ":::"r3");
}


void Default_Handler_a(void)
{
    register uint32_t *itr_num asm("r0");
    asm volatile  ("mrs %[out], psr    \n"
                  :[out] "=r" (itr_num)
                   ::);
    interrupt_num = ((int)itr_num & 0xFF) - 16;
    while (1) {}
}

//void update_proc_stack(void)
//{
//    uint32_t ps_p;
//    asm volatile ("mrs %[out], psp    \n"
//                  :[out] "=r" (ps_p)
//                  :
//                  :);
//    process_stack = (hw_stackframe_t*)ps_p;
//}
