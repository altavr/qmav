#include "qos_itc.h"
#include "qos_ln_list.h"
#include "qos_sys_functions.h"

void mutex_init(q_mutex_t* mutex)
{
    _qos_close_item_lns(&mutex->lnlist);
}


void _q_queue_init(void* queue, u16 length, u16 buf_size)
{
    *(u16 *)LEN_P_OF_QUEUE(queue) = length;
    *(u16 *)BUF_P_SIZE_OF_QUEUE(queue) = buf_size;
    *(u16 *)HEAD_P_SIZE_OF_QUEUE(queue) = 0;
    *(u16 *)TAIL_P_SIZE_OF_QUEUE(queue) = 0;
    *(u16 *)START_P_SIZE_OF_QUEUE(queue) = 0;
    *(u16 *)END_P_SIZE_OF_QUEUE(queue) = 0;

    mutex_init((q_mutex_t *)MUTEX_P_OF_QUEUE(queue));
}

_FORCE_INLINE static void _datacopy(u8* src, u8* dst, u16 length)
{
    for (u32 i=0; i<length; i++) {
        dst[i] = src[i];
    }
}

qos_err_code q_queue_push(void* item, u16 item_s, void* queue)
{
    qos_err_code status = Q_SUCCESS;

    u16 len = *(u16 *)LEN_P_OF_QUEUE(queue);
    u16 buf_s = *(u16 *)BUF_P_SIZE_OF_QUEUE(queue);
    u16 head = *(u16 *)HEAD_P_SIZE_OF_QUEUE(queue);
    u16 tail = *(u16 *)TAIL_P_SIZE_OF_QUEUE(queue);
    u16 start = *(u16 *)START_P_SIZE_OF_QUEUE(queue);
    u16 end = *(u16 *)END_P_SIZE_OF_QUEUE(queue);
    q_mutex_t* mutex = (q_mutex_t *)MUTEX_P_OF_QUEUE(queue);
    struct q_qprop* items_arr = (struct q_qprop *)ITEMS_P_OF_QUEUE(queue);
    u8 *buf = BUF_P_OF_QUEUE(queue, len);

    q_mutex_lock(mutex);

    if ((head + 1 == tail) || (head == 0 && tail == len)) {
        return Q_QUEUE_FULL;
    }
    u16 buf_free_space = buf_s - ((end - start) & (buf_s - 1)) ;
    if (item_s > buf_free_space) {
        return Q_NONE_FREE_SPACE;
    }

    items_arr[tail].len = item_s;
    items_arr[tail].start = end;
    end = (end == len ? 0 : end + 1);


    q_mutex_unlock(mutex);
    return status;
}
