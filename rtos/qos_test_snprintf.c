#include "qos_string.h"
#include <stdio.h>
#include <string.h>

int main(void)
{
    char s[50];
    
    q_snprintf(s, 10, "string - %s, int - %d, float - %f\n", "test", 112, 5556.578);
    printf(s);
    printf("length q_snprintf- %lu\n", strlen(s));
    
    snprintf(s, 10, "string - %s, int - %d, float - %f\n", "test", 112, 5556.578);
    printf(s);
    printf("length snprintf- %lu\n", strlen(s));
    
   
    return 0;
}
