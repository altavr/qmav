

//#define DESKTOP_TEST
#include "qos_test.h"

#ifdef DESKTOP_TEST
#include "qos_ln_list.h"
#include "qos_threads.h"

#include "qos_memory.h"

#include "qos_debug.h"
#include "stdio.h"

const uint32_t __bss_end__ = 0x20000cb4;
const uint32_t _estack = 0x2000a000; /* end of 40K RAM */
const uint32_t _Min_Stack_Size = 0x200; /* required amount of stack */
const uint32_t _Min_Heap_Size = 0x400; /* required amount of heap  */

//extern q_lnim_t qos_prior_thread_list[PRIOR_MAX];

q_th_t qos_threads_pool[THREADS_MAX];
extern uint32_t qos_threads_num;
extern q_lnim_t qos_prior_thread_list[PRIOR_MAX];
extern q_lnim_t qos_timer_thread_list;
extern q_lnim_t qos_wait_res_thread_list[RESOURCE_MAX];

//int qos_thread_init(uint8_t prior)
//{
//    if (qos_threads_num >= THREADS_MAX) {
//        return -1;
//    }

//    q_th_t* thread = &qos_threads_pool[qos_threads_num];

//    thread->id = qos_threads_num;
//    thread->prior = prior;
//    qos_threads_num++;

//    return qos_threads_num - 1;
//}


void print_list_item(q_lnim_t *list)
{
    printf("List item pointers: %p %p %p\n", list, list->prev, list->next);
}

void print_thread(q_th_t *thread)
{

//    printf("________\n");
    PRN_THREAD(thread);
//    printf("Thread id - %d, prior - %d, \n", thread->id);
    print_list_item(&thread->list_item);
//    printf("___________________\n");
}

void print_list(q_lnim_t* list, char* note)
{
    printf("________________LIST %s:______________________\n", note);

    q_lnim_t *p = list->next;
    for (; p != list; p = p->next) {
        print_thread(container_of(p, q_th_t, list_item));
    }
}

void func(void)
{

}

void qos_clean_thread_context(q_th_t *thread)
{

}

int main(void)
{
    q_th_t* thread;
    qos_init_all_lists();
    qos_thread_mem_init();


    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            thread = qos_get_new_thread();
            qos_thread_init(thread, 20, j, func);
            qos_thread_set_runnable(thread);
//            print_thread(thread);
        }
    }
//    for (int i = 0; i < 3; i++) {
//        print_list_item()
//    }

    print_list(&qos_prior_thread_list[0], "PRIO 0");
    print_list(&qos_prior_thread_list[1], "PRIO 1");
    print_list(&qos_prior_thread_list[2], "PRIO 2");


    thread = container_of(_qos_get_first_item_ln(&qos_prior_thread_list[0]), q_th_t, list_item);
    printf("THREAD___________________\n");
    print_thread(thread);
    qos_thread_set_time_wait_state(thread, &qos_timer_thread_list, 20);


    print_list(&qos_timer_thread_list, "TIMER LIST");
    print_list(&qos_prior_thread_list[0], "PRIO 0");

    qos_thread_set_runnable(thread);

    print_list(&qos_timer_thread_list, "TIMER LIST");
    print_list(&qos_prior_thread_list[0], "PRIO 0");

    print_list(&qos_prior_thread_list[1], "PRIO 1");
    print_list(&qos_prior_thread_list[2], "PRIO 2");





}
#else


uint32_t var = 0;
uint32_t mutex_test = 5;

uint32_t exc_access(void) {
    uint32_t res = 2;

    asm volatile ( "clrex \n"
                    "ldrex r2, [%[in]] \n"
                  :
                  :[in] "r" (&mutex_test)
                  :"r2");

    asm volatile ("dmb \n"
                  "mov r1, #1 \n"
                  "strex %[out], r1, [%[in]] \n"
                  :[out] "=r" (res)
                  :[in] "r" (&mutex_test)
                  :"r1");

    return res;
}

uint32_t exc_access_fail(void) {
    uint32_t res = 2;

    asm volatile ( "ldrex r2, [%[in]] \n"
                   "dmb \n"
                  :
                  :[in] "r" (&mutex_test)
                  :"r2");
    mutex_test = 7;
    asm volatile ("dmb \n"
                 "mov r1, #9 \n"
                  "strex %[out], r1, [%[in]] \n"
                  :[out] "=r" (res)
                  :[in] "r" (&mutex_test)
                  :"r1");

    return res;
}

void exc_read(void)
{
    asm volatile ( "clrex \n"
                   "ldrex r2, [%[in]] \n"
                  :
                  :[in] "r" (&mutex_test)
                  :"r2");
}

uint32_t exc_write(void)
{
    register uint32_t res = 2;
    asm volatile ("clrex \n"
                  "mov r1, #9 \n"
                  "strex %[out], r1, [%[in]] \n"
                  "dmb \n"
                  :[out] "=r" (res)
                  :[in] "r" (&mutex_test)
                  :"r1");
    return res;
}
#endif

